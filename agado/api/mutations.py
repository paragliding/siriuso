"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель категорий деятельности
class RedaktuAgadoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    agado_kategorioj = graphene.Field(AgadoKategorioNode,
        description=_('Созданная/изменённая категория деятельности'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        agado_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('agado.povas_krei_agado_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            agado_kategorioj = AgadoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(agado_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(agado_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    agado_kategorioj.realeco.set(realeco)
                                else:
                                    agado_kategorioj.realeco.clear()

                            agado_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            agado_kategorioj = AgadoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('agado.povas_forigi_agado_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('agado.povas_shanghi_agado_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                agado_kategorioj.forigo = kwargs.get('forigo', agado_kategorioj.forigo)
                                agado_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                agado_kategorioj.arkivo = kwargs.get('arkivo', agado_kategorioj.arkivo)
                                agado_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                agado_kategorioj.publikigo = kwargs.get('publikigo', agado_kategorioj.publikigo)
                                agado_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                agado_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(agado_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(agado_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        agado_kategorioj.realeco.set(realeco)
                                    else:
                                        agado_kategorioj.realeco.clear()

                                agado_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except AgadoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuAgadoKategorio(status=status, message=message, agado_kategorioj=agado_kategorioj)


# Модель типов деятельности
class RedaktuAgadoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    agado_tipoj = graphene.Field(AgadoTipoNode, 
        description=_('Созданный/изменённый тип деятельности'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        agado_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('agado.povas_krei_agado_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            agado_tipoj = AgadoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(agado_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(agado_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    agado_tipoj.realeco.set(realeco)
                                else:
                                    agado_tipoj.realeco.clear()

                            agado_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            agado_tipoj = AgadoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('agado.povas_forigi_agado_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('agado.povas_shanghi_agado_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                agado_tipoj.forigo = kwargs.get('forigo', agado_tipoj.forigo)
                                agado_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                agado_tipoj.arkivo = kwargs.get('arkivo', agado_tipoj.arkivo)
                                agado_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                agado_tipoj.publikigo = kwargs.get('publikigo', agado_tipoj.publikigo)
                                agado_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                agado_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(agado_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(agado_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        agado_tipoj.realeco.set(realeco)
                                    else:
                                        agado_tipoj.realeco.clear()

                                agado_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except AgadoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuAgadoTipo(status=status, message=message, agado_tipoj=agado_tipoj)


# Модель деятельности
class RedaktuAgado(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    agado = graphene.Field(AgadoNode, 
        description=_('Созданная/изменённая деятельность'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий деятельности'))
        tipo_id = graphene.Int(description=_('Код типа деятельности'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        agado = None
        realeco = Realeco.objects.none()
        kategorio = AgadoKategorio.objects.none()
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('agado.povas_krei_agado'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = AgadoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории деятельности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = AgadoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except AgadoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип деятельности'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип деятельности'),'tipo_id')

                        if not message:
                            agado = Agado.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto = uzanto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(agado.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(agado.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    agado.realeco.set(realeco)
                                else:
                                    agado.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    agado.kategorio.set(kategorio)
                                else:
                                    agado.kategorio.clear()

                            agado.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            agado = Agado.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('agado.povas_forigi_agado')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('agado.povas_shanghi_agado'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = AgadoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории деятельности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = AgadoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except AgadoTipo.DoesNotExist:
                                        message = _('Неверный тип деятельности')
                            if not message:

                                agado.forigo = kwargs.get('forigo', agado.forigo)
                                agado.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                agado.arkivo = kwargs.get('arkivo', agado.arkivo)
                                agado.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                agado.publikigo = kwargs.get('publikigo', agado.publikigo)
                                agado.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                agado.posedanto = uzanto
                                agado.tipo = tipo if kwargs.get('tipo_id', False) else agado.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(agado.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(agado.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        agado.realeco.set(realeco)
                                    else:
                                        agado.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        agado.kategorio.set(kategorio)
                                    else:
                                        agado.kategorio.clear()

                                agado.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Agado.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuAgado(status=status, message=message, agado=agado)


class AgadoMutations(graphene.ObjectType):
    redaktu_agado_kategorio = RedaktuAgadoKategorio.Field(
        description=_('''Создаёт или редактирует категории деятельности''')
    )
    redaktu_agado_tipo = RedaktuAgadoTipo.Field(
        description=_('''Создаёт или редактирует типы деятельности''')
    )
    redaktu_agado = RedaktuAgado.Field(
        description=_('''Создаёт или редактирует деятельность''')
    )
