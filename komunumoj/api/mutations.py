"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils.translation import gettext_lazy as _
from django.utils import timezone
import graphene

from siriuso.api.filters import SiriusoFilterConnectionField
from komunumoj import models
from main.models import Uzanto
from informiloj.models import InformilojSciigoTipo
from .common import get_komunumo, get_komunumo_by_id
from .schema import KomunumoProprietoj


class AplikiSciigojn(graphene.Mutation):
    class Arguments:
        uuid = graphene.UUID()
        id = graphene.Int()
        tipo = graphene.String()
        uzanto_id = graphene.Int()
        sciigoj = graphene.List(graphene.String, required=True)

    status = graphene.Boolean()
    message = graphene.String()
    sciigoj = graphene.List(graphene.String)

    @staticmethod
    def __get_membro_model(tipo):
        model = None
        if tipo == models.Komunumo._meta.model_name:
            model = models.KomunumoMembro

        return model

    @staticmethod
    def mutate(root, info, sciigoj, **kwargs):
        if not info.context.user.is_authenticated:
            return AplikiSciigojn(status=False, message=_('Требуется авторизация'))

        status = False
        kom = None
        kom_membro_model = None
        kom_sciigoj = None
        tipo = None
        # Тут надо будет доделать проверку прав текущего ползователя
        # изменять настройки других пользователей. Пока пользователь
        # может изменять свои настройки уведомлений
        uzanto_id = info.context.user.id

        if 'uuid' in kwargs:
            kom_uuid = kwargs.get('uuid')
            res = get_komunumo(kom_uuid)

            if res:
                kom, tipo = res
        elif 'id' in kwargs and 'tipo' in kwargs:
            kom_id = kwargs.get('id')
            tipo = kwargs.get('tipo')
            kom = get_komunumo_by_id(kom_id, tipo)
        else:
            message = _('Не заданы обязательные параметры для определения сообщества: "uuid" или пара "id" и "tipo"')
            return AplikiSciigojn(status=status, message=message)

        kom_membro_model = AplikiSciigojn.__get_membro_model(tipo)

        if kom and kom_membro_model:
            try:
                kom_membro = kom_membro_model.objects.get(autoro_id=uzanto_id, posedanto=kom, forigo=False)
                kom_sciigoj = InformilojSciigoTipo.objects.filter(kodo__in=sciigoj, forigo=False)
                kom_membro.muro_sciigo.set(kom_sciigoj)
                kom_sciigoj = tuple(sc.kodo for sc in kom_sciigoj)
                message = _('Настройки уведомлений применены')
                status = True
            except kom_membro_model.DoesNotExist:
                message = _('Пользователь должен быть участником сообщества')
        else:
            message = _('Сообщество не найдено')

        return AplikiSciigojn(status=status, message=message, sciigoj=kom_sciigoj)


def get_membro_model(kom_uuid):
    models_dict = {
        models.Komunumo: models.KomunumoMembro,
    }

    for socio_model, membro_model in models_dict.items():
        try:
            kom = socio_model.objects.get(uuid=kom_uuid, forigo=False)
            return (kom, membro_model)
        except socio_model.DoesNotExist:
            pass

    return None


def get_default_membro_tipo(komunumo, membro_tipo=None):
    try:
        # Устанавливаем тип членства по умолчанию для типа сообщества
        if komunumo.tipo.kodo in ('soveto', 'sindikato'):
            membro_tipo = models.KomunumoMembroTipo.objects.get(kodo='abonanto', forigo=False)
        elif komunumo.tipo.kodo == 'entrepreno' and komunumo.speco and komunumo.speco.kodo == 'popolaentrepreno':
            membro_tipo = models.KomunumoMembroTipo.objects.get(kodo='abonanto', forigo=False)
        else:
            membro_tipo = models.KomunumoMembroTipo.objects.get(kodo='komunumano', forigo=False)

        return membro_tipo
    except models.KomunumoMembroTipo.DoesNotExist:
        return None


class KuniguKomunumon(graphene.Mutation):
    class Arguments:
        komunumo_uuid = graphene.UUID(required=True)
        uzanto_id = graphene.Int()
        membra_tipo = graphene.String()

    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(KomunumoProprietoj)

    @staticmethod
    def mutate(root, info, komunumo_uuid, **kwargs):
        status = False

        if info.context.user.is_authenticated:
            has_adm_perm = (info.context.user.is_admin or info.context.user.is_superuser)
            # Тут нужно будет переделать проверку прав под систему прав django
            if ('membra_tipo' in kwargs or 'uzanto_id' in kwargs) and not has_adm_perm:
                message = _('Недостаточно прав для для использования полей "membra_tipo" и/или "uzanto_id"')
                return KuniguKomunumon(status=status, message=message)

            # 1. определяем модель участников сообщества по UUID сообщества
            membro_model = get_membro_model(komunumo_uuid)

            if membro_model:
                komunumo, membro_model = membro_model
                uzanto = info.context.user
                membro_tipo_kodo = None
                membro_tipo = None
                # 2. Определяем типы членства (+проверяем корректность указанного админом типа)
                # Тут нужно будет переделать проверку прав под систему прав django
                if has_adm_perm:
                    # проверяем админские параметры
                    membro_tipo_kodo = kwargs.get('membra_tipo')

                    if 'uzanto_id' in kwargs:
                        try:
                            uzanto = Uzanto.objects.get(id=kwargs.get('uzanto_id'))
                        except Uzanto.DoesNotExist:
                            pass

                membro_tipo = get_default_membro_tipo(komunumo, membro_tipo_kodo)

                if membro_tipo:
                    # 3. Проверяем наличие членства
                    try:
                        current = (membro_model.objects.select_related('tipo')
                                   .get(posedanto_id=komunumo_uuid, autoro=uzanto, forigo=False))
                        # Если уже существует членство с таким же типом
                        if current.tipo == membro_tipo or not has_adm_perm:
                            return KuniguKomunumon(status=status, message=_('Пользователь уже связан с Сообществом'))

                        current.forigo=True
                        current.foriga_dato = timezone.now()
                        current.save()
                    except membro_model.DoesNotExist:
                        pass

                    # 4. Определяем типы способов оповещений для подписки
                    # надо будет переделать под настройки пользователя (должна быть глобальная настройка
                    # подписок по умолчанию для всех Сообществ и для каждого типа по отдельности)
                    # А пока выставляем все виды оповещения...
                    kom_sciigoj = InformilojSciigoTipo.objects.filter(forigo=False)

                    # 5. Создаём подписку
                    nova_kunigu = membro_model(posedanto_id=komunumo_uuid, autoro=uzanto, forigo=False,
                                               tipo=membro_tipo)
                    nova_kunigu.save()
                    nova_kunigu.muro_sciigo.set(kom_sciigoj)

                    # Выборка владельца со статистикой
                    kom_model = nova_kunigu.posedanto._meta.model
                    komunumo = kom_model.objects.filter(uuid=nova_kunigu.posedanto_id)
                    komunumo = (SiriusoFilterConnectionField
                                .komunumo_queryset(kom_model, komunumo))[0]

                    status = True
                    message = _('Пользователь присоединился к Сообществу')
                    return KuniguKomunumon(status=status, message=message,
                                           statistiko=KomunumoProprietoj(komunumo))

                else:
                    message = _('Указан неверный тип членства')

            else:
                message = _('Сообщество не найдено')

        else:
            message = _('Требуется авторизация')

        return KuniguKomunumon(status=status, message=message)


class ForlasuKomunumon(graphene.Mutation):
    class Arguments:
        komunumo_uuid = graphene.UUID(required=True)
        uzanto_id = graphene.Int()

    status = graphene.Boolean()
    message = graphene.String()
    statistiko = graphene.Field(KomunumoProprietoj)

    @staticmethod
    def mutate(root, info, komunumo_uuid, **kwargs):
        status = False

        if info.context.user.is_authenticated:
            uzanto = info.context.user
            has_adm_perm = uzanto.is_admin or uzanto.is_superuser

            if 'uzanto_id' in kwargs and not has_adm_perm:
                message = _('Недостаточно прав для для использования поля "uzanto_id"')
                return ForlasuKomunumon(status=status, message=message)

            if has_adm_perm:
                if 'uzanto_id' in kwargs:
                    try:
                        uzanto = Uzanto.objects.get(id=kwargs.get('uzanto_id'))
                    except Uzanto.DoesNotExist:
                        pass

            membro_model = get_membro_model(komunumo_uuid)

            if membro_model:
                kom, membro_model = membro_model
                try:
                    membro = membro_model.objects.get(posedanto_id=komunumo_uuid, forigo=False, autoro=uzanto)

                    # Пользователь без администраторских прав не может выйти
                    # из Сообщества с нестандартным/обязательным типом членства
                    # (администратор, модератор, член клманды, член Совета и член Народного предприятия)
                    if membro.tipo == get_default_membro_tipo(kom) or has_adm_perm:
                        membro.forigo = True
                        membro.foriga_dato = timezone.now()
                        membro.save()

                        status = True
                        message = _('Пользователь успещно покинул Сообщество')
                        return ForlasuKomunumon(status=status, message=message,
                                                statistiko=KomunumoProprietoj(membro.posedanto))
                    else:
                        message = _('Пользователь не может покинуть данное Сообщество')

                except:
                    message = _('Пользователь не состоит в Сообществе')
            else:
                message = _('Сообщество не найдено')

        else:
            message = _('Требуется авторизация')

        return ForlasuKomunumon(status=status, message=message)


class Mutations(graphene.ObjectType):
    apliki_sciigojn = AplikiSciigojn.Field(
        description=_('''Применяет настройки оповещения для конкретного Сообщества. Для идентификации Сообщества можно 
        использовать его ID (параметр "id" в паре с типом сообщества "tipo"), так и UUID (параметр "uuid"). Для
        пользователя с администраторскими правами доступен параметр "uzanto_id" (задаёт ID пользователя).''')
    )

    kunigu_komunumon = KuniguKomunumon.Field(
        description=_('''Включает пользователя в указанное ("komunumoUuid") Сообщество.
        Для пользователя с администраторскими правами доступны параметры "uzanto_id" (задаёт ID пользователя) и 
        "membra_tipo" (задает тип членства).''')
    )

    forlasu_komunumon = ForlasuKomunumon.Field(
        description=_('''Исключает пользователя из указанного ("komunumoUuid") Сообщества.
        Для пользователя с администраторскими правами доступен параметр "uzanto_id" (задаёт ID пользователя),
        позволяя исключить определённого пользователя из заданного Сообщества.''')
    )
