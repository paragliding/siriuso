"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from objektoj.models import ObjektoPosedanto, ObjektoLigiloTipo, ObjektoLigilo,\
    ObjektoStokejo, loza_volumeno
from objektoj.api.subscription import ObjektoEventoj
from objektoj.api.mutations import objekto_ligilo_save, objekto_save
from .schema import *
from ..models import *


# Модель категорий проектов
class RedaktuProjektojProjektoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_kategorioj = graphene.Field(ProjektojProjektoKategorioNode,
        description=_('Созданная/изменённая категория проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            projekto_kategorioj = ProjektojProjektoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    projekto_kategorioj.realeco.set(realeco)
                                else:
                                    projekto_kategorioj.realeco.clear()

                            projekto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_kategorioj = ProjektojProjektoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                projekto_kategorioj.forigo = kwargs.get('forigo', projekto_kategorioj.forigo)
                                projekto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_kategorioj.arkivo = kwargs.get('arkivo', projekto_kategorioj.arkivo)
                                projekto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_kategorioj.publikigo = kwargs.get('publikigo', projekto_kategorioj.publikigo)
                                projekto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        projekto_kategorioj.realeco.set(realeco)
                                    else:
                                        projekto_kategorioj.realeco.clear()

                                projekto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoKategorio(status=status, message=message, projekto_kategorioj=projekto_kategorioj)


# Модель типов проектов
class RedaktuProjektojProjektoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_tipoj = graphene.Field(ProjektojProjektoTipoNode, 
        description=_('Созданный/изменённый тип проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            projekto_tipoj = ProjektojProjektoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    projekto_tipoj.realeco.set(realeco)
                                else:
                                    projekto_tipoj.realeco.clear()

                            projekto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_tipoj = ProjektojProjektoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:

                                projekto_tipoj.forigo = kwargs.get('forigo', projekto_tipoj.forigo)
                                projekto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_tipoj.arkivo = kwargs.get('arkivo', projekto_tipoj.arkivo)
                                projekto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_tipoj.publikigo = kwargs.get('publikigo', projekto_tipoj.publikigo)
                                projekto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        projekto_tipoj.realeco.set(realeco)
                                    else:
                                        projekto_tipoj.realeco.clear()

                                projekto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoTipo(status=status, message=message, projekto_tipoj=projekto_tipoj)


# Модель статусов проектов
class RedaktuProjektojProjektoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_statusoj = graphene.Field(ProjektojProjektoStatusoNode, 
        description=_('Созданный/изменённый статусов проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            projekto_statusoj = ProjektojProjektoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_statusoj = ProjektojProjektoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:

                                projekto_statusoj.forigo = kwargs.get('forigo', projekto_statusoj.forigo)
                                projekto_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_statusoj.arkivo = kwargs.get('arkivo', projekto_statusoj.arkivo)
                                projekto_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_statusoj.publikigo = kwargs.get('publikigo', projekto_statusoj.publikigo)
                                projekto_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoStatuso(status=status, message=message, projekto_statusoj=projekto_statusoj)


# Модель проектов
class RedaktuProjektojProjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto = graphene.Field(ProjektojProjektoNode, 
        description=_('Созданный/изменённый проект'))
    projekto_posedantoj = graphene.Field(ProjektojProjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец проекта'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов'))
        tipo_id = graphene.Int(description=_('Тип проектов'))
        statuso_id = graphene.Int(description=_('Статус проектов'))
        pozicio = graphene.Int(description=_('Позиция в списке'))
        objekto_uuid = graphene.String(description=_('Объект'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения проекта по оси X в кубе'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Y в кубе'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Z в кубе'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения проекта по оси X в кубе'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения проекта по оси Y в кубе'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения проекта по оси z в кубе'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец проекта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец проекта'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец проекта'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца проекта'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца проекта'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец проекта')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto = None
        realeco = None
        kategorio = ProjektojProjektoKategorio.objects.none()
        tipo = None
        statuso = None
        objekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_objekto = None
        posedanto_komunumo = None
        uzanto = info.context.user
        posedanto_objekto_perm = None # если пользователь владелец объекта по проекту
        projekto_posedantoj = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not (message or kwargs.get('uuid', False)):
                    # если пользователь владелец объекта, по которому создаётся проект, то можно создавать
                    # проверяем по полю posedanto
                    # if not uzanto.has_perm('projektoj.povas_krei_projektoj'):
                    #     posedanto_objekto_perm, message = provado_posedanto_objekto(uzanto, **kwargs)
                    if uzanto.has_perm('projektoj.povas_krei_projektoj') or posedanto_objekto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = ProjektojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории проектов'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjektoTipo.DoesNotExist:
                                    message = _('Неверный тип проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = ProjektojProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjektoStatuso.DoesNotExist:
                                    message = _('Неверный статус проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект'),'objekto_uuid')

                        if not message:
                            projekto = ProjektojProjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                tipo = tipo,
                                statuso = statuso,
                                objekto = objekto,
                                kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                                kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                                kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                                fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                                fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                                fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                                pozicio = kwargs.get('pozicio', None),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    projekto.kategorio.set(kategorio)
                                else:
                                    projekto.kategorio.clear()

                            projekto.save()

                            status = True
                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = ProjektojProjektoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца проекта'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = ProjektojProjektoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except ProjektojProjektoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца проекта'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto = Uzanto.objects.get(
                                                id=kwargs.get('posedanto_uzanto_id'), 
                                                is_active=True,
                                                konfirmita=True
                                            )
                                        except Uzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь проекта')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец проекта')

                                if not message:
                                    if 'posedanto_komunumo_id' in kwargs:
                                        try:
                                            posedanto_komunumo = Komunumo.objects.get(
                                                id=kwargs.get('posedanto_komunumo_id'),
                                                forigo=False,
                                                arkivo=False
                                            )
                                        except Komunumo.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'posedanto_komunumo_id')

                                if not message:
                                    if 'posedanto_objekto_uuid' in kwargs:
                                        try:
                                            posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Objekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')

                                if not message:
                                    projekto_posedantoj = ProjektojProjektoPosedanto.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        realeco=realeco,
                                        projekto=projekto,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = posedanto_objekto,
                                        posedanto_komunumo = posedanto_komunumo,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    projekto_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')
    
                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                elif not message:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) 
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)
                            or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                            or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                            or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                            or kwargs.get('objekto', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)

                            if ((not uzanto.has_perm('projektoj.povas_forigi_projektoj'))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('projektoj.povas_shanghi_projektoj')):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = ProjektojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojProjektoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojProjektoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoStatuso.DoesNotExist:
                                        message = _('Неверный статус проектов')

                            if not message:

                                projekto.forigo = kwargs.get('forigo', projekto.forigo)
                                projekto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto.arkivo = kwargs.get('arkivo', projekto.arkivo)
                                projekto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto.publikigo = kwargs.get('publikigo', projekto.publikigo)
                                projekto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto.realeco = realeco if kwargs.get('realeco_id', False) else projekto.realeco
                                projekto.statuso = statuso if kwargs.get('statuso_id', False) else projekto.statuso
                                projekto.tipo = tipo if kwargs.get('tipo_id', False) else projekto.tipo
                                projekto.pozicio = kwargs.get('pozicio', projekto.pozicio)
                                projekto.objekto = objekto if kwargs.get('objekto_uuid', False) else projekto.objekto
                                projekto.kom_koordinato_x = kwargs.get('kom_koordinato_x', projekto.kom_koordinato_x)
                                projekto.kom_koordinato_y = kwargs.get('kom_koordinato_y', projekto.kom_koordinato_y)
                                projekto.kom_koordinato_z = kwargs.get('kom_koordinato_z', projekto.kom_koordinato_z)
                                projekto.fin_koordinato_x = kwargs.get('fin_koordinato_x', projekto.fin_koordinato_x)
                                projekto.fin_koordinato_y = kwargs.get('fin_koordinato_y', projekto.fin_koordinato_y)
                                projekto.fin_koordinato_z = kwargs.get('fin_koordinato_z', projekto.fin_koordinato_z)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        projekto.kategorio.set(kategorio)
                                    else:
                                        projekto.kategorio.clear()

                                projekto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjekto(status=status, message=message, projekto=projekto, projekto_posedantoj=projekto_posedantoj)


# Модель типов владельцев проектов
class RedaktuProjektojProjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj_tipoj = graphene.Field(ProjektojProjektoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_posedantoj_tipoj = ProjektojProjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj_tipoj = ProjektojProjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_posedantoj_tipoj.forigo = kwargs.get('forigo', projekto_posedantoj_tipoj.forigo)
                                projekto_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj_tipoj.arkivo = kwargs.get('arkivo', projekto_posedantoj_tipoj.arkivo)
                                projekto_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj_tipoj.publikigo = kwargs.get('publikigo', projekto_posedantoj_tipoj.publikigo)
                                projekto_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoPosedantoTipo(status=status, message=message, projekto_posedantoj_tipoj=projekto_posedantoj_tipoj)


# Модель статусов владельцев проектов
class RedaktuProjektojProjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj_statusoj = graphene.Field(ProjektojProjektoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца проектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_posedantoj_statusoj = ProjektojProjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj_statusoj = ProjektojProjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_posedantoj_statusoj.forigo = kwargs.get('forigo', projekto_posedantoj_statusoj.forigo)
                                projekto_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj_statusoj.arkivo = kwargs.get('arkivo', projekto_posedantoj_statusoj.arkivo)
                                projekto_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj_statusoj.publikigo = kwargs.get('publikigo', projekto_posedantoj_statusoj.publikigo)
                                projekto_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoPosedantoStatuso(status=status, message=message, projekto_posedantoj_statusoj=projekto_posedantoj_statusoj)


# Модель владельцев проектов
class RedaktuProjektojProjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_posedantoj = graphene.Field(ProjektojProjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец проекта'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец проекта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец проекта'))
        posedanto_komunumo_id = graphene.Int(description=_('Сообщество владелец проекта'))
        tipo_id = graphene.Int(description=_('Тип владельца проекта'))
        statuso_id = graphene.Int(description=_('Статус владельца проекта'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец проекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_posedantoj = None
        realeco = None
        projekto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_komunumo = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        posedanto_perm = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_posedantoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец проекта'),'posedanto_organizo_uuid')

                        if not message:
                            if 'posedanto_komunumo_id' in kwargs:
                                try:
                                    posedanto_komunumo = Komunumo.objects.get(
                                        id=kwargs.get('posedanto_komunumo_id'),
                                        forigo=False,
                                        arkivo=False
                                    )
                                except Komunumo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'posedanto_komunumo_id')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный проект'),'projekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный параллельный мир'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjektoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = ProjektojProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjektoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца проектов')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = Objekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                        if 'posedanto_uzanto_id' in kwargs:
                            try:
                                posedanto_uzanto = Uzanto.objects.get(
                                    id=kwargs.get('posedanto_uzanto_id'), 
                                    is_active=True,
                                    konfirmita=True
                                )
                            except Uzanto.DoesNotExist:
                                message = _('Неверный владелец/пользователь проекта')

                        if not message:
                            projekto_posedantoj = ProjektojProjektoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                projekto = projekto,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                posedanto_komunumo = posedanto_komunumo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_posedantoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_posedantoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('posedanto_komunumo_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('projekto_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_posedantoj = ProjektojProjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not (uzanto.has_perm('projektoj.povas_forigi_projektoj_posedantoj'))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('projektoj.povas_shanghi_projektoj_posedantoj')):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь проекта')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец проекта')

                            if not message:
                                if 'posedanto_komunumo_id' in kwargs:
                                    try:
                                        posedanto_komunumo = Komunumo.objects.get(
                                            id=kwargs.get('posedanto_komunumo_id'),
                                            forigo=False,
                                            arkivo=False
                                        )
                                    except Komunumo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'posedanto_komunumo_id')

                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjekto.DoesNotExist:
                                        message = _('Неверный проект')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojProjektoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца проектов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojProjektoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца проектов')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = Objekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец проекта'), 'posedanto_objekto_uuid')

                            if not message:

                                projekto_posedantoj.forigo = kwargs.get('forigo', projekto_posedantoj.forigo)
                                projekto_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_posedantoj.arkivo = kwargs.get('arkivo', projekto_posedantoj.arkivo)
                                projekto_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_posedantoj.publikigo = kwargs.get('publikigo', projekto_posedantoj.publikigo)
                                projekto_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else projekto_posedantoj.realeco
                                projekto_posedantoj.projekto = realeco if kwargs.get('projekto_uuid', False) else projekto_posedantoj.projekto
                                projekto_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else projekto_posedantoj.posedanto_uzanto
                                projekto_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else projekto_posedantoj.posedanto_organizo
                                projekto_posedantoj.posedanto_komunumo = posedanto_komunumo if kwargs.get('posedanto_komunumo_id', False) else projekto_posedantoj.posedanto_komunumo
                                projekto_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else projekto_posedantoj.statuso
                                projekto_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else projekto_posedantoj.tipo
                                projekto_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else projekto_posedantoj.posedanto_objekto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_posedantoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_posedantoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoPosedanto(status=status, message=message, projekto_posedantoj=projekto_posedantoj)


# Модель типов связей проектов между собой
class RedaktuProjektojProjektoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_ligilo_tipoj = graphene.Field(ProjektojProjektoLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей проектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            projekto_ligilo_tipoj = ProjektojProjektoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(projekto_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(projekto_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            projekto_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_ligilo_tipoj = ProjektojProjektoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                projekto_ligilo_tipoj.forigo = kwargs.get('forigo', projekto_ligilo_tipoj.forigo)
                                projekto_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_ligilo_tipoj.arkivo = kwargs.get('arkivo', projekto_ligilo_tipoj.arkivo)
                                projekto_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_ligilo_tipoj.publikigo = kwargs.get('publikigo', projekto_ligilo_tipoj.publikigo)
                                projekto_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(projekto_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(projekto_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                projekto_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoLigiloTipo(status=status, message=message, projekto_ligilo_tipoj=projekto_ligilo_tipoj)


# Модель связей проектов между собой
class RedaktuProjektojProjektoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto_ligilo = graphene.Field(ProjektojProjektoLigiloNode, 
        description=_('Создание/изменение связи проектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID проекта владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемого проекта'))
        tipo_id = graphene.Int(description=_('ID типа связи проектов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        projekto_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_projektoj_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = ProjektojProjekto.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный проект'),'posedanto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = ProjektojProjekto.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный проект'),'ligilo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojProjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjektoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи проектов между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            projekto_ligilo = ProjektojProjektoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            projekto_ligilo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_id', False) or kwargs.get('ligilo_id', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            projekto_ligilo = ProjektojProjektoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_projektoj_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_projektoj_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = ProjektojProjekto.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный проект'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = ProjektojProjekto.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный проект'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojProjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи проектов между собой'),'tipo_id')

                            if not message:

                                projekto_ligilo.forigo = kwargs.get('forigo', projekto_ligilo.forigo)
                                projekto_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                projekto_ligilo.arkivo = kwargs.get('arkivo', projekto_ligilo.arkivo)
                                projekto_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                projekto_ligilo.publikigo = kwargs.get('publikigo', projekto_ligilo.publikigo)
                                projekto_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                projekto_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else projekto_ligilo.posedanto
                                projekto_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else projekto_ligilo.ligilo
                                projekto_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else projekto_ligilo.tipo

                                projekto_ligilo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojProjektoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojProjektoLigilo(status=status, message=message, projekto_ligilo=projekto_ligilo)


# Модель категорий задач
class RedaktuProjektojTaskoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_kategorioj = graphene.Field(ProjektojTaskoKategorioNode,
        description=_('Созданная/изменённая категория задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            taskoj_kategorioj = ProjektojTaskoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    taskoj_kategorioj.realeco.set(realeco)
                                else:
                                    taskoj_kategorioj.realeco.clear()

                            taskoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_kategorioj = ProjektojTaskoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_taskoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_taskoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                taskoj_kategorioj.forigo = kwargs.get('forigo', taskoj_kategorioj.forigo)
                                taskoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_kategorioj.arkivo = kwargs.get('arkivo', taskoj_kategorioj.arkivo)
                                taskoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_kategorioj.publikigo = kwargs.get('publikigo', taskoj_kategorioj.publikigo)
                                taskoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        taskoj_kategorioj.realeco.set(realeco)
                                    else:
                                        taskoj_kategorioj.realeco.clear()

                                taskoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoKategorio(status=status, message=message, taskoj_kategorioj=taskoj_kategorioj)


# Модель типов задач
class RedaktuProjektojTaskoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_tipoj = graphene.Field(ProjektojTaskoTipoNode, 
        description=_('Созданный/изменённый тип задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            taskoj_tipoj = ProjektojTaskoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    taskoj_tipoj.realeco.set(realeco)
                                else:
                                    taskoj_tipoj.realeco.clear()

                            taskoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_tipoj = ProjektojTaskoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_taskoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_taskoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                taskoj_tipoj.forigo = kwargs.get('forigo', taskoj_tipoj.forigo)
                                taskoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_tipoj.arkivo = kwargs.get('arkivo', taskoj_tipoj.arkivo)
                                taskoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_tipoj.publikigo = kwargs.get('publikigo', taskoj_tipoj.publikigo)
                                taskoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        taskoj_tipoj.realeco.set(realeco)
                                    else:
                                        taskoj_tipoj.realeco.clear()

                                taskoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoTipo(status=status, message=message, taskoj_tipoj=taskoj_tipoj)


# Модель статусов задач
class RedaktuProjektojTaskoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_statusoj = graphene.Field(ProjektojTaskoStatusoNode, 
        description=_('Созданный/изменённый статус задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            taskoj_statusoj = ProjektojTaskoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            taskoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_statusoj = ProjektojTaskoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_taskoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_taskoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                taskoj_statusoj.forigo = kwargs.get('forigo', taskoj_statusoj.forigo)
                                taskoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_statusoj.arkivo = kwargs.get('arkivo', taskoj_statusoj.arkivo)
                                taskoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_statusoj.publikigo = kwargs.get('publikigo', taskoj_statusoj.publikigo)
                                taskoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                taskoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoStatuso(status=status, message=message, taskoj_statusoj=taskoj_statusoj)


# функция закрытия задачи
def fermo_tasko(taskoj):
    # находим статус закрытия задачи
    try:
        statuso = ProjektojTaskoStatuso.objects.get(id=4, forigo=False,
                                            arkivo=False, publikigo=True)
    except ProjektojTaskoStatuso.DoesNotExist:
        message = _('Отсутствует статус закрытия задачи')
        return message
    # закрываем задачу
    taskoj.statuso = statuso
    taskoj.save()
    return None


# функция закрытия проекта
def fermo_projekto(projekto):
    # находим статус закрытия проекта
    try:
        statuso = ProjektojProjektoStatuso.objects.get(id=4, forigo=False,
                                            arkivo=False, publikigo=True)
    except ProjektojProjektoStatuso.DoesNotExist:
        message = _('Отсутствует статус закрытия проекта')
        return message
    # закрываем проект движения
    projekto.statuso = statuso
    projekto.save()
    return None


# функция закрытия проекта и задачи
def fermo_projekto_tasko(taskoj):
    message = fermo_projekto(taskoj.projekto)
    if message:
        return message
    return fermo_tasko(taskoj)


# функция обработки запроса на выход из станции
def eliro_kosmostacio(taskoj, tasko_posedantoj):
    # проверка наличия необходимых данных
    if not taskoj.objekto:
        return
    if not tasko_posedantoj.posedanto_objekto:
        return
    # находим связь
    try:
        objektoj_ligiloj = ObjektoLigilo.objects.get(
            posedanto=taskoj.objekto,
            ligilo=tasko_posedantoj.posedanto_objekto,
            forigo=False,
            arkivo=False, 
            publikigo=True)
    except ObjektoLigilo.DoesNotExist:
        message = _('Отсутствует связь')
        return message
    # закрываем задачу и проект
    message = fermo_projekto_tasko(taskoj)
    if message:
        return message
    # устанавливаем координы, куб объекту
    tasko_posedantoj.posedanto_objekto.kubo = taskoj.objekto.kubo
    tasko_posedantoj.posedanto_objekto.koordinato_x = taskoj.objekto.koordinato_x + 120
    tasko_posedantoj.posedanto_objekto.koordinato_y = taskoj.objekto.koordinato_y + 120
    tasko_posedantoj.posedanto_objekto.koordinato_z = taskoj.objekto.koordinato_z + 200
    tasko_posedantoj.posedanto_objekto.rotacia_x = 0
    tasko_posedantoj.posedanto_objekto.rotacia_y = 0
    tasko_posedantoj.posedanto_objekto.rotacia_z = 0
    objekto_save(tasko_posedantoj.posedanto_objekto, None)
    # помечаем на удаление связь на нахождение внутри станции
    objektoj_ligiloj.forigo = True
    objekto_ligilo_save(objektoj_ligiloj)


# сохраняем задачи
def taskoj_save(taskoj):

    taskoj.save()


# Модель задач
class RedaktuProjektojTasko(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj = graphene.Field(ProjektojTaskoNode, 
        description=_('Созданный/изменённый задача'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект, в который входит задача'))
        objekto_uuid = graphene.String(description=_('Объект владелец задачи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий задач'))
        tipo_id = graphene.Int(description=_('Тип задач'))
        statuso_id = graphene.Int(description=_('Статус задач'))
        kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.Int(description=_('Позиция в списке'))

        # параметры для создания владельца
        posedanto_uzanto_id = graphene.Int(description=_('Владелец задачи'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задачи'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задачи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj = None
        realeco = None
        projekto = None
        objekto = None
        kategorio = ProjektojTaskoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        posedanto_tipo = None
        posedanto_statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        posedanto_perm = None # если пользователь владелец объекта по проекту

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj') or posedanto_perm:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категории задач'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'projekto_uuid' in kwargs:
                                try:
                                    projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojProjekto.DoesNotExist:
                                    message = _('Неверный проект в который входит задача')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = _('Неверный объект')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTaskoTipo.DoesNotExist:
                                    message = _('Неверный тип задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = ProjektojTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTaskoStatuso.DoesNotExist:
                                    message = _('Неверный статус задач')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            taskoj = ProjektojTasko.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco = realeco,
                                projekto = projekto,
                                objekto = objekto,
                                tipo = tipo,
                                statuso = statuso,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now(),
                                kom_koordinato_x = kwargs.get('kom_koordinato_x', None),
                                kom_koordinato_y = kwargs.get('kom_koordinato_y', None),
                                kom_koordinato_z = kwargs.get('kom_koordinato_z', None),
                                fin_koordinato_x = kwargs.get('fin_koordinato_x', None),
                                fin_koordinato_y = kwargs.get('fin_koordinato_y', None),
                                fin_koordinato_z = kwargs.get('fin_koordinato_z', None),
                                pozicio = kwargs.get('pozicio', None),
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'kategorio' in kwargs:
                                if kategorio:
                                    taskoj.kategorio.set(kategorio)
                                else:
                                    taskoj.kategorio.clear()
                            
                            taskoj_save(taskoj)

                            status = True

                            # далее блок добавления владельца
                            if (kwargs.get('posedanto_tipo_id', False)):
                                if not message:
                                    try:
                                        posedanto_tipo = ProjektojTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца задачи'),'posedanto_tipo_id')

                                if not message:
                                    if 'posedanto_statuso_id' in kwargs:
                                        try:
                                            posedanto_statuso = ProjektojTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except ProjektojTaskoPosedantoStatuso.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный статус владельца задач'),'posedanto_statuso_id')
                                    else:
                                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                                if not message:
                                    if 'posedanto_uzanto_id' in kwargs:
                                        try:
                                            posedanto_uzanto = Uzanto.objects.get(
                                                id=kwargs.get('posedanto_uzanto_id'), 
                                                is_active=True,
                                                konfirmita=True
                                            )
                                        except Uzanto.DoesNotExist:
                                            message = _('Неверный владелец/пользователь задачи')

                                if not message:
                                    if 'posedanto_organizo_uuid' in kwargs:
                                        try:
                                            posedanto_organizo = Organizo.objects.get(
                                                uuid=kwargs.get('posedanto_organizo_uuid'),
                                                forigo=False,
                                                arkivo=False,  
                                                publikigo=True
                                            )
                                        except Organizo.DoesNotExist:
                                            message = _('Неверная организация владелец задачи')

                                if not message:
                                    if 'posedanto_objekto_uuid' in kwargs:
                                        try:
                                            posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Objekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')

                                if not message:
                                    tasko_posedantoj = ProjektojTaskoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        tasko = taskoj,
                                        posedanto_uzanto = posedanto_uzanto,
                                        posedanto_organizo = posedanto_organizo,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = posedanto_objekto,
                                        publikigo = kwargs.get('publikigo', False),
                                        publikiga_dato = timezone.now()
                                    )

                                    tasko_posedantoj.save()

                                    status = True
                                    message = _('Обе записи созданы')

                            if not message:
                                message = _('Запись создана')

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kategorio', False)
                            or kwargs.get('projekto_uuid', False) or kwargs.get('objekto_uuid', False)
                            or kwargs.get('tipo_id', False) 
                            or kwargs.get('kom_koordinato_x', False) or kwargs.get('kom_koordinato_y', False)
                            or kwargs.get('kom_koordinato_z', False) or kwargs.get('fin_koordinato_x', False)
                            or kwargs.get('fin_koordinato_y', False) or kwargs.get('fin_koordinato_z', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('pozicio', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj = ProjektojTasko.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not (uzanto.has_perm('projektoj.povas_forigi_taskoj'))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('projektoj.povas_shanghi_taskoj')):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))
                                    if len(kategorio_id):
                                        kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))
                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категории проектов'),
                                                        str(dif)
                                            )

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'projekto_uuid' in kwargs:
                                    try:
                                        projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjekto.DoesNotExist:
                                        message = _('Неверный проект в который входит задача')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = _('Неверный объект')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoTipo.DoesNotExist:
                                        message = _('Неверный тип проектов')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojTaskoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус проектов')

                            if not message:

                                taskoj.forigo = kwargs.get('forigo', taskoj.forigo)
                                taskoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj.arkivo = kwargs.get('arkivo', taskoj.arkivo)
                                taskoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj.publikigo = kwargs.get('publikigo', taskoj.publikigo)
                                taskoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj.realeco = realeco if kwargs.get('realeco_id', False) else taskoj.realeco
                                taskoj.projekto = projekto if kwargs.get('projekto_uuid', False) else taskoj.projekto
                                taskoj.objekto = objekto if kwargs.get('objekto_uuid', False) else taskoj.objekto
                                taskoj.statuso = statuso if kwargs.get('statuso_id', False) else taskoj.statuso
                                taskoj.tipo = tipo if kwargs.get('tipo_id', False) else taskoj.tipo
                                taskoj.kom_koordinato_x = kwargs.get('kom_koordinato_x', taskoj.kom_koordinato_x)
                                taskoj.kom_koordinato_y = kwargs.get('kom_koordinato_y', taskoj.kom_koordinato_y)
                                taskoj.kom_koordinato_z = kwargs.get('kom_koordinato_z', taskoj.kom_koordinato_z)
                                taskoj.fin_koordinato_x = kwargs.get('fin_koordinato_x', taskoj.fin_koordinato_x)
                                taskoj.fin_koordinato_y = kwargs.get('fin_koordinato_y', taskoj.fin_koordinato_y)
                                taskoj.fin_koordinato_z = kwargs.get('fin_koordinato_z', taskoj.fin_koordinato_z)
                                taskoj.pozicio = kwargs.get('pozicio', taskoj.pozicio)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        taskoj.kategorio.set(kategorio)
                                    else:
                                        taskoj.kategorio.clear()

                                taskoj_save(taskoj)
                                status = True

                                message = _('Запись успешно изменена')
                        except ProjektojTasko.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTasko(status=status, message=message, taskoj=taskoj)


# Модель типов владельцев задач
class RedaktuProjektojTaskoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj_posedantoj_tipoj = graphene.Field(ProjektojTaskoPosedantoTipoNode, 
        description=_('Созданный/изменённый тип владельцев задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            taskoj_posedantoj_tipoj = ProjektojTaskoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(taskoj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            taskoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            taskoj_posedantoj_tipoj = ProjektojTaskoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_taskoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_taskoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                taskoj_posedantoj_tipoj.forigo = kwargs.get('forigo', taskoj_posedantoj_tipoj.forigo)
                                taskoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                taskoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', taskoj_posedantoj_tipoj.arkivo)
                                taskoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                taskoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', taskoj_posedantoj_tipoj.publikigo)
                                taskoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                taskoj_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(taskoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(taskoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                taskoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoPosedantoTipo(status=status, message=message, taskoj_posedantoj_tipoj=taskoj_posedantoj_tipoj)


# Модель статусов владельцев задач
class RedaktuProjektojTaskoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_posedantoj_statusoj = graphene.Field(ProjektojTaskoPosedantoStatusoNode, 
        description=_('Созданный/изменённый статус владельца задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            tasko_posedantoj_statusoj = ProjektojTaskoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(tasko_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            tasko_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_posedantoj_statusoj = ProjektojTaskoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_taskoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_taskoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                tasko_posedantoj_statusoj.forigo = kwargs.get('forigo', tasko_posedantoj_statusoj.forigo)
                                tasko_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_posedantoj_statusoj.arkivo = kwargs.get('arkivo', tasko_posedantoj_statusoj.arkivo)
                                tasko_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_posedantoj_statusoj.publikigo = kwargs.get('publikigo', tasko_posedantoj_statusoj.publikigo)
                                tasko_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(tasko_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(tasko_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                tasko_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoPosedantoStatuso(status=status, message=message, tasko_posedantoj_statusoj=tasko_posedantoj_statusoj)


# Модель владельцев задач
class RedaktuProjektojTaskoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_posedantoj = graphene.Field(ProjektojTaskoPosedantoNode, 
        description=_('Созданный/изменённый владелец задач'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tasko_uuid = graphene.String(description=_('Задача'))
        posedanto_uzanto_id = graphene.Int(description=_('Владелец задачи'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец задачи'))
        tipo_id = graphene.Int(description=_('Тип владельца задачи'))
        statuso_id = graphene.Int(description=_('Статус владельца задачи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_posedantoj = None
        realeco = None
        tasko = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        posedanto_objekto = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_taskoj_posedantoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        is_active=True,
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный владелец/пользователь задачи')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'),
                                        forigo=False,
                                        arkivo=False,  
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = _('Неверная организация владелец задачи')

                        if not message:
                            if 'tasko_uuid' in kwargs:
                                try:
                                    tasko = ProjektojTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTasko.DoesNotExist:
                                    message = _('Неверный проект')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tasko_uuid')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = _('Неверный параллельный мир')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTaskoPosedantoTipo.DoesNotExist:
                                    message = _('Неверный тип владельца задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = ProjektojTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTaskoPosedantoStatuso.DoesNotExist:
                                    message = _('Неверный статус владельца задачи')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

                        if not message:
                            if 'posedanto_objekto_uuid' in kwargs:
                                try:
                                    posedanto_objekto = Objekto.objects.get(
                                        uuid=kwargs.get('posedanto_objekto_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец задачи'),'posedanto_objekto_uuid')

                        if not message:
                            tasko_posedantoj = ProjektojTaskoPosedanto.objects.create(
                                forigo = False,
                                arkivo = False,
                                realeco = realeco,
                                tasko = tasko,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                statuso = statuso,
                                posedanto_objekto = posedanto_objekto,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato = timezone.now()
                            )

                            tasko_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('tasko_uuid', False)
                            or kwargs.get('posedanto_objekto_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_posedantoj = ProjektojTaskoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)

                            if (not (uzanto.has_perm('projektoj.povas_forigi_taskoj_posedantoj'))
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('projektoj.povas_shanghi_taskoj_posedantoj')):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный владелец/пользователь задачи')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец задачи')

                            if not message:
                                if 'tasko_uuid' in kwargs:
                                    try:
                                        tasko = ProjektojTasko.objects.get(uuid=kwargs.get('tasko_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTasko.DoesNotExist:
                                        message = _('Неверная задача')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = _('Неверный параллельный мир')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojTaskoPosedantoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoPosedantoTipo.DoesNotExist:
                                        message = _('Неверный тип владельца задачи')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojTaskoPosedantoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoPosedantoStatuso.DoesNotExist:
                                        message = _('Неверный статус владельца задачи')

                            if not message:
                                if 'posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = Objekto.objects.get(
                                            uuid=kwargs.get('posedanto_objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец задачи'),'posedanto_objekto_uuid')

                            if not message:

                                tasko_posedantoj.forigo = kwargs.get('forigo', tasko_posedantoj.forigo)
                                tasko_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_posedantoj.arkivo = kwargs.get('arkivo', tasko_posedantoj.arkivo)
                                tasko_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_posedantoj.publikigo = kwargs.get('publikigo', tasko_posedantoj.publikigo)
                                tasko_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else tasko_posedantoj.realeco
                                tasko_posedantoj.tasko = realeco if kwargs.get('tasko_uuid', False) else tasko_posedantoj.tasko
                                tasko_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else tasko_posedantoj.posedanto_uzanto
                                tasko_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else tasko_posedantoj.posedanto_organizo
                                tasko_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else tasko_posedantoj.statuso
                                tasko_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else tasko_posedantoj.tipo
                                tasko_posedantoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else tasko_posedantoj.posedanto_objekto

                                tasko_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoPosedanto(status=status, message=message, tasko_posedantoj=tasko_posedantoj)


# Модель добавления множества задач с их владельцами
class RedaktuKreiProjektojTaskojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    taskoj = graphene.Field(graphene.List(ProjektojTaskoNode), 
        description=_('Создание списка задач с владельцами'))

    class Arguments:
        # параметры, одинаковые для всех задач
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        projekto_uuid = graphene.String(description=_('Проект в который входит задача'))
        tipo_id = graphene.Int(description=_('Тип задач'))
        # параметры задач в перечне (могут быть разные для разных задач)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий задач')))
        objekto_uuid = graphene.List(graphene.String, description=_('Объект'))
        statuso_id = graphene.List(graphene.Int, description=_('Статус задач'))
        kom_koordinato_x = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владелец задач - объект
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца задач'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца задач'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        taskoj = []
        realeco = None
        projekto = None
        objekto = []
        posedanto_objekto = None
        kategorio = ProjektojTaskoKategorio.objects.none()
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца объекта/задачи

        if uzanto.is_authenticated:
            with transaction.atomic():
                if uzanto.has_perm('projektoj.povas_krei_taskoj'):
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif not len(kwargs.get('kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории задач'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'projekto_uuid' in kwargs:
                            try:
                                projekto = ProjektojProjekto.objects.get(uuid=kwargs.get('projekto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ProjektojProjekto.DoesNotExist:
                                message = _('Неверный проект в который входит задача')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'projekto_uuid')

                    if not message:
                        if 'posedanto_objekto_uuid' in kwargs:
                            try:
                                posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект'),'posedanto_objekto_uuid')

                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = ProjektojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ProjektojTaskoTipo.DoesNotExist:
                                message = _('Неверный тип задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')


                    if not message:
                        if 'posedanto_tipo_id' in kwargs:
                            try:
                                posedanto_tipo = ProjektojTaskoPosedantoTipo.objects.get(id=kwargs.get('posedanto_tipo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except ProjektojTaskoPosedantoTipo.DoesNotExist:
                                message = _('Неверный тип владельца задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_tipo_id')

                    if not message:
                        if 'posedanto_statuso_id' in kwargs:
                            try:
                                posedanto_statuso = ProjektojTaskoPosedantoStatuso.objects.get(id=kwargs.get('posedanto_statuso_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except ProjektojTaskoPosedantoStatuso.DoesNotExist:
                                message = _('Неверный статус владельца задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_statuso_id')

                    if not message:
                        objekto_uuid = set(kwargs.get('objekto_uuid',[]))
                        # удаляем None из списка
                        objekto_uuid.discard(None)
                        if len(objekto_uuid):
                            try:
                                objekto = Objekto.objects.filter(uuid__in=objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                                set_value = set(objekto.values_list('uuid', flat=True))
                                arr_value = []
                                for s in set_value:
                                    arr_value.append(str(s))

                                dif = set(objekto_uuid) - set(arr_value)

                                if len(dif):
                                    message='{}: {}'.format(
                                                    _('Указаны несуществующие объекты'),
                                                    str(dif)
                                    )
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект'),objekto_uuid)

                    if not kwargs.get('pozicio',False):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    if (not message) and kwargs.get('pozicio',False):
                        pozicioj = kwargs.get('pozicio')
                        statusoj = kwargs.get('statuso_id')
                        i = 0
                        for pozic in pozicioj:
                            tasko = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojTaskoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус задач')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                            else:
                                break

                            objekto = None
                            if ('objekto_uuid' in kwargs) and (kwargs.get('objekto_uuid')[i]):
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid')[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = _('Неверный объект')

                            if not message:
                                tasko = ProjektojTasko.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    projekto = projekto,
                                    objekto = objekto,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    kom_koordinato_x = kwargs.get('kom_koordinato_x', None)[i] if len(kwargs.get('kom_koordinato_x', []))>i else None,
                                    kom_koordinato_y = kwargs.get('kom_koordinato_y', None)[i] if len(kwargs.get('kom_koordinato_y', []))>i else None,
                                    kom_koordinato_z = kwargs.get('kom_koordinato_z', None)[i] if len(kwargs.get('kom_koordinato_z', []))>i else None,
                                    fin_koordinato_x = kwargs.get('fin_koordinato_x', None)[i] if len(kwargs.get('fin_koordinato_x', []))>i else None,
                                    fin_koordinato_y = kwargs.get('fin_koordinato_y', None)[i] if len(kwargs.get('fin_koordinato_y', []))>i else None,
                                    fin_koordinato_z = kwargs.get('fin_koordinato_z', None)[i] if len(kwargs.get('fin_koordinato_z', []))>i else None,
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(tasko.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(tasko.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = ProjektojTaskoKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)

                                    if kategorio:
                                        tasko.kategorio.set(kategorio)
                                    else:
                                        tasko.kategorio.clear()

                                taskoj_save(tasko)
                                taskoj.append(tasko)

                                if not message:
                                    tasko_posedantoj = ProjektojTaskoPosedanto.objects.create(
                                        forigo = False,
                                        arkivo = False,
                                        realeco = realeco,
                                        tasko = tasko,
                                        tipo = posedanto_tipo,
                                        statuso = posedanto_statuso,
                                        posedanto_objekto = posedanto_objekto,
                                        publikigo = True,
                                        publikiga_dato = timezone.now()
                                    )

                                    tasko_posedantoj.save()

                                status = True
                            
                            i += 1

                        if status:
                            message = _('Запись создана')
                else:
                    message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiProjektojTaskojPosedanto(status=status, message=message, taskoj=taskoj)


# Модель добавления проекта с владельцем и множества задач с их владельцами
class RedaktuKreiProjektojProjektoTaskojPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    projekto = graphene.Field(ProjektojProjektoNode, 
        description=_('Создание проекта с владельцем и списка задач с владельцами'))
    taskoj = graphene.Field(graphene.List(ProjektojTaskoNode), 
        description=_('Создание списка задач с владельцами'))

    class Arguments:
        # параметры, одинаковые для проекта и всех задач
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # параметры для проекта
        prj_nomo = graphene.String(description=_('Название'))
        prj_priskribo = graphene.String(description=_('Описание'))
        prj_kategorio = graphene.List(graphene.Int,description=_('Список категорий проектов'))
        prj_tipo_id = graphene.Int(description=_('Тип проектов'))
        prj_statuso_id = graphene.Int(description=_('Статус проектов'))
        prj_objekto_uuid = graphene.String(description=_('Объект'))
        prj_kom_koordinato_x = graphene.Float(description=_('Начальные координаты выполнения проекта по оси X в кубе'))
        prj_kom_koordinato_y = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Y в кубе'))
        prj_kom_koordinato_z = graphene.Float(description=_('Начальные координаты выполнения проекта по оси Z в кубе'))
        prj_fin_koordinato_x = graphene.Float(description=_('Конечные координаты выполнения проекта по оси X в кубе'))
        prj_fin_koordinato_y = graphene.Float(description=_('Конечные координаты выполнения проекта по оси Y в кубе'))
        prj_fin_koordinato_z = graphene.Float(description=_('Конечные координаты выполнения проекта по оси z в кубе'))
        # параметры для создания владельца проекта
        prj_posedanto_uzanto_id = graphene.Int(description=_('ID владельца-лица проекта'))
        prj_posedanto_organizo_uuid = graphene.String(description=_('Организация-владелец проекта'))
        prj_posedanto_tipo_id = graphene.Int(description=_('Тип владельца проекта'))
        prj_posedanto_statuso_id = graphene.Int(description=_('Статус владельца проекта'))
        prj_posedanto_objekto_uuid = graphene.String(description=_('Объект-владелец проекта')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект
        # параметры, одинаковые для всех задач
        tipo_id = graphene.Int(description=_('Тип задач'))
        stokejo_uuid = graphene.String(description=_('Место хранения при передаче объектов'))
        # параметры задач в перечне (могут быть разные для разных задач)
        nomo = graphene.List(graphene.String, description=_('Название'))
        priskribo = graphene.List(graphene.String, description=_('Описание'))
        kategorio = graphene.List(graphene.List(graphene.Int, description=_('Список категорий задач')))
        objekto_uuid = graphene.List(graphene.String, description=_('Объект'))
        statuso_id = graphene.List(graphene.Int, description=_('Статус задач'))
        kom_koordinato_x = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси X в кубе'))
        kom_koordinato_y = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Y в кубе'))
        kom_koordinato_z = graphene.List(graphene.Float, description=_('Начальные координаты выполнения задачи по оси Z в кубе'))
        fin_koordinato_x = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси X в кубе'))
        fin_koordinato_y = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси Y в кубе'))
        fin_koordinato_z = graphene.List(graphene.Float, description=_('Конечные координаты выполнения задачи по оси z в кубе'))
        pozicio = graphene.List(graphene.Int, description=_('Позиция в списке'))

        # Владелец задач - объект
        # владельцев может быть много
        posedanto_tipo_id = graphene.List(graphene.Int, description=_('Тип владельца задач'))
        posedanto_statuso_id = graphene.List(graphene.Int, description=_('Статус владельца задач'))
        posedanto_objekto_uuid = graphene.List(graphene.String, description=_('Объект владелец задачи')) # если не передаётся, а передаётся objekto_uuid, то это один и тот же объект

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        message_krei = None
        projekto = None
        posedanto_organizo = None
        posedanto_uzanto = None
        taskoj = []
        realeco = None
        posedanto_objekto = None
        kategorio = ProjektojTaskoKategorio.objects.none()
        prj_kategorio = ProjektojProjektoKategorio.objects.none()
        objekto = None
        tipo = None
        statuso = None
        posedanto_tipo = None
        posedanto_statuso = None
        uzanto = info.context.user
        posedanto_perm = None # для проверки владельца объекта/задачи
        stokejo = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                if not kwargs.get('pozicio',False):
                    message = '{} "{}"'.format(_('Поле обязательно для заполнения (должна быть хотя бы одна задача)'),'pozicio')
                if uzanto.has_perm('projektoj.povas_krei_projektoj') :
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('prj_nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_nomo')

                    if not message:
                        if 'prj_kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_kategorio')
                        elif not len(kwargs.get('prj_kategorio')):
                            message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'prj_kategorio')
                        else:
                            kategorio_id = set(kwargs.get('prj_kategorio'))

                            if len(kategorio_id):
                                prj_kategorio = ProjektojProjektoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                dif = set(kategorio_id) - set(prj_kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории проектов'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    if not message:
                        if 'realeco_id' in kwargs:
                            try:
                                realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Realeco.DoesNotExist:
                                message = _('Неверный параллельный мир')

                    if not message:
                        if 'prj_tipo_id' in kwargs:
                            try:
                                tipo = ProjektojProjektoTipo.objects.get(id=kwargs.get('prj_tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ProjektojProjektoTipo.DoesNotExist:
                                message = _('Неверный тип проектов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_tipo_id')

                    if not message:
                        if 'prj_statuso_id' in kwargs:
                            try:
                                statuso = ProjektojProjektoStatuso.objects.get(id=kwargs.get('prj_statuso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ProjektojProjektoStatuso.DoesNotExist:
                                message = _('Неверный статус проектов')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_statuso_id')

                    if not message:
                        if 'prj_objekto_uuid' in kwargs:
                            try:
                                objekto = Objekto.objects.get(uuid=kwargs.get('prj_objekto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект'),'prj_objekto_uuid')

                    if not message:
                        if 'stokejo_uuid' in kwargs:
                            try:
                                stokejo = ObjektoStokejo.objects.get(uuid=kwargs.get('stokejo_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ObjektoStokejo.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверное место хранения'),'stokejo_uuid')

                    if not message:
                        projekto = ProjektojProjekto.objects.create(
                            forigo=False,
                            arkivo=False,
                            realeco = realeco,
                            tipo = tipo,
                            statuso = statuso,
                            objekto = objekto,
                            kom_koordinato_x = kwargs.get('prj_kom_koordinato_x', None),
                            kom_koordinato_y = kwargs.get('prj_kom_koordinato_y', None),
                            kom_koordinato_z = kwargs.get('prj_kom_koordinato_z', None),
                            fin_koordinato_x = kwargs.get('prj_fin_koordinato_x', None),
                            fin_koordinato_y = kwargs.get('prj_fin_koordinato_y', None),
                            fin_koordinato_z = kwargs.get('prj_fin_koordinato_z', None),
                            publikigo = True,
                            publikiga_dato = timezone.now()
                        )

                        if (kwargs.get('prj_nomo', False)):
                            set_enhavo(projekto.nomo, kwargs.get('prj_nomo'), info.context.LANGUAGE_CODE)
                        if (kwargs.get('prj_priskribo', False)):
                            set_enhavo(projekto.priskribo, 
                                        kwargs.get('prj_priskribo'), 
                                        info.context.LANGUAGE_CODE)
                        if 'prj_kategorio' in kwargs:
                            if prj_kategorio:
                                projekto.kategorio.set(prj_kategorio)
                            else:
                                projekto.kategorio.clear()

                        projekto.save()

                        status = True
                        # далее блок добавления владельца
                        if (kwargs.get('prj_posedanto_uzanto_id', False) or kwargs.get('prj_posedanto_objekto_uuid', False) or kwargs.get('prj_posedanto_organizo_uuid', False)):
                            if not message:
                                if 'prj_posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('prj_posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )

                                    except Uzanto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Несуществующий пользователь проекта'),'prj_posedanto_uzanto_id')

                            if not message:
                                if 'prj_posedanto_tipo_id' in kwargs:
                                    try:
                                        posedanto_tipo = ProjektojProjektoPosedantoTipo.objects.get(id=kwargs.get('prj_posedanto_tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца проекта'),'prj_posedanto_tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_tipo_id')

                            if not message:
                                if 'prj_posedanto_statuso_id' in kwargs:
                                    try:
                                        posedanto_statuso = ProjektojProjektoPosedantoStatuso.objects.get(id=kwargs.get('prj_posedanto_statuso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojProjektoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца проекта'),'prj_posedanto_statuso_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'prj_posedanto_statuso_id')

                            if not message:
                                if 'prj_posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('prj_posedanto_organizo_uuid'),
                                            forigo=False,
                                            arkivo=False,  
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация владелец проекта')

                            if not message:
                                if 'prj_posedanto_objekto_uuid' in kwargs:
                                    try:
                                        posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('prj_posedanto_objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект'),'prj_posedanto_objekto_uuid')

                            if not message:
                                projekto_posedantoj = ProjektojProjektoPosedanto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco=realeco,
                                    projekto=projekto,
                                    posedanto_organizo = posedanto_organizo,
                                    posedanto_uzanto = posedanto_uzanto,
                                    tipo = posedanto_tipo,
                                    statuso = posedanto_statuso,
                                    posedanto_objekto = posedanto_objekto,
                                    publikigo = True,
                                    publikiga_dato = timezone.now()
                                )

                                projekto_posedantoj.save()

                                status = True
                                message_krei = _('Записи проекта и владельца созданы')

                        if not message_krei:
                            message_krei = _('Запись проекта создана')

                posedanto_objekto = None

                pozicioj = kwargs.get('pozicio')
                if pozicioj and projekto and (not message) and (uzanto.has_perm('projektoj.povas_krei_taskoj')):
                    # Проверяем наличие всех полей
                    if not message:
                        if not kwargs.get('nomo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        else:
                            if len(kwargs.get('nomo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество наименований меньше количества позиций'),len(kwargs.get('nomo')),len(pozicioj))


                    if not message:
                        if not kwargs.get('priskribo'):
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')
                        else:
                            if len(kwargs.get('priskribo'))<len(pozicioj):
                                message = '{} "{} < {}"'.format(_('количество описаний меньше количества позиций'),len(kwargs.get('priskribo')),len(pozicioj))

                    if not message:
                        if 'kategorio' not in kwargs:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                        elif len(kwargs.get('kategorio'))<len(pozicioj):
                            message = '{} "{} < {}"'.format(_('количество категорий меньше количества позиций'),len(kwargs.get('kategorio')),len(pozicioj))
                        else:
                            mas_kategorio = []
                            for kat in kwargs.get('kategorio'):
                                mas_kategorio.extend(kat)
                            kategorio_id = set(mas_kategorio)

                            if len(kategorio_id):
                                kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                if len(dif):
                                    message='{}: {}'.format(
                                                _('Указаны несуществующие категории задач'),
                                                str(dif)
                                    )

                    # проверяем наличие записей с таким кодом
                    tipo = None
                    if not message:
                        if 'tipo_id' in kwargs:
                            try:
                                tipo = ProjektojTaskoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                            except ProjektojTaskoTipo.DoesNotExist:
                                message = _('Неверный тип задачи')
                        else:
                            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                    if not message:
                        objekto_uuid = set(kwargs.get('objekto_uuid',[]))
                        # удаляем None из списка
                        objekto_uuid.discard(None)
                        if len(objekto_uuid):
                            objektoj = Objekto.objects.filter(uuid__in=objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(objektoj.values_list('uuid', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(str(s))

                            dif = set(objekto_uuid) - set(arr_value)

                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие объекты'),
                                                str(dif)
                                )

                    if not message:
                        tasko_posedanto_tipo_id = set(kwargs.get('posedanto_tipo_id',[]))
                        if len(tasko_posedanto_tipo_id):
                            tasko_posedanto_tipo = ProjektojTaskoPosedantoTipo.objects.filter(id__in=tasko_posedanto_tipo_id, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(tasko_posedanto_tipo.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(tasko_posedanto_tipo_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие типы владельцев'),
                                                str(dif)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны типы владельцев'),
                                            'posedanto_tipo_id'
                            )

                    if not message:
                        posedanto_objekto = None
                        posedanto_objekto_uuid = set(kwargs.get('posedanto_objekto_uuid',[]))
                        # удаляем None из списка
                        posedanto_objekto_uuid.discard(None)
                        if len(posedanto_objekto_uuid):
                            posedanto_objekto = Objekto.objects.filter(uuid__in=posedanto_objekto_uuid, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(posedanto_objekto.values_list('uuid', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(str(s))
                            dif = set(posedanto_objekto_uuid) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие объекты'),
                                                str(dif)
                                )
                            elif len(posedanto_objekto)<len(tasko_posedanto_tipo):
                                message='{}: {}<{}'.format(
                                                _('количество объектов владения меньше количества типов владельцев задач'),
                                                len(posedanto_objekto),
                                                len(tasko_posedanto_tipo)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны объекты-владельцы'),
                                            'posedanto_objekto_uuid'
                            )

                    if not message:
                        posedanto_statuso_id = set(kwargs.get('posedanto_statuso_id',[]))
                        if len(posedanto_statuso_id):
                            posedanto_statuso = ProjektojTaskoPosedantoStatuso.objects.filter(id__in=posedanto_statuso_id, forigo=False, arkivo=False, publikigo=True)
                            set_value = set(posedanto_statuso.values_list('id', flat=True))
                            arr_value = []
                            for s in set_value:
                                arr_value.append(s)
                            dif = set(posedanto_statuso_id) - set(arr_value)
                            if len(dif):
                                message='{}: {}'.format(
                                                _('Указаны несуществующие статусы владельцев'),
                                                str(dif)
                                )
                            elif len(posedanto_statuso_id)<len(tasko_posedanto_tipo):
                                message='{}: {}<{}'.format(
                                                _('количество статусов владельцев меньше количества типов владельцев задач'),
                                                len(posedanto_statuso_id),
                                                len(tasko_posedanto_tipo)
                                )
                        else:
                            message='{}: {}'.format(
                                            _('Не указаны статусы владельцев'),
                                            'posedanto_statuso_id'
                            )

                    if (not message) and (not kwargs.get('pozicio',False)):
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'pozicio')

                    statusoj = kwargs.get('statuso_id')
                    if len(statusoj)<len(pozicioj):
                        message = '{} "{} < {}"'.format(_('количество статусов меньше количества позиций'),len(statusoj),len(pozicioj))
                    if (not message) and kwargs.get('pozicio',False):
                        i = 0
                        statuso = None
                        status = False
                        for pozic in pozicioj:
                            tasko = None
                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ProjektojTaskoStatuso.objects.get(id=statusoj[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoStatuso.DoesNotExist:
                                        message = _('Неверный статус задач')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')
                                    break
                            else:
                                break

                            objekto = None
                            if ('objekto_uuid' in kwargs) and (kwargs.get('objekto_uuid')[i]):
                                try:
                                    objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid')[i], forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = _('Неверный объект')

                            if not message:
                                tasko = ProjektojTasko.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    realeco = realeco,
                                    projekto = projekto,
                                    objekto = objekto,
                                    tipo = tipo,
                                    statuso = statuso,
                                    publikigo = True,
                                    publikiga_dato = timezone.now(),
                                    kom_koordinato_x = kwargs.get('kom_koordinato_x', [])[i] if len(kwargs.get('kom_koordinato_x', []))>i else None,
                                    kom_koordinato_y = kwargs.get('kom_koordinato_y', [])[i] if len(kwargs.get('kom_koordinato_y', []))>i else None,
                                    kom_koordinato_z = kwargs.get('kom_koordinato_z', [])[i] if len(kwargs.get('kom_koordinato_z', []))>i else None,
                                    fin_koordinato_x = kwargs.get('fin_koordinato_x', None)[i] if len(kwargs.get('fin_koordinato_x', []))>i else None,
                                    fin_koordinato_y = kwargs.get('fin_koordinato_y', None)[i] if len(kwargs.get('fin_koordinato_y', []))>i else None,
                                    fin_koordinato_z = kwargs.get('fin_koordinato_z', None)[i] if len(kwargs.get('fin_koordinato_z', []))>i else None,
                                    pozicio = pozic,
                                )

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(tasko.nomo, kwargs.get('nomo')[i], info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(tasko.priskribo, 
                                            kwargs.get('priskribo')[i], 
                                            info.context.LANGUAGE_CODE)
                                # категории брать из параметров
                                if ('kategorio' in kwargs) and (kwargs.get('kategorio')[i]):
                                    kategorio = ProjektojTaskoKategorio.objects.none()

                                    kategorio_id = set(kwargs.get('kategorio')[i])
                                    if len(kategorio_id):
                                        kategorio = ProjektojTaskoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)

                                    if kategorio:
                                        tasko.kategorio.set(kategorio)
                                    else:
                                        tasko.kategorio.clear()

                                taskoj_save(tasko)
                                taskoj.append(tasko)

                                if not message:
                                    i = 0
                                    fermo_projekto_link = None # по окончании цикла закрыть указанный проект
                                    for posedanto_tipo in tasko_posedanto_tipo:

                                        tasko_posedantoj = ProjektojTaskoPosedanto.objects.create(
                                            forigo = False,
                                            arkivo = False,
                                            realeco = realeco,
                                            tasko = tasko,
                                            tipo = posedanto_tipo,
                                            statuso = posedanto_statuso[i],
                                            posedanto_objekto = posedanto_objekto[i],
                                            publikigo = True,
                                            publikiga_dato = timezone.now()
                                        )

                                        tasko_posedantoj.save()
                                    if message:
                                        message = fermo_projekto_tasko(tasko)
                                        fermo_projekto_link = None
                                        status = False
                                    elif fermo_projekto_link:
                                        # закрываем проект
                                        message = fermo_projekto(fermo_projekto_link)
                                if not message:
                                    status = True
                            i += 1
                            if message:
                                break

                        if status and not message:
                            message_krei = '{}, {}'.format(message_krei,_('Запись задачи создана'))
                elif (not projekto) and (not message):
                    message = _('Проект не создан')
                elif not (uzanto.has_perm('projektoj.povas_krei_taskoj') or posedanto_perm) and (not message):
                    message = _('Недостаточно прав')
                    if projekto:
                        # закрываем проект
                        message = fermo_projekto(projekto)
                if not message:
                    message = message_krei
                elif message_krei:
                    message = "{}, и ошибка - {}".format(message_krei, message)
        else:
            message = _('Требуется авторизация')

        return RedaktuKreiProjektojProjektoTaskojPosedanto(
            status=status, 
            message=message, 
            projekto=projekto, 
            taskoj=taskoj
        )


# Модель типов связей проектов между собой
class RedaktuProjektojTaskoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_ligilo_tipoj = graphene.Field(ProjektojTaskoLigiloTipoNode, 
        description=_('Созданный/изменённый тип связей проектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_ligilo_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_tasko_ligilo_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            tasko_ligilo_tipoj = ProjektojTaskoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(tasko_ligilo_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(tasko_ligilo_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            tasko_ligilo_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_ligilo_tipoj = ProjektojTaskoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_tasko_ligilo_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_tasko_ligilo_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                tasko_ligilo_tipoj.forigo = kwargs.get('forigo', tasko_ligilo_tipoj.forigo)
                                tasko_ligilo_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_ligilo_tipoj.arkivo = kwargs.get('arkivo', tasko_ligilo_tipoj.arkivo)
                                tasko_ligilo_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_ligilo_tipoj.publikigo = kwargs.get('publikigo', tasko_ligilo_tipoj.publikigo)
                                tasko_ligilo_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_ligilo_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(tasko_ligilo_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(tasko_ligilo_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                tasko_ligilo_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoLigiloTipo(status=status, message=message, tasko_ligilo_tipoj=tasko_ligilo_tipoj)


# Модель связей проектов между собой
class RedaktuProjektojTaskoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    tasko_ligilo = graphene.Field(ProjektojTaskoLigiloNode, 
        description=_('Создание/изменение связи проектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        posedanto_id = graphene.Int(description=_('ID проекта владельца связи'))
        ligilo_id = graphene.Int(description=_('ID связываемого проекта'))
        tipo_id = graphene.Int(description=_('ID типа связи проектов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tasko_ligilo = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('projektoj.povas_krei_tasko_ligilo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = ProjektojTasko.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTasko.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная задача'),'posedanto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = ProjektojTasko.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTasko.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная задача'),'ligilo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ProjektojTaskoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ProjektojTaskoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип связи задач между собой'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            tasko_ligilo = ProjektojTaskoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            tasko_ligilo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tasko_ligilo = ProjektojTaskoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('projektoj.povas_forigi_tasko_ligilo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('projektoj.povas_shanghi_tasko_ligilo'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = ProjektojTasko.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTasko.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная задача'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = ProjektojTasko.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTasko.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная задача'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ProjektojTaskoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ProjektojTaskoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип связи задач между собой'),'tipo_id')

                            if not message:

                                tasko_ligilo.forigo = kwargs.get('forigo', tasko_ligilo.forigo)
                                tasko_ligilo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tasko_ligilo.arkivo = kwargs.get('arkivo', tasko_ligilo.arkivo)
                                tasko_ligilo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tasko_ligilo.publikigo = kwargs.get('publikigo', tasko_ligilo.publikigo)
                                tasko_ligilo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                tasko_ligilo.posedanto = posedanto if kwargs.get('posedanto_id', False) else tasko_ligilo.posedanto
                                tasko_ligilo.ligilo = ligilo if kwargs.get('ligilo_id', False) else tasko_ligilo.ligilo
                                tasko_ligilo.tipo = tipo if kwargs.get('tipo_id', False) else tasko_ligilo.tipo

                                tasko_ligilo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ProjektojTaskoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuProjektojTaskoLigilo(status=status, message=message, tasko_ligilo=tasko_ligilo)


class ProjektojMutations(graphene.ObjectType):
    redaktu_projektoj_projekto_kategorio = RedaktuProjektojProjektoKategorio.Field(
        description=_('''Создаёт или редактирует категории проектов''')
    )
    redaktu_projektoj_projekto_tipo = RedaktuProjektojProjektoTipo.Field(
        description=_('''Создаёт или редактирует типы проектов''')
    )
    redaktu_projektoj_projekto_statuso = RedaktuProjektojProjektoStatuso.Field(
        description=_('''Создаёт или редактирует статусы проектов''')
    )
    redaktu_projektoj_projekto = RedaktuProjektojProjekto.Field(
        description=_('''Создаёт или редактирует проекты''')
    )
    redaktu_projektoj_projekto_posedantoj_tipo = RedaktuProjektojProjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев проектов''')
    )
    redaktu_projektoj_projekto_posedantoj_statuso = RedaktuProjektojProjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев проектов''')
    )
    redaktu_projektoj_projekto_posedantoj = RedaktuProjektojProjektoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев проекты''')
    )
    redaktu_projektoj_projekto_ligilo_tipo = RedaktuProjektojProjektoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей проектов между собой''')
    )
    redaktu_projektoj_projekto_ligilo = RedaktuProjektojProjektoLigilo.Field(
        description=_('''Создаёт или редактирует связи проектов между собой''')
    )
    redaktu_projektoj_taskoj_kategorio = RedaktuProjektojTaskoKategorio.Field(
        description=_('''Создаёт или редактирует категории задач''')
    )
    redaktu_projektoj_taskoj_tipo = RedaktuProjektojTaskoTipo.Field(
        description=_('''Создаёт или редактирует типы задач''')
    )
    redaktu_projektoj_tasko_statuso = RedaktuProjektojTaskoStatuso.Field(
        description=_('''Создаёт или редактирует статусы задач''')
    )
    redaktu_projektoj_taskoj = RedaktuProjektojTasko.Field(
        description=_('''Создаёт или редактирует задачи''')
    )
    redaktu_projektoj_taskoj_posedantoj_tipo = RedaktuProjektojTaskoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев задач''')
    )
    redaktu_projektoj_tasko_posedantoj_statuso = RedaktuProjektojTaskoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельцев задач''')
    )
    redaktu_projektoj_tasko_posedantoj = RedaktuProjektojTaskoPosedanto.Field(
        description=_('''Создаёт или редактирует владельцев задачи''')
    )
    redaktu_krei_projektoj_taskoj_posedanto = RedaktuKreiProjektojTaskojPosedanto.Field(
        description=_('''Создаёт список задач с владельцами''')
    )
    redaktu_krei_projektoj_projekto_taskoj_posedanto = RedaktuKreiProjektojProjektoTaskojPosedanto.Field(
        description=_('''Создание проекта с владельцем и списка задач с владельцами''')
    )
    redaktu_projektoj_tasko_ligilo_tipo = RedaktuProjektojTaskoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей задач между собой''')
    )
    redaktu_projektoj_tasko_ligilo = RedaktuProjektojTaskoLigilo.Field(
        description=_('''Создаёт или редактирует связи задач между собой''')
    )

