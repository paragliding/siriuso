"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from muroj.models import *


# Стены пользователей
@admin.register(MurojUzantoMuro)
class MurojUzantoMuroAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MurojUzantoMuro._meta.fields]

    class Meta:
        model = MurojUzantoMuro

# Таблица текстов записей на стенах пользователей
# Изображения записей на стенах пользователей
class MurojUzantoEnskriboBildoInline(admin.TabularInline):
    model = MurojUzantoEnskriboBildo
    extra = 1


# Записи пользователей
@admin.register(MurojUzantoEnskribo)
class MurojUzantoEnskriboAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MurojUzantoEnskribo._meta.fields]
    exclude = ('id', 'teksto',)
    inlines = [MurojUzantoEnskriboBildoInline,]

    class Meta:
        model = MurojUzantoEnskribo

# Лайки записей (интересно) пользователей
@admin.register(MurojUzantoEnskriboInterese)
class MurojUzantoEnskriboIntereseAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MurojUzantoEnskriboInterese._meta.fields]

    class Meta:
        model = MurojUzantoEnskriboInterese


# Таблица текстов комментариев записей пользователей
# Комментарии записей пользователей
@admin.register(MurojUzantoEnskriboKomento)
class MurojUzantoEnskriboKomentoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MurojUzantoEnskriboKomento._meta.fields]
    exclude = ('teksto',)

    class Meta:
        model = MurojUzantoEnskriboKomento


# Стены общих моделей сообществ
@admin.register(Muro)
class MuroAdmin(admin.ModelAdmin):
    list_display = ('krea_dato', 'posedanto', 'uuid',)

    class Meta:
        model = Muro


# Изображения записей на стенах общих моделей сообществ
class MuroEnskriboBildoInline(admin.TabularInline):
    model = MuroEnskriboBildo
    extra = 1


# Записи общих моделей сообществ
@admin.register(MuroEnskribo)
class MuroEnskriboAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MuroEnskribo._meta.fields]
    exclude = ('id', 'teksto',)
    inlines = [MuroEnskriboBildoInline,]

    class Meta:
        model = MuroEnskribo


# Лайки записей (интересно) общих моделей сообществ
@admin.register(MuroEnskriboInterese)
class MuroEnskriboIntereseAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MuroEnskriboInterese._meta.fields]

    class Meta:
        model = MuroEnskriboInterese


# Комментарии записей общих моделей сообществ
@admin.register(MuroEnskriboKomento)
class MuroEnskriboKomentoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in MuroEnskriboKomento._meta.fields]
    exclude = ('teksto',)

    class Meta:
        model = MuroEnskriboKomento
