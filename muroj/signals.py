"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils import timezone
from komunumoj.models import Komunumo, KomunumojAliro
from .models import Muro


@receiver(post_save, sender=Komunumo)
def create_muro(sender, instance, **kwargs):
    """Создаём стену для сообщества, если её нет"""
    try:
        Muro.objects.get(posedanto=instance, forigo=False)
    except Muro.DoesNotExist:
        aliro = KomunumojAliro.objects.get(kodo='chiuj')

        Muro.objects.create(
            posedanto=instance,
            aliro=aliro,
            komentado_aliro=aliro,
            publikigo=True,
            publikiga_dato=timezone.now(),
            arkivo=False,
            forigo=False,
        )
