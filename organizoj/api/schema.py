"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения
from dokumentoj.models import DokumentoEkspedo


statusoj_id = None
# per = 0

# Модель типов организаций в Универсо
class OrganizoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = OrganizoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель организаций Универсо
class OrganizoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    tuta_dokumento_statusoj_id = graphene.Int(description=_('Общее количество документов организации'))
    tuta_dokumento_all = graphene.Int(description=_('Общее количество документов организации'))
    tuta_dokumento_forigi = graphene.Int(description=_('Количество закрытых (статус = 8, 11 или 14) документов организации'))
    tuta_dokumento_aktivo = graphene.Int(description=_('Количество документов организации, находящихся в работе (статус = 9 или 13) '))
    
    class Meta:
        model = Organizo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'dokumentoj_dokumentoekspedo_kliento__uuid':['exact'],
            'dokumentoj_dokumentoekspedo_kliento__dokumento__taskoj_taskojprojektoposedanto_posedanto_dokumento__projekto__statuso__id':['exact', 'in'],
            'organizomembro__uzanto__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_tuta_dokumento_statusoj_id(self, info, **kwargs):
        global statusoj_id
        if statusoj_id:
            statusoj_id = list(statusoj_id.split(sep=None, maxsplit=-1))
            pass
        else:
            statusoj_id = [8, 11, 14]
        # считаем количество по DokumentoPosedanto, т.к. у каждого документа одна организация является владельцем один раз
        return DokumentoEkspedo.objects.filter(
            dokumento__universo_taskoj_taskojprojektoposedanto_posedanto_dokumento__projekto__statuso__id__in=statusoj_id,
            kliento=self, forigo=False, arkivo=False, publikigo=True
                                                       ).count()

    def resolve_tuta_dokumento_all(self, info, **kwargs):
        # считаем количество по DokumentoPosedanto, т.к. у каждого документа одна организация является владельцем один раз
        return DokumentoEkspedo.objects.filter(kliento=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_tuta_dokumento_forigi(self, info, **kwargs):
        # считаем количество по DokumentoPosedanto, т.к. у каждого документа одна организация является владельцем один раз
        return DokumentoEkspedo.objects.filter(
            dokumento__universo_taskoj_taskojprojektoposedanto_posedanto_dokumento__projekto__statuso__id__in=[8, 11, 14],
            kliento=self, forigo=False, arkivo=False, publikigo=True
                                                       ).count()

    def resolve_tuta_dokumento_aktivo(self, info, **kwargs):
        # считаем количество по DokumentoPosedanto, т.к. у каждого документа одна организация является владельцем один раз
        return DokumentoEkspedo.objects.filter(
            dokumento__universo_taskoj_taskojprojektoposedanto_posedanto_dokumento__projekto__statuso__id__in=[9, 13],
            kliento=self, forigo=False, arkivo=False, publikigo=True
                                                       ).count()


# Модель членства в организациях Универсо
class OrganizoMembroNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = OrganizoMembro
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'uzanto__id': ['exact'],
            'uzanto__uuid': ['exact'],
            'organizo__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class OrganizojQuery(graphene.ObjectType):
    organizo_tipo = SiriusoFilterConnectionField(
        OrganizoTipoNode,
        description=_('Выводит все доступные модели типов организаций')
    )
    organizo = SiriusoFilterConnectionField(
        OrganizoNode,
        description=_('Выводит все доступные модели организаций'),
        statusoj_id=graphene.String()
    )
    organizo_membro = SiriusoFilterConnectionField(
        OrganizoMembroNode,
        description=_('Выводит все доступные модели членства в организациях')
    )

    @staticmethod
    def resolve_organizo(root, info, **kwargs):
        global statusoj_id
        if 'statusoj_id' in kwargs:
            print('=== statusoj_id 0 === ',kwargs)
            statusoj_id = kwargs.get('statusoj_id')
        else:
            statusoj_id = None
        return Organizo.objects.all()

