"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующей транзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
import requests
from django.conf import settings

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель типов организаций
class RedaktuOrganizoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizoj_tipoj = graphene.Field(OrganizoTipoNode, 
        description=_('Созданный/изменённый тип организации'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        organizoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('organizoj.povas_krei_organizoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну реальность'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            organizoj_tipoj = OrganizoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(organizoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(organizoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    organizoj_tipoj.realeco.set(realeco)
                                else:
                                    organizoj_tipoj.realeco.clear()

                            organizoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            organizoj_tipoj = OrganizoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('organizoj.povas_forigi_organizoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('organizoj.povas_shanghi_organizoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                organizoj_tipoj.forigo = kwargs.get('forigo', organizoj_tipoj.forigo)
                                organizoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                organizoj_tipoj.arkivo = kwargs.get('arkivo', organizoj_tipoj.arkivo)
                                organizoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                organizoj_tipoj.publikigo = kwargs.get('publikigo', organizoj_tipoj.publikigo)
                                organizoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                organizoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(organizoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(organizoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        organizoj_tipoj.realeco.set(realeco)
                                    else:
                                        organizoj_tipoj.realeco.clear()

                                organizoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except OrganizoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuOrganizoTipo(status=status, message=message, organizoj_tipoj=organizoj_tipoj)


# Модель организаций
class RedaktuOrganizo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizo = graphene.Field(OrganizoNode, 
        description=_('Созданная/изменённая организация'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        tipo_id = graphene.Int(description=_('Код типа ресурсов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        organizo = None
        realeco = Realeco.objects.none()
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('organizo.povas_krei_organizo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = OrganizoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except OrganizoTipo.DoesNotExist:
                                    message = _('Неверный тип организации')
                            else:
                                message = _('Не указан тип организации')

                        if not message:
                            organizo = Organizo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(organizo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(organizo.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    organizo.realeco.set(realeco)
                                else:
                                    organizo.realeco.clear()

                            organizo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            organizo = Organizo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('organizo.povas_forigi_organizo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('organizo.povas_shanghi_organizo'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = OrganizoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except OrganizoTipo.DoesNotExist:
                                        message = _('Неверный тип ресурсов')
                            if not message:

                                organizo.forigo = kwargs.get('forigo', organizo.forigo)
                                organizo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                organizo.arkivo = kwargs.get('arkivo', organizo.arkivo)
                                organizo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                organizo.publikigo = kwargs.get('publikigo', organizo.publikigo)
                                organizo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                organizo.autoro = uzanto
                                organizo.tipo = tipo if kwargs.get('tipo_id', False) else organizo.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(organizo.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(organizo.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        organizo.realeco.set(realeco)
                                    else:
                                        organizo.realeco.clear()

                                organizo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Organizo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuOrganizo(status=status, message=message, organizo=organizo)


# Модель членства в организация
class RedaktuOrganizoMembro(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizoj_membroj = graphene.Field(OrganizoMembroNode, 
        description=_('Созданная/изменённая членства в организациях'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        organizo_uuid = graphene.String(description=_('Организация'))
        uzanto_id = graphene.Int(description=_('Код пользователя, член организации'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        organizoj_membroj = None
        organizo = None
        uzanto_ = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('organizoj.povas_krei_organizoj_membroj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'uzanto_id' in kwargs:
                                try:
                                    uzanto_ = Uzanto.objects.get(id=kwargs.get('uzanto_id'), 
                                                                 konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')
                            else:
                                message = _('Не указан пользователь')

                        if not message:
                            if 'organizo_uuid' in kwargs:
                                try:
                                    organizo = Organizo.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                except Organizo.DoesNotExist:
                                    message = _('Неверная организация')
                            else:
                                message = _('Не указан пользователь')

                        if not message:
                            organizoj_membroj = OrganizoMembro.objects.create(
                                forigo=False,
                                arkivo=False,
                                uzanto = uzanto_,
                                organizo = organizo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            organizoj_membroj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('uzanto_id', False) or kwargs.get('organizo_uuid', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            organizoj_membroj = OrganizoMembro.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('organizoj.povas_forigi_organizoj_membroj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('organizoj.povas_shanghi_organizoj_membroj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'uzanto_id' in kwargs:
                                    try:
                                        uzanto_ = Uzanto.objects.get(id=kwargs.get('uzanto_id'), 
                                                                     konfirmita=True)
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')
                            if not message:
                                if 'organizo_uuid' in kwargs:
                                    try:
                                        organizo = Organizo.objects.get(uuid=kwargs.get('organizo_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                    except Organizo.DoesNotExist:
                                        message = _('Неверная организация')
                            if not message:

                                organizoj_membroj.forigo = kwargs.get('forigo', organizoj_membroj.forigo)
                                organizoj_membroj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                organizoj_membroj.arkivo = kwargs.get('arkivo', organizoj_membroj.arkivo)
                                organizoj_membroj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                organizoj_membroj.publikigo = kwargs.get('publikigo', organizoj_membroj.publikigo)
                                organizoj_membroj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                organizoj_membroj.uzanto = uzanto_ if kwargs.get('uzanto_id', False) else organizoj_membroj.uzanto
                                organizoj_membroj.organizo = organizo if kwargs.get('organizo_uuid', False) else organizoj_membroj.organizo

                                organizoj_membroj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except OrganizoMembro.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuOrganizoMembro(status=status, message=message, organizoj_membroj=organizoj_membroj)


# Импорт организаций из 1с
class ImportoOrganizo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizo = graphene.Field(OrganizoNode, 
        description=_('Созданная/изменённая организация'))

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))
        tipo_id = graphene.Int(description=_('Код типа организации'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        organizo = None
        realeco = Realeco.objects.none()
        tipo = None
        uzanto = info.context.user

        url = settings.ODATA_URL+'Catalog_Организации?$inlinecount=allpages&$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD
        
        if uzanto.is_authenticated:
            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                i = 0
                kvanto = 0
                jsj = r.json()
                for js in jsj['value']:
                    # print('===js=== ', js)
                    if js['НаименованиеПолное']:
                        try:
                            organizo = Organizo.objects.get(uuid=js['Ref_Key'], forigo=False)
                            # Проверяем и изменяем найденную
                            pass
                        except Organizo.DoesNotExist:
                            # Добавляем новую организацию
                            if not message:
                                realeco_id = [1,]
                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )
                                        break

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = OrganizoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                    except OrganizoTipo.DoesNotExist:
                                        message = _('Неверный тип организации')
                                else:
                                    message = _('Не указан тип организации')

                            if not message:
                                organizo = Organizo.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    autoro = uzanto,
                                    tipo = tipo,
                                    uuid = js['Ref_Key'],
                                    inn = js['ИНН'],
                                    kpp = js['КПП'],
                                    kodo = js['Code'],
                                    okpo = js['КодПоОКПО'],

                                    # imposta_numero = js['НалоговыйНомер'],
                                    registronumero = js['РегистрационныйНомерПФР'],
                                    # email = js['EmailУведомлений'],
                                    akto_serio_numero = js['СвидетельствоСерияНомер'],
                                    akto_dato_eligo = js['СвидетельствоДатаВыдачи'],
                                    dato_registrado = js['ДатаРегистрации'],
                                    # dato_faro = js['ДатаСоздания'],
                                    publikigo=True,
                                    publikiga_dato=timezone.now()
                                )

                                set_enhavo(organizo.nomo, js['НаименованиеПолное'], info.context.LANGUAGE_CODE)
                                set_enhavo(organizo.priskribo, js['НаименованиеСокращенное'], info.context.LANGUAGE_CODE)
                                if realeco:
                                    organizo.realeco.set(realeco)

                                organizo.save()
                                kvanto += 1

                    i += 1
                if not message:
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')

        else:
            message = _('Требуется авторизация')

        return ImportoOrganizo1c(status=status, message=message, organizo=organizo)


# Импорт контрагентов из 1с
class ImportoKombatanto1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    organizo = graphene.Field(OrganizoNode, 
        description=_('Созданная/изменённая организация'))

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))
        tipo_id = graphene.Int(description=_('Код типа организации'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        organizo = None
        realeco = Realeco.objects.none()
        tipo = None
        uzanto = info.context.user

        url = settings.ODATA_URL+'Catalog_Контрагенты?$inlinecount=allpages&$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD
        
        if uzanto.is_authenticated:
            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                i = 0
                kvanto = 0
                jsj = r.json()
                for js in jsj['value']:
                    # print('===js=== ', js)
                    if js['НаименованиеПолное']:
                        try:
                            organizo = Organizo.objects.get(uuid=js['Ref_Key'], forigo=False)
                            # Проверяем и изменяем найденную
                            pass
                        except Organizo.DoesNotExist:
                            # Добавляем новую организацию
                            if not message:
                                realeco_id = [1,]
                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )
                                        break

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = OrganizoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                    except OrganizoTipo.DoesNotExist:
                                        message = _('Неверный тип организации')
                                else:
                                    message = _('Не указан тип организации')

                            if not message:
                                organizo = Organizo.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    autoro = uzanto,
                                    tipo = tipo,
                                    uuid = js['Ref_Key'],
                                    inn = js['ИНН'],
                                    kpp = js['КПП'],
                                    kodo = js['Code'],
                                    okpo = js['КодПоОКПО'],

                                    imposta_numero = js['НалоговыйНомер'],
                                    registronumero = js['РегистрационныйНомер'],
                                    email = js['EmailУведомлений'],
                                    akto_serio_numero = js['СвидетельствоСерияНомер'],
                                    akto_dato_eligo = js['СвидетельствоДатаВыдачи'],
                                    dato_registrado = js['ДатаРегистрации'],
                                    dato_faro = js['ДатаСоздания'],
                                    publikigo=True,
                                    publikiga_dato=timezone.now()
                                )

                                set_enhavo(organizo.nomo, js['НаименованиеПолное'], info.context.LANGUAGE_CODE)
                                set_enhavo(organizo.priskribo, js['Description'], info.context.LANGUAGE_CODE)
                                if realeco:
                                    organizo.realeco.set(realeco)

                                organizo.save()
                                kvanto += 1

                    i += 1
                if not message:
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')

        else:
            message = _('Требуется авторизация')

        return ImportoKombatanto1c(status=status, message=message, organizo=organizo)


class OrganizoMutations(graphene.ObjectType):
    redaktu_organizo_tipo = RedaktuOrganizoTipo.Field(
        description=_('''Создаёт или редактирует типы организаций''')
    )
    redaktu_organizo = RedaktuOrganizo.Field(
        description=_('''Создаёт или редактирует организацию''')
    )
    redaktu_organizo_membro = RedaktuOrganizoMembro.Field(
        description=_('''Создаёт или редактирует членства в организациях''')
    )
    importo_organizo1c = ImportoOrganizo1c.Field(
        description=_('''Импорт организаций из 1с''')
    )
    importo_kombatanto1c = ImportoKombatanto1c.Field(
        description=_('''Импорт организаций из 1с''')
    )
