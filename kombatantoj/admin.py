"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django import forms
from siriuso.utils import get_enhavo
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
import json

from .models import *


class KombatantoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = KombatantoTipo
        fields = [field.name for field in KombatantoTipo._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out



@admin.register(KombatantoTipo)
class KombatantoTipoAdmin(admin.ModelAdmin):
    form = KombatantoTipoFormo
    list_display = ('nomo_enhavo', 'kodo',)

    class Meta:
        model = KombatantoTipo

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


class KombatantoSpecoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = KombatantoSpeco
        fields = [field.name for field in KombatantoTipo._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out



@admin.register(KombatantoSpeco)
class KombatantoSpecoAdmin(admin.ModelAdmin):
    form = KombatantoSpecoFormo
    list_display = ('nomo_enhavo', 'kodo',)

    class Meta:
        model = KombatantoSpeco

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')



class KombatantoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)
    unua_nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Unua nomo'), required=False)
    dua_nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Dua nomo'), required=False)
    familinomo = forms.CharField(widget=LingvoInputWidget(), label=_('Familinomo'), required=False)
    informoj = forms.CharField(widget=LingvoTextWidget(), label=_('Pliaj informoj'), required=False)

    class Meta:
        model = Kombatanto
        fields = [field.name for field in Kombatanto._meta.fields if field.name not in ('uuid', 'hid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_unua_nomo(self):
        out = self.cleaned_data['unua_nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_dua_nomo(self):
        out = self.cleaned_data['dua_nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_familinomo(self):
        out = self.cleaned_data['familinomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_informoj(self):
        out = self.cleaned_data['informoj']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


class KombatantoKategorioFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = KombatantoKategorio
        fields = [field.name for field in KombatantoKategorio._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(KombatantoKategorio)
class KombatantoKategorioAdmin(admin.ModelAdmin):
    form = KombatantoTipoFormo
    list_display = ('nomo_enhavo', 'krea_dato')

    class Meta:
        model = KombatantoKategorio

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


@admin.register(Kombatanto)
class KombatantoAdmin(admin.ModelAdmin):
    form = KombatantoFormo
    list_display = ('nomo_enhavo', 'tipo', 'speco', 'publikigo', 'forigo', 'krea_dato')
    list_filter = ('forigo', 'publikigo', 'tipo', 'speco')
    search_fields = ('nomo', 'unua_nomo', 'familinomo',)

    class Meta:
        model = Kombatanto

    def nomo_enhavo(self, obj):
        posedanto_nomo = get_enhavo(obj.posedanto.nomo, empty_values=True)[0] if obj.posedanto else None
        posedanto_nomo = (
            '{} - {}'.format(get_enhavo(obj.posedanto.posedanto.nomo, empty_values=True)[0], posedanto_nomo)
        ) if obj.posedanto and obj.posedanto.posedanto else posedanto_nomo

        nomo = get_enhavo(obj.nomo, empty_values=True)[0] or '{} {}'.format(
            get_enhavo(obj.unua_nomo, empty_values=True)[0],
            get_enhavo(obj.familinomo, empty_values=True)[0],
        )

        nomo = (
            '{} ({})'.format(nomo, posedanto_nomo)
            if obj.posedanto else nomo
        )
        return nomo
    nomo_enhavo.short_description = _('Nomo')


class KombatantoRoloFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = KombatantoRolo
        fields = [field.name for field in KombatantoRolo._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


class KombatantoRoloRajtojInline(admin.TabularInline):
    model = KombatantoRoloRajto
    fk = 'posedanto'
    extra = 0
    filter_horizontal = ('rajtoj',)


class KombatantoRoloDokRajtojInline(admin.TabularInline):
    model = KombatantoRoloDokumentoRajto
    fk = 'posedanto'
    extra = 0
    filter_horizontal = ('rajtoj',)


@admin.register(KombatantoRolo)
class KombatantoRolo(admin.ModelAdmin):
    form = KombatantoRoloFormo
    list_display = ('posedanto', 'nomo_enhavo',)
    inlines = (KombatantoRoloRajtojInline, KombatantoRoloDokRajtojInline)

    def nomo_enhavo(self, obj):
        posedanto_nomo = get_enhavo(obj.posedanto.nomo, empty_values=True)[0] if obj.posedanto else None
        posedanto_nomo = (
            '{} - {}'.format(get_enhavo(obj.posedanto.posedanto.nomo, empty_values=True)[0], posedanto_nomo)
        ) if obj.posedanto and obj.posedanto.posedanto else posedanto_nomo

        nomo = get_enhavo(obj.nomo, empty_values=True)[0] or '{} {}'.format(
            get_enhavo(obj.unua_nomo, empty_values=True)[0],
            get_enhavo(obj.familinomo, empty_values=True)[0],
        )

        nomo = (
            '{} ({})'.format(nomo, posedanto_nomo)
            if obj.posedanto else nomo
        )
        return nomo
    nomo_enhavo.short_description = _('Nomo')


class KontaktoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Название'), required=True)

    class Meta:
        model = KontaktoTipo
        fields = [field.name for field in KombatantoTipo._meta.fields if field.name not in ('uuid', 'krea_dato')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


@admin.register(KontaktoTipo)
class KontaktoTipoAdmin(admin.ModelAdmin):
    form = KontaktoTipoFormo
    list_display = ('nomo_enhavo', 'kodo',)

    class Meta:
        model = KontaktoTipo

    def nomo_enhavo(self, obj):
        return get_enhavo(obj.nomo, empty_values=True)[0]
    nomo_enhavo.short_description = _('Наименование')


class KontaktoFormo(forms.ModelForm):

    class Meta:
        model = Kontakto
        fields = [field.name for field in Kontakto._meta.fields if field.name not in ('uuid', 'krea_dato')]


@admin.register(Kontakto)
class KontaktoAdmin(admin.ModelAdmin):
    form = KontaktoFormo
    list_display = ('uuid', 'kombatanto', 'valoro')

    class Meta:
        model = Kontakto

