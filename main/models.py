"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from uuid import uuid4
from django.db import models
from django.urls import reverse
# from django.core.cache import cache
from django.contrib.auth.models import Group, Permission
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.validators import MinValueValidator, MaxValueValidator
from informiloj.models import InformilojLando, InformilojRegiono, InformilojLoko, InformilojLingvo, \
    InformilojFamiliaStato
from django.utils.timezone import now
from siriuso.utils import default_lingvo, get_enhavo
from siriuso.utils.perms import has_registrita_perm
from siriuso.models.abstract import SiriusoAutomataKreinto
from siriuso.models.postgres import CallableEncoder
import datetime
import secrets


# Менеджер пользователя, определяются действия с пользователями
class MyUserManager(BaseUserManager):
    def create_user(self, chefa_retposhto, password=None, chefa_telefonanumero=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not chefa_retposhto:
            raise ValueError('Users must have an email address')

        user = self.model(
            chefa_retposhto=self.normalize_email(chefa_retposhto),
            chefa_telefonanumero=chefa_telefonanumero,
            is_active=True,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, chefa_retposhto, password, chefa_telefonanumero=None):
        user = self.create_user(chefa_retposhto=chefa_retposhto, chefa_telefonanumero=chefa_telefonanumero,
                                password=password)
        user.is_admin = True
        user.konfirmita = True
        user.save(using=self._db)
        return user


def maks_naskighdato_validator(value):
    return MaxValueValidator(datetime.date.today())(value)


def uzaj_agordoj():
    """
    Возвращает словарь с настройками
    пользователя по умолчанию
    Можно улучшить и предусмотреть
    больше параметров по умолчанию
    """
    return {'abono_shlosilo': secrets.token_urlsafe(24)}


# Модель пользователей, использует джанговский абстрактный класс AbstractBaseUser
class Uzanto(AbstractBaseUser, PermissionsMixin, SiriusoAutomataKreinto):
    sekso_choices = (
        ('vira', _('Vira')),
        ('virina', _('Virina'))
    )

    # ID записи
    id = models.AutoField(_('ID'), primary_key=True, editable=False)

    # UUID записи, ВНИМАНИЕ!!! у пользователей НЕ примари кей, у пользователей примарикей по ID
    uuid = models.UUIDField(_('UUID'), unique=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=True, null=True)

    # тип пользователя
    tipo = models.ForeignKey('UzantoTipo', verbose_name=_('Tipo'), blank=True, null=True,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # если это дочерняя учётная запись, то ссылка на материнскую запись (рекурсивная связь)
    posedanto = models.ForeignKey('self', verbose_name=_('Posedanto'), blank=True, null=True,
                                  related_name='%(app_label)s_%(class)s_posedanto',
                                  on_delete=models.SET_NULL)

    # имя
    unua_nomo = models.JSONField(verbose_name=_('Unua nomo'), null=True, blank=True, default=default_lingvo,
                                   encoder=CallableEncoder)

    # второе имя (отчество)
    dua_nomo = models.JSONField(verbose_name=_('Dua nomo'), null=True, blank=True, default=default_lingvo,
                                  encoder=CallableEncoder)

    # фамилия
    familinomo = models.JSONField(verbose_name=_('Familinomo'), null=True, blank=True, default=default_lingvo,
                                    encoder=CallableEncoder)

    # пол
    sekso = models.CharField(_('Sekso'), max_length=6, choices=sekso_choices, blank=True, null=True)

    # главная электронная почта, так же будут дополнительные в таблице контактов
    chefa_retposhto = models.EmailField(_('Retpoŝto'), unique=True)

    # пароль пользователя
    password = models.CharField(_('Pasvorto'), max_length=500)

    # главный телефонный номер, так же будут дополнительные в таблице контактов
    chefa_telefonanumero = models.CharField(
        _('Telefonanumero'), max_length=18, blank=True, null=True, db_index=True
    )

    # Главный язык пользователя
    chefa_lingvo = models.ForeignKey(InformilojLingvo, verbose_name=_('Lingvo'), null=True,
                                     related_name='%(app_label)s_%(class)s_chefa_lingvo',
                                     on_delete=models.CASCADE)
    # дата рождения пользователя
    naskighdato = models.DateField(_('Naskiĝdato'),
                                   blank=True,
                                   null=True,
                                   validators=[
                                       MinValueValidator(datetime.date(1900, 1, 1)),
                                       maks_naskighdato_validator
                                   ])

    # страна рождения
    naskighlando = models.ForeignKey(InformilojLando, verbose_name=_('Naskiĝlando'), blank=True, null=True,
                                     related_name='%(app_label)s_%(class)s_naskighlando',
                                     on_delete=models.CASCADE)
    # регион рождения
    naskighregiono = models.ForeignKey(InformilojRegiono, verbose_name=_('Naskiĝregiono'), blank=True, null=True,
                                       related_name='%(app_label)s_%(class)s_naskighregiono',
                                       on_delete=models.CASCADE)

    # место рождения
    naskighloko = models.ForeignKey(InformilojLoko, verbose_name=_('Naskiĝloko'), blank=True, null=True,
                                    related_name='%(app_label)s_%(class)s_naskighloko',
                                    on_delete=models.CASCADE)

    # страна текущего проживания
    loghlando = models.ForeignKey(InformilojLando, verbose_name=_('Loĝlando'), blank=True, null=True,
                                  related_name='%(app_label)s_%(class)s_loghlando', on_delete=models.CASCADE)

    # регион текущего проживания
    loghregiono = models.ForeignKey(InformilojRegiono, verbose_name=_('Loĝregiono'), blank=True, null=True,
                                    related_name='%(app_label)s_%(class)s_loghregiono', on_delete=models.CASCADE)

    # место текущего проживания
    loghloko = models.ForeignKey(InformilojLoko, verbose_name=_('Loĝloko'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_loghloko', on_delete=models.CASCADE)

    # семейное положение
    marital_status = models.ForeignKey(InformilojFamiliaStato, verbose_name=_('Familia stato'), blank=True, null=True,
                                       related_name='%(app_label)s_%(class)s_marital_status',
                                       on_delete=models.CASCADE)

    # пометка, что пользователь активен (не заблокирован)
    is_active = models.BooleanField(default=False)

    # пометка, что пользователь админ (не суперюзер)
    is_admin = models.BooleanField(default=False)

    # признак подтверждения регистрации пользователя (подтверждение e-mail)
    konfirmita = models.BooleanField(verbose_name=_('Statuso de konfirmo'), default=False)

    # хэш подтверждения регистрации
    konfirmita_hash = models.CharField(max_length=256, blank=True, null=True)

    # признак плохого email
    malbona_retposhto = models.BooleanField(verbose_name=_('Malbona retpoŝta adreso'), default=False)

    # JSONB поле для хранения настроек пользователя
    agordoj = models.JSONField(verbose_name=_('Uzaj agordoj'), null=True, blank=True, default=uzaj_agordoj,
                                 encoder=CallableEncoder)

    # псевдополе для автоматического создания связанных сущностей
    infanaj_modeloj = [
        {
            # Создаем Стену для нового пользователя
            # из какого приложения используем зависимую модель
            'app': 'muroj',
            # имя зависимой модели
            'model': 'MurojUzantoMuro',
            # поле зависимой модели, которое является внешним ключом и указывает на текущую модель
            'fk': 'posedanto',
            # поля зависимой модели, обязательные для заполнения при сохранении
            'fields': {'aliro': ['uzantoj.UzantojAliro', {'kodo': 'chiuj'}],
                       'komentado_aliro': ['uzantoj.UzantojAliro', {'kodo': 'chiuj'}]}
        },
    ]

    USERNAME_FIELD = 'chefa_retposhto'
    #REQUIRED_FIELDS = ['chefa_telefonanumero']
    EMAIL_FIELD = 'chefa_retposhto'
    objects = MyUserManager()

    class Meta:
        db_table = 'uzantoj'
        verbose_name = _('Uzanto')
        verbose_name_plural = _('Uzantoj')
        # права
        permissions = (
            ('povas_vidi_uzanton', _('Povas vidi uzanton')),
            ('povas_krei_uzanton', _('Povas krei uzanton')),
            ('povas_forigi_uzanton', _('Povas forigu uzanton')),
            ('povas_shanghi_uzantan_avataron', _('Povas ŝanĝi uzantan avataron')),
            ('povas_shanghi_uzantan_kovrilon', _('Povas ŝanĝi uzantan kovrilon')),
            ('povas_shanghi_uzantan_nomon', _('Povas ŝanĝi uzantan nomon')),
            ('povas_shanghi_uzantan_statuson', _('Povas ŝanĝi uzantan statuson')),
            ('povas_shanghi_uzantan_realecon', _('Povas ŝanĝi uzantan realecon'))
        )

    # абсолютный путь
    def get_absolute_url(self):
        return reverse('muroj:muro_uzanto', args=[str(self.id)])

    def age(self):
        return datetime.datetime.now()

    def get_short_name(self):
        return self.chefa_retposhto

    def get_full_name(self):
        return self.chefa_retposhto

    def __str__(self):
        # кэширование текстового представления
        # if self.id:
        #    cache_key = 'uzanto_{}'.format(self.id)
        #    out = cache.get(cache_key)
        #
        #    if out is None:
        #        out = '{} {} {}'.format(self.chefa_retposhto, get_enhavo(self.familinomo, empty_values=True)[0],
        #                        get_enhavo(self.unua_nomo, empty_values=True)[0])
        #        cache.set(cache_key, out, timeout=300)
        # else:
        #    out = '{} {} {}'.format(self.chefa_retposhto, get_enhavo(self.familinomo, empty_values=True)[0],
        #                        get_enhavo(self.unua_nomo, empty_values=True)[0])

        out = '{} {} {}'.format(self.chefa_retposhto, get_enhavo(self.familinomo, empty_values=True)[0],
                                get_enhavo(self.unua_nomo, empty_values=True)[0])

        return out

    @property
    def is_staff(self):
        return self.is_admin

    @property
    def is_authenticated(self):
        return self.is_active and self.konfirmita

    #@property
    #def email(self):
    #    return self.chefa_retposhto

    #@property
    #def first_name(self):
    #    return '{}'.format(get_enhavo(self.unua_nomo)[0]) if self.unua_nomo else ''

    #@property
    #def last_name(self):
    #    return '{}'.format(get_enhavo(self.familinomo)[0]) if self.familinomo else ''

    #@property
    #def username(self):
    #    username = '{}_{}_{}'.format(self.unua_nomo, self.familinomo, self.id)
    #    return username.lower()

    def _get_user_permissions(self, user_obj):
        perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if user_obj.is_active and self.id == user_obj.id:
                try:
                    grupoj = SpecialajGrupoj.objects.get(kodo='uzanta_pagho').grupoj.all()

                    for grupo in grupoj:
                        perms |= grupo.permissions.filter(content_type__app_label__in=['main', ])
                except SpecialajGrupoj.DoesNotExist:
                    pass

        return perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.has_perm('main.povas_vidi_uzanton'):
            cond = models.Q()
        else:
            cond = models.Q(uuid__isnull=True)

        return cond


# Модель ключей аутентификации пользователей
class UzantaShlosilo(models.Model):
    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # Пользователь ключа
    uzanto = models.ForeignKey('Uzanto', verbose_name=_('Uzanto'), blank=False, null=False,
                               related_name='%(app_label)s_%(class)s_uzanto',
                               on_delete=models.CASCADE)

    # Дата начала действия
    starta_dato = models.DateTimeField(_('Starta dato'), auto_now_add=True, auto_now=False, blank=False, null=False)

    # Дата окончания действия
    fina_dato = models.DateTimeField(_('Fina dato'), blank=True, null=True)

    # Ключ доступа
    shlosilo = models.CharField(_('Shlosilo'), max_length=128, unique=True)

    # Контекст использования ключа
    kunteksto = models.CharField(_('Kunteksto'), max_length=128, blank=True, null=True)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzanta_slosilo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Uzanta shlosilo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Uzantaj shlosiloj')


# Глобальные шаблоны
class SiriusoWablono(models.Model):
    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дата и время автоудаления
    a_foriga_dato = models.DateTimeField(_('Aŭtomata foriga dato'), auto_now_add=False, auto_now=False, blank=True,
                                         null=True, default=None)

    # опубликовано (да или нет)
    publikigo = models.BooleanField(_('Publikigis'), default=False)

    # дата и время публикации
    publikiga_dato = models.DateTimeField(_('Dato de publikigo'), auto_now_add=False, auto_now=False, blank=True,
                                          null=True, default=None)

    # архивная (да или нет)
    arkivo = models.BooleanField(_('Arkiva'), default=False)

    # дата и время помещения в архив
    arkiva_dato = models.DateTimeField(_('Arkiva dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # ссылка на пользователя последней модификации
    lasta_autoro = models.ForeignKey(Uzanto, verbose_name=_('Modifita de'), blank=True, null=True,
                                     related_name='%(app_label)s_%(class)s_lasta_autoro',
                                     on_delete=models.SET_NULL)

    # дата последнего изменения
    lasta_dato = models.DateTimeField(_('Dato de lasta modifo'), blank=True, null=True)

    # название многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'wablono'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ĝenerala ŝablono')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ĝenerala ŝablonoj')
        # доступ
        permissions = (
            ('povas_vidi_realecan_wablonon', _('Povas vidi realecan wablonon')),
            ('povas_krei_realecan_wablonon', _('Povas krei realecan wablonon')),
            ('povas_forigi_realecan_wablonon', _('Povas forigi realecan wablonon')),
            ('povas_arkivi_realecan_wablonon', _('Povas arkivi realecan wablonon')),
            ('povas_shanghi_realecan_wablonon', _('Povas ŝanĝi realecan wablonon')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])

    def _get_user_permissions(self, user_obj):
        perms = Permission.objects.none()
        return perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated and (user_obj.has_perm('main.povas_vidi_realecan_wablonon')
                                          or has_registrita_perm('main.povas_vidi_realecan_wablonon')):
            cond = models.Q()
        else:
            cond = models.Q(uuid__isnull=True)

        return cond


# Основной абстрактный класс
class SiriusoBazaAbstrakta(models.Model):
    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # к какому глобальному шаблону относится
    wablomo = models.ForeignKey(SiriusoWablono, verbose_name=_('Ĝenerala ŝablono'), blank=True, null=True,
                                related_name='%(app_label)s_%(class)s_wablomo',
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Основной абстрактный класс 2, дополняющий общими полями SiriusoBazaAbstrakta
class SiriusoBazaAbstrakta2(SiriusoBazaAbstrakta):
    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дата и время автоудаления
    a_foriga_dato = models.DateTimeField(_('Aŭtomata foriga dato'), auto_now_add=False, auto_now=False, blank=True,
                                         null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Основной абстрактный класс 3, дополняющий полями для сообществ и т.д. SiriusoBazaAbstrakta2
class SiriusoBazaAbstrakta3(SiriusoBazaAbstrakta2):
    # опубликовано (да или нет)
    publikigo = models.BooleanField(_('Publikigis'), default=False)

    # дата и время публикации
    publikiga_dato = models.DateTimeField(_('Dato de publikigo'), auto_now_add=False, auto_now=False, blank=True,
                                          null=True, default=None)

    # архивная (да или нет)
    arkivo = models.BooleanField(_('Arkiva'), default=False)

    # дата и время помещения в архив
    arkiva_dato = models.DateTimeField(_('Arkiva dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Основной абстрактный класс 4, дополняющий полями для сообществ и т.д. SiriusoBazaAbstrakta2
class SiriusoBazaAbstraktaKomunumoj(SiriusoBazaAbstrakta3):
    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # Ссылка на пользователя последней модификации
    lasta_autoro = models.ForeignKey(Uzanto, verbose_name=_('Modifita de'), blank=True, null=True,
                                     related_name='%(app_label)s_%(class)s_lasta_autoro',
                                     on_delete=models.SET_NULL)

    # Дата последнего изменения
    lasta_dato = models.DateTimeField(_('Dato de lasta modifo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Основной абстрактный класс 5, дополняющий полями для пользователей SiriusoBazaAbstrakta3
class SiriusoBazaAbstraktaUzanto(SiriusoBazaAbstrakta3):
    # владелец (пользователь)
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  related_name='%(app_label)s_%(class)s_posedanto',
                                  on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Абстрактный класс типов, использует абстрактный класс SiriusoBazaAbstrakta
class SiriusoTipoAbstrakta(SiriusoBazaAbstrakta3):
    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Типы пользователей, использует абстрактный класс SiriusoTipoAbstrakta
class UzantoTipo(SiriusoTipoAbstrakta):
    # название типа пользователя
    nomo = models.JSONField(verbose_name=_('Nomo'), null=True, blank=True, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'uzantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Uzanto tipo")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Uzantoj tipoj")

    def __str__(self):
        # для представления объекта будет выведено поле nomo модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Абстрактный класс комментариев, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class SiriusoKomentoAbstrakta(SiriusoBazaAbstraktaKomunumoj):
    # если это ответ, то на какой комментарий (рекурсивная связь)
    respondo = models.ForeignKey('self', verbose_name=_('Respondo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_respondo', on_delete=models.CASCADE)

    # Ссылка на пользователя последней модификации
    lasta_autoro = models.ForeignKey(Uzanto, verbose_name=_('Modifita de'), blank=True, null=True,
                                     related_name='%(app_label)s_%(class)s_lasta_autoro',
                                     on_delete=models.SET_NULL)

    # Дата последнего изменения
    lasta_dato = models.DateTimeField(_('Dato de lasta modifo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True
        # читабельное название модели, в единственном числе
        verbose_name = _('Komento')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Komentoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле teksto модели
        return '{}'.format(self.teksto)


# Общий класс слагов, использует абстрактный класс SiriusoBazaAbstrakta3
class SiriusoSlugo(SiriusoBazaAbstrakta3):
    # Название слага
    slugo = models.SlugField(_('Slugo'), unique=True, max_length=32, blank=False, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'siriuso_slugoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Slugo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Slugoj')

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле teksto модели
        return '{}'.format(self.slugo)


# Абстрактная модель для хранения ревизий,
# остальные необходимые поля для хранения резервных значений добавляем руками
class RevizioAbstrakta(models.Model):
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)
    posedanto = models.UUIDField(_('Posedanta UUID'), blank=False, db_index=True)
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None, 
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)
    agado_flago = models.BooleanField(_('Agado flago'), default=False, blank=False)

    class Meta:
        abstract = True


# Модель специальных групп (объединеня групп прав доступа)
class SpecialajGrupoj(models.Model):
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)
    kodo = models.CharField(_('Kodo'), max_length=64, blank=False, null=False, db_index=True)
    nomo = models.JSONField(verbose_name=_('Unua nomo'), null=True, blank=True, default=default_lingvo,
                              encoder=CallableEncoder)
    grupoj = models.ManyToManyField(Group, verbose_name=_('Grupoj de rajtoj'), blank=False)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'specialaj_grupoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speciala grupo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specialaj grupoj')


# Модель заданий
class SistemaPeto(models.Model):
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Autoro'), blank=True, null=True,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.SET_NULL)
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)
    tasko = models.CharField(_('Tasko nomo'), max_length=1024, blank=False, null=False)
    tasko_id = models.UUIDField(_('Celery ID'), blank=True, null=True, db_index=True)
    komenca_dato = models.DateTimeField(_('Komenca dato'), blank=True, null=True)
    fina_dato = models.DateTimeField(_('Fina dato'), blank=True, null=True)
    parametroj = models.JSONField(_('Taskaj parametroj'), null=False, blank=False)
    statuso = models.CharField(_('Statuso'), max_length=64, blank=True, default='')
    rezulto = models.JSONField(_('Tasko rezulto'), null=True, blank=True)

    class Meta:
        db_table = 'sistemaj_petoj'
        verbose_name = 'Sitema peto'
        verbose_name_plural = 'Sistemaj petoj'
