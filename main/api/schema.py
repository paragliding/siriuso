"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from graphene import relay, ObjectType, Field
from graphene_django import DjangoObjectType
from graphene_permissions.permissions import AllowAny
from django.utils.translation import gettext_lazy as _
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from ..models import SiriusoWablono 


class SiriusoWablonoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)
    priskribo = Field(SiriusoLingvo)

    class Meta:
        model = SiriusoWablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro_id': ['exact', 'in']
        }
        interfaces = (relay.Node,)


class SiriusoQuery(ObjectType):
    realecoj_wablonoj = SiriusoFilterConnectionField(
        SiriusoWablonoNode,
        description=_('Возвращает список доступных шаблонов реальностей')
    )
