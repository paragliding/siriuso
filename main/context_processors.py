"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models import Q
from .models import Uzanto


def siriuso_context(request):
    if request.user.is_authenticated:
        a_uzanto = (Uzanto.objects
                    .filter(Q(fotoj_fotojuzantojavataro_posedanto__forigo=False) | Q(fotoj_fotojuzantojavataro_posedanto__forigo__isnull=True),
                         fotoj_fotojuzantojavataro_posedanto__aktiva=True,
                         id=request.user.id)
                    .values('id', 'uuid', 'unua_nomo', 'dua_nomo', 'familinomo',
                            'fotoj_fotojuzantojavataro_posedanto__bildo', 'fotoj_fotojuzantojavataro_posedanto__bildo_min', 'sekso', 'is_admin',
                            'is_active')
                    )

        a_uzanto = a_uzanto[0] if a_uzanto.count() else None

        scontext = {'a_uzanto': a_uzanto}
    else:
        scontext = {}

    return scontext
