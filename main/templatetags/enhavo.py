"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django import template
from django.template.defaultfilters import stringfilter
from siriuso.utils.modules import get_enhavo


register = template.Library()


@register.filter
def enhavo(field, locale=None):
    return get_enhavo(field, lingvo=locale)[0]
