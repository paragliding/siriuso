"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель владельцев задач
class TaskojTaskoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = TaskojTaskoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tasko__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'statuso__id': ['exact'],
            'tipo__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель задач
class TaskojTaskoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # владельцы (какое именно оружие выполняет задачу выстрела)
    posedanto =  SiriusoFilterConnectionField(TaskojTaskoPosedantoNode,
        description=_('Выводит владельцев проектов'))

    class Meta:
        model = TaskojTasko
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'objekto__uuid': ['exact'],
            'statuso__id': ['exact','in'],
            'tipo__id': ['exact'],
            'kategorio__id': ['exact'],
            'projekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_posedanto(self, info, **kwargs):
        return TaskojTaskoPosedanto.objects.filter(tasko=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель категорий проектов
class TaskojProjektoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов проектов
class TaskojProjektoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов проектов
class TaskojProjektoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев проектов
class TaskojProjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = TaskojProjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'projekto__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_komunumo__id': ['exact'],
            'posedanto_komunumo__uuid': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact'],
            'posedanto_objekto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель проектов
class TaskojProjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # задача
    tasko =  SiriusoFilterConnectionField(TaskojTaskoNode,
        description=_('Выводит задачи, привязанные к проекту'))

    # владельцы
    posedanto =  SiriusoFilterConnectionField(TaskojProjektoPosedantoNode,
        description=_('Выводит владельцев проектов'))

    class Meta:
        model = TaskojProjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact','in'],
            'kategorio__id': ['exact','in'],
            'objekto__uuid': ['exact'],
            'taskojprojektoposedanto__posedanto_komunumo__id': ['exact'],
            'taskojprojektoposedanto__posedanto_uzanto__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_tasko(self, info, **kwargs):
        return TaskojTasko.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_posedanto(self, info, **kwargs):
        return TaskojProjektoPosedanto.objects.filter(projekto=self, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов владельцев проектов
class TaskojProjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца проекта
class TaskojProjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей проектов между собой
class TaskojProjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojProjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей проектов между собой
class TaskojProjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = TaskojProjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель категорий задач
class TaskojTaskoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = TaskojTaskoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов задач
class TaskojTaskoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = TaskojTaskoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов задач
class TaskojTaskoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = TaskojTaskoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов владельцев задач
class TaskojTaskoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = TaskojTaskoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца задач
class TaskojTaskoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = TaskojTaskoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов связей задач между собой
class TaskojTaskoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = TaskojTaskoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей задач между собой
class TaskojTaskoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = TaskojTaskoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'ligilo__uuid': ['exact'],
            'tipo__id': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class TaskojQuery(graphene.ObjectType):
    taskoj_projekto_kategorio = SiriusoFilterConnectionField(
        TaskojProjektoKategorioNode,
        description=_('Выводит все доступные модели категорий проектов')
    )
    taskoj_projekto_tipo = SiriusoFilterConnectionField(
        TaskojProjektoTipoNode,
        description=_('Выводит все доступные модели типов проектов')
    )
    taskoj_projekto_statuso = SiriusoFilterConnectionField(
        TaskojProjektoStatusoNode,
        description=_('Выводит все доступные модели статусов проектов')
    )
    taskoj_projekto = SiriusoFilterConnectionField(
        TaskojProjektoNode,
        description=_('Выводит все доступные модели проектов')
    )
    taskoj_projekto_posedantoj_tipo = SiriusoFilterConnectionField(
        TaskojProjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев проектов')
    )
    taskoj_projekto_posedantoj_statuso = SiriusoFilterConnectionField(
        TaskojProjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца проекта')
    )
    taskoj_projekto_posedantoj = SiriusoFilterConnectionField(
        TaskojProjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев проектов')
    )
    taskoj_projekto_ligilo_tipo = SiriusoFilterConnectionField(
        TaskojProjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей проектов между собой')
    )
    taskoj_projekto_ligilo = SiriusoFilterConnectionField(
        TaskojProjektoLigiloNode,
        description=_('Выводит все доступные модели связей проектов между собой')
    )
    taskoj_tasko_kategorio = SiriusoFilterConnectionField(
        TaskojTaskoKategorioNode,
        description=_('Выводит все доступные модели категорий задач')
    )
    taskoj_tasko_tipo = SiriusoFilterConnectionField(
        TaskojTaskoTipoNode,
        description=_('Выводит все доступные модели типов задач')
    )
    taskoj_tasko_statuso = SiriusoFilterConnectionField(
        TaskojTaskoStatusoNode,
        description=_('Выводит все доступные модели статусов задач')
    )
    taskoj_tasko = SiriusoFilterConnectionField(
        TaskojTaskoNode,
        description=_('Выводит все доступные модели задач')
    )
    taskoj_tasko_posedantoj_tipo = SiriusoFilterConnectionField(
        TaskojTaskoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев задач')
    )
    taskoj_tasko_posedantoj_statuso = SiriusoFilterConnectionField(
        TaskojTaskoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца задач')
    )
    taskoj_tasko_posedanto = SiriusoFilterConnectionField(
        TaskojTaskoPosedantoNode,
        description=_('Выводит все доступные модели владельцев задач')
    )
    taskoj_tasko_ligilo_tipo = SiriusoFilterConnectionField(
        TaskojTaskoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей задач между собой')
    )
    taskoj_tasko_ligilo = SiriusoFilterConnectionField(
        TaskojTaskoLigiloNode,
        description=_('Выводит все доступные модели связей задач между собой')
    )
