"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
import requests
from django.conf import settings

from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель справочника валют
class RedaktuMonoValuto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_valutoj = graphene.Field(MonoValutoNode, 
        description=_('Созданный/изменённый справочник валют'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_valutoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_valutoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            mono_valutoj = MonoValuto.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(mono_valutoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(mono_valutoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    mono_valutoj.realeco.set(realeco)
                                else:
                                    mono_valutoj.realeco.clear()

                            mono_valutoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_valutoj = MonoValuto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_valutoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_valutoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                mono_valutoj.forigo = kwargs.get('forigo', mono_valutoj.forigo)
                                mono_valutoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_valutoj.arkivo = kwargs.get('arkivo', mono_valutoj.arkivo)
                                mono_valutoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_valutoj.publikigo = kwargs.get('publikigo', mono_valutoj.publikigo)
                                mono_valutoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_valutoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(mono_valutoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(mono_valutoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        mono_valutoj.realeco.set(realeco)
                                    else:
                                        mono_valutoj.realeco.clear()

                                mono_valutoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoValuto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoValuto(status=status, message=message, mono_valutoj=mono_valutoj)


# Модель типов счетов
class RedaktuMonoKontoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_kontoj_tipoj = graphene.Field(MonoKontoTipoNode, 
        description=_('Созданный/изменённый тип счетов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        valuto_id = graphene.Int(description=_('Валюта типа счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_kontoj_tipoj = None
        realeco = Realeco.objects.none()
        valuto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_kontoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'valuto_id' in kwargs:
                                try:
                                    valuto = MonoValuto.objects.get(id=kwargs.get('valuto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoValuto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная валюта типа счёта'),'valuto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'valuto_id')

                        if not message:
                            mono_kontoj_tipoj = MonoKontoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                valuto = valuto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(mono_kontoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(mono_kontoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    mono_kontoj_tipoj.realeco.set(realeco)
                                else:
                                    mono_kontoj_tipoj.realeco.clear()

                            mono_kontoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('valuto_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_kontoj_tipoj = MonoKontoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_kontoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_kontoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'valuto_id' in kwargs:
                                    try:
                                        valuto = MonoValuto.objects.get(id=kwargs.get('valuto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoValuto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная валюта типа счёта'),'valuto_id')

                            if not message:

                                mono_kontoj_tipoj.forigo = kwargs.get('forigo', mono_kontoj_tipoj.forigo)
                                mono_kontoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_kontoj_tipoj.arkivo = kwargs.get('arkivo', mono_kontoj_tipoj.arkivo)
                                mono_kontoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_kontoj_tipoj.publikigo = kwargs.get('publikigo', mono_kontoj_tipoj.publikigo)
                                mono_kontoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_kontoj_tipoj.autoro = uzanto
                                mono_kontoj_tipoj.valuto = valuto if kwargs.get('valuto_id', False) else mono_kontoj_tipoj.valuto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(mono_kontoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(mono_kontoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        mono_kontoj_tipoj.realeco.set(realeco)
                                    else:
                                        mono_kontoj_tipoj.realeco.clear()

                                mono_kontoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoKontoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoKontoTipo(status=status, message=message, mono_kontoj_tipoj=mono_kontoj_tipoj)


# Модель шаблонов счетов (кошельков)
class RedaktuMonoKontoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_kontoj_sxablonoj = graphene.Field(MonoKontoSxablonoNode, 
        description=_('Созданный/изменённый шаблон счета (кошелька)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип валюты счёта'))
        bilanco = graphene.Float(description=_('Баланс счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_kontoj_sxablonoj = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_kontoj_sxablonoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = MonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoKontoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип валюты счёта'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            mono_kontoj_sxablonoj = MonoKontoSxablono.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                realeco = realeco,
                                bilanco = kwargs.get('bilanco', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(mono_kontoj_sxablonoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(mono_kontoj_sxablonoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            mono_kontoj_sxablonoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('bilanco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_kontoj_sxablonoj = MonoKontoSxablono.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_kontoj_sxablonoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_kontoj_sxablonoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = MonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoKontoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип валюты счёта'),'tipo_id')

                            if not message:

                                mono_kontoj_sxablonoj.forigo = kwargs.get('forigo', mono_kontoj_sxablonoj.forigo)
                                mono_kontoj_sxablonoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_kontoj_sxablonoj.arkivo = kwargs.get('arkivo', mono_kontoj_sxablonoj.arkivo)
                                mono_kontoj_sxablonoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_kontoj_sxablonoj.publikigo = kwargs.get('publikigo', mono_kontoj_sxablonoj.publikigo)
                                mono_kontoj_sxablonoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_kontoj_sxablonoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else mono_kontoj_sxablonoj.realeco
                                mono_kontoj_sxablonoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else mono_kontoj_sxablonoj.tipo
                                mono_kontoj_sxablonoj.autoro = uzanto
                                mono_kontoj_sxablonoj.bilanco = kwargs.get('bilanco', mono_kontoj_sxablonoj.bilanco)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(mono_kontoj_sxablonoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(mono_kontoj_sxablonoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                mono_kontoj_sxablonoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoKontoSxablono.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoKontoSxablono(status=status, message=message, mono_kontoj_sxablonoj=mono_kontoj_sxablonoj)


# Модель счетов (кошельков)
class RedaktuMonoKonto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_kontoj = graphene.Field(MonoKontoNode, 
        description=_('Созданный/изменённый счет (кошелёк)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип валюты счёта'))
        posedanto_uzanto_id = graphene.String(description=_('Пользователь владелец счёта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец счёта'))
        bilanco = graphene.Float(description=_('Баланс счёта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_kontoj = None
        posedanto_uzanto = None
        posedanto_organizo = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_kontoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = MonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoKontoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип валюты счёта'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'posedanto_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный пользователь'),'posedanto_uzanto_id')

                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(id=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация владелец счёта'),'posedanto_organizo_uuid')

                        if not message:
                            mono_kontoj = MonoKonto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto = posedanto_uzanto,
                                posedanto_organizo = posedanto_organizo,
                                tipo = tipo,
                                realeco = realeco,
                                bilanco = kwargs.get('bilanco', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            mono_kontoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uzanto_id', False) 
                            or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('bilanco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_kontoj = MonoKonto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_kontoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_kontoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = MonoKontoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoKontoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип валюты счёта'),'tipo_id')

                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный пользователь Универсо'),'posedanto_uzanto_id')

                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(id=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Organizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация владелец счёта'),'posedanto_organizo_uuid')

                            if not message:

                                mono_kontoj.forigo = kwargs.get('forigo', mono_kontoj.forigo)
                                mono_kontoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_kontoj.arkivo = kwargs.get('arkivo', mono_kontoj.arkivo)
                                mono_kontoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_kontoj.publikigo = kwargs.get('publikigo', mono_kontoj.publikigo)
                                mono_kontoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_kontoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else mono_kontoj.realeco
                                mono_kontoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else mono_kontoj.tipo
                                mono_kontoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) \
                                    else mono_kontoj.posedanto_uzanto
                                mono_kontoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) \
                                    else mono_kontoj.posedanto_organizo
                                mono_kontoj.bilanco = kwargs.get('bilanco', mono_kontoj.bilanco)

                                mono_kontoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoKonto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoKonto(status=status, message=message, mono_kontoj=mono_kontoj)


# Модель типов инвойсов (счетов на оплату)
class RedaktuMonoFakturoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_fakturoj_tipoj = graphene.Field(MonoFakturoTipoNode, 
        description=_('Созданный/изменённый тип инвойсов (счетов на оплату)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_fakturoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_fakturoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            mono_fakturoj_tipoj = MonoFakturoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(mono_fakturoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(mono_fakturoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    mono_fakturoj_tipoj.realeco.set(realeco)
                                else:
                                    mono_fakturoj_tipoj.realeco.clear()

                            mono_fakturoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_fakturoj_tipoj = MonoFakturoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_fakturoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_fakturoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )

                            if not message:

                                mono_fakturoj_tipoj.forigo = kwargs.get('forigo', mono_fakturoj_tipoj.forigo)
                                mono_fakturoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_fakturoj_tipoj.arkivo = kwargs.get('arkivo', mono_fakturoj_tipoj.arkivo)
                                mono_fakturoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_fakturoj_tipoj.publikigo = kwargs.get('publikigo', mono_fakturoj_tipoj.publikigo)
                                mono_fakturoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_fakturoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(mono_fakturoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(mono_fakturoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        mono_fakturoj_tipoj.realeco.set(realeco)
                                    else:
                                        mono_fakturoj_tipoj.realeco.clear()

                                mono_fakturoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoFakturoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoFakturoTipo(status=status, message=message, mono_fakturoj_tipoj=mono_fakturoj_tipoj)


# Модель инвойсов (счетов на оплату)
class RedaktuMonoFakturo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_fakturoj = graphene.Field(MonoFakturoNode, 
        description=_('Созданный/изменённый инвойс (счет на оплату)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип инвойса'))
        ekspedanto_uuid = graphene.String(description=_('Выставитель инвойса'))
        ricevanto_uuid = graphene.String(description=_('Получатель инвойса'))
        sumo = graphene.Float(description=_('Сумма инвойса по всем позициям'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_fakturoj = None
        ekspedanto = None
        ricevanto = None
        realeco = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_fakturoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = MonoFakturoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoFakturoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип инвойса'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'ekspedanto_uuid' in kwargs:
                                try:
                                    ekspedanto = MonoKonto.objects.get(
                                        uuid=kwargs.get('ekspedanto_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except MonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный выставитель инвойса'),'ekspedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                        if not message:
                            if 'ricevanto_uuid' in kwargs:
                                try:
                                    ricevanto = MonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный получатель инвойса'),'ricevanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                        if not message:
                            mono_fakturoj = MonoFakturo.objects.create(
                                forigo=False,
                                arkivo=False,
                                ekspedanto = ekspedanto,
                                ricevanto = ricevanto,
                                tipo = tipo,
                                realeco = realeco,
                                sumo = kwargs.get('sumo', 0.0),
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            mono_fakturoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('ekspedanto_uuid', False) 
                            or kwargs.get('ricevanto_uuid', False) or kwargs.get('sumo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_fakturoj = MonoFakturo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_fakturoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_fakturoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = MonoFakturoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoFakturoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип инвойса'),'tipo_id')

                            if not message:
                                if 'ekspedanto_uuid' in kwargs:
                                    try:
                                        ekspedanto = MonoKonto.objects.get(
                                            uuid=kwargs.get('ekspedanto_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except MonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный выставитель инвойса'),'ekspedanto_uuid')

                            if not message:
                                if 'ricevanto_uuid' in kwargs:
                                    try:
                                        ricevanto = MonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный получатель инвойса'),'ricevanto_uuid')

                            if not message:

                                mono_fakturoj.forigo = kwargs.get('forigo', mono_fakturoj.forigo)
                                mono_fakturoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_fakturoj.arkivo = kwargs.get('arkivo', mono_fakturoj.arkivo)
                                mono_fakturoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_fakturoj.publikigo = kwargs.get('publikigo', mono_fakturoj.publikigo)
                                mono_fakturoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_fakturoj.sumo = kwargs.get('sumo', mono_fakturoj.sumo)
                                mono_fakturoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else mono_fakturoj.realeco
                                mono_fakturoj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else mono_fakturoj.tipo
                                mono_fakturoj.ekspedanto = ekspedanto if kwargs.get('ekspedanto_uuid', False) \
                                    else mono_fakturoj.ekspedanto
                                mono_fakturoj.ricevanto = ricevanto if kwargs.get('ricevanto_uuid', False) \
                                    else mono_fakturoj.ricevanto

                                mono_fakturoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoFakturo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoFakturo(status=status, message=message, mono_fakturoj=mono_fakturoj)


# Модель объектов указанных в инвойсах (содержимое табличной части)
class RedaktuMonoFakturoObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_fakturoj_objektoj = graphene.Field(MonoFakturoObjektoNode, 
        description=_('Созданный/изменённый объект указанный в инвойсах (содержимое табличной части)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        fakturo_uuid = graphene.String(description=_('Выставитель инвойса'))
        objekto_uuid = graphene.String(description=_('Получатель инвойса'))
        prezo = graphene.Float(description=_('Цена этой позиции'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_fakturoj_objektoj = None
        fakturo = None
        objekto = None
        realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_fakturoj_objektoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'fakturo_uuid' in kwargs:
                                try:
                                    fakturo = MonoFakturo.objects.get(
                                        uuid=kwargs.get('fakturo_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except MonoFakturo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный инвойс к которому относится запись'),'fakturo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')

                        if not message:
                            if 'objekto_uuid' in kwargs:
                                try:
                                    objekto = Objekto.objects.get(id=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная объект инвойса (на каждый объект отдельная запись в этой моделе)'),'objekto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'objekto_uuid')

                        if not message:
                            mono_fakturoj_objektoj = MonoFakturoObjekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                fakturo = fakturo,
                                objekto = objekto,
                                realeco = realeco,
                                publikigo = kwargs.get('publikigo', False),
                                prezo =  kwargs.get('prezo', 0.0),
                                publikiga_dato=timezone.now()
                            )

                            mono_fakturoj_objektoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('fakturo_uuid', False) or kwargs.get('realeco_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False
                            or kwargs.get('prezo', False))):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_fakturoj_objektoj = MonoFakturoObjekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_fakturoj_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_fakturoj_objektoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')

                            if not message:
                                if 'fakturo_uuid' in kwargs:
                                    try:
                                        fakturo = MonoFakturo.objects.get(
                                            uuid=kwargs.get('fakturo_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except MonoFakturo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный инвойс к которому относится запись'),'fakturo_uuid')

                            if not message:
                                if 'objekto_uuid' in kwargs:
                                    try:
                                        objekto = Objekto.objects.get(id=kwargs.get('objekto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная объект инвойса (на каждый объект отдельная запись в этой моделе)'),'objekto_uuid')

                            if not message:

                                mono_fakturoj_objektoj.forigo = kwargs.get('forigo', mono_fakturoj_objektoj.forigo)
                                mono_fakturoj_objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_fakturoj_objektoj.arkivo = kwargs.get('arkivo', mono_fakturoj_objektoj.arkivo)
                                mono_fakturoj_objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_fakturoj_objektoj.publikigo = kwargs.get('publikigo', mono_fakturoj_objektoj.publikigo)
                                mono_fakturoj_objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_fakturoj_objektoj.prezo = kwargs.get('prezo', mono_fakturoj_objektoj.prezo)
                                mono_fakturoj_objektoj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else mono_fakturoj_objektoj.realeco
                                mono_fakturoj_objektoj.fakturo = fakturo if kwargs.get('fakturo_uuid', False) \
                                    else mono_fakturoj_objektoj.fakturo
                                mono_fakturoj_objektoj.objekto = objekto if kwargs.get('objekto_uuid', False) \
                                    else mono_fakturoj_objektoj.objekto

                                mono_fakturoj_objektoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoFakturoObjekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoFakturoObjekto(status=status, message=message, mono_fakturoj_objektoj=mono_fakturoj_objektoj)


# Модель типов транзакций между счетами (кошельками)
class RedaktuMonoTransakcioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_transakcioj_tipoj = graphene.Field(MonoTransakcioTipoNode, 
        description=_('Созданный/изменённый тип транзакций между счетами (кошельками)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_transakcioj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_transakcioj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            mono_transakcioj_tipoj = MonoTransakcioTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(mono_transakcioj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(mono_transakcioj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    mono_transakcioj_tipoj.realeco.set(realeco)
                                else:
                                    mono_transakcioj_tipoj.realeco.clear()

                            mono_transakcioj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_transakcioj_tipoj = MonoTransakcioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_transakcioj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_transakcioj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )

                            if not message:

                                mono_transakcioj_tipoj.forigo = kwargs.get('forigo', mono_transakcioj_tipoj.forigo)
                                mono_transakcioj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_transakcioj_tipoj.arkivo = kwargs.get('arkivo', mono_transakcioj_tipoj.arkivo)
                                mono_transakcioj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_transakcioj_tipoj.publikigo = kwargs.get('publikigo', mono_transakcioj_tipoj.publikigo)
                                mono_transakcioj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_transakcioj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(mono_transakcioj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(mono_transakcioj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        mono_transakcioj_tipoj.realeco.set(realeco)
                                    else:
                                        mono_transakcioj_tipoj.realeco.clear()

                                mono_transakcioj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoTransakcioTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoTransakcioTipo(status=status, message=message, mono_transakcioj_tipoj=mono_transakcioj_tipoj)


# Модель инвойсов (счетов на оплату)
class RedaktuMonoTransakcio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mono_transakcioj = graphene.Field(MonoTransakcioNode, 
        description=_('Созданный/изменённый инвойс (счет на оплату)'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        tipo_id = graphene.Int(description=_('Тип транзакции'))
        fakturo_uuid = graphene.String(description=_('Инвойс на основе которого делается транзакция'))
        ekspedanto_uuid = graphene.String(description=_('Отправитель платежа'))
        ricevanto_uuid = graphene.String(description=_('Получатель платежа'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mono_transakcioj = None
        ekspedanto = None
        ricevanto = None
        realeco = None
        tipo = None
        fakturo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mono.povas_krei_mono_transakcioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = MonoTransakcioTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoTransakcioTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип транзакции'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            if 'fakturo_uuid' in kwargs:
                                try:
                                    fakturo = MonoKonto.objects.get(
                                        uuid=kwargs.get('fakturo_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except MonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный инвойс на основе которого делается транзакция'),'fakturo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')
                        if not message:
                            if 'ekspedanto_uuid' in kwargs:
                                try:
                                    ekspedanto = MonoKonto.objects.get(
                                        uuid=kwargs.get('ekspedanto_uuid'), 
                                        forigo=False, 
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except MonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный отправитель платежа'),'ekspedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                        if not message:
                            if 'ricevanto_uuid' in kwargs:
                                try:
                                    ricevanto = MonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MonoKonto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный получатель платежа'),'ricevanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                        if not message:
                            mono_transakcioj = MonoTransakcio.objects.create(
                                forigo=False,
                                arkivo=False,
                                ekspedanto = ekspedanto,
                                ricevanto = ricevanto,
                                tipo = tipo,
                                realeco = realeco,
                                fakturo = fakturo,
                                publikigo = kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            mono_transakcioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('ekspedanto_uuid', False) 
                            or kwargs.get('ricevanto_uuid', False) or kwargs.get('fakturo_uuid', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mono_transakcioj = MonoTransakcio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mono.povas_forigi_mono_transakcioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mono.povas_shanghi_mono_transakcioj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Указана несуществующая реальность'),'realeco_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = MonoTransakcioTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoTransakcioTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип транзакции'),'tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                            if not message:
                                if 'fakturo_uuid' in kwargs:
                                    try:
                                        fakturo = MonoKonto.objects.get(
                                            uuid=kwargs.get('fakturo_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except MonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный инвойс на основе которого делается транзакция'),'fakturo_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'fakturo_uuid')
                            if not message:
                                if 'ekspedanto_uuid' in kwargs:
                                    try:
                                        ekspedanto = MonoKonto.objects.get(
                                            uuid=kwargs.get('ekspedanto_uuid'), 
                                            forigo=False, 
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except MonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный отправитель платежа'),'ekspedanto_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ekspedanto_uuid')

                            if not message:
                                if 'ricevanto_uuid' in kwargs:
                                    try:
                                        ricevanto = MonoKonto.objects.get(id=kwargs.get('ricevanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except MonoKonto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный получатель платежа'),'ricevanto_uuid')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'ricevanto_uuid')

                            if not message:

                                mono_transakcioj.forigo = kwargs.get('forigo', mono_transakcioj.forigo)
                                mono_transakcioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                mono_transakcioj.arkivo = kwargs.get('arkivo', mono_transakcioj.arkivo)
                                mono_transakcioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mono_transakcioj.publikigo = kwargs.get('publikigo', mono_transakcioj.publikigo)
                                mono_transakcioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                mono_transakcioj.realeco = realeco if kwargs.get('realeco_id', False) \
                                    else mono_transakcioj.realeco
                                mono_transakcioj.tipo = tipo if kwargs.get('tipo_id', False) \
                                    else mono_transakcioj.tipo
                                mono_transakcioj.ekspedanto = ekspedanto if kwargs.get('ekspedanto_uuid', False) \
                                    else mono_transakcioj.ekspedanto
                                mono_transakcioj.ricevanto = ricevanto if kwargs.get('ricevanto_uuid', False) \
                                    else mono_transakcioj.ricevanto
                                mono_transakcioj.fakturo = fakturo if kwargs.get('fakturo_uuid', False) \
                                    else mono_transakcioj.fakturo

                                mono_transakcioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MonoTransakcio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMonoTransakcio(status=status, message=message, mono_transakcioj=mono_transakcioj)


# Импорт типов валюты из 1с
class ImportoMonoValuto1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(MonoValutoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        url = settings.ODATA_URL+'Catalog_Валюты?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if uzanto.has_perm('mono.povas_krei_mono_valutoj'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                errors = list()
                # проверяем наличие записей с таким кодом
                realeco_id = [1,]
                if len(realeco_id):
                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))
                    if len(dif):
                        errors.append(ErrorNode(
                            field=str(dif),
                            message=_('Указаны несуществующие реальности')
                        ))

                for js in jsj['value']:
                    if len(errors)>0:
                        break

                    uuid = js['Ref_Key']
                    
                    try:
                        importo = MonoValuto.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except MonoValuto.DoesNotExist:
                        pass

                        nomo = js['Description']
                        priskribo = js['НаименованиеПолное']

                        if not len(errors):
                            importo = MonoValuto.objects.create(
                                uuid=uuid,
                                autoro=uzanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                id=js['Code'],
                            )
                            if nomo:
                                set_enhavo(importo.nomo, nomo, 'ru_RU')
                            if priskribo:
                                set_enhavo(importo.priskribo, priskribo, 'ru_RU')
                            if realeco:
                                importo.realeco.set(realeco)

                            importo.save()
                            kvanto += 1
                if len(errors):
                    status = False
                    message = _('Nevalida argumentvaloroj')
                    for error in errors:
                        message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                            error.message, _('в поле'), error.field)
                else:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoMonoValuto1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoMonoValuto1c(status=status, message=message, errors=errors, importo=importo)


class MonoMutations(graphene.ObjectType):
    redaktu_mono_valutoj = RedaktuMonoValuto.Field(
        description=_('''Создаёт или редактирует модель справочника валют''')
    )
    redaktu_mono_kontoj_tipoj = RedaktuMonoKontoTipo.Field(
        description=_('''Создаёт или редактирует модель типов счетов''')
    )
    redaktu_mono_kontoj_sxablonoj = RedaktuMonoKontoSxablono.Field(
        description=_('''Создаёт или редактирует модель шаблонов счетов (кошельков)''')
    )
    redaktu_mono_kontoj = RedaktuMonoKonto.Field(
        description=_('''Создаёт или редактирует модель счетов (кошельков)''')
    )
    redaktu_mono_fakturoj_tipoj = RedaktuMonoFakturoTipo.Field(
        description=_('''Создаёт или редактирует модель типов инвойсов (счетов на оплату)''')
    )
    redaktu_mono_fakturoj = RedaktuMonoFakturo.Field(
        description=_('''Создаёт или редактирует модель инвойсов (счетов на оплату)''')
    )
    redaktu_mono_fakturoj_objektoj = RedaktuMonoFakturoObjekto.Field(
        description=_('''Создаёт или редактирует модель объектов указанных в инвойсах (содержимое табличной части)''')
    )
    redaktu_mono_transakcioj_tipoj = RedaktuMonoTransakcioTipo.Field(
        description=_('''Создаёт или редактирует модель типов транзакций между счетами (кошельками)''')
    )
    redaktu_mono_transakcioj = RedaktuMonoTransakcio.Field(
        description=_('''Создаёт или редактирует модель инвойсов (счетов на оплату)''')
    )
    importo_mono_valuto1c = ImportoMonoValuto1c.Field(
        description=_('''Импорт типов валюты из 1с''')
    )
