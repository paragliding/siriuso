"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.db.models import Value
from django.db.models.functions import Concat

from siriuso.utils import get_enhavo


class TekstoMixin:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    @admin.display(ordering='nomo')
    def nomo_teksto(self, obj):
        return self.teksto(obj=obj, field='nomo')
    nomo_teksto.short_description = _('Nomo')
    # nomo_teksto.short_description = 'Наименование'

    @admin.display(ordering='priskribo')
    def priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='priskribo')
    priskribo_teksto.short_description = 'Описание'

    @admin.display(ordering='teksto')
    def teksto_teksto(self, obj):
        return self.teksto(obj=obj, field='teksto')
    teksto_teksto.short_description = 'Текст'

    @admin.display(ordering='sxablono_sistema_priskribo')
    def sxablono_priskribo_teksto(self, obj):
        return self.teksto(obj=obj, field='sxablono_sistema_priskribo')
    sxablono_priskribo_teksto.short_description = 'Описание шаблона'

    # autoro_id не работает - возможно подхватывает внутренний id, по этому заменил на autoro_nn
    @admin.display(
        description='ID автора',
        ordering='autoro__id'
    )
    def autoro_nn(self, obj):
        return obj.autoro.id

    @admin.display(ordering='autoro__chefa_retposhto',
                   description='Эл.почта автора'
    )
    def autoro_email(self, obj):
        return obj.autoro.chefa_retposhto

    @admin.display(ordering='posedanto__nomo')
    def posedanto_nomo(self, obj):
        return self.teksto(obj=obj.posedanto, field='nomo')

    @admin.display(ordering='ligilo__nomo')
    def ligilo_nomo(self, obj):
        return self.teksto(obj=obj.ligilo, field='nomo')

    @admin.display(ordering='formo')
    def formo_teksto(self, obj):
        return self.teksto(obj=obj, field='formo')

    @admin.display(ordering='statuso')
    def statuso_teksto(self, obj):
        return self.teksto(obj=obj, field='statuso')

    @admin.display(ordering='siriuso_uzanto__chefa_retposhto')
    def email(self, obj):
        return obj.siriuso_uzanto.chefa_retposhto

    @admin.display(
        ordering=Concat('numero_versio', Value('.'), 'numero_subversio', Value('.'), 'numero_korektado'),
        description='Версия'
    )
    def versio(self, obj):
        return '{}.{}.{}'.format(obj.numero_versio, obj.numero_subversio, obj.numero_korektado)

