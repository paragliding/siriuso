"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from informiloj.api.schema import InformiloQuery
from informiloj.api.mutations import InformilojMutations
from uzantoj.api.mutations import Mutations as UzantoMutations
from uzantoj.api.schema import UzantoQuery
from uzantoj.api.sciigoj import SciigojQuery
from komunumoj.api.mutations import Mutations as KomMutations
from statistiko.api.queries import Queries as StatQueries
from komunumoj.api.schema import KomunumoQuery
from fotoj.api.schema import FotojQuery
from fotoj.api.mutations import FotojMutations
from akademio.api.schema import AkademioQuery
from akademio.api.mutations import AkademioMutations
from enciklopedio.api.schema import EnciklopedioQuery
from enciklopedio.api.mutations import EnciklopedioMutations
from kodo.api.schema import KodoQuery
from kodo.api.mutations import KodoMutations
from konferencoj.api.schema import KonferencojQuery
from konferencoj.api.mutations import KonferencojMutations
from esploradoj.api.schema import EsploradojQuery
from esploradoj.api.mutations import EsploradojMutations
from mesagxilo.api.schema import MesagxiloQuery
from mesagxilo.api.mutations import MesagxiloMutations
from memorkartoj.api.schema import MemorkartojQuery
from memorkartoj.api.mutations import MemorkartojMutations
from muroj.api.schema import MuroQuery
from muroj.api.mutations import Mutations as MurojMutations
from versioj.api.mutations import VersioMutations

from premioj.api.schema import PremiojQuery
from premioj.api.mutations import PremiojMutations
from tamagocxi.api.schema import TamagocxiQuery
from tamagocxi.api.mutations import TamagocxiMutations
from lokalizo.api.schema import LokalizoQuery
from lokalizo.api.mutations import LokalizoMutations

from main.api.schema import SiriusoQuery

from kombatantoj.api.scheme import KombatantoQuery
from kombatantoj.api.mutation import KombatantoMutation
from profiloj.api.schema import ProfiloQuery
from profiloj.api.mutations import ProfiloMutations
from universo_bazo.api.schema import UniversoBazoQuery
from universo_bazo.api.mutations import UniversoBazoMutations
from resursoj.api.schema import ResursojQuery
from resursoj.api.mutations import ResursoMutations
from organizoj.api.schema import OrganizojQuery
from organizoj.api.mutations import OrganizoMutations
from objektoj.api.schema import ObjektojQuery
from objektoj.api.mutations import ObjektoMutations
from taskoj.api.schema import TaskojQuery
from taskoj.api.mutations import TaskojMutations
from projektoj.api.schema import ProjektojQuery
from projektoj.api.mutations import ProjektojMutations
from kosmo.api.schema import KosmoQuery
from kosmo.api.mutations import KosmoMutations
from mono.api.schema import MonoQuery
from mono.api.mutations import MonoMutations
from sxablonoj.api.schema import SxablonoQuery
from scipovoj.api.schema import ScipovojQuery
from scipovoj.api.mutations import ScipovoMutations
from rajtoj.api.schema import RajtojQuery
from rajtoj.api.mutations import RajtojMutations
from universo_uzantoj_websocket.api.schema import UniversoUzantojWebsocketQuery
from enketo.api.schema import EnketoQuery
from enketo.api.mutations import EnketoMutations
from dokumentoj.api.schema import DokumentojQuery
from dokumentoj.api.mutations import DokumentoMutations
from laborspacoj.api.schema import LaborspacojQuery
from laborspacoj.api.mutations import LaborspacojMutations
from kanvasoj.api.schema import KanvasojQuery
from kanvasoj.api.mutations import KanvasojMutations

from mesagxilo.api.subscription import MesagxiloSubscription
from objektoj.api.subscription import ObjektoSubscription
from profiloj.api.subscription import UzantoWSSubscription
from dokumentoj.api.subscription import DokumentoSubscription
from kanvasoj.api.subscription import KanvasoSubscription

import graphene
import graphql_jwt


class Query(UzantoQuery, StatQueries, KomunumoQuery, InformiloQuery, MuroQuery,
                  AkademioQuery, EnciklopedioQuery, KonferencojQuery, FotojQuery, KodoQuery, SciigojQuery,
                  PremiojQuery, SiriusoQuery, KombatantoQuery, EsploradojQuery,
                  MesagxiloQuery, MemorkartojQuery, TamagocxiQuery, LokalizoQuery, ProfiloQuery,
                  UniversoBazoQuery, ResursojQuery, OrganizojQuery, ObjektojQuery,
                  TaskojQuery, KosmoQuery, MonoQuery, SxablonoQuery,
                  ScipovojQuery, RajtojQuery, ProjektojQuery, UniversoUzantojWebsocketQuery,
                  EnketoQuery, DokumentojQuery, LaborspacojQuery, KanvasojQuery, graphene.ObjectType):
    pass


class Mutation(UzantoMutations, KomMutations, MurojMutations,
                      FotojMutations, AkademioMutations, EnciklopedioMutations, KonferencojMutations,
                      MesagxiloMutations, VersioMutations, KodoMutations, PremiojMutations,
                      KombatantoMutation, EsploradojMutations,
                      MemorkartojMutations, TamagocxiMutations, LokalizoMutations, ProfiloMutations,
                      UniversoBazoMutations, ResursoMutations, OrganizoMutations,
                      ObjektoMutations, TaskojMutations, KosmoMutations,
                      MonoMutations, ScipovoMutations, RajtojMutations,
                      ProjektojMutations, EnketoMutations, DokumentoMutations,
                      InformilojMutations, LaborspacojMutations, KanvasojMutations, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    revoke_token = graphql_jwt.Revoke.Field()
    pass


class Subsription(MesagxiloSubscription, ObjektoSubscription, UzantoWSSubscription,
                  DokumentoSubscription, KanvasoSubscription,
                  graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation, subscription=Subsription)
