"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from graphene import ObjectType, String, Boolean, List
from siriuso.utils import get_enhavo, get_lang_kodo
from graphene.types.generic import GenericScalar # Solution


class SiriusoLingvo(ObjectType):
    lingvoj = List(String)
    lingvo = String()
    enhavo = String()
    chefa_varianto = Boolean()
    json = GenericScalar()

    @classmethod
    def resolve_json(cls, root, info):
        if root:
            return root
        return None

    @classmethod
    def resolve_lingvoj(cls, root, info):
        if root:
            return root.get('lingvo')

        return None

    @classmethod
    def resolve_lingvo(cls, root, info):
        # Тут нужно доделать определение локали
        lingvo = get_lang_kodo(info.context)

        if root:
            result = get_enhavo(root, lingvo) or get_enhavo(root, empty_values=True)

            if result:
                return result[1]

        return None

    @classmethod
    def resolve_enhavo(cls, root, info):
        # Тут нужно доделать определение локали
        lingvo = get_lang_kodo(info.context)

        if root:
            result = get_enhavo(root, lingvo) or get_enhavo(root, empty_values=True)

            if result:
                return result[0]

        return None

    @classmethod
    def resolve_chefa_varianto(cls, root, info):
        lingvo = cls.resolve_lingvo(root, info)

        if lingvo:
            return lingvo == root['chefa_varianto']

        return None

    @classmethod
    def resolve_datumo(cls, root, info):
        if root:
            return root.get('datumo')
        return None


class ErrorNode(ObjectType):
    field = String()
    modelo = String()
    message = String()
