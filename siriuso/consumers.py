"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import channels_graphql_ws
import channels
import channels.auth

from django.conf import settings
import os

from asgiref.sync import sync_to_async
# from channels.db import database_sync_to_async
from graphql_jwt.shortcuts import get_user_by_token

from siriuso.middleware.asgi import SiriusoAsgiUzantoLingvo
from .api.schema import schema
from universo_uzantoj_websocket.models import UniversoUzantojWebsocket


class SiriusoGraphqlWsConsumer(channels_graphql_ws.GraphqlWsConsumer):
    """Channels WebSocket consumer which provides GraphQL API."""
    schema = schema

    # Uncomment to send keepalive message every 42 seconds.
    # send_keepalive_every = 42

    # Uncomment to process requests sequentially (useful for tests).
    # strict_ordering = True

    middleware = [
        SiriusoAsgiUzantoLingvo,
    ]

    async def on_connect(self, payload):
        """New client connection handler."""
        # You can `raise` from here to reject the connection.
        print("New client connected!")
        # проверка авторизации по JWT
        jwt = False
        if 'authorization' in payload:
            # убираем в начале JWT_AUTH_HEADER_PREFIX (они идут через пробел - просто делим)
            prefix, jwt_kode = payload['authorization'].split(' ')
            if prefix == settings.GRAPHQL_JWT['JWT_AUTH_HEADER_PREFIX']:
                self.scope["user"] = await sync_to_async(get_user_by_token)(jwt_kode)
                jwt = True
        if not jwt: # если не по JWT, то пользователя проверяет channels
            self.scope["user"] = await channels.auth.get_user(self.scope)
        # нужно сделать через сигналы, т.к. так не работает
        # if self.scope["user"].is_authenticated:
        #     print("== есть пользователь = ", self.scope["user"])
        #     uzanto_websocket = None
        #     try:
        #         # uzanto_websocket = await sync_to_async(UniversoUzantojWebsocket.objects.get)(posedanto = self.scope["user"])
        #         uzanto_websocket = await database_sync_to_async(UniversoUzantojWebsocket.objects.get)(posedanto = self.scope["user"])
        #         print("== нашли данные = ", uzanto_websocket)
        # #         uzanto_websocket = UniversoUzantojWebsocket.objects.get(
        # #             posedanto = self.scope["user"]
        # #         )

        #     except UniversoUzantojWebsocket.DoesNotExist:
        #         print("== ошибка нахождения, пытаемся создать  = ")
        #         pass
        #         # uzanto_websocket = await sync_to_async(UniversoUzantojWebsocket.objects.create)(
        #         uzanto_websocket = await database_sync_to_async(UniversoUzantojWebsocket.objects.create)(
        #             posedanto = self.scope["user"],
        #             publikigo = True,
        #             publikiga_dato=timezone.now(),
        #             online = True,
        #         )
        #         print("== создали данные = ", uzanto_websocket)
        #     uzanto_websocket.online = True
        #     # sync_to_async(uzanto_websocket.save)()
        #     sync_to_async(uzanto_websocket.save)()
        #     sync_to_async(transaction.on_commit)()
        #     print("== подправили и сохранили данные = ", uzanto_websocket)



    #для метода disconnect(self, code)
    async def disconnect(self, code):
        print('disconnect')
        # вызываем родительский метод
        await super().disconnect(code)

        # Тут ниже свой код
        # self.scope["user"] = await channels.auth.get_user(self.scope)
        # нужно сделать через сигналы, т.к. так не работает
        # if self.scope["user"].is_authenticated:
        #     uzanto_websocket = None
        #     try:
        #         # uzanto_websocket = await database_sync_to_async(UniversoUzantojWebsocket.objects.get)(
        #         uzanto_websocket = await sync_to_async(UniversoUzantojWebsocket.objects.get)(
        #             posedanto = self.scope["user"]
        #         )
        #         uzanto_websocket.online = False
        #         uzanto_websocket.subscription_kosmo = False
        #         uzanto_websocket.subscription_kosmostacio = False
        #         uzanto_websocket.subscription_mesagxilo = False
        #         sync_to_async(uzanto_websocket.save)()
        #         # database_sync_to_async(uzanto_websocket.save)()
        #     except UniversoUzantojWebsocket.DoesNotExist:
        #         print('не сохранили закрытие ws')
        #         pass

