"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""


from django.utils.deprecation import MiddlewareMixin
from django.utils import translation


class SiriusoUzantoLingvo(MiddlewareMixin):
    def process_request(self, request):
        if 'HTTP_X_CLIENT_LANG' in request.META:
            lingvo = request.META['HTTP_X_CLIENT_LANG']

            if translation.check_for_language(lingvo):
                translation.activate(lingvo)

                if hasattr(request, 'session'):
                    request.session[translation.LANGUAGE_SESSION_KEY] = lingvo
                    request.session.modified = True

        if 'HTTP_X_CONTENT_LANG' in request.META:
            content_lingvo = request.META['HTTP_X_CONTENT_LANG']

            if translation.check_for_language(content_lingvo):
                request.CONTENT_LANGUAGE_CODE = content_lingvo
