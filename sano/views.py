"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render, get_object_or_404
from django.template.response import TemplateResponse
from django.utils import timezone
from .models import *
from main.models import Uzanto
from siriuso.views import SiriusoTemplateView


# Задачи здоровья
class SanoTasko(SiriusoTemplateView):

    template_name = 'sano/sano_uzanto.html'
    mobile_template_name = 'sano/portebla/t_sano_uzanto.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_uzanto = Uzanto.objects.values('id', 'uuid', 'sekso', 'unua_nomo__enhavo', 'familinomo__enhavo',
                                                'fotoj_fotojuzantojavataro_posedanto__bildo', 'uzantojkovrilo__bildo',
                                                'uzantojstatuso__teksto__enhavo')

        uzanto = get_object_or_404(queryset_uzanto, id=kwargs['uzanto_id'], is_active=True, konfirmita=True,
                                   is_superuser=False)

        taskoj = SanoXeneralaTasko.objects \
            .filter(krea_dato__lte=timezone.now(), publikigo=True, arkivo=False, forigo=False,
                    autoro__komunumojorganizomembro__posedanto__tipo__kodo='gvardio') \
            .order_by('krea_dato').values('uuid', 'id', 'priskribo__enhavo', 'autoro__id',
                                          'autoro__unua_nomo__enhavo', 'autoro__familinomo__enhavo',
                                          'autoro__fotoj_fotojuzantojavataro_posedanto__bildo_min',
                                          'autoro__komunumojorganizomembro__laborarolo__nomo__enhavo')

        context.update({'taskoj': taskoj})

        return context
