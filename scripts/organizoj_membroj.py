"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from main.models import Uzanto
from komunumoj.models import KomunumojOrganizo, KomunumojOrganizoMembro, KomunumojOrganizoMembroTipo
from django.db.models import Q


def run(**kwargs):
    q = {}
    if 'uzanto' in kwargs:
        q['id'] = kwargs['uzanto']

    uzantoj = Uzanto.objects.filter(**q)
    tipoj = ('membro-adm', 'membro-mod', 'membro', 'moderiganto', 'administranto', 'abonanto')

    for u in uzantoj:
        print('Пользователь:', u)
        qo = {}

        if 'organizo' in kwargs:
            qo['uuid'] = kwargs['organizo']

        organizoj = KomunumojOrganizo.objects.filter(uuid__in=(
            KomunumojOrganizoMembro.objects.filter(autoro=u, forigo=False).values_list('posedanto_id', flat=True)
        ), **qo)

        if organizoj.count():
            for o in organizoj:
                print('==>', o, '<==')
                membroj = KomunumojOrganizoMembro.objects.select_related('tipo').filter(posedanto=o, autoro=u, forigo=False)

                if membroj.count() > 1:
                    for t in tipoj:
                        res = (KomunumojOrganizoMembro.objects
                               .filter(posedanto=o, autoro=u, forigo=False, tipo__kodo=t)
                               .order_by('krea_dato'))

                        if res.count():
                            print('найдено членство с типом ', t)

                            if res.count() > 1:
                                print('найдено больше 1 членства с типом ', t)
                                print('удаляем лишние членства...')

                                for r in res[0:res.count()-1]:
                                    r.forigo = True
                                    r.save()

                            malbone = KomunumojOrganizoMembro.objects.filter(~Q(tipo__kodo=t), posedanto=o,
                                                                              autoro=u, forigo=False)

                            for m in malbone:
                                print('удаляем членство с типом ', m.tipo.kodo, '...')
                                m.forigo = True
                                m.save()
                else:
                    print('имеет 1 запись членства с типом ', membroj[0].tipo.kodo)
        else:
            print('не участвует в организациях, пропускаем')
