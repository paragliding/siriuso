"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения


# Модель категорий навыков
class ScipovoKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ScipovoKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель типов навыков
class ScipovoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ScipovoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель уровней навыков
class ScipovoNiveloNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ScipovoNivelo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель навыков
class ScipovoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Информация'))

    class Meta:
        model = Scipovo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель навыков объектов
class ScipovoObjektoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ScipovoObjekto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'objekto__uuid': ['exact'],
            'scipovo__id': ['exact'],
            'nivelo__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class ScipovojQuery(graphene.ObjectType):
    scipovo_kategorio = SiriusoFilterConnectionField(
        ScipovoKategorioNode,
        description=_('Выводит все доступные модели категорий навыков')
    )
    scipovo_tipo = SiriusoFilterConnectionField(
        ScipovoTipoNode,
        description=_('Выводит все доступные модели типов навыков')
    )
    scipovo_nivelo = SiriusoFilterConnectionField(
        ScipovoNiveloNode,
        description=_('Выводит все доступные модели уровней навыков')
    )
    scipovo = SiriusoFilterConnectionField(
        ScipovoNode,
        description=_('Выводит все доступные модели навыков')
    )
    scipovo_objecto = SiriusoFilterConnectionField(
        ScipovoObjektoNode,
        description=_('Выводит все доступные модели навыков объектов')
    )
