"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from .models import *


# Типы категорий декретумов
@admin.register(DekretumojKategorioTipo)
class DekretumojKategorioTipoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojKategorioTipo._meta.fields]

    class Meta:
        model = DekretumojKategorioTipo


# Категории декретумов советов
@admin.register(DekretumojKategorioSoveto)
class DekretumojKategorioSovetoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojKategorioSoveto._meta.fields]

    class Meta:
        model = DekretumojKategorioSoveto


# Типы исследований
@admin.register(DekretumojDekretumoTipo)
class DekretumojDekretumoTipoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojDekretumoTipo._meta.fields]

    class Meta:
        model = DekretumojDekretumoTipo


# Исследования
@admin.register(DekretumojDekretumoSoveto)
class DekretumojDekretumoSovetoAdmin(admin.ModelAdmin):
    list_display = [field.name for field in DekretumojDekretumoSoveto._meta.fields]

    class Meta:
        model = DekretumojDekretumoSoveto
