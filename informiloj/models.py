"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from uuid import uuid4
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
import random, string
import sys


# АБСТРАКТНЫЕ КЛАССЫ

# Абстрактный класс справочников
class InformilojBazaAbstrakta(models.Model):

    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # дата и время изменения
    renoviga_dato = models.DateTimeField(_('Renoviga dato'), auto_now_add=False, auto_now=True, blank=True, null=True)

    # помечена на удаление (да или нет)
    forigo = models.BooleanField(_('Forigo'), blank=True, default=False)

    # дата и время пометки на удаление
    foriga_dato = models.DateTimeField(_('Foriga dato'), auto_now_add=False, auto_now=False, blank=True, null=True,
                                       default=None)

    # дата и время автоудаления
    a_foriga_dato = models.DateTimeField(_('Aŭtomata foriga dato'), auto_now_add=False, auto_now=False, blank=True,
                                         null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}'.format(get_enhavo(self.nomo, empty_values=True)[0])


# Абстрактный класс мультиязычного контента справочников
class InformilojLingvaAbstrakta(models.Model):
    @staticmethod
    def model_name_ticket():
        return '%(app_label)s_%(class)s_'

    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    # дата и время создания
    krea_dato = models.DateTimeField(_('Krea dato'), auto_now_add=True, auto_now=False, blank=False)

    # последняя дата и время обновления
    renoviga_dato = models.DateTimeField(_('Lasta renoviga dato'), blank=True, null=True)

    # владелец (главная запись для которой создаётся мультиязычный контент)
    posedanto = models.UUIDField(_('Posedanta UUID'), blank=False, db_index=True)

    # выбор языкового кода из справочника
    lingvo = models.ForeignKey('InformilojLingvo', verbose_name=_('Lingvo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # содержимое
    enhavo = models.TextField(_('Enhavo'), null=False, blank=False)

    # главный вариант (да или нет)
    chefa_varianto = models.BooleanField(_('Ĉefa varianto'), default=False)

    # имя поля внешнего поля в родительской модели
    posedanto_kampo = None

    # дополнительные настройки для модели
    class Meta:
        # указание что это абстрактный класс
        abstract = True
        # читабельное название модели, в единственном числе
        verbose_name = _('Teksto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tekstoj')

    def __str__(self):
        return '{}'.format(self.enhavo)

    def __getattribute__(self, item):
        if item == 'teksto':
            # Тут определяется код языка, нужно будет доработать для глобального определения!!!
            return self.content(lang='ru')
        return super(InformilojLingvaAbstrakta, self).__getattribute__(item)

    def content(self, lang=None):
        enhavo = None

        if lang is None:
            return super(InformilojLingvaAbstrakta, self).__getattribute__('enhavo')

        kodo = InformilojLingvo.objects.filter(kodo=lang)

        if kodo.count():
            lingvo_model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            enhavo = lingvo_model.objects.filter(posedanto=self.posedanto, lingvo=kodo[0])

            if enhavo.count():
                return super(InformilojLingvaAbstrakta, enhavo[0]).__getattribute__('enhavo')

        # Здесь нужно будет задать сообщение при отсутствии перевода на нужный язык,
        # пока возвращается пустая строка
        return super(InformilojLingvaAbstrakta, self).__getattribute__('enhavo')

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super(InformilojLingvaAbstrakta, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                                update_fields=update_fields)
        # Проверяем наличие привязки у модели-владельца связи с объектом этой модели,
        # если её нет, то связываем текущий объект с объектом модели-владельца
        if not self.posedanto_kampo is None\
                and self.posedanto_kampo in dir(self.posedanto)\
                and self.posedanto.__getattribute__(self.posedanto_kampo) is None:
            self.posedanto.__setattr__(self.posedanto_kampo, self)
            self.posedanto.save()


# СПРАВОЧНИКИ

# Таблица названий cправочника стран мира, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojLandoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojLando', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_landoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Генерация случайного названия и переименование загружаемых картинок флагов стран
def informiloj_landoj_flagoj(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for _ in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'informiloj/landoj/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Справочник стран мира, использует абстрактный класс InformilojBazaAbstrakta
class InformilojLando(InformilojBazaAbstrakta):

    # код страны
    kodo = models.CharField(_('Kodo'), max_length=10)

    # телефонный код страны
    telefonakodo = models.CharField(_('Telefona kodo'), max_length=10)

    # название страны
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # иконка флага страны
    flago = models.ImageField(_('Flago'), upload_to=informiloj_landoj_flagoj, null=True)

    # если это синоним другой записи - страны (рекурсивная связь)
    sinonimo = models.ForeignKey('self', verbose_name=_('Sinonimo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_sinonimo', on_delete=models.CASCADE)

    # если это синоним региона другой страны
    sinonimo_regiono = models.ForeignKey('InformilojRegiono', verbose_name=_('Sinonimo de regiono'), blank=True,
                                         null=True, on_delete=models.CASCADE)

    # соседние страны (с которыми есть граница какого-то типа)
    apudaj_landoj = models.ManyToManyField('self', verbose_name=_('Apudaj landoj'), symmetrical=True, blank=True,
                                           related_name='%(app_label)s_%(class)s_apudaj_landoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_landoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Lando")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Landoj")


# Таблица названий cправочника регионов, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojRegionoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojRegiono', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_regionoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник регионов, использует абстрактный класс InformilojBazaAbstrakta
class InformilojRegiono(InformilojBazaAbstrakta):

    # код региона
    kodo = models.CharField(_('Kodo'), max_length=20)

    # название региона
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страна региона
    lando = models.ForeignKey(InformilojLando, verbose_name=_('Lando'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # если это синоним другой записи (рекурсивная связь)
    sinonimo = models.ForeignKey('self', verbose_name=_('Sinonimo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_sinonimo', on_delete=models.CASCADE)

    # соседние регионы
    apudaj_regionoj = models.ManyToManyField('self', verbose_name=_('Apudaj regionoj'), symmetrical=True, blank=True,
                                             related_name='%(app_label)s_%(class)s_apudaj_regionoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_regionoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Regiono")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Regionoj")

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле speco этой модели
        return '{} - {}'.format(get_enhavo(self.nomo, empty_values=True)[0], self.lando)


# Таблица названий типов небесных тел, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojAstroTipoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojAstroTipo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_astroj_tipoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Типы небесных тел, использует абстрактный класс InformilojBazaAbstrakta
class InformilojAstroTipo(InformilojBazaAbstrakta):

    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_astroj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de astroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de astroj')


# Таблица названий справочника небесных тел, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojAstroNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojAstro', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_astroj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник небесных тел, использует абстрактный класс InformilojBazaAbstrakta
class InformilojAstro(InformilojBazaAbstrakta):

    # тип небесного тела
    tipo = models.ForeignKey(InformilojAstroTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код небесного тела
    kodo = models.CharField(_('Kodo'), max_length=20)

    # название небесного тела
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # главное (родительское) небесное тело
    qefa_astro = models.ManyToManyField('self', verbose_name=_('Ĉefa astro'), symmetrical=True, blank=True,
                                        related_name='%(app_label)s_%(class)s_qefa_astro')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_astroj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Astro")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Astroj")


# Таблица названий cправочника макрорегионов Технокома, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojMakroregionoTeknokomoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojMakroregionoTeknokomo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_makroregionoj_teknokomo_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник макрорегионов Технокома, использует абстрактный класс InformilojBazaAbstrakta
class InformilojMakroregionoTeknokomo(InformilojBazaAbstrakta):

    # код макрорегиона технокома
    kodo = models.CharField(_('Kodo'), max_length=20)

    # название макрорегиона технокома
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страны макрорегиона технокома
    lando = models.ManyToManyField(InformilojLando, verbose_name=_('Landoj'), blank=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_makroregionoj_teknokomo'
        # читабельное название модели, в единственном числе
        verbose_name = _("Makroregiono de Teknokomo")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Makroregionoj de Teknokomo")


# Таблица названий cправочника мезорегионов Технокома, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojMezoregionoTeknokomoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojMezoregionoTeknokomo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_mezoregionoj_teknokomo_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник мезорегионов Технокома, использует абстрактный класс InformilojBazaAbstrakta
class InformilojMezoregionoTeknokomo(InformilojBazaAbstrakta):

    # код мезорегиона технокома
    kodo = models.CharField(_('Kodo'), max_length=20)

    # название мезорегиона технокома
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # макрорегион технокома
    makroregiono = models.ForeignKey(InformilojMakroregionoTeknokomo, verbose_name=_('Makroregiono de Teknokomo'),
                                   blank=False, default=None, on_delete=models.CASCADE)

    # соседние мезорегионы технокома
    apudaj_regionoj = models.ManyToManyField('self', verbose_name=_('Apudaj mezoregionoj de Teknokomo'), blank=True,
                                             symmetrical=True, related_name='%(app_label)s_%(class)s_apudaj_regionoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_mezoregionoj_teknokomo'
        # читабельное название модели, в единственном числе
        verbose_name = _("Mezoregiono de Teknokomo")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Mezoregionoj de Teknokomo")


# Таблица названий cправочника регионов Технокома, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojRegionoTeknokomoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojRegionoTeknokomo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_regionoj_teknokomo_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник регионов Технокома, использует абстрактный класс InformilojBazaAbstrakta
class InformilojRegionoTeknokomo(InformilojBazaAbstrakta):

    # код региона технокома
    kodo = models.CharField(_('Kodo'), max_length=20)

    # название региона технокома
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страны региона технокома
    lando = models.ManyToManyField(InformilojLando, verbose_name=_('Landoj'), blank=False, default=None)

    # макрорегион технокома
    makroregiono = models.ForeignKey(InformilojMakroregionoTeknokomo, verbose_name=_('Makroregiono de Teknokomo'),
                                   blank=False, default=None, on_delete=models.CASCADE)

    # мезорегион технокома
    mezoregiono = models.ForeignKey(InformilojMezoregionoTeknokomo, verbose_name=_('Mezoregiono de Teknokomo'),
                                    blank=True, null=True, on_delete=models.CASCADE)

    # регионы стран
    regiono_lando = models.ManyToManyField(InformilojRegiono, verbose_name=_('Regionoj de landoj'), blank=True)

    # соседние регионы технокома
    apudaj_regionoj = models.ManyToManyField('self', verbose_name=_('Apudaj regionoj de Teknokomo'), symmetrical=True,
                                             blank=True, related_name='%(app_label)s_%(class)s_apudaj_regionoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_regionoj_teknokomo'
        # читабельное название модели, в единственном числе
        verbose_name = _("Regiono de Teknokomo")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Regionoj de Teknokomo")


# Таблица названий cправочника районов, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojDistriktoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojDistrikto', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_distriktoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник районов (дистриктов, округов), использует абстрактный класс InformilojBazaAbstrakta
class InformilojDistrikto(InformilojBazaAbstrakta):

    # код района (дистрикта, округа)
    kodo = models.CharField(_('Kodo'), max_length=20, blank=False)

    # название района (дистрикта, округа)
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страна района (дистрикта, округа)
    lando = models.ForeignKey(InformilojLando, verbose_name=_('Lando'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # регион района (дистрикта, округа)
    regiono = models.ForeignKey(InformilojRegiono, verbose_name=_('Regiono'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # если это синоним другой записи (рекурсивная связь)
    sinonimo = models.ForeignKey('self', verbose_name=_('Sinonimo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_sinonimo', on_delete=models.CASCADE)

    # соседние районы (дистрикты, округа)
    apudaj_distriktoj = models.ManyToManyField('self', verbose_name=_('Apudaj distriktoj'), symmetrical=True, blank=True,
                                               related_name='%(app_label)s_%(class)s_apudaj_distriktoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_distriktoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Distrikto")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Distriktoj")


# Таблица названий типов мест, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojLokoTipoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojLokoTipo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_lokoj_tipoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Типы мест, использует абстрактный класс InformilojBazaAbstrakta
class InformilojLokoTipo(InformilojBazaAbstrakta):

    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_lokoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de lokoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de lokoj')


# Таблица названий cправочника смест, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojLokoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojLoko', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_lokoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник мест (населённых пунктов), использует абстрактный класс InformilojBazaAbstrakta
class InformilojLoko(InformilojBazaAbstrakta):

    # тип места (населённого пункта)
    tipo = models.ForeignKey(InformilojLokoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код места
    kodo = models.CharField(_('Kodo'), max_length=20, blank=False)

    # название места
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страна места (населённого пункта)
    lando = models.ForeignKey(InformilojLando, verbose_name=_('Lando'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # регион места (населённого пункта)
    regiono = models.ForeignKey(InformilojRegiono, verbose_name=_('Regiono'), blank=True, null=True,
                                related_name='%(app_label)s_%(class)s_regiono', on_delete=models.CASCADE)

    # является регионом (установление равенства с выбранным регионом)
    regiona_loko = models.OneToOneField(InformilojRegiono, verbose_name=_('Estas regiono'), blank=True, null=True,
                                        related_name='%(app_label)s_%(class)s_regiona_loko', on_delete=models.CASCADE)

    # район места (населённого пункта)
    distrikto = models.ForeignKey(InformilojDistrikto, verbose_name=_('Distrikto'), blank=True, null=True,
                                  related_name='%(app_label)s_%(class)s_distrikto', on_delete=models.CASCADE)

    # является районом (установление равенства с выбранным районом)
    distrikta_loko = models.OneToOneField(InformilojDistrikto, verbose_name=_('Estas distrikto'), blank=True, null=True,
                                          related_name='%(app_label)s_%(class)s_distrikta_loko',
                                          on_delete=models.CASCADE)

    # если это синоним другой записи (рекурсивная связь)
    sinonimo = models.ForeignKey('self', verbose_name=_('Sinonimo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_sinonimo', on_delete=models.CASCADE)

    # соседние места (населённые пункты)
    apudaj_lokoj = models.ManyToManyField('self', verbose_name=_('Apudaj lokoj'), symmetrical=True, blank=True,
                                          related_name='%(app_label)s_%(class)s_apudaj_lokoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_lokoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Loko (urbo)")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Lokoj (urboj)")


# Таблица названий типов административных округов, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojArondismentoTipoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojArondismentoTipo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_arondismentoj_tipoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Типы административных округов, использует абстрактный класс InformilojBazaAbstrakta
class InformilojArondismentoTipo(InformilojBazaAbstrakta):

    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_arondismentoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de arondismentoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de arondismentoj')


# Таблица названий административных округов, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojArondismentoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojArondismento', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_arondismentoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник административных округов (мест, городов), использует абстрактный класс InformilojBazaAbstrakta
class InformilojArondismento(InformilojBazaAbstrakta):

    # тип административного округа
    tipo = models.ForeignKey(InformilojArondismentoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код административного округа
    kodo = models.CharField(_('Kodo'), max_length=20, blank=False)

    # название административного округа
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # страна административного округа
    lando = models.ForeignKey(InformilojLando, verbose_name=_('Lando'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # регион административного округа
    regiono = models.ForeignKey(InformilojRegiono, verbose_name=_('Regiono'), blank=True, null=True,
                                on_delete=models.CASCADE)

    # район административного округа
    distrikto = models.ForeignKey(InformilojDistrikto, verbose_name=_('Distrikto'), blank=True, null=True,
                                  related_name='%(app_label)s_%(class)s_distrikto', on_delete=models.CASCADE)

    # является районом (установление равенства с выбранным районом)
    distrikta_arondismento = models.OneToOneField(InformilojDistrikto, verbose_name=_('Estas distrikto'), blank=True,
                                                  null=True, related_name='%(app_label)s_%(class)s_distr_arondismento',
                                                  on_delete=models.CASCADE)

    # место (город) административного округа
    loko = models.ForeignKey(InformilojLoko, verbose_name=_('Loko (urbo)'), blank=True, null=True,
                             related_name='%(app_label)s_%(class)s_loko', on_delete=models.CASCADE)

    # является районом (установление равенства с выбранным районом)
    loka_arondismento = models.OneToOneField(InformilojLoko, verbose_name=_('Estas loko (urbo)'), blank=True, null=True,
                                             related_name='%(app_label)s_%(class)s_loka_arondismento',
                                             on_delete=models.CASCADE)

    # если это синоним другой записи (рекурсивная связь)
    sinonimo = models.ForeignKey('self', verbose_name=_('Sinonimo'), blank=True, null=True,
                                 related_name='%(app_label)s_%(class)s_sinonimo', on_delete=models.CASCADE)

    # соседние административного округа
    apudaj_arondismentoj = models.ManyToManyField('self', verbose_name=_('Apudaj lokoj'), symmetrical=True, blank=True,
                                                  related_name='%(app_label)s_%(class)s_apudaj_lokoj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_arondismentoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Arondismento')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Arondismentoj')


# Генерация случайного названия и переименование загружаемых картинок флагов языков
def informiloj_lingvoj_flagoj(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for _ in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'informiloj/lingvoj/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Справочник языков, использует абстрактный класс InformilojBazaAbstrakta
class InformilojLingvo(InformilojBazaAbstrakta):

    # код языка (пяти символьный)
    kodo = models.CharField(_('Kodo'), max_length=5, blank=False)

    # название языка (на этом языке)
    nomo = models.CharField(_('Nomo'), max_length=32, blank=False)

    # иконка флага языка
    flago = models.ImageField(_('Flago'), upload_to=informiloj_lingvoj_flagoj, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_lingvoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Lingvo")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Lingvoj")

    def __str__(self):
        return '{}'.format(self.nomo)


# Таблица названий cправочника семейных положений, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojFamiliaStatoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojFamiliaStato', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_familiaj_statoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник семейных положений, использует абстрактный класс InformilojBazaAbstrakta
class InformilojFamiliaStato(InformilojBazaAbstrakta):

    # код семейного положения
    kodo = models.CharField(_('Kodo'), max_length=10, blank=False)

    # семейное положение
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_familiaj_statoj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Familia stato")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Familiaj statoj")


# Таблица названий типов рабочих ролей, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojLaboraroloTipoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojLaboraroloTipo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_laboraroloj_tipoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Типы рабочих ролей, использует абстрактный класс InformilojBazaAbstrakta
class InformilojLaboraroloTipo(InformilojBazaAbstrakta):

    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_laboraroloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de laboraroloj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de laboraroloj')


# Таблица названий рабочих ролей, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojLaboraroloNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojLaborarolo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_laboraroloj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Справочник рабочих ролей, использует абстрактный класс InformilojBazaAbstrakta
class InformilojLaborarolo(InformilojBazaAbstrakta):

    # тип рабочей роли
    tipo = models.ForeignKey(InformilojLaboraroloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # код рабочей роли
    kodo = models.CharField(_('Kodo'), max_length=20, blank=False)

    # специальная рабочая роль (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название рабочей роли
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_laboraroloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Laborarolo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Laboraroloj')


# Таблица названий типов уведомлений, использует абстрактный класс InformilojLingvaAbstrakta
class InformilojSciigoTipoNomo(InformilojLingvaAbstrakta):

    # имя модели в которой нужное поле, имя взято в кавычки, так как класс ещё не объявлен
    posedanto = models.ForeignKey('InformilojSciigoTipo', null=True, on_delete=models.SET_NULL)

    # название поля для которого создаётся многоязычный контент (для лёгкого доступа из него)
    posedanto_kampo = 'nomo'

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_sciigoj_tipoj_nomoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Nomo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Nomoj')


# Типы уведомлений, использует абстрактный класс InformilojBazaAbstrakta
class InformilojSciigoTipo(InformilojBazaAbstrakta):

    # Код типа
    kodo = models.CharField(_('Kodo'), max_length=16)

    # специальный тип (да или нет)
    speciala = models.BooleanField(_('Speciala'), default=False)

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_sciigoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de sciigoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de sciigoj')


# Справочник местонахождение / местоположение, использует абстрактный класс InformilojBazaAbstrakta
class InformilojUrboj(InformilojBazaAbstrakta):

    # код места
    kodo = models.CharField(_('Kodo'), max_length=20, blank=True, null=True, default=None)

    # название места
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'informilo_urboj'
        # читабельное название модели, в единственном числе
        verbose_name = _("Urboj")
        # читабельное название модели, во множественном числе
        verbose_name_plural = _("Urboj")
        # права
        permissions = (
            ('povas_vidi_informilo_urboj', _('Povas vidi informilo de urboj')),
            ('povas_krei_informilo_urboj', _('Povas krei informilo de urboj')),
            ('povas_forigi_informilo_urboj', _('Povas forigi informilo de urboj')),
            ('povas_shangxi_informilo_urboj', _('Povas ŝanĝi informilo de urboj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.kodo, get_enhavo(self.nomo, empty_values=True)[0])

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('informiloj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'informiloj.povas_vidi_informilo_urboj', 
                'informiloj.povas_krei_informilo_urboj',
                'informiloj.povas_forigi_informilo_urboj',
                'informiloj.povas_shangxi_informilo_urboj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='informiloj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('informiloj.povas_vidi_informilo_urboj')
                    or user_obj.has_perm('informiloj.povas_vidi_informilo_urboj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('informiloj.povas_vidi_informilo_urboj'):
                # Если есть права на просмотр
                cond = models.Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = models.Q(uuid__isnull=True)

        return cond

