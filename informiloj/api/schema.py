"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from graphene import relay, ObjectType, Field
from graphene_django import DjangoObjectType
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId
from graphene_permissions.permissions import AllowAny
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.types import SiriusoLingvo
from siriuso.utils import build_absolute_uri
from ..models import *
import re


class InformilojLabororoloNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    nomo = Field(SiriusoLingvo)

    class Meta:
        model = InformilojLaborarolo
        fields = ['uuid', 'kodo', 'speciala', 'nomo']
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', ],
            'nomo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (relay.Node,)


class InformilojLingvoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = InformilojLingvo
        fields = ['uuid', 'kodo', 'nomo', 'flago']
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact',],
            'nomo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (relay.Node,)

    def resolve_flago(self, info):
        if getattr(self, 'flago', None) and hasattr(self.flago, 'url'):
            return info.context.build_absolute_uri(self.flago.url)
        return None


class InformilojLandoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = Field(SiriusoLingvo)

    class Meta:
        model = InformilojLando
        fields = ['uuid', 'kodo', 'telefonakodo', 'nomo', 'flago']
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'telefonakodo': ['exact', 'icontains', 'istartswith']
        }
        interfaces = (relay.Node,)

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        if re.search(r'^/static/', str(bildo)):
            image = bildo.name
        else:
            image = getattr(bildo, 'url') if bildo else default

        return build_absolute_uri(request, image) if image else None

    def resolve_flago(self, info):
        return InformilojLandoNode.__resolve_bildo(info.context, getattr(self, 'flago'))


class InformilojRegionoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'lando__nomo__enhavo': ['contains', 'icontains', 'istartswith'],
    }

    nomo = Field(SiriusoLingvo)

    class Meta:
        model = InformilojRegiono
        fields = ['uuid', 'kodo', 'nomo', 'lando']
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
            'lando__uuid': ['exact'],
            'lando__kodo': ['exact', 'icontains', 'istartswith'],
            'lando': ['exact']
        }
        interfaces = (relay.Node,)

class InformilojSciigoTipoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = Field(SiriusoLingvo, description=_('Наименование типа уведомлений'))

    class Meta:
        model = InformilojSciigoTipo
        fields = ['uuid', 'kodo', 'nomo',]
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


# Справочник местонахождение / местоположение
class InformilojUrbojNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = Field(SiriusoLingvo, description=_('Наименование'))

    class Meta:
        model = InformilojUrboj
        fields = ['uuid', 'kodo', 'nomo',]
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


class InformiloQuery(ObjectType):
    informilo_lingvo = relay.Node.Field(InformilojLingvoNode)
    informiloj_lingvoj = SiriusoFilterConnectionField(InformilojLingvoNode)

    informilo_lando = relay.Node.Field(InformilojLandoNode)
    informiloj_landoj = SiriusoFilterConnectionField(InformilojLandoNode)

    informilo_regiono = relay.Node.Field(InformilojRegionoNode)
    informiloj_regionoj = SiriusoFilterConnectionField(InformilojRegionoNode)

    informiloj_sciigoj = SiriusoFilterConnectionField(
       InformilojSciigoTipoNode,
       description=_('Выводит всех наименования типов уведомлений')
    )
    informiloj_urboj = SiriusoFilterConnectionField(
       InformilojUrbojNode,
       description=_('Выводит все местонахождения / местоположения')
    )
