"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from .models import InformilojLingvo


def get_infolingvo_by_code(code):
    try:
        return InformilojLingvo.objects.get(kodo=code, forigo=False)
    except InformilojLingvo.DoesNotExist:
        return None
