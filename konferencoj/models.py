"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.db.models import Max, Q
from django.contrib.auth.models import Permission
from django.utils.translation import gettext_lazy as _
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo
from siriuso.utils import perms
from main.models import SiriusoBazaAbstraktaKomunumoj
from main.models import SiriusoKomentoAbstrakta
from main.models import SiriusoTipoAbstrakta, Uzanto
from komunumoj.models import KomunumojAliro
from komunumoj.models import Komunumo, KomunumoMembro
from informiloj.models import InformilojSciigoTipo
import sys


# Функционал коференций
# Типы категорий тем (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class KonferencojKategorioTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'konferencoj_kategorioj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de kategorioj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de kategorioj')
        # права
        permissions = (
            ('povas_vidi_kategorian_tipon', _('Povas vidi kategorian tipon')),
            ('povas_krei_kategorian_tipon', _('Povas krei kategorian tipon')),
            ('povas_forigi_kategorian_tipon', _('Povas forigu kategorian tipon')),
            ('povas_shanghi_kategorian_tipon', _('Povas ŝanĝi kategorian tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Категории тем групп, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class KonferencojKategorio(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (Сообщество)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=True, null=True, default=None,
                                  on_delete=models.SET_NULL)
    # тип категорий
    tipo = models.ForeignKey(KonferencojKategorioTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий категорий, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний категории, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # доступ к категории
    aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Aliro'), blank=False, default=None,
                              on_delete=models.CASCADE)

    # администраторы категории
    administrantoj = models.ManyToManyField('main.Uzanto', verbose_name=_('Kategoriaj administrantoj'),
                                            related_name='konferencojkategorio_administrantoj')

    # модераторы категории
    moderatoroj = models.ManyToManyField('main.Uzanto', verbose_name=_('Kategoriaj moderatoroj'),
                                         related_name='konferencojkategorio_moderatoroj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'konferencoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj')
        # права
        permissions = (
            ('povas_vidi_kategorion', _('Povas vidi kategorion')),
            ('povas_krei_kategorion', _('Povas krei kategorion')),
            ('povas_forigi_kategorion', _('Povas forigu kategorion')),
            ('povas_shanghi_kategorion', _('Povas ŝanĝi kategorion')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KonferencojKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения конференций
            all_perms = set(perms.user_registrita_perms(apps=('konferencoj',)))

            # Определяем тип членства для пользователя в родительских собществах
            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                # Если есть права администратора или администратора сообщества
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('konferencoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                # Если есть права модератора или модератора сообщества
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('konferencoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'konferencoj.povas_vidi_kategorion', 'konferencoj.povas_krei_kategorion',
                'konferencoj.povas_forigi_kategorion', 'konferencoj.povas_shanghi_kategorion',
                'konferencoj.povas_krei_teman_tipon'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='konferencoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('konferencoj.povas_vidi_kategorion')
                    or user_obj.has_perm('konferencoj.povas_vidi_kategorion')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('konferencoj.povas_vidi_kategorion'):
                    # Однако, если есть родительские сообщества, где пользователь является администратором,
                    # получаем список uuid для этих сообществ
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('konferencoj.povas_vidi_kategorion'):
                    # Или сообщества, где пользователь является модератором,
                    # получаем список uuid для этих сообществ
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    # Добавляем условие через логическое ИЛИ (|=)
                    cond |= Q(posedanto_id__in=mods)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('konferencoj.povas_vidi_kategorion'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы тем конференций (справочник), использует абстрактный класс SiriusoTipoAbstrakta
class KonferencojTemoTipo(SiriusoTipoAbstrakta):

    # название в таблице названий типов, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'konferencoj_temoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de temoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de temoj')
        # права
        permissions = (
            ('povas_vidi_teman_tipon', _('Povas vidi teman tipon')),
            ('povas_krei_teman_tipon', _('Povas krei teman tipon')),
            ('povas_forigi_teman_tipon', _('Povas forigu teman tipon')),
            ('povas_shanghi_teman_tipon', _('Povas ŝanĝi teman tipon')),
        )

    @staticmethod
    def _get_perm_cond(user_obj):
        return Q()


# Темы конференций групп, использует абстрактный класс SiriusoBazaAbstraktaKomunumoj
class KonferencojTemo(SiriusoBazaAbstraktaKomunumoj):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # владелец (сообщество)
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=True, null=True, default=None,
                                  on_delete=models.CASCADE)

    # категория
    kategorio = models.ForeignKey(KonferencojKategorio, verbose_name=_('Kategorio'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # тип темы
    tipo = models.ForeignKey(KonferencojTemoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название в таблице названий тем, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание в таблице описаний темы, от туда будет браться описание с нужным языковым тегом
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)
 
    # тема закрыта (да или нет)
    fermita = models.BooleanField(_('Fermita'), blank=False, default=False)

    # кому разрешено писать в закрытую тему
    komentado_aliro = models.ForeignKey(KomunumojAliro, verbose_name=_('Komentada aliro'), blank=False, default=None,
                                        on_delete=models.CASCADE)

    # тема закреплена (да или нет)
    fiksa = models.BooleanField(_('Fiksa'), blank=False, default=False)

    # позиция среди закреплённых тем
    fiksa_listo = models.IntegerField(_('Fiksa listo'), blank=True, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'konferencoj_temoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Temo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Temoj')
        # права
        permissions = (
            ('povas_vidi_temon', _('Povas vidi temon')),
            ('povas_krei_temon', _('Povas krei temon')),
            ('povas_forigi_temon', _('Povas forigu temon')),
            ('povas_shanghi_temon', _('Povas ŝanĝi temon')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KonferencojTemo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            all_perms = set(perms.user_registrita_perms(apps=('konferencoj',)))

            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('konferencoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('konferencoj',)))

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'konferencoj.povas_vidi_temon', 'konferencoj.povas_krei_temon',
                'konferencoj.povas_forigi_temon', 'konferencoj.povas_shanghi_temon',
                'konferencoj.povas_krei_teman_komenton'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='konferencoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('konferencoj.povas_vidi_temon')
                    or user_obj.has_perm('konferencoj.povas_vidi_temon')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('konferencoj.povas_vidi_temon'):
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)

                    cond |= Q(posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('konferencoj.povas_vidi_temon'):
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    cond |= Q(posedanto_id__in=mods)

        else:
            if perms.has_neregistrita_perm('konferencoj.povas_vidi_temon'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond


# Комментарии тем конференций групп, использует абстрактный класс SiriusoKomentoAbstrakta
class KonferencojTemoKomento(SiriusoKomentoAbstrakta):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), db_index=True, null=True, default=0)

    # владелец (тема)
    posedanto = models.ForeignKey(KonferencojTemo, verbose_name=_('Posedanto'), blank=False, default=None,
                                  on_delete=models.CASCADE)

    # текст в таблице текстов комментариев тем, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # комментируемое сообщение
    komento = models.ForeignKey('self', verbose_name=_('Komento temo komento'), blank=True, default=None,
                             on_delete=models.SET_NULL, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'konferencoj_temoj_komentoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tema komento')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Temaj komentoj')
        # права
        permissions = (
            ('povas_vidi_teman_komenton', _('Povas vidi teman komenton')),
            ('povas_krei_teman_komenton', _('Povas krei teman komenton')),
            ('povas_forigi_teman_komenton', _('Povas forigu teman komenton')),
            ('povas_shanghi_teman_komenton', _('Povas ŝanĝi teman komenton')),
        )

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KonferencojTemoKomento, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('konferencoj',)))
            else:
                all_perms = set()

            membro = KomunumoMembro.objects.select_related('tipo').get(
                posedanto_id=self.posedanto.posedanto_id, autoro=user_obj, forigo=False
            )

            if membro.tipo.kodo in ('administranto', 'komunumano-adm'):
                all_perms |= set(perms.user_komunumo_adm_perms(apps=('konferencoj',)))
            elif membro.tipo.kodo in ('moderiganto', 'komunumano-mod'):
                all_perms |= set(perms.user_komunumo_mod_perms(apps=('konferencoj',)))

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'konferencoj.povas_vidi_teman_komenton', 'konferencoj.povas_krei_teman_komenton',
                'konferencoj.povas_forigi_teman_komenton', 'konferencoj.povas_shanghi_teman_komenton'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='konferencoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('konferencoj.povas_vidi_teman_komenton')
                    or user_obj.has_perm('konferencoj.povas_vidi_teman_komenton')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('konferencoj.povas_vidi_teman_komenton'):
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)

                    cond |= Q(posedanto__posedanto_id__in=adms)

                if perms.has_komunumo_mod_perms('konferencoj.povas_vidi_temon'):
                    mods = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('moderiganto', 'komunumano-mod')
                    ).values_list('posedanto_id', flat=True)
                    cond |= Q(posedanto__posedanto_id__in=mods)

        else:
            if perms.has_neregistrita_perm('konferencoj.povas_vidi_teman_komenton'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond


# Таблица истории посещений пользователями тем концеренций
class KonferencojUzantoHistorio(models.Model):
    # поле связи 1к1, одновременно и PK
    posedanto = models.OneToOneField(Uzanto, on_delete=models.CASCADE, primary_key=True)

    # JSON поле с историей посещений тем конференций
    historio = models.JSONField(verbose_name=_('История посещений'), default=dict, blank=True,
                                 db_index=True)

    class Meta:
        db_table = 'konferencoj_uzantoj_historioj'


# Таблица подписок пользователей на Темы конференций
class KonferencojSciigoj(SiriusoBazaAbstraktaKomunumoj):
    # связь с темой конференций
    temo = models.ForeignKey(KonferencojTemo, on_delete=models.CASCADE, blank=False, null=False)

    # тип способа уведомления
    sciigoj = models.ManyToManyField(InformilojSciigoTipo, verbose_name=_('Sciigo (muro)'), blank=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        # db_table = 'konferencoj_temoj_komentoj'
        # читабельное название модели, в единственном числе
        # verbose_name = _('Tema komento')
        # читабельное название модели, во множественном числе
        # verbose_name_plural = _('Temaj komentoj')
        # права
        permissions = (
            ('povas_vidi_sciigoj', _('Povas vidi sciigoj')),
            ('povas_krei_sciigoj', _('Povas krei sciigoj')),
            ('povas_forigi_sciigoj', _('Povas forigu sciigoj')),
            ('povas_shanghi_sciigoj', _('Povas ŝanĝi sciigoj')),
        )

    def _get_user_permissions(self, user_obj):
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if self.autoro == user_obj:
                all_perms = set(perms.user_registrita_perms(apps=('konferencoj',)))
            else:
                all_perms = set()

            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'konferencoj.povas_vidi_sciigoj', 'konferencoj.povas_krei_sciigoj',
                'konferencoj.povas_forigi_sciigoj', 'konferencoj.povas_shanghi_sciigoj'
            ))

            user_perms = Permission.objects.filter(content_type__app_label='konferencoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            if (perms.has_registrita_perm('konferencoj.povas_vidi_sciigoj')
                    or user_obj.has_perm('konferencoj.povas_vidi_sciigoj')):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

                if perms.has_komunumo_adm_perms('konferencoj.povas_vidi_sciigoj'):
                    adms = KomunumoMembro.objects.filter(
                        autoro=user_obj, forigo=False, tipo__kodo__in=('administranto', 'komunumano-adm')
                    ).values_list('posedanto_id', flat=True)

                    cond |= Q(posedanto__posedanto_id__in=adms)

        else:
            if perms.has_neregistrita_perm('konferencoj.povas_vidi_sciigoj'):
                cond = Q()
            else:
                cond = Q(uuid__isnull=True)

        return cond

