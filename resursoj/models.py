"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, Realeco
from organizoj.models import Organizo
from main.models import Uzanto


# Категории ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='resursoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_kategorioj', _('Povas vidi kategorioj de resursoj')),
            ('povas_krei_resursoj_kategorioj', _('Povas krei kategorioj de resursoj')),
            ('povas_forigi_resursoj_kategorioj', _('Povas forigi kategorioj de resursoj')),
            ('povas_shangxi_resursoj_kategorioj', _('Povas ŝanĝi kategorioj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoKategorio, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_kategorioj', 
                'resursoj.povas_krei_resursoj_kategorioj',
                'resursoj.povas_forigi_resursoj_kategorioj',
                'resursoj.povas_shangxi_resursoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_kategorioj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей категорий ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class ResursoKategorioLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_kategorioj_resursoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de kategorioj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de kategorioj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de kategorioj de resursoj')),
            ('povas_krei_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de kategorioj de resursoj')),
            ('povas_forigi_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de kategorioj de resursoj')),
            ('povas_sxangxi_resursoj_kategorioj_resursoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de kategorioj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoKategorioLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'resursoj.povas_krei_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'resursoj.povas_forigi_resursoj_kategorioj_resursoj_ligiloj_tipoj',
                'resursoj.povas_sxangxi_resursoj_kategorioj_resursoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь категорий ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class ResursoKategorioLigilo(UniversoBazaMaks):

    # категория ресурсов владелец связи
    posedanto = models.ForeignKey(ResursoKategorio, verbose_name=_('Kategorio - posedanto'), blank=False,
                                  null=False, default=None, on_delete=models.CASCADE)

    # связываемая категория ресурсов
    ligilo = models.ForeignKey(ResursoKategorio, verbose_name=_('Kategorio - ligilo'), blank=False, null=False,
                               default=None, related_name='%(app_label)s_%(class)s_ligilo', on_delete=models.CASCADE)

    # тип связи категорий ресурсов
    tipo = models.ForeignKey(ResursoKategorioLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_kategorioj_resursoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de kategorioj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de kategorioj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas vidi ligiloj de kategorioj de resursoj')),
            ('povas_krei_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas krei ligiloj de kategorioj de resursoj')),
            ('povas_forigi_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas forigi ligiloj de kategorioj de resursoj')),
            ('povas_sxangxi_resursoj_kategorioj_resursoj_ligiloj',
             _('Povas ŝanĝi ligiloj de kategorioj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj',
                'resursoj.povas_krei_resursoj_kategorioj_resursoj_ligiloj',
                'resursoj.povas_forigi_resursoj_kategorioj_resursoj_ligiloj',
                'resursoj.povas_sxangxi_resursoj_kategorioj_resursoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_kategorioj_resursoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='resursoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_tipoj', _('Povas vidi tipoj de resursoj')),
            ('povas_krei_resursoj_tipoj', _('Povas krei tipoj de resursoj')),
            ('povas_forigi_resursoj_tipoj', _('Povas forigi tipoj de resursoj')),
            ('povas_shangxi_resursoj_tipoj', _('Povas ŝanĝi tipoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_tipoj', 
                'resursoj.povas_krei_resursoj_tipoj',
                'resursoj.povas_forigi_resursoj_tipoj',
                'resursoj.povas_shangxi_resursoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Классы ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoKlaso(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='resursoj_klasoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_klasoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Klaso de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Klasoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_klasoj', _('Povas vidi klasoj de resursoj')),
            ('povas_krei_resursoj_klasoj', _('Povas krei klasoj de resursoj')),
            ('povas_forigi_resursoj_klasoj', _('Povas forigi klasoj de resursoj')),
            ('povas_shangxi_resursoj_klasoj', _('Povas ŝanĝi klasoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoKlaso, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_klasoj',
                'resursoj.povas_krei_resursoj_klasoj',
                'resursoj.povas_forigi_resursoj_klasoj',
                'resursoj.povas_shangxi_resursoj_klasoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_klasoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_klasoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_klasoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Справочник ресурсов, использует абстрактный класс UniversoBazaMaks
class Resurso(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # категория ресурсов
    kategorio = models.ManyToManyField(ResursoKategorio, verbose_name=_('Kategorio'),
                                       db_table='resursoj_kategorioj_ligiloj')

    # тип ресурсов
    tipo = models.ForeignKey(ResursoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # класс ресурсов
    klaso = models.ForeignKey(ResursoKlaso, verbose_name=_('Klaso'), blank=True, null=True, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='resursoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Resurso')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj', _('Povas vidi resursoj')),
            ('povas_krei_resursoj', _('Povas krei resursoj')),
            ('povas_forigi_resursoj', _('Povas forigi resursoj')),
            ('povas_shangxi_resursoj', _('Povas ŝanĝi resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(Resurso, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                          update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj', 
                'resursoj.povas_krei_resursoj',
                'resursoj.povas_forigi_resursoj',
                'resursoj.povas_shangxi_resursoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Модификации ресурсов и их характеристики UniversoBazaMaks
class ResursoModifo(UniversoBazaMaks):

    # ресурс владелец модификации
    posedanto = models.ForeignKey(Resurso, verbose_name=_('Posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID модификации в рамках ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность ресурса
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # мощность ресурса
    potenco = models.IntegerField(_('Potenco'), blank=True, null=True, default=None)

    # внутренний объём
    volumeno_interna = models.FloatField(_('Interna volumeno'), blank=True, null=True, default=None)

    # внешний объём
    volumeno_ekstera = models.FloatField(_('Ekstera volumeno'), blank=True, null=True, default=None)

    # объём хронения (внешний)
    volumeno_stokado = models.FloatField(_('Volumeno de stokado'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_modifoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Modifo de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Modifoj de resursoj')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_resursoj_modifoj', _('Povas vidi modifoj de resursoj')),
            ('povas_krei_resursoj_modifoj', _('Povas krei modifoj de resursoj')),
            ('povas_forigi_resursoj_modifoj', _('Povas forigi modifoj de resursoj')),
            ('povas_sxangxi_resursoj_modifoj', _('Povas ŝanĝi modifoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoModifo, self).save(force_insert=force_insert, force_update=force_update,
                                                using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_modifoj',
                'resursoj.povas_krei_resursoj_modifoj',
                'resursoj.povas_forigi_resursoj_modifoj',
                'resursoj.povas_sxangxi_resursoj_modifoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_modifoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_modifoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_modifoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Характеристики модификаций ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoModifoKarakterizadoTipo(UniversoBazaMaks):

    # уникальный личный ID характеристики модификации
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_modifoj_karakterizadoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipoj de karakterizadoj de modifoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de karakterizadoj de modifoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_modifoj_karakterizadoj_tipoj', _('Povas vidi tipoj de karakterizadoj de modifoj de resursoj')),
            ('povas_krei_resursoj_modifoj_karakterizadoj_tipoj', _('Povas krei tipoj de karakterizadoj de modifoj de resursoj')),
            ('povas_forigi_resursoj_modifoj_karakterizadoj_tipoj', _('Povas forigi tipoj de karakterizadoj de modifoj de resursoj')),
            ('povas_sxangxi_resursoj_modifoj_karakterizadoj_tipoj', _('Povas ŝanĝi tipoj de karakterizadoj de modifoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoModifoKarakterizadoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                            using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_tipoj',
                'resursoj.povas_krei_resursoj_modifoj_karakterizadoj_tipoj',
                'resursoj.povas_forigi_resursoj_modifoj_karakterizadoj_tipoj',
                'resursoj.povas_sxangxi_resursoj_modifoj_karakterizadoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Характеристики модификаций ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoModifoKarakterizado(UniversoBazaMaks):
    latero_choices = (
        ('interne', _('Interne')), # внутри
        ('ekstera', _('Ekstera')),# снаружи
        ('tuta', _('Tuta')) # целый, весь - одинаковая характеристика и для внешнего и для внутреннего состояния
    )

    # тип характеристики модификации (название характеристики) (по котороой идёт идентификация характеристики)
    tipo = models.ForeignKey(ResursoModifoKarakterizadoTipo, verbose_name=_('Tipo'), blank=True, default=None,
                            null=True, on_delete=models.CASCADE)

    # latero - сторона (геометрическая) 
    latero_karakterizado = models.CharField(_('Latero de karakterizado'), max_length=24, choices=latero_choices, blank=True,
                                            null=False, default='tuta')

    # ŝancoj - "возможности" - два варианта: "Возможно" и "Не возможно".
    #  Но в независимости от названия, суть такая, что типы определяют для характеристики суть её применения.
    #  Или характеристика определяет, что модификация ресурса может "вот это" с "такими-то параметрами",
    #  или определяет, что модификация ресурса не может "вот это" с "такими-то параметрами".
    sxancaj = models.BooleanField(_('Sxancaj'), blank=False, default=True)

    # Ne deviga - не обязательно
    # наличие характеристики не обязательно (пример: дроны можно положить в ангар с характеристикой 
    #   запуска или в склад без возможности запуска (если параметра нет - запуск не возможен))
    deviga = models.BooleanField(_('Deviga'), blank=False, default=True)

    # единица измерения
    # unuo_mezuro = models.CharField(_('Unueco de mezuro'), max_length=24, blank=True,
    #                                null=True, default=None)

    # тип характеристики:
    # tipo_karakterizado = models.IntegerField(_('Tipo de karakterizadoj'), blank=True, null=True, default=None)
    # 1 - целочисленная
    # 2 - строчная - указывается длина строки
    # 3 - с плавающей точкой
    # 4 - валютная (плавающая точка до 2-х знаков)
    # 5 - дата

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_modifoj_karakterizadoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Karakterizadoj de modifoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Karakterizadoj de modifoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_modifoj_karakterizadoj', _('Povas vidi karakterizadoj de modifoj de resursoj')),
            ('povas_krei_resursoj_modifoj_karakterizadoj', _('Povas krei karakterizadoj de modifoj de resursoj')),
            ('povas_forigi_resursoj_modifoj_karakterizadoj', _('Povas forigi karakterizadoj de modifoj de resursoj')),
            ('povas_sxangxi_resursoj_modifoj_karakterizadoj', _('Povas ŝanĝi karakterizadoj de modifoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({} - {})'.format(self.uuid, self.tipo, self.latero_karakterizado)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_modifoj_karakterizadoj',
                'resursoj.povas_krei_resursoj_modifoj_karakterizadoj',
                'resursoj.povas_forigi_resursoj_modifoj_karakterizadoj',
                'resursoj.povas_sxangxi_resursoj_modifoj_karakterizadoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь модификаций с характеристиками модификаций, использует абстрактный класс UniversoBazaMaks
class ResursoModifoKarakterizadoLigilo(UniversoBazaMaks):

    # связи с моделью модификации ресуров
    modifo = models.ForeignKey(ResursoModifo, verbose_name=_('Modifo de resursoj'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # связи с моделью характеристики
    karakterizado = models.ForeignKey(ResursoModifoKarakterizado, verbose_name=_('Karakterizadoj de modifoj de resursoj'), blank=False,
                                          null=False, default=None, on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_modifoj_karakterizadoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de karakterizadoj de modifoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de karakterizadoj de modifoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_modifoj_karakterizadoj_ligiloj', _('Povas vidi ligiloj de karakterizadoj de modifoj de resursoj')),
            ('povas_krei_resursoj_modifoj_karakterizadoj_ligiloj', _('Povas krei ligiloj de karakterizadoj de modifoj de resursoj')),
            ('povas_forigi_resursoj_modifoj_karakterizadoj_ligiloj', _('Povas forigi ligiloj de karakterizadoj de modifoj de resursoj')),
            ('povas_sxangxi_resursoj_modifoj_karakterizadoj_ligiloj', _('Povas ŝanĝi ligiloj de karakterizadoj de modifoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_ligiloj', 
                'resursoj.povas_krei_resursoj_modifoj_karakterizadoj_ligiloj',
                'resursoj.povas_forigi_resursoj_modifoj_karakterizadoj_ligiloj',
                'resursoj.povas_sxangxi_resursoj_modifoj_karakterizadoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_ligiloj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_modifoj_karakterizadoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Состояния модификаций ресурсов, в зависимости от уровня поверждения UniversoBazaMaks
class ResursoModifoStato(UniversoBazaMaks):

    # модификация ресурса владелец состояния
    posedanto = models.ForeignKey(ResursoModifo, verbose_name=_('Posedanto'), blank=False, null=False,
                                  default=None, on_delete=models.CASCADE)

    # уникальный личный ID состояния в рамках модификации ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # предыдущее по повышению уровня целостности состояние модификации ресурса
    stato_antaua = models.ForeignKey('self', verbose_name=_('Antaŭa stato'), blank=True, null=True,
                                     default=None, related_name='%(app_label)s_%(class)s_stato_antaua',
                                     on_delete=models.CASCADE)

    # следующее по понижению уровня целостности состояние модификации ресурса
    stato_sekva = models.ForeignKey('self', verbose_name=_('Sekva stato'), blank=True, null=True,
                                     default=None, related_name='%(app_label)s_%(class)s_stato_sekva',
                                     on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # целостность ресурса
    integreco = models.IntegerField(_('Integreco'), blank=True, null=True, default=None)

    # мощность ресурса
    potenco = models.IntegerField(_('Potenco'), blank=True, null=True, default=None)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_modifoj_statoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stato de modifoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statoj de modifoj de resursoj')
        # установка уникальности личного ID места хранения в рамках объекта
        unique_together = ("id", "posedanto")
        # права
        permissions = (
            ('povas_vidi_resursoj_modifoj_statoj',
             _('Povas vidi statoj de modifoj de resursoj')),
            ('povas_krei_resursoj_modifoj_statoj',
             _('Povas krei statoj de modifoj de resursoj')),
            ('povas_forigi_resursoj_modifoj_statoj',
             _('Povas forigi statoj de modifoj de resursoj')),
            ('povas_sxangxi_resursoj_modifoj_statoj',
             _('Povas ŝanĝi statoj de modifoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoModifoStato, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_modifoj_statoj',
                'resursoj.povas_krei_resursoj_modifoj_statoj',
                'resursoj.povas_forigi_resursoj_modifoj_statoj',
                'resursoj.povas_sxangxi_resursoj_modifoj_statoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_modifoj_statoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_modifoj_statoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_modifoj_statoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы мест хранения ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoStokejoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_stokejoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de stokejoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de stokejoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_stokejoj_tipoj',
             _('Povas vidi tipoj de stokejoj de resursoj')),
            ('povas_krei_resursoj_stokejoj_tipoj',
             _('Povas krei tipoj de stokejoj de resursoj')),
            ('povas_forigi_resursoj_stokejoj_tipoj',
             _('Povas forigi tipoj de stokejoj de resursoj')),
            ('povas_shangxi_resursoj_stokejoj_tipoj',
             _('Povas ŝanĝi tipoj de stokejoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoStokejoTipo, self).save(force_insert=force_insert, force_update=force_update, using=using,
                                            update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_stokejoj_tipoj',
                'resursoj.povas_krei_resursoj_stokejoj_tipoj',
                'resursoj.povas_forigi_resursoj_stokejoj_tipoj',
                'resursoj.povas_shangxi_resursoj_stokejoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_stokejoj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_stokejoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_stokejoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Логические места хранения в ресурсах, использует абстрактный класс UniversoBazaMaks
class ResursoStokejo(UniversoBazaMaks):

    # ресурс - владелец места хранения
    posedanto = models.ForeignKey(Resurso, verbose_name=_('Posedanto'), blank=False, null=False,
                                         default=None, on_delete=models.CASCADE)

    # уникальный личный ID места хранения в рамках ресурса
    id = models.IntegerField(_('ID'), blank=True, null=True, default=None)

    # тип места хранения ресурсов на основе которого создано это место хранения в ресурсе
    tipo = models.ForeignKey(ResursoStokejoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_stokejoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Stokejo de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Stokejoj de resursoj')
        # установка уникальности личного ID места хранения в рамках объекта
        # unique_together = ("id", "posedanto_modifo")
        # права
        permissions = (
            ('povas_vidi_resursoj_stokejoj', _('Povas vidi stokejoj de resursoj')),
            ('povas_krei_resursoj_stokejoj', _('Povas krei stokejoj de resursoj')),
            ('povas_forigi_resursoj_stokejoj', _('Povas forigi stokejoj de resursoj')),
            ('povas_sxangxi_resursoj_stokejoj', _('Povas ŝanĝi stokejoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{} ({})'.format(self.uuid, get_enhavo(self.priskribo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.filter(posedanto=self.posedanto).aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoStokejo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_stokejoj',
                'resursoj.povas_krei_resursoj_stokejoj',
                'resursoj.povas_forigi_resursoj_stokejoj',
                'resursoj.povas_sxangxi_resursoj_stokejoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_stokejoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_stokejoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_stokejoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы цен ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoPrezoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_prezoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de prezoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de prezoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_prezoj_tipoj', _('Povas vidi tipoj de prezoj de resursoj')),
            ('povas_krei_resursoj_prezoj_tipoj', _('Povas krei tipoj de prezoj de resursoj')),
            ('povas_forigi_resursoj_prezoj_tipoj', _('Povas forigi tipoj de prezoj de resursoj')),
            ('povas_sxangxi_resursoj_prezoj_tipoj', _('Povas ŝanĝi tipoj de prezoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoPrezoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_prezoj_tipoj',
                'resursoj.povas_krei_resursoj_prezoj_tipoj',
                'resursoj.povas_forigi_resursoj_prezoj_tipoj',
                'resursoj.povas_sxangxi_resursoj_prezoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_prezoj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_prezoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_prezoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Виды цен ресурсов, использует абстрактный класс UniversoBazaMaks
class ResursoPrezoSpeco(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_prezoj_specoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Speco de prezoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Specoj de prezoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_prezoj_specoj', _('Povas vidi specoj de prezoj de resursoj')),
            ('povas_krei_resursoj_prezoj_specoj', _('Povas krei specoj de prezoj de resursoj')),
            ('povas_forigi_resursoj_prezoj_specoj', _('Povas forigi specoj de prezoj de resursoj')),
            ('povas_sxangxi_resursoj_prezoj_specoj', _('Povas ŝanĝi specoj de prezoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoPrezoSpeco, self).save(force_insert=force_insert, force_update=force_update,
                                                   using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_prezoj_specoj',
                'resursoj.povas_krei_resursoj_prezoj_specoj',
                'resursoj.povas_forigi_resursoj_prezoj_specoj',
                'resursoj.povas_sxangxi_resursoj_prezoj_specoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_prezoj_specoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_prezoj_specoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_prezoj_specoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус цены ресурса, использует абстрактный класс UniversoBazaMaks
class ResursoPrezoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_prezoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de prezoj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de prezoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_prezoj_statusoj',
             _('Povas vidi statusoj de prezoj de resursoj')),
            ('povas_krei_resursoj_prezoj_statusoj',
             _('Povas krei statusoj de prezoj de resursoj')),
            ('povas_forigi_resursoj_prezoj_statusoj',
             _('Povas forigi statusoj de prezoj de resursoj')),
            ('povas_sxangxi_resursoj_prezoj_statusoj',
             _('Povas ŝanĝi statusoj de prezoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoPrezoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                      using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_prezoj_statusoj',
                'resursoj.povas_krei_resursoj_prezoj_statusoj',
                'resursoj.povas_forigi_resursoj_prezoj_statusoj',
                'resursoj.povas_sxangxi_resursoj_prezoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_prezoj_statusoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_prezoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_prezoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Цены ресурсов, использует абстрактный класс UniversoBazaRealeco
class ResursoPrezo(UniversoBazaRealeco):

    # ресурс для которого устанавливается цена
    resurso = models.ForeignKey(Resurso, verbose_name=_('Resurso'), blank=False, null=False,
                                default=None, on_delete=models.CASCADE)

    # пользователь владелец цены ресурса
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         default=None, on_delete=models.CASCADE)

    # организация владелец цены ресурса
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           null=True, default=None, on_delete=models.CASCADE)

    # значение цены, максимальное значение 15 знаков, из них 2 после точки
    prezo = models.DecimalField(_('Prezo'), max_digits=15, decimal_places=2, blank=True, null=True, default=0)

    # тип цены ресурса
    tipo = models.ForeignKey(ResursoPrezoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # вид цены ресурса
    speco = models.ForeignKey(ResursoPrezoSpeco, verbose_name=_('Speco'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # статус цены ресурса
    statuso = models.ForeignKey(ResursoPrezoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_prezoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Prezo de resurso')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Prezoj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_prezoj', _('Povas vidi prezoj de resursoj')),
            ('povas_krei_resursoj_prezoj', _('Povas krei prezoj de resursoj')),
            ('povas_forigi_resursoj_prezoj', _('Povas forigi prezoj de resursoj')),
            ('povas_sxangxi_resursoj_prezoj', _('Povas ŝanĝi prezoj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле id владельца Siriuso и/или nomo организации владельца этой модели
        prezo = "{}".format(self.resurso.uuid)
        start = True
        if self.posedanto_uzanto:
            prezo = "{}: {}".format(prezo, self.posedanto_uzanto.id)
            start = False
        if self.posedanto_organizo:
            if start:
                prezo = "{}: {}".format(prezo, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
            else:
                prezo = "{}; {}".format(prezo, get_enhavo(self.posedanto_organizo.nomo, empty_values=True)[0])
        return prezo

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(ResursoPrezo, self).save(force_insert=force_insert, force_update=force_update,
                                                       using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_prezoj',
                'resursoj.povas_krei_resursoj_prezoj',
                'resursoj.povas_forigi_resursoj_prezoj',
                'resursoj.povas_sxangxi_resursoj_prezoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_prezoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_prezoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_prezoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class ResursoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de resursoj')),
            ('povas_krei_resursoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de resursoj')),
            ('povas_forigi_resursoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de resursoj')),
            ('povas_sxangxi_resursoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(ResursoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                          using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_ligiloj_tipoj', 
                'resursoj.povas_krei_resursoj_ligiloj_tipoj',
                'resursoj.povas_forigi_resursoj_ligiloj_tipoj',
                'resursoj.povas_sxangxi_resursoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_ligiloj_tipoj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь ресурсов между собой, использует абстрактный класс UniversoBazaMaks
class ResursoLigilo(UniversoBazaMaks):

    # ресурс владелец связи
    posedanto = models.ForeignKey(Resurso, verbose_name=_('Resurso - posedanto'), blank=False, null=False,
                                          default=None, on_delete=models.CASCADE)

    # место хранения владельца связи
    posedanto_stokejo = models.ForeignKey(ResursoStokejo, verbose_name=_('Stokejo de posedanto'), blank=True,
                                          null=True, default=None, on_delete=models.CASCADE)

    # разъём (слот), который используется у родительского ресурса
    konektilo_posedanto = models.IntegerField(_('Konektilo - posedanto'), blank=True, null=True, default=None)

    # связываемый ресурс
    ligilo = models.ForeignKey(Resurso, verbose_name=_('Resurso - ligilo'), blank=False, null=False,
                                       default=None, related_name='%(app_label)s_%(class)s_ligilo',
                                       on_delete=models.CASCADE)

    # разъём (слот), который используется у связываемого ресурса
    konektilo_ligilo = models.IntegerField(_('Konektilo - ligilo'), blank=True, null=True, default=None)

    # тип связи ресурсов
    tipo = models.ForeignKey(ResursoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             on_delete=models.CASCADE)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'resursoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de resursoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de resursoj')
        # права
        permissions = (
            ('povas_vidi_resursoj_ligiloj', _('Povas vidi ligiloj de resursoj')),
            ('povas_krei_resursoj_ligiloj', _('Povas krei ligiloj de resursoj')),
            ('povas_forigi_resursoj_ligiloj', _('Povas forigi ligiloj de resursoj')),
            ('povas_sxangxi_resursoj_ligiloj', _('Povas ŝanĝi ligiloj de resursoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('resursoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'resursoj.povas_vidi_resursoj_ligiloj', 
                'resursoj.povas_krei_resursoj_ligiloj',
                'resursoj.povas_forigi_resursoj_ligiloj',
                'resursoj.povas_sxangxi_resursoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='resursoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('resursoj.povas_vidi_resursoj_ligiloj')
                    or user_obj.has_perm('resursoj.povas_vidi_resursoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('resursoj.povas_vidi_resursoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
