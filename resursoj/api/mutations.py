"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель категорий ресурсов
class RedaktuResursoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_kategorioj = graphene.Field(ResursoKategorioNode,
        description=_('Созданная/изменённая категория ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            resursoj_kategorioj = ResursoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    resursoj_kategorioj.realeco.set(realeco)
                                else:
                                    resursoj_kategorioj.realeco.clear()

                            resursoj_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_kategorioj = ResursoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                resursoj_kategorioj.forigo = kwargs.get('forigo', resursoj_kategorioj.forigo)
                                resursoj_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_kategorioj.arkivo = kwargs.get('arkivo', resursoj_kategorioj.arkivo)
                                resursoj_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_kategorioj.publikigo = kwargs.get('publikigo', resursoj_kategorioj.publikigo)
                                resursoj_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        resursoj_kategorioj.realeco.set(realeco)
                                    else:
                                        resursoj_kategorioj.realeco.clear()

                                resursoj_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoKategorio(status=status, message=message, resursoj_kategorioj=resursoj_kategorioj)


# Модель типов связей категорий ресурсов между собой
class RedaktuResursoKategorioLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_kategorioj_resursoj_ligiloj_tipoj = graphene.Field(ResursoKategorioLigiloTipoNode,
        description=_('Созданная/изменённая модель типов связей категорий ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_kategorioj_resursoj_ligiloj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            resursoj_kategorioj_resursoj_ligiloj_tipoj = ResursoKategorioLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_kategorioj_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_kategorioj_resursoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_kategorioj_resursoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_kategorioj_resursoj_ligiloj_tipoj = ResursoKategorioLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_kategorioj_resursoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_kategorioj_resursoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:

                                resursoj_kategorioj_resursoj_ligiloj_tipoj.forigo = kwargs.get('forigo', resursoj_kategorioj_resursoj_ligiloj_tipoj.forigo)
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', resursoj_kategorioj_resursoj_ligiloj_tipoj.arkivo)
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', resursoj_kategorioj_resursoj_ligiloj_tipoj.publikigo)
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_kategorioj_resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_kategorioj_resursoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_kategorioj_resursoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoKategorioLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoKategorioLigiloTipo(status=status, message=message, resursoj_kategorioj_resursoj_ligiloj_tipoj=resursoj_kategorioj_resursoj_ligiloj_tipoj)


# Модель связей категорий ресурсов между собой
class RedaktuResursoKategorioLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_kategorioj_resursoj_ligiloj = graphene.Field(ResursoKategorioLigiloNode,
        description=_('Созданная/изменённая модель связей категорий ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.String(description=_('ID категории ресурсов владельца связи'))
        ligilo_id = graphene.String(description=_('ID связываемой категории ресурсов'))
        tipo_id = graphene.String(description=_('ID типа связи категорий ресурсов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_kategorioj_resursoj_ligiloj = None
        posedanto = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_kategorioj_resursoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = ResursoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная категория ресурсов владельца связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указана категория ресурсов владелец связи'),'posedanto_id')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = ResursoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoKategorio.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная связываемая категория ресурсов'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указана связываемая категория ресурсов'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ResursoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoKategorioLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи категорий ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи категорий ресурсов'),'tipo_id')

                        if not message:
                            resursoj_kategorioj_resursoj_ligiloj = ResursoKategorioLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                ligilo=ligilo,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            resursoj_kategorioj_resursoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto', False) or kwargs.get('ligilo', False)
                            or kwargs.get('tipo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_kategorioj_resursoj_ligiloj = ResursoKategorioLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_kategorioj_resursoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_kategorioj_resursoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = ResursoKategorio.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная категория ресурсов владельца связи'),'posedanto_id')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = ResursoKategorio.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoKategorio.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная связываемая категория ресурсов'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ResursoKategorioLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoKategorioLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи категорий ресурсов'),'tipo_id')

                            if not message:

                                resursoj_kategorioj_resursoj_ligiloj.forigo = kwargs.get('forigo', resursoj_kategorioj_resursoj_ligiloj.forigo)
                                resursoj_kategorioj_resursoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj.arkivo = kwargs.get('arkivo', resursoj_kategorioj_resursoj_ligiloj.arkivo)
                                resursoj_kategorioj_resursoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj.publikigo = kwargs.get('publikigo', resursoj_kategorioj_resursoj_ligiloj.publikigo)
                                resursoj_kategorioj_resursoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_kategorioj_resursoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_id', False) else resursoj_kategorioj_resursoj_ligiloj.posedanto
                                resursoj_kategorioj_resursoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_id', False) else resursoj_kategorioj_resursoj_ligiloj.ligilo
                                resursoj_kategorioj_resursoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else resursoj_kategorioj_resursoj_ligiloj.tipo

                                resursoj_kategorioj_resursoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoKategorioLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoKategorioLigilo(status=status, message=message, resursoj_kategorioj_resursoj_ligiloj=resursoj_kategorioj_resursoj_ligiloj)


# Модель типов ресурсов
class RedaktuResursoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_tipoj = graphene.Field(ResursoTipoNode, 
        description=_('Созданный/изменённый тип ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            resursoj_tipoj = ResursoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    resursoj_tipoj.realeco.set(realeco)
                                else:
                                    resursoj_tipoj.realeco.clear()

                            resursoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_tipoj = ResursoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                resursoj_tipoj.forigo = kwargs.get('forigo', resursoj_tipoj.forigo)
                                resursoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_tipoj.arkivo = kwargs.get('arkivo', resursoj_tipoj.arkivo)
                                resursoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_tipoj.publikigo = kwargs.get('publikigo', resursoj_tipoj.publikigo)
                                resursoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        resursoj_tipoj.realeco.set(realeco)
                                    else:
                                        resursoj_tipoj.realeco.clear()

                                resursoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoTipo(status=status, message=message, resursoj_tipoj=resursoj_tipoj)


# Модель классов ресурсов
class RedaktuResursoKlaso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_klasoj = graphene.Field(ResursoKlasoNode, 
        description=_('Созданный/изменённый класс ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_klasoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_klasoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            resursoj_klasoj = ResursoKlaso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_klasoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_klasoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    resursoj_klasoj.realeco.set(realeco)
                                else:
                                    resursoj_klasoj.realeco.clear()

                            resursoj_klasoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_klasoj = ResursoKlaso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_klasoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_klasoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                resursoj_klasoj.forigo = kwargs.get('forigo', resursoj_klasoj.forigo)
                                resursoj_klasoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_klasoj.arkivo = kwargs.get('arkivo', resursoj_klasoj.arkivo)
                                resursoj_klasoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_klasoj.publikigo = kwargs.get('publikigo', resursoj_klasoj.publikigo)
                                resursoj_klasoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_klasoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_klasoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_klasoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        resursoj_klasoj.realeco.set(realeco)
                                    else:
                                        resursoj_klasoj.realeco.clear()

                                resursoj_klasoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoKlaso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoKlaso(status=status, message=message, resursoj_klasoj=resursoj_klasoj)


# Модель типов мест хранения ресурсов
class RedaktuResursoStokejoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_stokejoj_tipoj = graphene.Field(ResursoStokejoTipoNode, 
        description=_('Созданный/изменённый тип мест хранения ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_stokejoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_stokejoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            resursoj_stokejoj_tipoj = ResursoStokejoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_stokejoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_stokejoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_stokejoj_tipoj = ResursoStokejoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_stokejoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_stokejoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                resursoj_stokejoj_tipoj.forigo = kwargs.get('forigo', resursoj_stokejoj_tipoj.forigo)
                                resursoj_stokejoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_stokejoj_tipoj.arkivo = kwargs.get('arkivo', resursoj_stokejoj_tipoj.arkivo)
                                resursoj_stokejoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_stokejoj_tipoj.publikigo = kwargs.get('publikigo', resursoj_stokejoj_tipoj.publikigo)
                                resursoj_stokejoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_stokejoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_stokejoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_stokejoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_stokejoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoStokejoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoStokejoTipo(status=status, message=message, resursoj_stokejoj_tipoj=resursoj_stokejoj_tipoj)


# Модель ресурсов
class RedaktuResurso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj = graphene.Field(ResursoNode, 
        description=_('Созданный/изменённый ресурс'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий ресурсов'))
        tipo_id = graphene.Int(description=_('Код типа ресурсов'))
        klaso_id = graphene.Int(description=_('Код класса ресурсов'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj = None
        realeco = Realeco.objects.none()
        kategorio = ResursoKategorio.objects.none()
        tipo = None
        klaso = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одно значение'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = ResursoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие категория ресурсов'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ResursoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип ресурсов'),'tipo_id')

                        if not message:
                            if 'klaso_id' in kwargs:
                                try:
                                    klaso = ResursoKlaso.objects.get(id=kwargs.get('klaso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoKlaso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный класс ресурсов'),'klaso_id')

                        if not message:
                            resursoj = Resurso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                klaso = klaso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    resursoj.realeco.set(realeco)
                                else:
                                    resursoj.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    resursoj.kategorio.set(kategorio)
                                else:
                                    resursoj.kategorio.clear()

                            resursoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('klaso_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj = Resurso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = ResursoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие категория ресурсов'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ResursoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoTipo.DoesNotExist:
                                        message = _('Неверный тип ресурсов')
                            if not message:
                                if 'klaso_id' in kwargs:
                                    try:
                                        klaso = ResursoKlaso.objects.get(id=kwargs.get('klaso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoKlaso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный класс ресурсов'),'klaso_id')

                            if not message:

                                resursoj.forigo = kwargs.get('forigo', resursoj.forigo)
                                resursoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj.arkivo = kwargs.get('arkivo', resursoj.arkivo)
                                resursoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj.publikigo = kwargs.get('publikigo', resursoj.publikigo)
                                resursoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj.autoro = uzanto
                                resursoj.tipo = tipo if kwargs.get('tipo_id', False) else resursoj.tipo
                                resursoj.klaso = klaso if kwargs.get('klaso_id', False) else resursoj.klaso

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        resursoj.realeco.set(realeco)
                                    else:
                                        resursoj.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        resursoj.kategorio.set(kategorio)
                                    else:
                                        resursoj.kategorio.clear()

                                resursoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Resurso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResurso(status=status, message=message, resursoj=resursoj)


# Модель логических мест хранения ресурсов
class RedaktuResursoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_stokejoj = graphene.Field(ResursoStokejoNode, 
        description=_('Созданные/изменённые логические места хранения ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('Ресурс владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения ресурсов на основе которого создано это место хранения в ресурсе'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        tipo = None
        resursoj_stokejoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')
                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoStokejoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип места хранения ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип места хранения ресурсов'),'tipo_id')

                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Resurso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Resurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс владелец места хранения'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс владелец места хранения'),'posedanto_id')

                        if not message:
                            resursoj_stokejoj = ResursoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                tipo=tipo,
                                posedanto=posedanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_id', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_stokejoj = ResursoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_stokejoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoStokejoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип места хранения ресурсов'),'tipo_id')
                                else:
                                    message = '{}: {}'.format(_('Не указан тип места хранения ресурсов'),'tipo_id')

                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = Resurso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Resurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс владелец места хранения'),'posedanto_id')
                                else:
                                    message = '{}: {}'.format(_('Не указан ресурс владелец места хранения'),'posedanto_id')

                            if not message:

                                resursoj_stokejoj.forigo = kwargs.get('forigo', resursoj_stokejoj.forigo)
                                resursoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_stokejoj.arkivo = kwargs.get('arkivo', resursoj_stokejoj.arkivo)
                                resursoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_stokejoj.publikigo = kwargs.get('publikigo', resursoj_stokejoj.publikigo)
                                resursoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else resursoj_stokejoj.tipo
                                resursoj_stokejoj.posedanto = posedanto if kwargs.get('posedanto_id', False) else resursoj_stokejoj.posedanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoStokejo(status=status, message=message, resursoj_stokejoj=resursoj_stokejoj)


# Модель типов цен ресурсов
class RedaktuResursoPrezoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_prezoj_tipoj = graphene.Field(ResursoPrezoTipoNode, 
        description=_('Созданная/изменённая модель типов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_prezoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_prezoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            resursoj_prezoj_tipoj = ResursoPrezoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_prezoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_prezoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_prezoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_prezoj_tipoj = ResursoPrezoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_prezoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_prezoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                resursoj_prezoj_tipoj.forigo = kwargs.get('forigo', resursoj_prezoj_tipoj.forigo)
                                resursoj_prezoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_prezoj_tipoj.arkivo = kwargs.get('arkivo', resursoj_prezoj_tipoj.arkivo)
                                resursoj_prezoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_prezoj_tipoj.publikigo = kwargs.get('publikigo', resursoj_prezoj_tipoj.publikigo)
                                resursoj_prezoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_prezoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_prezoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_prezoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_prezoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoPrezoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoPrezoTipo(status=status, message=message, resursoj_prezoj_tipoj=resursoj_prezoj_tipoj)


# Модель видов цен ресурсов
class RedaktuResursoPrezoSpeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_prezoj_specoj = graphene.Field(ResursoPrezoSpecoNode, 
        description=_('Созданная/изменённая модель видов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_prezoj_specoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_prezoj_specoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            resursoj_prezoj_specoj = ResursoPrezoSpeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_prezoj_specoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_prezoj_specoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_prezoj_specoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_prezoj_specoj = ResursoPrezoSpeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_prezoj_specoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_prezoj_specoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                resursoj_prezoj_specoj.forigo = kwargs.get('forigo', resursoj_prezoj_specoj.forigo)
                                resursoj_prezoj_specoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_prezoj_specoj.arkivo = kwargs.get('arkivo', resursoj_prezoj_specoj.arkivo)
                                resursoj_prezoj_specoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_prezoj_specoj.publikigo = kwargs.get('publikigo', resursoj_prezoj_specoj.publikigo)
                                resursoj_prezoj_specoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_prezoj_specoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_prezoj_specoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_prezoj_specoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_prezoj_specoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoPrezoSpeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoPrezoSpeco(status=status, message=message, resursoj_prezoj_specoj=resursoj_prezoj_specoj)


# Модель статусов цен ресурсов
class RedaktuResursoPrezoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_prezoj_statusoj = graphene.Field(ResursoPrezoStatusoNode, 
        description=_('Созданная/изменённая модель статусов цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_prezoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_prezoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            resursoj_prezoj_statusoj = ResursoPrezoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_prezoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_prezoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_prezoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_prezoj_statusoj = ResursoPrezoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_prezoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_prezoj_statusoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                resursoj_prezoj_statusoj.forigo = kwargs.get('forigo', resursoj_prezoj_statusoj.forigo)
                                resursoj_prezoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_prezoj_statusoj.arkivo = kwargs.get('arkivo', resursoj_prezoj_statusoj.arkivo)
                                resursoj_prezoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_prezoj_statusoj.publikigo = kwargs.get('publikigo', resursoj_prezoj_statusoj.publikigo)
                                resursoj_prezoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_prezoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_prezoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_prezoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_prezoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoPrezoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoPrezoStatuso(status=status, message=message, resursoj_prezoj_statusoj=resursoj_prezoj_statusoj)


# Модель цен ресурсов
class RedaktuResursoPrezo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_prezoj = graphene.Field(ResursoPrezoNode, 
        description=_('Созданная/изменённая модель цен ресурсов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        resurso_id = graphene.Int(description=_('Ресурс для которого устанавливается цена'))
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец цены ресурса'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец цены ресурса'))
        prezo = graphene.Float(description=_('Значение цены, максимальное значение 15 знаков, из них 2 после точки'))
        tipo_id = graphene.Int(description=_('Тип цены ресурса'))
        speco_id = graphene.Int(description=_('Вид цены ресурса'))
        statuso_id = graphene.Int(description=_('Статус цены ресурса'))
        realeco_id = graphene.Int(description=_('ID параллельного мира'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_prezoj = None
        resurso = None
        posedanto_uzanto = None
        posedanto_organizo = None
        tipo = None
        speco = None
        statuso = None
        realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_prezoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'resurso_id' in kwargs:
                                try:
                                    resurso = Resurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Resurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс для которого устанавливается цена'),'resurso_id')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс для которого устанавливается цена'),'resurso_id')

                        if not message:
                            if (kwargs.get('posedanto_uzanto_id', False)):
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        konfirmita=True
                                    )
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')

                        if not message:
                            if (kwargs.get('posedanto_organizo_uuid', False)):
                                try:
                                    posedanto_organizo = Organizo.objects.get(
                                        uuid=kwargs.get('posedanto_organizo_uuid'), 
                                        forigo=False,
                                        arkivo=False, 
                                        publikigo=True
                                    )
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный организация владелец цены ресурса'),
                                                               'posedanto_organizo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ResursoPrezoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                                arkivo=False, publikigo=True)
                                except ResursoPrezoTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип цены ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип цены ресурсов'),'tipo_id')

                        if not message:
                            if 'speco_id' in kwargs:
                                try:
                                    speco = ResursoPrezoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                                  arkivo=False, publikigo=True)
                                except ResursoPrezoSpeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный вид цены ресурсов'),'speco_id')
                            else:
                                message = '{}: {}'.format(_('Не указан вид цены ресурсов'),'speco_id')

                        if not message:
                            if 'statuso_id' in kwargs:
                                try:
                                    statuso = ResursoPrezoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                                      arkivo=False, publikigo=True)
                                except ResursoPrezoStatuso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный статус цены ресурсов'),'statuso_id')
                            else:
                                message = '{}: {}'.format(_('Не указан статус цены ресурсов'),'statuso_id')

                        if not message:
                            if 'realeco_id' in kwargs:
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                                      arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверная реальность'),'realeco_id')

                        if not message:
                            resursoj_prezoj = ResursoPrezo.objects.create(
                                forigo=False,
                                arkivo=False,
                                realeco=realeco,
                                resurso=resurso,
                                posedanto_uzanto=posedanto_uzanto,
                                posedanto_organizo=posedanto_organizo,
                                tipo=tipo,
                                prezo=kwargs.get('prezo', None),
                                speco=speco,
                                statuso=statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            resursoj_prezoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('resurso_id', False) or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('posedanto_organizo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('prezo', False) or kwargs.get('speco_id', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('realeco_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_prezoj = ResursoPrezo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_prezoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_prezoj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'resurso_id' in kwargs:
                                    try:
                                        resurso = Resurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Resurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс для которого устанавливается цена'),'resurso_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный организация владелец цены ресурса'),
                                                                'posedanto_organizo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ResursoPrezoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                                    arkivo=False, publikigo=True)
                                    except ResursoPrezoTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип цены ресурсов'),'tipo_id')

                            if not message:
                                if 'speco_id' in kwargs:
                                    try:
                                        speco = ResursoPrezoSpeco.objects.get(id=kwargs.get('speco_id'), forigo=False,
                                                                                    arkivo=False, publikigo=True)
                                    except ResursoPrezoSpeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный вид цены ресурсов'),'speco_id')

                            if not message:
                                if 'statuso_id' in kwargs:
                                    try:
                                        statuso = ResursoPrezoStatuso.objects.get(id=kwargs.get('statuso_id'), forigo=False,
                                                                                        arkivo=False, publikigo=True)
                                    except ResursoPrezoStatuso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный статус цены ресурсов'),'statuso_id')

                            if not message:
                                if 'realeco_id' in kwargs:
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверная реальность'),'realeco_id')

                            if not message:

                                resursoj_prezoj.forigo = kwargs.get('forigo', resursoj_prezoj.forigo)
                                resursoj_prezoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_prezoj.arkivo = kwargs.get('arkivo', resursoj_prezoj.arkivo)
                                resursoj_prezoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_prezoj.publikigo = kwargs.get('publikigo', resursoj_prezoj.publikigo)
                                resursoj_prezoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_prezoj.resurso = resurso if kwargs.get('resurso_id', False) else resursoj_prezoj.resurso
                                resursoj_prezoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else resursoj_prezoj.posedanto_uzanto
                                resursoj_prezoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else resursoj_prezoj.posedanto_organizo
                                resursoj_prezoj.tipo = tipo if kwargs.get('tipo_id', False) else resursoj_prezoj.tipo
                                resursoj_prezoj.speco = speco if kwargs.get('speco_id', False) else resursoj_prezoj.speco
                                resursoj_prezoj.statuso = statuso if kwargs.get('statuso_id', False) else resursoj_prezoj.statuso
                                resursoj_prezoj.realeco = realeco if kwargs.get('realeco_id', False) else resursoj_prezoj.realeco
                                resursoj_prezoj.prezo = kwargs.get('prezo',  resursoj_prezoj.prezo)

                                resursoj_prezoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoPrezo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoPrezo(status=status, message=message, resursoj_prezoj=resursoj_prezoj)


# Модель типов связей ресурсов между собой
class RedaktuResursoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_ligiloj_tipoj = graphene.Field(ResursoLigiloTipoNode, 
        description=_('Созданная/изменённая модель типов связей ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_ligiloj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            resursoj_ligiloj_tipoj = ResursoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_ligiloj_tipoj = ResursoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                resursoj_ligiloj_tipoj.forigo = kwargs.get('forigo', resursoj_ligiloj_tipoj.forigo)
                                resursoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', resursoj_ligiloj_tipoj.arkivo)
                                resursoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', resursoj_ligiloj_tipoj.publikigo)
                                resursoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_ligiloj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(resursoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                resursoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoLigiloTipo(status=status, message=message, resursoj_ligiloj_tipoj=resursoj_ligiloj_tipoj)


# Модель связей ресурсов между собой
class RedaktuResursoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    resursoj_ligiloj = graphene.Field(ResursoLigiloNode, 
        description=_('Созданная/изменённая модель связей ресурсов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_id = graphene.Int(description=_('Ресурс владелец связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('Место хранения владельца связи'))
        ligilo_id = graphene.Int(description=_('Связываемый ресурс'))
        tipo_id = graphene.Int(description=_('Тип связи ресурсов'))
        konektilo_posedanto = graphene.Int(description=_('Разъём (слот), который занимает этот ресурс у родительского ресурса'))
        konektilo_ligilo = graphene.Int(description=_('Разъём (слот), который используется у связываемого ресурса'))
        priskribo = graphene.String(description=_('Описание'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        resursoj_ligiloj = None
        posedanto = None
        posedanto_stokejo = None
        ligilo = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('resursoj.povas_krei_resursoj_ligiloj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'posedanto_id' in kwargs:
                                try:
                                    posedanto = Resurso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Resurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный ресурс владелец связи'),'posedanto_id')
                            else:
                                message = '{}: {}'.format(_('Не указан ресурс владелец связи'),'posedanto_id')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = ResursoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoStokejo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_id' in kwargs:
                                try:
                                    ligilo = Resurso.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Resurso.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный связываемый ресурс'),'ligilo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан связываемый ресурс'),'ligilo_id')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ResursoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoLigiloTipo.DoesNotExist:
                                    message = '{}: {}'.format(_('Неверный тип связи ресурсов'),'tipo_id')
                            else:
                                message = '{}: {}'.format(_('Не указан тип связи ресурсов'),'tipo_id')

                        if not message:
                            resursoj_ligiloj = ResursoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto=posedanto,
                                posedanto_stokejo=posedanto_stokejo,
                                ligilo=ligilo,
                                tipo=tipo,
                                konektilo_posedanto=kwargs.get('konektilo_posedanto', None),
                                konektilo_ligilo=kwargs.get('konektilo_ligilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('priskribo', False)):
                                set_enhavo(resursoj_ligiloj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            resursoj_ligiloj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_id', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_id', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo_posedanto', False) 
                            or kwargs.get('konektilo_ligilo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            resursoj_ligiloj = ResursoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('resursoj.povas_forigi_resursoj_ligiloj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('resursoj.povas_shanghi_resursoj_ligiloj'):
                                message = _('Недостаточно прав для изменения')
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'posedanto_id' in kwargs:
                                    try:
                                        posedanto = Resurso.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Resurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный ресурс владелец связи'),'posedanto_id')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = ResursoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoStokejo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_id' in kwargs:
                                    try:
                                        ligilo = Resurso.objects.get(id=kwargs.get('ligilo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Resurso.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный связываемый ресурс'),'ligilo_id')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ResursoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoLigiloTipo.DoesNotExist:
                                        message = '{}: {}'.format(_('Неверный тип связи ресурсов'),'tipo_id')

                            if not message:

                                resursoj_ligiloj.forigo = kwargs.get('forigo', resursoj_ligiloj.forigo)
                                resursoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                resursoj_ligiloj.arkivo = kwargs.get('arkivo', resursoj_ligiloj.arkivo)
                                resursoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                resursoj_ligiloj.publikigo = kwargs.get('publikigo', resursoj_ligiloj.publikigo)
                                resursoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                resursoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_id', False) else resursoj_ligiloj.posedanto
                                resursoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else resursoj_ligiloj.posedanto_stokejo
                                resursoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_id', False) else resursoj_ligiloj.ligilo
                                resursoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else resursoj_ligiloj.tipo
                                resursoj_ligiloj.konektilo_posedanto = kwargs.get('konektilo_posedanto',  resursoj_ligiloj.konektilo_posedanto)
                                resursoj_ligiloj.konektilo_ligilo = kwargs.get('konektilo_ligilo',  resursoj_ligiloj.konektilo_ligilo)

                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(resursoj_ligiloj.priskribo, 
                                            kwargs.get('priskribo'), 
                                            info.context.LANGUAGE_CODE)

                                resursoj_ligiloj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ResursoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuResursoLigilo(status=status, message=message, resursoj_ligiloj=resursoj_ligiloj)


class ResursoMutations(graphene.ObjectType):
    redaktu_resurso_kategorio = RedaktuResursoKategorio.Field(
        description=_('''Создаёт или редактирует категории ресурсов''')
    )
    redaktu_resurso_kategorio_ligiloj_tipo = RedaktuResursoKategorioLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей категорий ресурсов между собой''')
    )
    redaktu_resurso_kategorio_ligiloj = RedaktuResursoKategorioLigilo.Field(
        description=_('''Создаёт или редактирует модели связей категорий ресурсов между собой''')
    )
    redaktu_resurso_tipo = RedaktuResursoTipo.Field(
        description=_('''Создаёт или редактирует типы ресурсов''')
    )
    redaktu_resurso_klaso = RedaktuResursoKlaso.Field(
        description=_('''Создаёт или редактирует классы ресурсов''')
    )
    redaktu_resurso_stokejoj_tipo = RedaktuResursoStokejoTipo.Field(
        description=_('''Создаёт или редактирует типы мест хранения ресурсов''')
    )
    redaktu_resurso = RedaktuResurso.Field(
        description=_('''Создаёт или редактирует ресурсы''')
    )
    redaktu_resurso_stokejoj = RedaktuResursoStokejo.Field(
        description=_('''Создаёт или редактирует логические места хранения ресурсов''')
    )
    redaktu_resurso_prezo_tipo = RedaktuResursoPrezoTipo.Field(
        description=_('''Создаёт или редактирует модели типов цен ресурсов''')
    )
    redaktu_resurso_prezo_speco = RedaktuResursoPrezoSpeco.Field(
        description=_('''Создаёт или редактирует модели видов цен ресурсов''')
    )
    redaktu_resurso_prezo_statuso = RedaktuResursoPrezoStatuso.Field(
        description=_('''Создаёт или редактирует модели статусов цен ресурсов''')
    )
    redaktu_resurso_prezoj = RedaktuResursoPrezo.Field(
        description=_('''Создаёт или редактирует модели цен ресурсов''')
    )
    redaktu_resurso_ligiloj_tipo = RedaktuResursoLigiloTipo.Field(
        description=_('''Создаёт или редактирует модели типов связей ресурсов между собой''')
    )
    redaktu_resurso_ligiloj = RedaktuResursoLigilo.Field(
        description=_('''Создаёт или редактирует модели связей ресурсов между собой''')
    )
