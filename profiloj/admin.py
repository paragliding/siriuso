"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _

from profiloj.models import *
from siriuso.utils.admin_mixins import TekstoMixin


# Форма для провиля пользователей
class ProfiloFormo(forms.ModelForm):
    
    class Meta:
        model = Profilo
        fields = [field.name for field in Profilo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Профиль пользователей
@admin.register(Profilo)
class ProfiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = ProfiloFormo
    list_display = ('siriuso_uzanto_id','retnomo','email')
    exclude = ('uuid',)
    class Meta:
        model = Profilo

