"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from siriuso.utils import get_enhavo
from .models import *
from siriuso.forms.widgets import LingvoInputWidget, LingvoTextWidget
from universo_bazo.models import Realeco
from siriuso.utils.admin_mixins import TekstoMixin


class TekstoMixinPosedantoj:
    @staticmethod
    def teksto(obj, field):
        kampo = getattr(obj, field)

        if kampo is not None:
            result = get_enhavo(kampo)

            if result:
                return result[0]
        return ''

    def posedantoj_(self, obj):
        posedantoj = DokumentoPosedanto.objects.filter(dokumento=obj, forigo=False, arkivo=False, publikigo=True)
        idj = ''
        i = True
        for user in posedantoj:
            if user.posedanto_uzanto: 
                if i:
                    idj = "{}".format(user.posedanto_uzanto.id)
                    i = False
                else:
                    idj = "{}; {}".format(idj, user.posedanto_uzanto.id)
            if user.posedanto_organizo: 
                if i:
                    idj = "{}".format(self.teksto(obj=user.posedanto_organizo, field='nomo'))
                    i = False
                else:
                    idj = "{}; {}".format(idj, self.teksto(obj=user.posedanto_organizo, field='nomo'))
        return idj


# Форма файлов документов
class DokumentoDosieroFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    formo = forms.CharField(widget=LingvoInputWidget(), label=_('Formo'), required=False)
    statuso = forms.CharField(widget=LingvoInputWidget(), label=_('Statuso'), required=False)

    class Meta:
        model = DokumentoDosiero
        fields = [field.name for field in DokumentoDosiero._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_formo(self):
        out = self.cleaned_data['formo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_statuso(self):
        out = self.cleaned_data['statuso']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Файлы документов
@admin.register(DokumentoDosiero)
class DokumentoDosieroAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoDosieroFormo
    list_display = ('uuid','nomo_teksto','formo_teksto','statuso_teksto')
    exclude = ('uuid',)
    class Meta:
        model = DokumentoDosiero


# Форма категорий документов
class DokumentoKategorioFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoKategorio
        fields = [field.name for field in DokumentoKategorio._meta.fields if field.name not in ('krea_dato', 'uuid')]\
                 + ['realeco',]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Категорий документов
@admin.register(DokumentoKategorio)
class DokumentoKategorioAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoKategorioFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    filter_horizontal = ('realeco',)
    class Meta:
        model = DokumentoKategorio


# Форма типов связей категорий документов между собой
class DokumentoKategorioLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoKategorioLigiloTipo
        fields = [field.name for field in DokumentoKategorioLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей категорий документов между собой
@admin.register(DokumentoKategorioLigiloTipo)
class DokumentoKategorioLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoKategorioLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    class Meta:
        model = DokumentoKategorioLigiloTipo


# Форма связей категорий документов между собой
class DokumentoKategorioLigiloFormo(forms.ModelForm):
    
    class Meta:
        model = DokumentoKategorioLigilo
        fields = [field.name for field in DokumentoKategorioLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Связи категорий документов между собой
@admin.register(DokumentoKategorioLigilo)
class DokumentoKategorioLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoKategorioLigiloFormo
    list_display = ('uuid','posedanto','tipo','ligilo')
    exclude = ('uuid',)
    class Meta:
        model = DokumentoKategorioLigilo


# Форма типов документов
class DokumentoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    # realeco = forms.ModelMultipleChoiceField(
    #     label=_('Параллельные миры'),
    #     queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    # )

    class Meta:
        model = DokumentoTipo
        fields = [field.name for field in DokumentoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]\
                 + ['realeco',]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы документов
@admin.register(DokumentoTipo)
class DokumentoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    filter_horizontal = ('realeco',)
    class Meta:
        model = DokumentoTipo


# Форма видов документов
class DokumentoSpecoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = DokumentoSpeco
        fields = [field.name for field in DokumentoSpeco._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Виды документов
@admin.register(DokumentoSpeco)
class DokumentoSpecoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoSpecoFormo
    list_display = ('nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    class Meta:
        model = DokumentoSpeco


# Форма типов мест хранения документов
class DokumentoStokejoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoStokejoTipo
        fields = [field.name for field in DokumentoStokejoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы мест хранения документов
@admin.register(DokumentoStokejoTipo)
class DokumentoStokejoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoStokejoTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    class Meta:
        model = DokumentoStokejoTipo


# Форма мест хранения документов
class DokumentoStokejoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Titolo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoStokejo
        fields = [field.name for field in DokumentoStokejo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Места хранения документов
@admin.register(DokumentoStokejo)
class DokumentoStokejoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoStokejoFormo
    list_display = ('nomo_teksto','priskribo_teksto','posedanto','id','tipo')
    exclude = ('uuid',)
    class Meta:
        model = DokumentoStokejo


# Места хранения документов
class DokumentoStokejoInline(admin.TabularInline):
    model = DokumentoStokejo
    fk_name = 'posedanto'
    # form = DokumentoStokejoFormo
    extra = 1


# Владелец документа из таблицы связей
class DokumentoLigiloInline(admin.TabularInline):
    model = DokumentoLigilo
    fk_name = 'posedanto'
    extra = 1


# Форма документов
class DokumentoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)
    kategorio = forms.ModelMultipleChoiceField(
        label=_('Категория документов'),
        queryset=DokumentoKategorio.objects.filter(forigo=False, publikigo=True)
    )
    realeco = forms.ModelMultipleChoiceField(
        label=_('Параллельные миры'),
        queryset=Realeco.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = Dokumento
        fields = [field.name for field in Dokumento._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Документы в
@admin.register(Dokumento)
class DokumentoAdmin(admin.ModelAdmin, TekstoMixin, TekstoMixinPosedantoj):
    form = DokumentoFormo
    list_display = ('id','nomo_teksto','posedantoj_','autoro_nn','priskribo_teksto')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    inlines = ( DokumentoStokejoInline, DokumentoLigiloInline)
    save_on_top = True

    class Meta:
        model = Dokumento


# Форма типов связей документов
class DokumentoLigiloTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoLigiloTipo
        fields = [field.name for field in DokumentoLigiloTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы связей документов
@admin.register(DokumentoLigiloTipo)
class DokumentoLigiloTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoLigiloTipoFormo
    list_display = ('id','nomo_teksto','autoro_nn','autoro_email')
    autocomplete_fields = ['autoro']
    exclude = ('uuid',)
    class Meta:
        model = DokumentoLigiloTipo


# Форма типов владельцев документов
class DokumentoPosedantoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoPosedantoTipo
        fields = [field.name for field in DokumentoPosedantoTipo._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы владельцев документов
@admin.register(DokumentoPosedantoTipo)
class DokumentoPosedantoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoPosedantoTipoFormo
    list_display = ('id', 'nomo_teksto', 'autoro_nn', 'autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = DokumentoPosedantoTipo


# Форма статусов владельца в рамках владения документом
class DokumentoPosedantoStatusoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoPosedantoStatuso
        fields = [field.name for field in DokumentoPosedantoStatuso._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Статусы владельцев в рамках владения документом
@admin.register(DokumentoPosedantoStatuso)
class DokumentoPosedantoStatusoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoPosedantoStatusoFormo
    list_display = ('nomo_teksto', 'autoro_nn', 'autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = DokumentoPosedantoStatuso


# Форма владельцев документов
class DokumentoPosedantoFormo(forms.ModelForm):
    sxablono_sistema_priskribo = forms.CharField(
        widget=LingvoTextWidget(),
        label=_('Priskribo de sistema ŝablono'),
        required=False
    )

    class Meta:
        model = DokumentoPosedanto
        fields = [field.name for field in DokumentoPosedanto._meta.fields if
                  field.name not in ('krea_dato', 'uuid')]

    def clean_sxablono_sistema_priskribo(self):
        out = self.cleaned_data['sxablono_sistema_priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Владельцы документов
@admin.register(DokumentoPosedanto)
class DokumentoPosedantoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoPosedantoFormo
    list_display = ('uuid', 'posedanto_organizo', 'tipo', 'sxablono_sistema_id', 'sxablono_sistema', 'sxablono_priskribo_teksto')
    exclude = ('uuid',)
    list_filter = ('sxablono_sistema',)

    class Meta:
        model = DokumentoPosedanto


# Форма мест хранения документов
class DokumentoLigiloFormo(forms.ModelForm):
    
    priskribo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = DokumentoLigilo
        fields = [field.name for field in DokumentoLigilo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы мест хранения документов
@admin.register(DokumentoLigilo)
class DokumentoLigiloAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoLigiloFormo
    list_display = ('uuid','posedanto','posedanto_stokejo','ligilo','tipo','priskribo_teksto')
    exclude = ('uuid',)
    class Meta:
        model = DokumentoLigilo


# Форма Видов Услуг Экспедирования
class DokumentoSpecoServoEkspedoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoTextWidget(), label=_('Nomo'), required=False)
    tipo_kosto = forms.CharField(widget=LingvoTextWidget(), label=_('Tipo kosto'), required=False)

    class Meta:
        model = DokumentoSpecoServoEkspedo
        fields = [field.name for field in DokumentoSpecoServoEkspedo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_tipo_kosto(self):
        out = self.cleaned_data['tipo_kosto']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Виды Услуг Экспедирования
@admin.register(DokumentoSpecoServoEkspedo)
class DokumentoSpecoServoEkspedoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoSpecoServoEkspedoFormo
    list_display = ('uuid', 'nomo_teksto',)
    exclude = ('uuid',)
    class Meta:
        model = DokumentoSpecoServoEkspedo


# Форма характеристик документов Экспедирования 
class DokumentoEkspedoFormo(forms.ModelForm):
    
    adreso_elsxargxado = forms.CharField(widget=LingvoTextWidget(), label=_('Adreso elsxargxado'), required=False)
    adreso_kargado = forms.CharField(widget=LingvoTextWidget(), label=_('Adreso kargado'), required=False)
    komento = forms.CharField(widget=LingvoTextWidget(), label=_('Komento'), required=False)
    komento_adreso_elsxargxado = forms.CharField(widget=LingvoTextWidget(), label=_('Komento adreso elsxargxado'), required=False)
    komento_adreso_kargado = forms.CharField(widget=LingvoTextWidget(), label=_('Komento adreso kargado'), required=False)
    komento_nuligo = forms.CharField(widget=LingvoTextWidget(), label=_('Komento nuligo'), required=False)
    kombatanto_adresoj = forms.CharField(widget=LingvoTextWidget(), label=_('Kombatanto adreso'), required=False)
    kombatanto_adresoj_elsxargxado = forms.CharField(widget=LingvoTextWidget(), label=_('Kombatanto adreso elsxargxado'), required=False)
    priskribo_kargo = forms.CharField(widget=LingvoTextWidget(), label=_('Priskribo kargo'), required=False)
    dosiero = forms.ModelMultipleChoiceField(
        label=_('Присоединённые файлы'),
        queryset=DokumentoDosiero.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = DokumentoEkspedo
        fields = [field.name for field in DokumentoEkspedo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_adreso_elsxargxado(self):
        out = self.cleaned_data['adreso_elsxargxado']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_adreso_kargado(self):
        out = self.cleaned_data['adreso_kargado']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_komento(self):
        out = self.cleaned_data['komento']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_komento_adreso_elsxargxado(self):
        out = self.cleaned_data['komento_adreso_elsxargxado']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_komento_adreso_kargado(self):
        out = self.cleaned_data['komento_adreso_kargado']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_komento_nuligo(self):
        out = self.cleaned_data['komento_nuligo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_kombatanto_adresoj(self):
        out = self.cleaned_data['kombatanto_adresoj']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_kombatanto_adresoj_elsxargxado(self):
        out = self.cleaned_data['kombatanto_adresoj_elsxargxado']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo_kargo(self):
        out = self.cleaned_data['priskribo_kargo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Характеристики документов Экспедирования 
@admin.register(DokumentoEkspedo)
class DokumentoEkspedoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoEkspedoFormo
    list_display = ('uuid','konservejo_ricevado_dato', 'konservejo_ricevado_tempo_de',)
    exclude = ('uuid',)
    class Meta:
        model = DokumentoEkspedo


# Форма грузов характеристик документов Экспедирования 
class DokumentoEkspedoKargoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoTextWidget(), label=_('Nomo'), required=False)
    numero_indikatoro = forms.CharField(widget=LingvoTextWidget(), label=_('Adreso kargado'), required=False)

    class Meta:
        model = DokumentoEkspedoKargo
        fields = [field.name for field in DokumentoEkspedoKargo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_numero_indikatoro(self):
        out = self.cleaned_data['numero_indikatoro']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Грузы характеристик документов Экспедирования
@admin.register(DokumentoEkspedoKargo)
class DokumentoEkspedoKargoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoEkspedoKargoFormo
    list_display = ('uuid', 'ekspedo',)
    exclude = ('uuid',)
    class Meta:
        model = DokumentoEkspedoKargo


# Форма складов характеристик документов Экспедирования
class DokumentoEkspedoKonservejoKargoFormo(forms.ModelForm):

    temperatura_reghimo = forms.CharField(widget=LingvoTextWidget(), label=_('Temperatura reghimo'), required=False)

    class Meta:
        model = DokumentoEkspedoKonservejoKargo
        fields = [field.name for field in DokumentoEkspedoKonservejoKargo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_temperatura_reghimo(self):
        out = self.cleaned_data['temperatura_reghimo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Склады характеристик документов Экспедирования
@admin.register(DokumentoEkspedoKonservejoKargo)
class DokumentoEkspedoKonservejoKargoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoEkspedoKonservejoKargoFormo
    list_display = ('uuid', 'ekspedo',)
    exclude = ('uuid',)
    class Meta:
        model = DokumentoEkspedoKonservejoKargo


# Форма складов характеристик документов Экспедирования
class DokumentoEkspedoKonservejoKargoKondukistoFormo(forms.ModelForm):
    
    temperatura_reghimo = forms.CharField(widget=LingvoTextWidget(), label=_('Temperatura reghimo'), required=False)

    class Meta:
        model = DokumentoEkspedoKonservejoKargoKondukisto
        fields = [field.name for field in DokumentoEkspedoKonservejoKargoKondukisto._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_temperatura_reghimo(self):
        out = self.cleaned_data['temperatura_reghimo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Склады характеристик документов Экспедирования
@admin.register(DokumentoEkspedoKonservejoKargoKondukisto)
class DokumentoEkspedoKonservejoKargoKondukistoAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoEkspedoKonservejoKargoKondukistoFormo
    list_display = ('uuid', 'ekspedo',)
    exclude = ('uuid',)
    class Meta:
        model = DokumentoEkspedoKonservejoKargoKondukisto


# Форма складов характеристик документов Экспедирования
class DokumentoContractFormo(forms.ModelForm):
    
    komisipago_kalkulo_metodo = forms.CharField(widget=LingvoTextWidget(), label=_('Komisipago kalkulo metodo'), required=False)
    dosiero = forms.ModelMultipleChoiceField(
        label=_('Присоединённые файлы'),
        queryset=DokumentoDosiero.objects.filter(forigo=False, publikigo=True)
    )

    class Meta:
        model = DokumentoContract
        fields = [field.name for field in DokumentoContract._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_komisipago_kalkulo_metodo(self):
        out = self.cleaned_data['komisipago_kalkulo_metodo']
        try:
            out = json.loads(out)
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Склады характеристик документов Экспедирования
@admin.register(DokumentoContract)
class DokumentoContractAdmin(admin.ModelAdmin, TekstoMixin):
    form = DokumentoContractFormo
    list_display = ('uuid', )
    exclude = ('uuid',)
    class Meta:
        model = DokumentoContract


