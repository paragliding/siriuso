"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene
from django.db.models import Q

from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from ..models import *  # модели приложения
from taskoj.api.schema import TaskojProjektoNode, TaskojTaskoNode
from taskoj.models import TaskojTasko, TaskojTaskoPosedanto, TaskojProjekto, TaskojProjektoPosedanto


# Модель шаблонов объектов
class ObjektoSxablonoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ObjektoSxablono
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto_objekto__uuid': ['exact'],
            'resurso__uuid': ['exact'],
            'kubo__id': ['exact'],
            'kubo__stelsistemo__id': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель владельцев объектов
class ObjektoPosedantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ObjektoPosedanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'objekto__uuid': ['exact'],
            'posedanto_organizo__uuid': ['exact'],
            'posedanto_uzanto__uuid': ['exact'],
            'posedanto_uzanto__id': ['exact'],
            'realeco__id': ['exact'],
            'tipo__id': ['exact'],
            'statuso__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связь объектов между собой
class ObjektoLigiloNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = ObjektoLigilo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__uuid': ['exact'],
            'posedanto_stokejo__uuid':['exact'],
            'ligilo__uuid':['exact'],
            'tipo__id':['exact', 'in'],
            'tipo__uuid':['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель объектов
class ObjektoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    sxablono_sistema_priskribo = graphene.Field(SiriusoLingvo, description=_('Описание системного шаблона'))

    in_cosmo = graphene.Boolean(description=_('Наличие объекта в космосе (с координатами и вне другого объекта)'))

    posedanto = SiriusoFilterConnectionField(ObjektoPosedantoNode,
        description=_('Выводит владельцев объекта'))

    posedanto_id = graphene.Int(description=_('Первый владелец объекта (ID пользователя Сириусо)'))

    # проект
    projekto =  SiriusoFilterConnectionField(TaskojProjektoNode,
        description=_('Выводит проекты, владельцами которых являются объекты'))

    # задача
    tasko =  SiriusoFilterConnectionField(TaskojTaskoNode,
        description=_('Выводит задачи, владельцами которых являются объекты'))

    # Связь объектов между собой
    ligilo =  SiriusoFilterConnectionField(ObjektoLigiloNode,
        description=_('Выводит связи объектов между собой, владельцами которых являются объекты'))

    # Связь объектов между собой со стороны подчинённого
    ligilo_ligilo =  SiriusoFilterConnectionField(ObjektoLigiloNode,
        description=_('Выводит связи объектов между собой, подчинёнными которых являются объекты'))

    loza_volumenoj = graphene.Float(description=_('Объём свободного внутреннего пространства'))

    class Meta:
        model = Objekto
        filter_fields = {
            'uuid': ['exact', 'in'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'koordinato_x': ['exact','gt','lt','range', 'isnull'],
            'koordinato_y': ['exact','gt','lt', 'isnull'],
            'koordinato_z': ['exact','gt','lt', 'isnull'],
            'posedanto_objekto__uuid': ['exact'],
            'resurso__uuid': ['exact'],
            'resurso__tipo__id': ['exact'],
            'realeco__id': ['exact'],
            'kubo': ['isnull'],
            'kubo__id': ['exact'],
            'kubo__stelsistemo__id': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__posedanto__uuid': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__tipo__id': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__tipo__uuid': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__tipo_id': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto': ['exact', 'isnull'],
            'objektoj_objektoligilo_ligilo_objekto__forigo': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__arkivo': ['exact'],
            'objektoj_objektoligilo_ligilo_objekto__publikigo': ['exact'],

            'objektoligilo__ligilo__uuid': ['exact'],
            'objektoligilo__uuid': ['exact'],
            'objektoligilo__tipo__id': ['exact'],
            'objektoposedanto__uuid': ['exact'],
            'objektoposedanto__posedanto_uzanto__uuid': ['exact'],
            'objektoposedanto__posedanto_uzanto__id': ['exact'],
            'objektoposedanto__forigo': ['exact'],
            'objektoposedanto__arkivo': ['exact'],
            'objektoposedanto__publikigo': ['exact'],

            'objektouzanto__autoro__id':['exact'],
            'objektouzanto__autoro__uuid':['exact'],
            'objektouzanto__objekto': ['exact'],
            'objektouzanto': ['isnull'],
        }
        # filterset_class = ObjektoFilter
        interfaces = (graphene.relay.Node,)

    def resolve_in_cosmo(self, info):
        # в космосе, если есть координаты и не находится внутри другого объекта
        in_cosmo = False
        if ((self.koordinato_x) and (self.koordinato_y) and (self.koordinato_z) and (self.posedanto_objekto is None)):
            in_cosmo = True
        return in_cosmo

    def resolve_posedanto(self, info, **kwargs):
        return ObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)

    def resolve_posedanto_id(self, info):
        posed = ObjektoPosedanto.objects.filter(objekto=self, forigo=False, arkivo=False, publikigo=True)
        for pos in posed:
            if (pos.posedanto_uzanto):
                return pos.posedanto_uzanto.id
        return None

    def resolve_tasko(self, info, **kwargs):
        # находим перечень задач, владельцами которых является объект
        taskoj = TaskojTaskoPosedanto.objects.filter(posedanto_objekto=self, forigo=False, arkivo=False, 
            publikigo=True).values('tasko')
        return TaskojTasko.objects.filter(uuid__in=taskoj, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_projekto(self, info, **kwargs):
        # находим перечень проектов, владельцами которых является объект
        projektoj = TaskojProjektoPosedanto.objects.filter(posedanto_objekto=self, forigo=False, arkivo=False, 
            publikigo=True).values('projekto')
        return TaskojProjekto.objects.filter(uuid__in=projektoj, forigo=False, arkivo=False, 
            publikigo=True)

    def resolve_ligilo(self, info, **kwargs):
        return ObjektoLigilo.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True)

    def resolve_ligilo_ligilo(self, info, **kwargs):
        return ObjektoLigilo.objects.filter(ligilo=self, forigo=False, arkivo=False, publikigo=True)

    def resolve_loza_volumenoj(self, info, **kwargs):
        return loza_volumeno(self)


# Модель типов владельцев объектов
class ObjektoPosedantoTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ObjektoPosedantoTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель статусов владельца в рамках владения объектом
class ObjektoPosedantoStatusoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ObjektoPosedantoStatuso
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель мест хранения в объектах
class ObjektoStokejoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    # Хранящиеся объекта в данном складе/месте хранения
    objekto =  SiriusoFilterConnectionField(ObjektoNode,
        description=_('Выводит объекты, хранящиеся в данном месте хранения'))

    class Meta:
        model = ObjektoStokejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto_objekto__uuid': ['exact'],
            'tipo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_objekto(self, info, **kwargs):
        # находим перечень связей, где упоминается данное место хранения
        ligiloj = ObjektoLigilo.objects.filter(posedanto_stokejo=self, forigo=False, arkivo=False, 
            publikigo=True).values('ligilo')
        return Objekto.objects.filter(uuid__in=ligiloj, forigo=False, arkivo=False, 
            publikigo=True)


# Модель типов связей объектов между собой
class ObjektoLigiloTipoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'nomo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание'))

    class Meta:
        model = ObjektoLigiloTipo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель связей пользователя с управляемым объектом в соответствующем мире
class ObjektoUzantoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = ObjektoUzanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'autoro__id': ['exact'],
            'autoro__uuid': ['exact'],
            'objekto__uuid': ['exact'],
            'realeco__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


# Модель температурного режима хранения/транспортировки
class ObjektoTemperaturaReghimoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'predefined_nomo__enhavo': ['contains', 'icontains'],
    }

    class Meta:
        model = ObjektoTemperaturaReghimo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов контейнеров
class ObjektoTipoKonteneroNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
        'predefined_nomo__enhavo': ['contains', 'icontains'],
        'komento__enhavo': ['contains', 'icontains'],
    }

    class Meta:
        model = ObjektoTipoKontenero
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


# Модель типов упаковок
class ObjektoTipoPakumoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    class Meta:
        model = ObjektoTipoPakumo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class ObjektojQuery(graphene.ObjectType):
    objekto_sxablono = SiriusoFilterConnectionField(
        ObjektoSxablonoNode,
        description=_('Выводит все доступные модели шаблонов объектов')
    )
    objekto = SiriusoFilterConnectionField(
        ObjektoNode,
        description=_('Выводит все доступные модели объектов')
    )
    objekto_posedanto_tipo = SiriusoFilterConnectionField(
        ObjektoPosedantoTipoNode,
        description=_('Выводит все доступные модели типов владельцев объектов')
    )
    objekto_posedanto_statuso = SiriusoFilterConnectionField(
        ObjektoPosedantoStatusoNode,
        description=_('Выводит все доступные модели статусов владельца в рамках владения объектом')
    )
    objekto_posedanto = SiriusoFilterConnectionField(
        ObjektoPosedantoNode,
        description=_('Выводит все доступные модели владельцев объектов')
    )
    objekto_stokejo = SiriusoFilterConnectionField(
        ObjektoStokejoNode,
        description=_('Выводит все доступные модели мест хранения в объектах')
    )
    objekto_ligilo_tipo = SiriusoFilterConnectionField(
        ObjektoLigiloTipoNode,
        description=_('Выводит все доступные модели типов связей объектов между собой')
    )
    objekto_ligilo = SiriusoFilterConnectionField(
        ObjektoLigiloNode,
        description=_('Выводит все доступные связи объектов между собой')
    )
    objekto_uzanto = SiriusoFilterConnectionField(
        ObjektoUzantoNode,
        description=_('Выводит все доступные связи пользователя с управляемым объектом в соответствующем мире')
    )
    objekto_projekto_resurso_stato = SiriusoFilterConnectionField(
        ObjektoNode,
        description=_('Выводит отфильтрованные модели объектов (которые не связаны подчинёнными или в связях подчинёнными ВСЕ помечены на удаление)')
    )
    filtered_objekto = SiriusoFilterConnectionField(
        ObjektoNode,
        description=_('Выводит отфильтрованные модели объектов (которые не связаны подчинёнными или в связях подчинёнными ВСЕ помечены на удаление)')
    )
    filtered_objekto_posedanto = SiriusoFilterConnectionField(
        ObjektoNode,
        description=_('Выводит отфильтрованные модели объектов пользователя (которые не связаны подчинёнными или в связях подчинёнными ВСЕ помечены на удаление)')
    )
    objekto_temperatura_reghimoj = SiriusoFilterConnectionField(
        ObjektoTemperaturaReghimoNode,
        description=_('Выводит отфильтрованные модели температурного режима хранения/транспортировки')
    )
    objekto_tipo_kontenero = SiriusoFilterConnectionField(
        ObjektoTipoKonteneroNode,
        description=_('Выводит отфильтрованные модели типов контейнеров')
    )
    objekto_tipo_pakumo = SiriusoFilterConnectionField(
        ObjektoTipoPakumoNode,
        description=_('Выводит отфильтрованные модели типов упаковок')
    )

    @staticmethod
    def resolve_objekto_projekto_resurso_stato(root, info, **kwargs):
        return Objekto.objects.select_related('resurso', 'stato').all().prefetch_related('universoprojektojprojekto_set__universoprojektojtasko_set')

    @staticmethod
    def resolve_objekto(root, info, **kwargs):
        return Objekto.objects.all().distinct()

    @staticmethod
    def resolve_filtered_objekto(root, info, **kwargs):
        objektoj = Objekto.objects.filter(objektoj_objektoligilo_ligilo_objekto__forigo=False,
            objektoj_objektoligilo_ligilo_objekto__arkivo=False,
            objektoj_objektoligilo_ligilo_objekto__publikigo=True,).values_list('uuid', flat=True)
        return Objekto.objects.exclude(uuid__in=objektoj)

    @staticmethod
    def resolve_filtered_objekto_posedanto(root, info, **kwargs):
        # выводит имущество пользователя
        # выбираем объекты по запросившему пользователю
        uzanto = info.context.user
        # все объекты пользователя
        # станции и др. объекты, где есть имущество пользователя
        objektoj = Objekto.objects.filter(objektoj_objektoligilo_ligilo_objekto__forigo=False,
            objektoj_objektoligilo_ligilo_objekto__arkivo=False,
            objektoj_objektoligilo_ligilo_objekto__publikigo=True,
            objektoposedanto__posedanto_uzanto=uzanto,
            objektoposedanto__forigo=False
            ).exclude(objektoj_objektoligilo_ligilo_objekto__posedanto__objektoposedanto__posedanto_uzanto=uzanto,
            objektoj_objektoligilo_ligilo_objekto__posedanto__objektoposedanto__forigo=False
            ).values_list('objektoj_objektoligilo_ligilo_objekto__posedanto__uuid', flat=True)
        objekto_stancio = Objekto.objects.filter(Q(uuid__in=objektoj))
        # объекты пользователя в космосе
        objektoj_kosmo = Objekto.objects.filter(objektoj_objektoligilo_ligilo_objekto__forigo=False,
            objektoj_objektoligilo_ligilo_objekto__arkivo=False,
            objektoj_objektoligilo_ligilo_objekto__publikigo=True
        ).values_list('uuid', flat=True)

        return Objekto.objects.filter(Q(uuid__in=objektoj)| ~Q(uuid__in=objektoj_kosmo)&Q(
            objektoposedanto__posedanto_uzanto=uzanto,
            objektoposedanto__forigo=False
            ))
        
