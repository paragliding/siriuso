"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
from graphene.types.generic import GenericScalar
import requests
import json
from django.conf import settings

from siriuso.utils import set_enhavo
from siriuso.api.types import ErrorNode
from .schema import *
from ..models import *
from .subscription import ObjektoEventoj, LigiloEventoj, ObjektoLigiloEventoj
from profiloj.api.subscription import UzantoWSSubscription
from universo_bazo.models import Realeco

from sxablonoj.signals import create_model
from sxablonoj.models import Sxablono

# сохраняем, каким объектом управляем
# kubo_old - т.к. предыдущий управляемый объект мог быть в космосе
def objekto_uzanto_save(objekto_uzanto):

    def eventoj():
        if objekto_uzanto.objekto.realeco:
            UzantoWSSubscription.objekto_uzanto(objekto_uzanto.objekto, objekto_uzanto.objekto.realeco)

    objekto_uzanto.save()
    # вызов функции через коммит транзакции обязателен, иначе не данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# сохраняем объекты
def objekto_save(objekto, kubo_old):

    def ligiloj(objekto_ligilo, posedi, step):
        step += 1
        for ligilo in objekto_ligilo:
            if ligilo.posedanto:
                if not posedi:
                    posedi = ligilo.posedanto
                if ligilo.posedanto.realeco and ligilo.posedanto.kubo:
                    ObjektoEventoj.kubo_objekto(objekto, ligilo.posedanto.realeco, ligilo.posedanto.kubo, posedi, ligilo.posedanto)
                    return
                else:
                    ligiloj(ObjektoLigilo.objects.filter(ligilo=ligilo.posedanto, forigo=False,
                        arkivo=False, publikigo=True), posedi, step)

    def eventoj():
        indico_kubo = False # признак передачи данных по кубу

        if objekto.realeco and objekto.kubo:
            ObjektoEventoj.kubo_objekto(objekto, objekto.realeco, objekto.kubo, None, None)
            indico_kubo = True
        # если был старый куб, то отправляем сообщение в другой куб
        if (kubo_old) and (kubo_old != objekto.kubo): 
            ObjektoEventoj.kubo_objekto(objekto, objekto.realeco, kubo_old, None, None)
            indico_kubo = True
        if not indico_kubo:
            # если объект находится внутри другого, который находится в нужном кубе
            # тогда точно нужно присылать данные по связанным объектам - повреждение у двигателя другого корабля и нужны это отразить в космосе
            objekto_ligilo = ObjektoLigilo.objects.filter(ligilo=objekto, forigo=False,
                arkivo=False, publikigo=True)
            # в цикле проверяем объекты по восходящим связям 
            ligiloj(objekto_ligilo, None, 1)
        # есть-ли восходящая связь, и если есть, то будем вызывать подписку по изменению восходящей связи
        objekto2, ligilo = sercxi_ligilo_posedanto(objekto, None)
        if ligilo:
            ObjektoLigiloEventoj.sxangxi_objekto_ligilo(objekto2, ligilo, None, objekto)

    objekto.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# Модель объектов
class RedaktuObjekto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj = graphene.Field(ObjektoNode, 
        description=_('Созданный/изменённый объект'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец, то есть родительский объект'))
        konektilo = graphene.Int(description=_('Разъём (слот), который занимает этот объект у родительского объекта'))
        resurso_id = graphene.Int(description=_('Ресурс, на основе которого был создан этот объект'))
        integreco = graphene.Int(description=_('Целостность объекта'))
        volumeno_interna = graphene.Float(description=_('Внутренний объём объекта'))
        volumeno_ekstera = graphene.Float(description=_('Внешний объём объекта'))
        volumeno_stokado = graphene.Float(description=_('Объем хронения (внешний) объекта'))
        modifo_uuid = graphene.String(description=_('Модификация ресурса, на основе которого был создан этот объект'))
        stato_uuid = graphene.String(description=_('Состояние модификации ресурса, в котором сейчас находится объект'))
        # далее параметры объекта, которые может править владелец объекта
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        kubo_id = graphene.Int(description=_('Ячейка (куб,чанк) звёздной системы в которой находится объект'))
        kubo_null = graphene.Boolean(description=_('Обнуление ячейки (куба,чанка) звёздной системы в которой перестал находится объект'))
        koordinato_x = graphene.Float(description=_('Координаты (месторасположение) объекта по оси X в кубе'))
        koordinato_y = graphene.Float(description=_('Координаты (месторасположение) объекта по оси Y в кубе'))
        koordinato_z = graphene.Float(description=_('Координаты (месторасположение) объекта по оси Z в кубе'))
        rotacia_x = graphene.Float(description=_('Вращение объекта по оси X в кубе'))
        rotacia_y = graphene.Float(description=_('Вращение объекта по оси Y в кубе'))
        rotacia_z = graphene.Float(description=_('Вращение объекта по оси Z в кубе'))
        # параметры владельца создаваемого объекта - нужно ли создавать владельца - ObjektoPosedanto
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец объекта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец объекта'))
        posedanto_parto = graphene.Int(description=_('Размер доли владения'))
        posedanto_tipo_id = graphene.Int(description=_('Тип владельца объекта'))
        posedanto_statuso_id = graphene.Int(description=_('Статус владельца объекта'))
        # создание связи (помещение объекта внутрь другого объекта) - ObjektoLigilo
        ligilo_tipo_id = graphene.Int(description=_('Код типа места хранения ресурсов на основе которого создано это место хранения в объекте'))
        ligilo_posedanto_uuid = graphene.String(description=_('UUID объекта владельца связи'))
        ligilo_posedanto_stokejo_uuid = graphene.String(description=_('UUID места хранения владельца связи'))
        ligilo_ligilo_uuid = graphene.String(description=_('UUID связываемого объекта'))
        ligilo_konektilo_posedanto = graphene.Int(description=_('Разъём (слот), который занимает этот объект у родительского объекта'))
        ligilo_konektilo_ligilo = graphene.Int(description=_('Разъём (слот), который занимает этот объект у связываемого объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        realeco = None
        posedanto_objekto = None
        resurso = None
        kubo = None
        modifo = None
        stato = None
        objektoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():

                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('objektoj.povas_krei_objektoj') or (kwargs.get('resurso_id',0) == 1):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            if (kwargs.get('posedanto_objekto_uuid', False)):
                                try:
                                    posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец, то есть родительский объект'),'posedanto_objekto_uuid')

                        if not message:
                            if (kwargs.get('resurso_id', False)):
                                try:
                                    resurso = Resurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                  arkivo=False, publikigo=True)
                                except Resurso.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный ресурс, на основе которого был создан этот объект'),'resurso_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'resurso_id')

                        if not message:
                            if (kwargs.get('kubo_id', False)):
                                try:
                                    kubo = KosmoStelsistemoKubo.objects.get(id=kwargs.get('kubo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KosmoStelsistemoKubo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная ячейка (куб,чанк) звёздной системы в которой находится объект'),'kubo_id')

                        if not message:
                            if (kwargs.get('modifo_uuid', False)):
                                try:
                                    modifo = ResursoModifo.objects.get(uuid=kwargs.get('modifo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoModifo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная модификаци ресурса, на основе которого был создан этот объект'),'modifo_uuid')

                        if not message:
                            if (kwargs.get('stato_uuid', False)):
                                try:
                                    stato = ResursoModifoStato.objects.get(uuid=kwargs.get('stato_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ResursoModifoStato.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное состояние модификации ресурса, в котором сейчас находится объект'),'stato_uuid')

                            if (('ligilo_posedanto_uuid' in kwargs)and('ligilo_ligilo_uuid' in kwargs)):
                                message = '{} "{}" {} "{}"'.format(_('Нельзя устанавливать связи'),'ligilo_posedanto_uuid',_('и'),'ligilo_ligilo_uuid')

                        if not message:
                            objektoj = Objekto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_objekto = posedanto_objekto,
                                realeco = realeco,
                                resurso = resurso,
                                kubo = kubo,
                                koordinato_x=kwargs.get('koordinato_x', 0),
                                koordinato_y=kwargs.get('koordinato_y', 0),
                                koordinato_z=kwargs.get('koordinato_z', 0),
                                rotacia_x=kwargs.get('rotacia_x', 0),
                                rotacia_y=kwargs.get('rotacia_y', 0),
                                rotacia_z=kwargs.get('rotacia_z', 0),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                                modifo=modifo,
                                stato=stato,
                                integreco=kwargs.get('integreco', 0),
                                volumeno_interna=kwargs.get('volumeno_interna', 0),
                                volumeno_ekstera=kwargs.get('volumeno_ekstera', 0),
                                volumeno_stokado=kwargs.get('volumeno_stokado', 0)
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(objektoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(objektoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            objekto_save(objektoj, None)

                            message_result = _('Запись объекта создана')

                            if (kwargs.get('posedanto_tipo_id', False)): # если нужно создать владельца
                                # поля для владельца создаваемого объекта
                                posedanto_uzanto = None
                                posedanto_organizo = None
                                tipo = None
                                statuso = None

                                message, posedanto_uzanto, posedanto_organizo, tipo, statuso = test_base_posedanto(
                                    kwargs.get('posedanto_uzanto_id', False), 
                                    posedanto_uzanto,
                                    kwargs.get('posedanto_organizo_uuid', False),
                                    posedanto_organizo,
                                    kwargs.get('posedanto_tipo_id', False),
                                    tipo,
                                    kwargs.get('posedanto_statuso_id', False),
                                    statuso
                                )

                                if not message:
                                    objektoj_posedantoj = ObjektoPosedanto.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        posedanto_uzanto = posedanto_uzanto,
                                        objekto = objektoj,
                                        realeco = realeco,
                                        posedanto_organizo = posedanto_organizo,
                                        parto = kwargs.get('posedanto_parto', 100),
                                        tipo = tipo,
                                        statuso = statuso,
                                        publikigo=kwargs.get('publikigo', False),
                                        publikiga_dato=timezone.now()
                                    )

                                    objektoj_posedantoj.save()

                                    message_result = "{} {}".format(message_result, _(', запись владельца создана'))


                            if (kwargs.get('ligilo_tipo_id', False) and (('ligilo_posedanto_uuid' in kwargs)or('ligilo_ligilo_uuid' in kwargs))): # если нужно создать связь
                                ligilo_posedanto = None
                                ligilo_posedanto_stokejo = None
                                ligilo_ligilo = None
                                ligilo_tipo = None
                                # проверяем наличие записей с таким кодом
                                if not message:
                                    if 'ligilo_posedanto_uuid' in kwargs:
                                        try:
                                            ligilo_posedanto = Objekto.objects.get(uuid=kwargs.get('ligilo_posedanto_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Objekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный объект владелец связи'),'ligilo_posedanto_uuid')
                                    else:
                                        ligilo_posedanto = objektoj

                                if not message:
                                    if 'ligilo_ligilo_uuid' in kwargs:
                                        try:
                                            ligilo_ligilo = Objekto.objects.get(uuid=kwargs.get('ligilo_ligilo_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except Objekto.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный связываемый объект'),'ligilo_ligilo_uuid')
                                    else:
                                        ligilo_ligilo = objektoj

                                if not message:
                                    if 'ligilo_posedanto_stokejo_uuid' in kwargs:
                                        try:
                                            ligilo_posedanto_stokejo = ObjektoStokejo.objects.get(uuid=kwargs.get('ligilo_posedanto_stokejo_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                        except ObjektoStokejo.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверное место хранения владельца связи'),'ligilo_posedanto_stokejo_uuid')

                                if not message:
                                    try:
                                        ligilo_tipo = ObjektoLigiloTipo.objects.get(id=kwargs.get('ligilo_tipo_id'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                    except ObjektoLigiloTipo.DoesNotExist:
                                            message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов'),'ligilo_tipo_id')

                                if not message:
                                    objektoj_ligiloj = ObjektoLigilo.objects.create(
                                        forigo=False,
                                        arkivo=False,
                                        posedanto = ligilo_posedanto,
                                        posedanto_stokejo = ligilo_posedanto_stokejo,
                                        ligilo = ligilo_ligilo,
                                        tipo=ligilo_tipo,
                                        konektilo_posedanto=kwargs.get('ligilo_konektilo_posedanto', None),
                                        konektilo_ligilo=kwargs.get('ligilo_konektilo_ligilo', None),
                                        publikigo=kwargs.get('publikigo', False),
                                        publikiga_dato=timezone.now()
                                    )

                                    objekto_ligilo_save(objektoj_ligiloj)

                                    # ещё раз пересохраняем объект, т.к. появилась связи и теперь вызывается подписка с новыми данными
                                    objekto_save(objektoj, None)

                                    status = True
                                    message_result = "{} {}".format(message_result,_(', запись связи создана'))

                            if message:
                                message_result = "{} {}".format(message_result, message)

                            status = True
                            message = message_result
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco_id', False) or kwargs.get('kubo_id', False)
                            or kwargs.get('posedanto_objekto_uuid', False) or kwargs.get('konektilo', False)
                            or kwargs.get('koordinato_x', False) or kwargs.get('koordinato_y', False)
                            or kwargs.get('koordinato_z', False) or kwargs.get('rotacia_x', False)
                            or kwargs.get('rotacia_y', False) or kwargs.get('rotacia_z', False)
                            or kwargs.get('resurso_id', False) or kwargs.get('modifo', False)
                            or kwargs.get('stato', False) or kwargs.get('integreco', False)
                            or kwargs.get('kubo_null', False) or kwargs.get('volumeno_interna', False)
                            or kwargs.get('volumeno_ekstera', False) or kwargs.get('volumeno_stokado', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj = Objekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_objekto_uuid', False)):
                                    try:
                                        posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец, то есть родительский объект'),'posedanto_objekto_uuid')

                            if not message:
                                if (kwargs.get('resurso_id', False)):
                                    try:
                                        resurso = Resurso.objects.get(id=kwargs.get('resurso_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                    except Resurso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный ресурс, на основе которого был создан этот объект'),'resurso_id')

                            if not message:
                                if (kwargs.get('kubo_id', False)):
                                    try:
                                        kubo = KosmoStelsistemoKubo.objects.get(id=kwargs.get('kubo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KosmoStelsistemoKubo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная ячейка (куб,чанк) звёздной системы в которой находится объект'),'kubo_id')

                            if not message:
                                if (kwargs.get('modifo_uuid', False)):
                                    try:
                                        modifo = ResursoModifo.objects.get(uuid=kwargs.get('modifo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoModifo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная модификаци ресурса, на основе которого был создан этот объект'),'modifo_uuid')

                            if not message:
                                if (kwargs.get('stato_uuid', False)):
                                    try:
                                        stato = ResursoModifoStato.objects.get(uuid=kwargs.get('stato_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ResursoModifoStato.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное состояние модификации ресурса, в котором сейчас находится объект'),'stato_uuid')

                            if not message:

                                kubo_old = objektoj.kubo

                                objektoj.forigo = kwargs.get('forigo', objektoj.forigo)
                                objektoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else objektoj.foriga_dato
                                objektoj.arkivo = kwargs.get('arkivo', objektoj.arkivo)
                                objektoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else objektoj.arkiva_dato
                                objektoj.publikigo = kwargs.get('publikigo', objektoj.publikigo)
                                objektoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else objektoj.publikiga_dato
                                objektoj.resurso = resurso if kwargs.get('resurso_id', False) else objektoj.resurso
                                objektoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else objektoj.posedanto_objekto
                                objektoj.konektilo = kwargs.get('konektilo', objektoj.konektilo) 
                                objektoj.realeco = realeco if kwargs.get('realeco_id', False) else objektoj.realeco
                                objektoj.kubo = kubo if kwargs.get('kubo_id', False) else objektoj.kubo
                                if kwargs.get('kubo_null', False):
                                    objektoj.kubo = None
                                objektoj.koordinato_x = kwargs.get('koordinato_x', objektoj.koordinato_x)
                                objektoj.koordinato_y = kwargs.get('koordinato_y', objektoj.koordinato_y)
                                objektoj.koordinato_z = kwargs.get('koordinato_z', objektoj.koordinato_z)
                                objektoj.rotacia_x = kwargs.get('rotacia_x', objektoj.rotacia_x)
                                objektoj.rotacia_y = kwargs.get('rotacia_y', objektoj.rotacia_y)
                                objektoj.rotacia_z = kwargs.get('rotacia_z', objektoj.rotacia_z)
                                objektoj.modifo = modifo if kwargs.get('modifo_uuid', False) else objektoj.modifo
                                objektoj.stato = stato if kwargs.get('stato_uuid', False) else objektoj.stato
                                objektoj.integreco = kwargs.get('integreco', objektoj.integreco)
                                objektoj.volumeno_interna = kwargs.get('volumeno_interna', objektoj.volumeno_interna)
                                objektoj.volumeno_ekstera = kwargs.get('volumeno_ekstera', objektoj.volumeno_ekstera)
                                objektoj.volumeno_stokado = kwargs.get('volumeno_stokado', objektoj.volumeno_stokado)

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(objektoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(objektoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                objekto_save(objektoj, kubo_old)
                                status = True
                                message = _('Запись успешно изменена')
                        except Objekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjekto(status=status, message=message, objektoj=objektoj)


# прверка, на случай если изменение проходит сразу на несколько уровней 
# проверяем разрушение
def stato_sekva(integreco, stato):
    if stato.stato_sekva and (integreco<=stato.stato_sekva.integreco):
        #изменяем состояние модификации
        return stato_sekva(integreco,stato.stato_sekva)
    else:
        return stato

# проверяем ремонт
def stato_antaua(integreco, stato):
     # если есть более высокое состояние модификации
    if stato.stato_antaua and (integreco>stato.integreco): # если показатель входит в уровень
        #меняем активное состояние модификации
        return stato_antaua(integreco, stato.stato_antaua)
    else:
        return stato


def forigo_posedanto(ligilo_objektoj, tipo_ligiloj):
    """ выбираем всех владельцев, которых необходимо пометить на удаление и помечаем на удаление """
    """ помечаем на удаление всё, что находится внутри складов (добывающие дроны) """
    objekto_posedantoj = ObjektoPosedanto.objects.filter(
        objekto__in=ligilo_objektoj, forigo=False, 
        arkivo=False, publikigo=True
    ).values('objekto')
    # выбираем все связанные объекты-части корабля
    objektoj_ligiloj = ObjektoLigilo.objects.filter(
        posedanto__in=objekto_posedantoj, tipo__in=tipo_ligiloj, 
        forigo=False, 
        arkivo=False, publikigo=True
    ).values('ligilo')
    if len(objektoj_ligiloj)>0:
        # повторяем выборку составляющих и пометку их на удаление
        forigo_posedanto(objektoj_ligiloj, tipo_ligiloj)
    # помечаем выбранное на удаление
    posedantoj = ObjektoPosedanto.objects.filter(
        objekto__in=ligilo_objektoj, forigo=False, 
        arkivo=False, publikigo=True
    ).update(forigo = True, foriga_dato = timezone.now())


# изменение целостности объекта
class RedaktuObjektoIntegreco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objekto = graphene.Field(ObjektoNode, 
        description=_('Изменённый объект'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        integreco = graphene.Int(description=_('На сколько изменять целостность объекта'))
        # дальнейшие параметры для редактирование астероидов
        volumeno_interna = graphene.Float(description=_('Внутренний объём объекта'))
        volumeno_ekstera = graphene.Float(description=_('Внешний объём объекта'))
        volumeno_stokado = graphene.Float(description=_('Объем хронения (внешний) объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                if kwargs.get('uuid', False):
                    # Изменяем запись
                    if not (kwargs.get('integreco', False)):
                        message = _('Не задано поле integreco для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objekto = Objekto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if not uzanto.has_perm('objektoj.povas_shanghi_objektoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                if objekto.integreco:
                                    integreco = objekto.integreco + kwargs.get('integreco')
                                else:
                                    integreco = kwargs.get('integreco')
                                if integreco<0:
                                    integreco = 0
                                    # если это корабль (тип = 2) и находится в космосе, то 
                                    if (objekto.resurso.tipo.id == 2) and\
                                       (objekto.kubo):
                                       
                                        # измененяем ресурса с корабля на осколки
                                        try:
                                            # находим модель ресурса осколков
                                            # оскольки - ресурс = 31
                                            resurso = Resurso.objects.get(id=31, forigo=False,
                                                                            arkivo=False, publikigo=True)
                                            objekto.resurso = resurso
                                            # изменение модификации id=1 (первая модификация) соответствующего владельца (ресурса)
                                            modifo = ResursoModifo.objects.get(id=1,
                                                    posedanto=resurso, forigo=False, 
                                                    arkivo=False, publikigo=True)
                                            objekto.modifo = modifo
                                            # изменение статуса
                                            stato = ResursoModifoStato.objects.get(id=1,
                                                    posedanto=modifo, forigo=False, 
                                                    arkivo=False, publikigo=True)
                                            objekto.stato = stato
                                            # владельцев помечаем на удаление
                                            # нужно пройти владельцев всех частей корабля
                                            # находим тип связи - id = 1 (Связь с персоналией) - все части корабля
                                            tipo_ligiloj = ObjektoLigiloTipo.objects.filter(
                                                id__in=[1,3], forigo=False, 
                                                arkivo=False, publikigo=True
                                            )
                                            # если больше нет корабля, то запускаем выдачу нового корабля по шаблону
                                            # нашли владельцев и создаём корабль тому,кто управлял данным кораблём
                                            posedanto_uzantoj = ObjektoPosedanto.objects.filter(
                                                objekto=objekto, forigo=False, 
                                                arkivo=False, publikigo=True)
                                            for posedanto_uzanto in posedanto_uzantoj:
                                                # если у пользователя данный управляемый объект, то создаём ему новый
                                                try:
                                                    new_objekto_uzanto = ObjektoUzanto.objects.get(
                                                        autoro=posedanto_uzanto.posedanto_uzanto,
                                                        objekto=objekto,
                                                        publikigo=True, forigo=False, arkivo=False
                                                    )
                                                    # нашли объект, значит нужно создавать новый управляемый объект
                                                    message, uzanto_objekto = defShanghiUzantoObjektoSxablono(uzanto, 
                                                        posedanto_uzanto_id=posedanto_uzanto.posedanto_uzanto.id,
                                                        realeco_id=objekto.realeco.id,
                                                    )
                                                except ObjektoUzanto.DoesNotExist:
                                                    # объект не управляемый
                                                    pass

                                            # выбираем все связанные объекты-части корабля
                                            ligilo_objektoj = ObjektoLigilo.objects.filter(
                                                posedanto=objekto, tipo__in=tipo_ligiloj, 
                                                forigo=False, 
                                                arkivo=False, publikigo=True
                                            ).values('ligilo')
                                            # выбираем всех владельцев, которых необходимо пометить на удаление и помечаем на удаление
                                            # помечаем на удаление всё, что находится внутри складов (добывающие дроны)
                                            forigo_posedanto(ligilo_objektoj, tipo_ligiloj)
                                            # помечаем на удаление владельца корабля
                                            posedantoj = ObjektoPosedanto.objects.filter(
                                                objekto=objekto, forigo=False, 
                                                arkivo=False, publikigo=True
                                            ).update(forigo = True, foriga_dato = timezone.now())
                                        except (Resurso.DoesNotExist, ResursoModifo.DoesNotExist, ResursoModifoStato.DoesNotExist, ObjektoLigiloTipo.DoesNotExist):
                                            pass

                                elif integreco != objekto.stato.integreco:
                                    # меньше текущего значения
                                    if integreco<objekto.stato.integreco: #проверять нижнюю границу
                                        # проверяем наличие нижней границы и границу перехода
                                        if objekto.stato.stato_sekva and (integreco<=objekto.stato.stato_sekva.integreco):
                                            # проверяем на сколько опустились в цикле
                                            objekto.stato = stato_sekva(integreco, objekto.stato.stato_sekva)
                                    elif integreco == objekto.stato.integreco: # если уровень равен максимому в данном состоянии
                                        pass
                                    elif objekto.stato.stato_antaua: # если есть более высокое состояние модификации
                                        objekto.stato = stato_antaua(integreco, objekto.stato.stato_antaua)
                                        if integreco > objekto.stato.integreco:
                                            integreco = objekto.stato.integreco
                                    else: # если это максимальное состояние модификации
                                        integreco = objekto.stato.integreco
                                objekto.integreco = integreco
                                objekto.volumeno_interna = kwargs.get('volumeno_interna', objekto.volumeno_interna)
                                objekto.volumeno_ekstera = kwargs.get('volumeno_ekstera', objekto.volumeno_ekstera)
                                objekto.volumeno_stokado = kwargs.get('volumeno_stokado', objekto.volumeno_stokado)

                                objekto_save(objekto, None)
                                status = True
                                message = _('Запись успешно изменена')
                        except Objekto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoIntegreco(status=status, message=message, objekto=objekto)


# Модель типов владельцев объектов
class RedaktuObjektoPosedantoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_posedantoj_tipoj = graphene.Field(ObjektoPosedantoTipoNode,
        description=_('Созданный/изменённый тип владельцев объектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj_posedantoj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('objektoj.povas_krei_objektoj_posedantoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            objektoj_posedantoj_tipoj = ObjektoPosedantoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(objektoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(objektoj_posedantoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            objektoj_posedantoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_posedantoj_tipoj = ObjektoPosedantoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj_posedantoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj_posedantoj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                objektoj_posedantoj_tipoj.forigo = kwargs.get('forigo', objektoj_posedantoj_tipoj.forigo)
                                objektoj_posedantoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                objektoj_posedantoj_tipoj.arkivo = kwargs.get('arkivo', objektoj_posedantoj_tipoj.arkivo)
                                objektoj_posedantoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                objektoj_posedantoj_tipoj.publikigo = kwargs.get('publikigo', objektoj_posedantoj_tipoj.publikigo)
                                objektoj_posedantoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                objektoj_posedantoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(objektoj_posedantoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(objektoj_posedantoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                objektoj_posedantoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoPosedantoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoPosedantoTipo(status=status, message=message, objektoj_posedantoj_tipoj=objektoj_posedantoj_tipoj)


# Модель статусов владельцев в рамках владения объектом
class RedaktuObjektoPosedantoStatuso(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_posedantoj_statusoj = graphene.Field(ObjektoPosedantoStatusoNode,
        description=_('Созданный/изменённый тип статусов владельцев объектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj_posedantoj_statusoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('objektoj.povas_krei_objektoj_posedantoj_statusoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            objektoj_posedantoj_statusoj = ObjektoPosedantoStatuso.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(objektoj_posedantoj_statusoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(objektoj_posedantoj_statusoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            objektoj_posedantoj_statusoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_posedantoj_statusoj = ObjektoPosedantoStatuso.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj_posedantoj_statusoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj_posedantoj_statusoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                objektoj_posedantoj_statusoj.forigo = kwargs.get('forigo', objektoj_posedantoj_statusoj.forigo)
                                objektoj_posedantoj_statusoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                objektoj_posedantoj_statusoj.arkivo = kwargs.get('arkivo', objektoj_posedantoj_statusoj.arkivo)
                                objektoj_posedantoj_statusoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                objektoj_posedantoj_statusoj.publikigo = kwargs.get('publikigo', objektoj_posedantoj_statusoj.publikigo)
                                objektoj_posedantoj_statusoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                objektoj_posedantoj_statusoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(objektoj_posedantoj_statusoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(objektoj_posedantoj_statusoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                objektoj_posedantoj_statusoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoPosedantoStatuso.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoPosedantoStatuso(status=status, message=message, objektoj_posedantoj_statusoj=objektoj_posedantoj_statusoj)


# проверяем поля для RedaktuObjektoPosedanto
def test_base_posedanto(posedanto_uzanto_id, posedanto_uzanto, 
        posedanto_organizo_uuid, posedanto_organizo,tipo_id,tipo,
        statuso_id, statuso):
    message = None
    if posedanto_uzanto_id:
        try:
            posedanto_uzanto = Uzanto.objects.get(
                id=posedanto_uzanto_id, 
                is_active=True,
                konfirmita=True
            )
        except Uzanto.DoesNotExist:
            message = _('Неверный пользователь')
    if not message:
        if posedanto_organizo_uuid:
            try:
                posedanto_organizo = Organizo.objects.get(
                    uuid=posedanto_organizo_uuid, 
                    forigo=False,
                    arkivo=False, 
                    publikigo=True
                )
            except Organizo.DoesNotExist:
                message = '{} "{}"'.format(_('Неверный организация владелец объекта'),
                                            'posedanto_organizo_uuid')

    if not message:
        if tipo_id:
            try:
                tipo = ObjektoPosedantoTipo.objects.get(
                    id=tipo_id, 
                    forigo=False,
                    arkivo=False, 
                    publikigo=True
                )
            except ObjektoPosedantoTipo.DoesNotExist:
                message = '{} "{}"'.format(_('Неверный тип владельца объекта'),'tipo_id')
        else:
            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

    if not message:
        if statuso_id:
            try:
                statuso = ObjektoPosedantoStatuso.objects.get(
                    id=statuso_id, 
                    forigo=False,
                    arkivo=False, 
                    publikigo=True
                )
            except ObjektoPosedantoStatuso.DoesNotExist:
                message = '{} "{}"'.format(_('Неверный статус владельца объекта'),'statuso_id')
        else:
            message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'statuso_id')

    return message, posedanto_uzanto, posedanto_organizo, tipo, statuso


# Модель владельцев объектов
class RedaktuObjektoPosedanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_posedantoj = graphene.Field(ObjektoPosedantoNode, 
        description=_('Созданный/изменённый владелец объектов'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        objekto_uuid = graphene.String(description=_('Объект владения'))
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец объекта'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация владелец объекта'))
        parto = graphene.Int(description=_('Размер доли владения'))
        tipo_id = graphene.Int(description=_('Тип владельца объекта'))
        statuso_id = graphene.Int(description=_('Статус владельца объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        # posedanto_uzanto = None
        realeco = None
        objekto = None
        posedanto_organizo = None
        tipo = None
        statuso = None
        objektoj_posedantoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Если запись создаётся по объекту, чьим владельцем является пользователь, то разрешаем
                    posedanto_objekto = None
                    # проверяем по полю posedanto
                    if 'objekto_uuid' in kwargs:
                        try:
                            objekto = Objekto.objects.get(uuid=kwargs.get('objekto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            try:
                                posedanto_objekto = ObjektoPosedanto.objects.get(objekto=objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                            except ObjektoPosedanto.DoesNotExist:
                                pass
                        except Objekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владения'),'objekto_uuid')
                    else:
                        message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'objekto_uuid')

                    if uzanto.has_perm('objektoj.povas_krei_objektoj_posedantoj') or posedanto_objekto:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        if not message:
                            message, posedanto_uzanto, posedanto_organizo, tipo, statuso = test_base_posedanto(
                                kwargs.get('posedanto_uzanto_id', False), 
                                uzanto,
                                kwargs.get('posedanto_organizo_uuid', False),
                                posedanto_organizo,
                                kwargs.get('tipo_id', False),
                                tipo,
                                kwargs.get('statuso_id', False),
                                statuso
                            )

                        if not message:
                            objektoj_posedantoj = ObjektoPosedanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_uzanto = posedanto_uzanto,
                                objekto = objekto,
                                realeco = realeco,
                                posedanto_organizo = posedanto_organizo,
                                parto = kwargs.get('parto', 100),
                                tipo = tipo,
                                statuso = statuso,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            objektoj_posedantoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('realeco_id', False) 
                            or kwargs.get('posedanto_uzanto_id', False)
                            or kwargs.get('objekto_uuid', False) or kwargs.get('parto', False)
                            or kwargs.get('statuso_id', False) or kwargs.get('posedanto_organizo_uuid', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_posedantoj = ObjektoPosedanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj_posedantoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj_posedantoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('realeco_id', False)):
                                    try:
                                        realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Realeco.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')

                            if not message:
                                if (kwargs.get('posedanto_uzanto_id', False)):
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            is_active=True,
                                            konfirmita=True
                                        )
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')

                            if not message:
                                if (kwargs.get('objekto_uuid', False)):
                                    try:
                                        objekto = Objekto.objects.get(
                                            uuid=kwargs.get('objekto_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владения'),'objekto_uuid')

                            if not message:
                                if (kwargs.get('posedanto_organizo_uuid', False)):
                                    try:
                                        posedanto_organizo = Organizo.objects.get(
                                            uuid=kwargs.get('posedanto_organizo_uuid'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except Organizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный организация владелец объекта'),
                                                                'posedanto_organizo_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = ObjektoPosedantoTipo.objects.get(
                                            id=kwargs.get('tipo_id'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except ObjektoPosedantoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип владельца объекта'),'tipo_id')

                            if not message:
                                if (kwargs.get('statuso_id', False)):
                                    try:
                                        statuso = ObjektoPosedantoStatuso.objects.get(
                                            id=kwargs.get('statuso_id'), 
                                            forigo=False,
                                            arkivo=False, 
                                            publikigo=True
                                        )
                                    except ObjektoPosedantoStatuso.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный статус владельца объекта'),'statuso_id')

                            if not message:

                                objektoj_posedantoj.forigo = kwargs.get('forigo', objektoj_posedantoj.forigo)
                                objektoj_posedantoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else objektoj_posedantoj.foriga_dato
                                objektoj_posedantoj.arkivo = kwargs.get('arkivo', objektoj_posedantoj.arkivo)
                                objektoj_posedantoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else objektoj_posedantoj.arkiva_dato
                                objektoj_posedantoj.publikigo = kwargs.get('publikigo', objektoj_posedantoj.publikigo)
                                objektoj_posedantoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else objektoj_posedantoj.publikiga_dato
                                objektoj_posedantoj.realeco = realeco if kwargs.get('realeco_id', False) else objektoj_posedantoj.realeco
                                objektoj_posedantoj.objekto = objekto if kwargs.get('objekto_uuid', False) else objektoj_posedantoj.objekto
                                objektoj_posedantoj.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else objektoj_posedantoj.posedanto_uzanto
                                objektoj_posedantoj.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else objektoj_posedantoj.posedanto_organizo
                                objektoj_posedantoj.parto = kwargs.get('parto', objektoj_posedantoj.parto) 
                                objektoj_posedantoj.tipo = tipo if kwargs.get('tipo_id', False) else objektoj_posedantoj.tipo
                                objektoj_posedantoj.statuso = statuso if kwargs.get('statuso_id', False) else objektoj_posedantoj.statuso

                                objektoj_posedantoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoPosedanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoPosedanto(status=status, message=message, objektoj_posedantoj=objektoj_posedantoj)


# Модель мест хранения в объектах
class RedaktuObjektoStokejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_stokejoj = graphene.Field(ObjektoStokejoNode, 
        description=_('Созданное/изменённое место хранения в объектах'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        posedanto_objekto_uuid = graphene.String(description=_('Объект владелец места хранения'))
        tipo_id = graphene.Int(description=_('Тип места хранения ресурсов на основе которого создано это место хранения в объекте'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj_stokejoj = None
        tipo = None
        posedanto_objekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('objektoj.povas_krei_objektoj_stokejoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if (kwargs.get('posedanto_objekto_uuid', False)):
                                try:
                                    posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'),
                                        forigo=False, arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец места хранения'),'posedanto_objekto_uuid')

                        if not message:
                            if (kwargs.get('tipo_id', False)):
                                try:
                                    tipo = ResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'),
                                        forigo=False, arkivo=False, publikigo=True)
                                except ResursoStokejoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов на основе которого создано это место хранения в объекте'),'tipo_id')

                        if not message:
                            objektoj_stokejoj = ObjektoStokejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto_objekto = posedanto_objekto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(objektoj_stokejoj.nomo, 
                                           kwargs.get('nomo'), 
                                           info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(objektoj_stokejoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            objektoj_stokejoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('posedanto_objekto', False)
                            or kwargs.get('nomo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_stokejoj = ObjektoStokejo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj_stokejoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj_stokejoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if (kwargs.get('posedanto_objekto_uuid', False)):
                                    try:
                                        posedanto_objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_objekto_uuid'),
                                            forigo=False, arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец места хранения'),'posedanto_objekto_uuid')

                            if not message:
                                if (kwargs.get('tipo_id', False)):
                                    try:
                                        tipo = ResursoStokejoTipo.objects.get(id=kwargs.get('tipo_id'),
                                            forigo=False, arkivo=False, publikigo=True)
                                    except ResursoStokejoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов на основе которого создано это место хранения в объекте'),'tipo_id')

                            if not message:
                                objektoj_stokejoj.forigo = kwargs.get('forigo', objektoj_stokejoj.forigo)
                                objektoj_stokejoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                objektoj_stokejoj.arkivo = kwargs.get('arkivo', objektoj_stokejoj.arkivo)
                                objektoj_stokejoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                objektoj_stokejoj.publikigo = kwargs.get('publikigo', objektoj_stokejoj.publikigo)
                                objektoj_stokejoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                objektoj_stokejoj.posedanto_objekto = posedanto_objekto if kwargs.get('posedanto_objekto_uuid', False) else objektoj_stokejoj.posedanto_objekto
                                objektoj_stokejoj.tipo = tipo if kwargs.get('tipo_id', False) else objektoj_stokejoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(objektoj_stokejoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(objektoj_stokejoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                objektoj_stokejoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoStokejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoStokejo(status=status, message=message, objektoj_stokejoj=objektoj_stokejoj)


# Модель типов связей объектов между собой
class RedaktuObjektoLigiloTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_ligiloj_tipoj = graphene.Field(ObjektoLigiloTipoNode,
        description=_('Созданный/изменённый тип связей объектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        nomo_json = GenericScalar(description=_('Название в JSON формате для администраторов'))
        priskribo = graphene.String(description=_('Описание'))
        priskribo_json = GenericScalar(description=_('Описание в JSON формате для администраторов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj_ligiloj_tipoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('objektoj.povas_krei_objektoj_ligiloj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            objektoj_ligiloj_tipoj = ObjektoLigiloTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo_json', False)):
                                if uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj_tipoj'):
                                    objektoj_ligiloj_tipoj.nomo = kwargs.get('nomo_json')
                            elif (kwargs.get('nomo', False)):
                                set_enhavo(objektoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo_json', False)):
                                if uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj_tipoj'):
                                    objektoj_ligiloj_tipoj.priskribo = kwargs.get('priskribo_json')
                            elif (kwargs.get('priskribo', False)):
                                set_enhavo(objektoj_ligiloj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            objektoj_ligiloj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('nomo_json', False) or kwargs.get('priskribo_json', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_ligiloj_tipoj = ObjektoLigiloTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('objektoj.povas_shanghi_objektoj_ligiloj_tipoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:

                                objektoj_ligiloj_tipoj.forigo = kwargs.get('forigo', objektoj_ligiloj_tipoj.forigo)
                                objektoj_ligiloj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                objektoj_ligiloj_tipoj.arkivo = kwargs.get('arkivo', objektoj_ligiloj_tipoj.arkivo)
                                objektoj_ligiloj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                objektoj_ligiloj_tipoj.publikigo = kwargs.get('publikigo', objektoj_ligiloj_tipoj.publikigo)
                                objektoj_ligiloj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                objektoj_ligiloj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo_json', False)):
                                    if uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj_tipoj'):
                                        objektoj_ligiloj_tipoj.nomo = kwargs.get('nomo_json', objektoj_ligiloj_tipoj.nomo)
                                elif (kwargs.get('nomo', False)):
                                    set_enhavo(objektoj_ligiloj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo_json', False)):
                                    if uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj_tipoj'):
                                        objektoj_ligiloj_tipoj.priskribo = kwargs.get('priskribo_json', objektoj_ligiloj_tipoj.priskribo)
                                elif (kwargs.get('priskribo', False)):
                                    set_enhavo(objektoj_ligiloj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                objektoj_ligiloj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoLigiloTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoLigiloTipo(status=status, message=message, objektoj_ligiloj_tipoj=objektoj_ligiloj_tipoj)


# serĉi - искать
# supro - верхняя оконечность
def sercxi_ligilo_posedanto(objekto, supro_ligilo):
    """ ищеть в ligilo по восходящей связи (posedanto) верхний объект"""
    ligiloj = ObjektoLigilo.objects.filter(
        ligilo=objekto, 
        forigo=False,
        arkivo=False, 
        publikigo=True
    )
    for ligilo in ligiloj:
        if ligilo.posedanto:
            return (sercxi_ligilo_posedanto(ligilo.posedanto, ligilo))
    return objekto, supro_ligilo


# сохраняем связь
def objekto_ligilo_save(objektoj_ligiloj):

    def eventoj():
        LigiloEventoj.sxangxi_ligilo(objektoj_ligiloj)
        if objektoj_ligiloj.posedanto:
            objekto, ligilo = sercxi_ligilo_posedanto(objektoj_ligiloj.posedanto, objektoj_ligiloj)
            ObjektoLigiloEventoj.sxangxi_objekto_ligilo(objekto, ligilo, objektoj_ligiloj, None)

    objektoj_ligiloj.save()
    # вызов функции через коммит транзакции обязателен, иначе данные в подписке ещё не изменёнными будут
    transaction.on_commit(eventoj)


# Модель связи объектов между собой
class RedaktuObjektoLigilo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj_ligiloj = graphene.Field(ObjektoLigiloNode, 
        description=_('Созданная/изменённая связь объектов между собой'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        tipo_id = graphene.Int(description=_('Код типа места хранения ресурсов на основе которого создано это место хранения в объекте'))
        posedanto_uuid = graphene.String(description=_('UUID объекта владельца связи'))
        posedanto_stokejo_uuid = graphene.String(description=_('UUID места хранения владельца связи'))
        ligilo_uuid = graphene.String(description=_('UUID связываемого объекта'))
        konektilo_posedanto = graphene.Int(description=_('Разъём (слот), который занимает этот объект у родительского объекта'))
        konektilo_ligilo = graphene.Int(description=_('Разъём (слот), который занимает этот объект у связываемого объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj_ligiloj = None
        posedanto = None
        tipo = None
        posedanto_stokejo = None
        ligilo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Если запись создаётся по объекту, чьим владельцем является пользователь, то разрешаем
                    posedanto_objekto = None
                    # проверяем по полю posedanto
                    if 'posedanto_uuid' in kwargs:
                        try:
                            objekto = Objekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            try:
                                posedanto_objekto = ObjektoPosedanto.objects.get(objekto=objekto, 
                                            posedanto_uzanto=uzanto,
                                            forigo=False, arkivo=False, publikigo=True)
                            except ObjektoPosedanto.DoesNotExist:
                                pass
                        except Objekto.DoesNotExist:
                            message = '{} "{}"'.format(_('Неверный объект владелец связи'),'posedanto_uuid')
                    else:
                        message = '{} "{}"'.format(_('Не указан объект владелец связи'),'posedanto_uuid')

                    if not posedanto_objekto:
                        # проверяем по полю ligilo
                        if 'ligilo_uuid' in kwargs:
                            try:
                                objekto = Objekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                try:
                                    posedanto_objekto = ObjektoPosedanto.objects.get(objekto=objekto, 
                                                posedanto_uzanto=uzanto,
                                                forigo=False, arkivo=False, publikigo=True)
                                except ObjektoPosedanto.DoesNotExist:
                                    pass
                            except Objekto.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный объект владелец связи'),'ligilo_uuid')
                    else:
                        message = '{} "{}"'.format(_('Не указан объект владелец связи'),'posedanto_uuid')
                    if uzanto.has_perm('objektoj.povas_krei_objektoj_ligiloj') or posedanto_objekto:
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uuid' in kwargs:
                                try:
                                    posedanto = Objekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный объект владелец связи'),'posedanto_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан объект владелец связи'),'posedanto_uuid')

                        if not message:
                            if 'posedanto_stokejo_uuid' in kwargs:
                                try:
                                    posedanto_stokejo = ObjektoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ObjektoStokejo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                        if not message:
                            if 'ligilo_uuid' in kwargs:
                                try:
                                    ligilo = Objekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Objekto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный связываемый объект'),'ligilo_uuid')
                            else:
                                message = '{} "{}"'.format(_('Не указан связываемый объект'),'ligilo_uuid')

                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = ObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except ObjektoLigiloTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Не указан тип места хранения ресурсов'),'tipo_id')

                        if not message:
                            objektoj_ligiloj = ObjektoLigilo.objects.create(
                                forigo=False,
                                arkivo=False,
                                posedanto = posedanto,
                                posedanto_stokejo = posedanto_stokejo,
                                ligilo = ligilo,
                                tipo=tipo,
                                konektilo_posedanto=kwargs.get('konektilo_posedanto', None),
                                konektilo_ligilo=kwargs.get('konektilo_ligilo', None),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            objekto_ligilo_save(objektoj_ligiloj)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_uuid', False) or kwargs.get('posedanto_stokejo_uuid', False)
                            or kwargs.get('ligilo_uuid', False) or kwargs.get('tipo_id', False)
                            or kwargs.get('konektilo_posedanto', False) or kwargs.get('konektilo_ligilo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            objektoj_ligiloj = ObjektoLigilo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            # Если редактируем запись связи по объекту, чьим владельцем является пользователь, то разрешаем
                            posedanto_objekto = None
                            # проверяем по полю posedanto
                            try:
                                posedanto_objekto = ObjektoPosedanto.objects.get(
                                    objekto=objektoj_ligiloj.posedanto, 
                                    posedanto_uzanto=uzanto,
                                    forigo=False, arkivo=False, publikigo=True)
                            except ObjektoPosedanto.DoesNotExist:
                                pass
                            if not posedanto_objekto:
                                # проверяем по полю ligilo
                                try:
                                    posedanto_objekto = ObjektoPosedanto.objects.get(
                                        objekto=objektoj_ligiloj.ligilo, 
                                        posedanto_uzanto=uzanto,
                                        forigo=False, arkivo=False, publikigo=True)
                                except ObjektoPosedanto.DoesNotExist:
                                        pass
                            if (not (uzanto.has_perm('objektoj.povas_forigi_objektoj_ligiloj')
                                    or posedanto_objekto)
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not (uzanto.has_perm('objektoj.povas_shanghi_objektoj_ligiloj')
                                    or posedanto_objekto):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uuid' in kwargs:
                                    try:
                                        posedanto = Objekto.objects.get(uuid=kwargs.get('posedanto_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный объект владелец связи'),'posedanto_uuid')

                            if not message:
                                if 'posedanto_stokejo_uuid' in kwargs:
                                    try:
                                        posedanto_stokejo = ObjektoStokejo.objects.get(uuid=kwargs.get('posedanto_stokejo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ObjektoStokejo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное место хранения владельца связи'),'posedanto_stokejo_uuid')

                            if not message:
                                if 'ligilo_uuid' in kwargs:
                                    try:
                                        ligilo = Objekto.objects.get(uuid=kwargs.get('ligilo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Objekto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный связываемый объект'),'ligilo_uuid')

                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = ObjektoLigiloTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except ObjektoLigiloTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип места хранения ресурсов'),'tipo_id')

                            if not message:

                                objektoj_ligiloj.forigo = kwargs.get('forigo', objektoj_ligiloj.forigo)
                                objektoj_ligiloj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                objektoj_ligiloj.arkivo = kwargs.get('arkivo', objektoj_ligiloj.arkivo)
                                objektoj_ligiloj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                objektoj_ligiloj.publikigo = kwargs.get('publikigo', objektoj_ligiloj.publikigo)
                                objektoj_ligiloj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                objektoj_ligiloj.posedanto = posedanto if kwargs.get('posedanto_uuid', False) else objektoj_ligiloj.posedanto
                                objektoj_ligiloj.posedanto_stokejo = posedanto_stokejo if kwargs.get('posedanto_stokejo_uuid', False) else objektoj_ligiloj.posedanto_stokejo
                                objektoj_ligiloj.ligilo = ligilo if kwargs.get('ligilo_uuid', False) else objektoj_ligiloj.ligilo
                                objektoj_ligiloj.tipo = tipo if kwargs.get('tipo_id', False) else objektoj_ligiloj.tipo
                                objektoj_ligiloj.konektilo_posedanto = kwargs.get('konektilo_posedanto', objektoj_ligiloj.konektilo_posedanto) 
                                objektoj_ligiloj.konektilo_ligilo = kwargs.get('konektilo_ligilo', objektoj_ligiloj.konektilo_ligilo) 

                                objekto_ligilo_save(objektoj_ligiloj)
                                status = True
                                message = _('Запись успешно изменена')
                        except ObjektoLigilo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuObjektoLigilo(status=status, message=message, objektoj_ligiloj=objektoj_ligiloj)


# Создание объекта по шаблону (okazigi - создать) в конкретной реальности
class OkazigiObjektoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj = graphene.Field(ObjektoNode, 
        description=_('Созданный объект'))

    class Arguments:
        sxablono_id = graphene.String(description=_('ID шаблона для создаваемого объекта')) # по умолчанию = 2 (Стартовый корабль после разрушения)
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # далее параметры объекта, которые может править владелец объекта
        # владелец создаваемого объекта
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        realeco = None
        objektoj = None
        uzanto = info.context.user
        posedanto_uzanto = None

        if uzanto.is_authenticated:
            with transaction.atomic():
                # находим владельца, на которого создаём
                if not message:
                    if (kwargs.get('posedanto_uzanto_id', False)):
                        try:
                            posedanto_uzanto = Uzanto.objects.get(
                                id=kwargs.get('posedanto_uzanto_id'), 
                                is_active=True,
                                konfirmita=True
                            )
                        except Uzanto.DoesNotExist:
                            message = _('Неверный пользователь')
                    else:
                        message = '{} "{}"'.format(_('Не задан пользователь'),'posedanto_uzanto_id')

                if not message:
                    if uzanto.has_perm('objektoj.povas_krei_objektoj'):

                        # Проверяем наличие всех полей
                        # проверяем наличие записей с таким кодом
                        if not message:
                            # находим нужную реальность
                            if (kwargs.get('realeco_id', False)):
                                try:
                                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Realeco.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')

                        # находим id шаблона
                        if not message:
                            sxablono_id = kwargs.get('sxablono_id', 2)
                            try:
                                sxablono = Sxablono.objects.get(id=sxablono_id, forigo=False, 
                                                                        arkivo=False, publikigo=True)

                                # идём по спискам шаблонов объектов
                                for obj in sxablono.objekto.all():
                                    if obj.realeco == realeco:
                                        shablono = []
                                        modeloj = []
                                        objektoj = create_model(obj,obj,'',posedanto_uzanto,shablono,modeloj,False)
                                        break

                            except Sxablono.DoesNotExist:
                                message = '{} "{}"'.format(_('Неверный шаблон'),'sxablono_id или 2')

                    else:
                        message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return OkazigiObjektoSxablono(status=status, message=message, objektoj=objektoj)


# функция создания нового управляемого объекта по шаблону в конкретной реальности с отключением старого управляемого объекта
def defShanghiUzantoObjektoSxablono(uzanto, **kwargs):
    message = ''
    if uzanto.has_perm('objektoj.povas_krei_objektoj'):
        # находим владельца, на которого создаём
        if not message:
            if (kwargs.get('posedanto_uzanto_id', False)):
                try:
                    posedanto_uzanto = Uzanto.objects.get(
                        id=kwargs.get('posedanto_uzanto_id'), 
                        is_active=True,
                        konfirmita=True
                    )
                except Uzanto.DoesNotExist:
                    message = _('Неверный пользователь')
            else:
                message = '{} "{}"'.format(_('Не задан пользователь'),'posedanto_uzanto_id')
        # Проверяем наличие всех полей
        # проверяем наличие записей с таким кодом
        if not message:
            # находим нужную реальность
            if (kwargs.get('realeco_id', False)):
                try:
                    realeco = Realeco.objects.get(id=kwargs.get('realeco_id'), forigo=False,
                                                        arkivo=False, publikigo=True)
                except Realeco.DoesNotExist:
                    message = '{} "{}"'.format(_('Неверная реальность'),'realeco_id')
            else:
                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco_id')
        # находим id шаблона
        if not message:
            sxablono_id = kwargs.get('sxablono_id', 2)
            try:
                sxablono = Sxablono.objects.get(id=sxablono_id, forigo=False, 
                                                        arkivo=False, publikigo=True)
                # идём по спискам шаблонов объектов
                for obj in sxablono.objekto.all():
                    if obj.realeco == realeco:
                        shablono = []
                        modeloj = []
                        # создаём новый объект
                        objektoj = create_model(obj,obj,'',posedanto_uzanto,shablono,modeloj,False)

                        try:
                            # меняем связь управляемого объекта на новый управляемый объект
                            uzanto_objekto = ObjektoUzanto.objects.get(realeco=realeco, autoro=posedanto_uzanto, 
                                                                            forigo=False, arkivo=False, publikigo=True)
                            uzanto_objekto.objekto = objektoj

                        except ObjektoUzanto.DoesNotExist:
                            # создаём связь управляемого объекта для нового управляемого объекта
                            uzanto_objekto = ObjektoUzanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                objekto = objektoj,
                                autoro = posedanto_uzanto,
                                realeco = realeco,
                                publikigo=True,
                                publikiga_dato=timezone.now()
                            )
                        objekto_uzanto_save(uzanto_objekto)

                        message = '{}'.format(_('Новый объект создан'))

            except Sxablono.DoesNotExist:
                message = '{} "{}"'.format(_('Неверный шаблон'),'sxablono_id или 2')
    else:
        message = _('Недостаточно прав')
    return message, uzanto_objekto


# Создание нового управляемого объекта по шаблону в конкретной реальности с отключением старого управляемого объекта
class ShanghiUzantoObjektoSxablono(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    objektoj = graphene.Field(ObjektoNode, 
        description=_('Созданный объект'))
    objekto_uzanto =  graphene.Field(ObjektoUzantoNode, 
        description=_('Связь управляемый объект'))

    class Arguments:
        sxablono_id = graphene.Int(description=_('ID шаблона для создаваемого объекта')) # по умолчанию = 2 (Стартовый корабль после разрушения)
        realeco_id = graphene.Int(description=_('Параллельный мир'))
        # далее параметры объекта, которые может править владелец объекта
        # владелец создаваемого объекта
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь владелец объекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        objektoj = None
        uzanto_objekto = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                message, uzanto_objekto = defShanghiUzantoObjektoSxablono(uzanto, **kwargs)
                objektoj = uzanto_objekto.objektoj
        else:
            message = _('Требуется авторизация')

        return ShanghiUzantoObjektoSxablono(status=status, message=message, objektoj=objektoj, objekto_uzanto=uzanto_objekto)


# Импорт температурного режима хранения/транспортировки из 1с
class ImportoObjektoTemperaturaReghimo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(ObjektoTemperaturaReghimoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        url = settings.ODATA_URL+'Catalog_СКЛ_ТемпературныйРежим?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('objektoj.povas_krei_objektoj_temperatura_reghimo'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                errors = list()
                jsj = r.json()
                for js in jsj['value']:

                    uuid = js['Ref_Key']
                    try:
                        importo = ObjektoTemperaturaReghimo.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except ObjektoTemperaturaReghimo.DoesNotExist:
                        kodo = js['Code']
                        priskribo = js['Description']
                        malsupra_limo = js['НижняяГраница']
                        supra_limo = js['ВерхняяГраница']
                        malsupra_limo_ne = js['НижнейГраницыНет']
                        supra_limo_ne = js['ВерхнейГраницыНет']
                        predefined = js['Predefined']
                        predefined_nomo = js['PredefinedDataName']

                        if not len(errors):
                            importo = ObjektoTemperaturaReghimo.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                kodo=kodo,
                                malsupra_limo=malsupra_limo,
                                supra_limo=supra_limo,
                                malsupra_limo_ne=malsupra_limo_ne,
                                supra_limo_ne=supra_limo_ne,
                                predefined=predefined
                            )
                            if priskribo:
                                set_enhavo(importo.priskribo, priskribo, 'ru_RU')
                            if predefined_nomo:
                                set_enhavo(importo.predefined_nomo, predefined_nomo, 'ru_RU')

                            importo.save()
                            kvanto += 1

                        else:
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoObjektoTemperaturaReghimo1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoObjektoTemperaturaReghimo1c(status=status, message=message, errors=errors, importo=importo)


# Импорт температурного режима хранения/транспортировки из 1с
class ImportoObjektoTipoKontenero1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(ObjektoTipoKonteneroNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        url = settings.ODATA_URL+'Catalog_Бит_ТипКонтейнера?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('objektoj.povas_krei_objektoj_tipo_kontenero'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                errors = list()
                for js in jsj['value']:

                    uuid = js['Ref_Key']
                    try:
                        importo = ObjektoTipoKontenero.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except ObjektoTipoKontenero.DoesNotExist:
                        pass

                        kodo = js['Code']
                        priskribo = js['Description']
                        volumeno = js['Объем']
                        pezo = js['Вес']
                        komento = js['Комментарий']
                        pezo_pakumo = js['ВесТары']
                        predefined = js['Predefined']
                        predefined_nomo = js['PredefinedDataName']

                        if not len(errors):
                            importo = ObjektoTipoKontenero.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                kodo=kodo,
                                volumeno=volumeno,
                                pezo=pezo,
                                pezo_pakumo=pezo_pakumo,
                                predefined=predefined
                            )
                            if priskribo:
                                set_enhavo(importo.priskribo, priskribo, 'ru_RU')
                            if komento:
                                set_enhavo(importo.komento, komento, 'ru_RU')
                            if predefined_nomo:
                                set_enhavo(importo.predefined_nomo, predefined_nomo, 'ru_RU')

                            importo.save()
                            kvanto += 1

                        else:
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoObjektoTipoKontenero1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoObjektoTipoKontenero1c(status=status, message=message, errors=errors, importo=importo)


# Добавление города в 1с
class AldoniObjektoTipoKontenero1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    aldoni = graphene.Field(ObjektoTipoKonteneroNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        user = info.context.user
        print('== вход ==')

        # url = "https://192.168.99.5/skl_test_site/odata/standard.odata/Catalog_СКЛ_Города(guid'eed47c08-3c81-11ec-8bc3-00155d091105')?$format=application/json"
        url = settings.ODATA_URL+'Catalog_СКЛ_Города?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('objektoj.povas_krei_objektoj_temperatura_reghimo'):
            posedanto = None
            komunumo = None
            uzanto = None

            with transaction.atomic():

                datoj = {
                #     # "Ref_Key": "99a1f080-62cf-11e5-80f1-000c298958d9",
                #     "Объем" : 2

                # }
                # существующий eed47c08-3c81-11ec-8bc3-00155d091105 Абакан 
                    # "guid": "18be3392-d720-11e5-80f8-000c298958d9",
                    "Ref_Key": "18be3391-d720-11e5-80f8-000c298958d9",
                    "Description": "Абакан",
                    # "DataVersion" : "AAAAAACFqYU=",
                    # "DeletionMark": False,
                    "Code": "000001",
                # datoj = { # работающий вариант
                #     "Description": "Магадан",
                }
                # headers = {'1C_OData-DataLoadMode': bytes(0)}

                json_string = json.dumps(datoj)
                print('== json_string == ', json_string)

                # r = requests.delete(url, auth=(login, password), verify=False)
                # r = requests.post(url, json_string, auth=(login, password), verify=False)
                # r = requests.post(url, headers=headers, data=json_string, auth=(login, password), verify=False)
                r = requests.post(url, data=json_string, auth=(login, password), verify=False)
                kvanto = 0
                print('== r == ',r)
                jsj = r.json()
                print('== jsj == ', jsj)
                # errors = list()
                # if not message:
                #     status = True
                #     errors = list()
                #     if kvanto > 0:
                #         message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                #     else:
                #         message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, aldoni

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        aldoni = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, aldoni = AldoniObjektoTipoKontenero1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return AldoniObjektoTipoKontenero1c(status=status, message=message, errors=errors, aldoni=aldoni)


# Импорт типов упаковок из 1с
class ImportoObjektoTipoPakumo1c(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    errors = graphene.List(ErrorNode)
    importo = graphene.Field(ObjektoTipoPakumoNode)

    class Arguments:
        adreso = graphene.String(description=_('Адрес сервера, откуда производить импорт данных'))

    @staticmethod
    def create(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        url = settings.ODATA_URL+'Catalog_ТипУпаковкиСКЛ?$format=application/json'
        login = settings.ODATA_USER
        password = settings.ODATA_PASSWORD

        if user.has_perm('objektoj.povas_krei_objektoj_tipo_pakumo'):

            with transaction.atomic():
                r = requests.get(url,auth=(login, password), verify=False)
                kvanto = 0
                jsj = r.json()
                errors = list()
                for js in jsj['value']:

                    uuid = js['Ref_Key']
                    try:
                        importo = ObjektoTipoPakumo.objects.get(uuid=uuid, forigo=False,
                                                            arkivo=False, publikigo=True)
                        next # такой есть, идём к следующему
                    except ObjektoTipoPakumo.DoesNotExist:
                        pass

                        kodo = js['Code']
                        priskribo = js['Description']
                        longo = js['Длина']
                        largho = js['Ширина']
                        alto = js['Высота']
                        pezo_fakta = js['ВесФактический']
                        retropasxo_pakumo = js['ВозвратУпаковки']

                        if not len(errors):
                            importo = ObjektoTipoPakumo.objects.create(
                                uuid=uuid,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now(),
                                kodo=kodo,
                                longo=longo,
                                largho=largho,
                                alto=alto,
                                pezo_fakta=pezo_fakta,
                                retropasxo_pakumo=retropasxo_pakumo
                            )
                            if priskribo:
                                set_enhavo(importo.priskribo, priskribo, 'ru_RU')

                            importo.save()
                            kvanto += 1

                        else:
                            message = _('Nevalida argumentvaloroj')
                            for error in errors:
                                message = '{};  {}: {} - {} {}'.format(message, _('Ошибка'),
                                    error.message, _('в поле'), error.field)
                if not message:
                    status = True
                    errors = list()
                    if kvanto > 0:
                        message =  '{} {} {}'.format(_('Добавлено'), kvanto, _('записей'))
                    else:
                        message =  _('Не найдено новых объектов для добавления')
        else:
            message = _('Nesufiĉaj rajtoj')

        return status, message, errors, importo

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        errors = list()
        importo = None
        user = info.context.user

        if user.is_authenticated:
            # Импортируем данные
            status, message, errors, importo = ImportoObjektoTipoPakumo1c.create(root, info, **kwargs)
        else:
            message = _('Bezonata rajtigo')

        return ImportoObjektoTipoPakumo1c(status=status, message=message, errors=errors, importo=importo)


class ObjektoMutations(graphene.ObjectType):
    redaktu_objekto = RedaktuObjekto.Field(
        description=_('''Создаёт или редактирует объекты''')
    )
    redaktu_objekto_integreco = RedaktuObjektoIntegreco.Field(
        description=_('''Редактирует целостность объекта''')
    )
    redaktu_objekto_posedantoj_tipoj = RedaktuObjektoPosedantoTipo.Field(
        description=_('''Создаёт или редактирует типы владельцев объектов''')
    )
    redaktu_objekto_posedantoj_statusoj = RedaktuObjektoPosedantoStatuso.Field(
        description=_('''Создаёт или редактирует статусы владельца в рамках владения объектом''')
    )
    redaktu_objekto_posedantoj = RedaktuObjektoPosedanto.Field(
        description=_('''Создаёт или редактирует объекты''')
    )
    redaktu_objekto_stokejoj = RedaktuObjektoStokejo.Field(
        description=_('''Создаёт или редактирует место хранения в объектах''')
    )
    redaktu_objekto_ligiloj_tipoj = RedaktuObjektoLigiloTipo.Field(
        description=_('''Создаёт или редактирует типы связей объектов между собой''')
    )
    redaktu_objekto_ligiloj = RedaktuObjektoLigilo.Field(
        description=_('''Создаёт или редактирует связи объектов между собой''')
    )
    okazigi_objekto_sxablono = OkazigiObjektoSxablono.Field(
        description=_('''Создаёт объект по шаблону (okazigi - создать) в конкретной реальности''')
    )
    shanghi_uzanto_objekto_sxablono = ShanghiUzantoObjektoSxablono.Field(
        description=_('''Создаёт объект по шаблону и делает его управляемым в конкретной реальности''')
    )
    importo_objekto_temperatura_reghimo1c = ImportoObjektoTemperaturaReghimo1c.Field(
        description=_('''Импорт температурного режима хранения/транспортировки из 1с''')
    )
    importo_objekto_tipo_kontenero1c = ImportoObjektoTipoKontenero1c.Field(
        description=_('''Импорт типов контейнеров из 1с''')
    )
    aldoni_objekto_tipo_kontenero1c = AldoniObjektoTipoKontenero1c.Field(
        description=_(''' типов контейнеров из 1с''')
    )
    importo_objekto_tipo_pakumo1c = ImportoObjektoTipoPakumo1c.Field(
        description=_('''Импорт типов упаковок из 1с''')
    )
