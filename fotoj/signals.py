"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils import timezone
from siriuso.utils import set_enhavo
from komunumoj.models import Komunumo, KomunumojAliro
from main.models import Uzanto
from uzantoj.models import UzantojAliro
from django.core.files.base import ContentFile
from siriuso.utils.thumbnailer import get_image_properties, get_image_resize
from PIL import Image
from .models import *
import io


@receiver(post_save, sender=Komunumo)
def create_kom_albumoj(sender, instance, **kwargs):
    """Проверяем наличие альбома для стены Сообщества, если его нет, то создаём его"""
    for tipo_kodo, albumo_nomo in (('baza', 'Основной'), ('muro', 'Изображения стены'), ('kovrilo', 'Обложки'),
                                   ('avataro', 'Аватары')):
        try:
            FotojAlbumoKomunumo.objects.get(posedanto=instance, forigo=False, tipo__kodo=tipo_kodo)
        except:
            tipo = FotojAlbumoKomunumoTipo.objects.get(kodo=tipo_kodo, forigo=False)
            aliro = KomunumojAliro.objects.get(kodo='chiuj')

            albumo = FotojAlbumoKomunumo.objects.create(
                autoro=instance.autoro,
                tipo=tipo,
                aliro=aliro,
                komentado_aliro=aliro,
                posedanto=instance,
                arkivo=False,
                publikigo=True,
                publikiga_dato=timezone.now(),
                forigo=False
            )

            set_enhavo(albumo.nomo, albumo_nomo, 'ru_RU')
            albumo.save()


@receiver(post_save, sender=Uzanto)
def create_uzanto_muro_album(sender, instance, **kwargs):
    """Проверяем наличие альбома для стены пользователя, если его нет, то создаём его"""
    for tipo_kodo, albumo_nomo in (('baza', 'Основной'), ('muro', 'Изображения стены'), ('kovrilo', 'Обложки'),
                                   ('avataro', 'Аватары')):
        try:
            FotojAlbumoUzanto.objects.get(posedanto=instance, forigo=False, tipo__kodo=tipo_kodo)
        except:
            tipo = FotojAlbumoUzantoTipo.objects.get(kodo=tipo_kodo, forigo=False)
            aliro = UzantojAliro.objects.get(kodo='chiuj')

            albumo = FotojAlbumoUzanto.objects.create(
                posedanto=instance,
                tipo=tipo,
                aliro=aliro,
                komentado_aliro=aliro,
                arkivo=False,
                publikigo=True,
                publikiga_dato=timezone.now(),
                forigo=False
            )

            set_enhavo(albumo.nomo, albumo_nomo, 'ru_RU')
            albumo.save()


@receiver(post_save, sender=FotojFotoDosiero)
def create_thumbnails(sender, instance, **kwargs):
    """Создаём миниатюры для базового изображения при создании объекта или при изменении исходного изображения"""

    if ('bildo_baza' in (kwargs.get('update_fields') or [])
            or not (instance.bildo_a and instance.bildo_b and instance.bildo_c and instance.bildo_d)):
        bildo = Image.open(instance.bildo_baza)
        bildo_params = {
            'width': instance.bildo_baza.width,
            'height': instance.bildo_baza.height,
        }

        for field, size in {
            'bildo_a': (1280, 1024), 'bildo_b': (770, 500), 'bildo_c': (400, 260), 'bildo_d': (300, 200),
        }.items():
            if bildo_params['width'] > size[0] or bildo_params['height'] > size[1]:
                nova_bildo = bildo.copy()
                nova_bildo.thumbnail(size, Image.ANTIALIAS)
            else:
                nova_bildo = None

            if nova_bildo:
                raw = io.BytesIO()
                nova_bildo.save(raw, bildo.format)
                raw.seek(0)
                nova_bildo = ContentFile(raw.read())
                nova_bildo.name = '{}_{}.{}'.format(
                    instance.bildo_baza.name.split('.')[0],
                    field.split('_')[1],
                    instance.bildo_baza.name.split('.')[-1]
                )
            setattr(instance, field, nova_bildo)

        post_save.disconnect(create_thumbnails, sender=FotojFotoDosiero)
        instance.save()
        post_save.connect(create_thumbnails, sender=FotojFotoDosiero)
