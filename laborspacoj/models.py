"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import sys
from uuid import uuid4
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import Permission
from django.db.models import Max, Q

from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo, perms
from main.models import Uzanto
from komunumoj.models import Komunumo
from universo_bazo.models import UniversoBazaMaks, UniversoBazaRealeco, Realeco
from organizoj.models import Organizo


# Категории рабочих пространств, использует абстрактный класс UniversoBazaMaks
class LaborspacoKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='laborspacoj_laborspacoj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_kategorioj', _('Povas vidi kategorioj de laborspacoj')),
            ('povas_krei_laborspacoj_kategorioj', _('Povas krei kategorioj de laborspacoj')),
            ('povas_forigi_laborspacoj_kategorioj', _('Povas forigi kategorioj de laborspacoj')),
            ('povas_shangxi_laborspacoj_kategorioj', _('Povas ŝanĝi kategorioj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                              using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_kategorioj',
                'laborspacoj.povas_krei_laborspacoj_kategorioj',
                'laborspacoj.povas_forigi_laborspacoj_kategorioj',
                'laborspacoj.povas_shangxi_laborspacoj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_kategorioj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы рабочих пространств, использует абстрактный класс UniversoBazaMaks
class LaborspacoTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='laborspacoj_laborspacoj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_tipoj', _('Povas vidi tipoj de laborspacoj')),
            ('povas_krei_laborspacoj_tipoj', _('Povas krei tipoj de laborspacoj')),
            ('povas_forigi_laborspacoj_tipoj', _('Povas forigi tipoj de laborspacoj')),
            ('povas_shangxi_laborspacoj_tipoj', _('Povas ŝanĝi tipoj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_tipoj',
                'laborspacoj.povas_krei_laborspacoj_tipoj',
                'laborspacoj.povas_forigi_laborspacoj_tipoj',
                'laborspacoj.povas_shangxi_laborspacoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус рабочего пространства, использует абстрактный класс UniversoBazaMaks
class LaborspacoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_statusoj', _('Povas vidi statusoj de laborspacoj')),
            ('povas_krei_laborspacoj_statusoj', _('Povas krei statusoj de laborspacoj')),
            ('povas_forigi_laborspacoj_statusoj', _('Povas forigi statusoj de laborspacoj')),
            ('povas_shangxi_laborspacoj_statusoj', _('Povas ŝanĝi statusoj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_statusoj',
                'laborspacoj.povas_krei_laborspacoj_statusoj',
                'laborspacoj.povas_forigi_laborspacoj_statusoj',
                'laborspacoj.povas_shangxi_laborspacoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_statusoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Рабочие пространства, использует абстрактный класс UniversoBazaRealeco
class Laborspaco(UniversoBazaRealeco):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # категория рабочих пространств
    kategorio = models.ManyToManyField(LaborspacoKategorio, verbose_name=_('Kategorio'),
                                       db_table='laborspacoj_laborspacoj_kategorioj_ligiloj')

    # тип рабочего пространства
    tipo = models.ForeignKey(LaborspacoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # статус рабочего пространства
    statuso = models.ForeignKey(LaborspacoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                related_name='%(app_label)s_%(class)s_statuso',
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Laborspaco')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj', _('Povas vidi laborspacoj')),
            ('povas_krei_laborspacoj', _('Povas krei laborspacoj')),
            ('povas_forigi_laborspacoj', _('Povas forigi laborspacoj')),
            ('povas_shangxi_laborspacoj', _('Povas ŝanĝi laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}, id={}'.format(get_enhavo(self.nomo, empty_values=True)[0], self.id)

    # реализация автоинкрементов при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        # реализация автоинкремента шаблона при сохранении
        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        # реализация автоинкремента при сохранении
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(Laborspaco, self).save(force_insert=force_insert, force_update=force_update,
                                     using=using, update_fields=update_fields)


    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if user_obj.is_superuser:
                all_perms = Permission.objects.filter(content_type__app_label='laborspacoj')
            else:
                # выбрать права, какие относятся к моделе
                model_perms = set([
                    'povas_vidi_laborspacoj',
                    'povas_krei_laborspacoj',
                    'povas_forigi_laborspacoj',
                    'povas_shangxi_laborspacoj'
                ])
                # выбираем права, какие есть у пользователя по данной моделе
                all_perms = set(user_obj.user_permissions.filter(
                    content_type__app_label='laborspacoj', codename__in=model_perms).values_list('codename', flat=True))

                # ЗДЕСЬ начинается блок добавления прав согласно условиям пользователя !!!
                # владелец пространства имеет все права
                count_posedantoj = LaborspacoPosedanto.objects.filter(laborspaco=self, posedanto_uzanto=user_obj,
                                                                      arkivo=False, forigo=False, publikigo=True).count()
                if count_posedantoj:
                    all_perms = all_perms.union({
                        'povas_vidi_laborspacoj',
                        'povas_krei_laborspacoj',
                        'povas_forigi_laborspacoj',
                        'povas_shangxi_laborspacoj',
                        'povas_krei_laborspacoj_posedantoj', # имеет право создавать владельцев
                        'povas_krei_klasteroj',
                        'povas_krei_klasteroj_posedantoj',
                    })
            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # на 13.02.2021 открыт просмотр рабочих пространств для всех
                cond = Q()

        return cond


# Типы владельцев рабочих пространств, использует абстрактный класс UniversoBazaMaks
class LaborspacoPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_posedantoj_tipoj',
             _('Povas vidi tipoj de posedantoj de laborspacoj')),
            ('povas_krei_laborspacoj_posedantoj_tipoj',
             _('Povas krei tipoj de posedantoj de laborspacoj')),
            ('povas_forigi_laborspacoj_posedantoj_tipoj',
             _('Povas forigi tipoj de posedantoj de laborspacoj')),
            ('povas_shangxi_laborspacoj_posedantoj_tipoj',
             _('Povas ŝanĝi tipoj de posedantoj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_posedantoj_tipoj',
                'laborspacoj.povas_krei_laborspacoj_posedantoj_tipoj',
                'laborspacoj.povas_forigi_laborspacoj_posedantoj_tipoj',
                'laborspacoj.povas_shangxi_laborspacoj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца рабочего пространства, использует абстрактный класс UniversoBazaMaks
class LaborspacoPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de laborspacoj')),
            ('povas_krei_laborspacoj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de laborspacoj')),
            ('povas_forigi_laborspacoj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de laborspacoj')),
            ('povas_shangxi_laborspacoj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_posedantoj_statusoj',
                'laborspacoj.povas_krei_laborspacoj_posedantoj_statusoj',
                'laborspacoj.povas_forigi_laborspacoj_posedantoj_statusoj',
                'laborspacoj.povas_shangxi_laborspacoj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_statusoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы рабочих пространств, использует абстрактный класс UniversoBazaRealeco
class LaborspacoPosedanto(UniversoBazaRealeco):

    # рабочее пространство
    laborspaco = models.ForeignKey(Laborspaco, verbose_name=_('Laborspaco'), blank=False, null=False,
                                   related_name='%(app_label)s_%(class)s_laborspaco',
                                   default=None, on_delete=models.CASCADE)

    # пользователь владелец рабочего пространства
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         related_name='%(app_label)s_%(class)s_posedanto_uzanto',
                                         default=None, on_delete=models.CASCADE)

    # организация владелец рабочего пространства
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           related_name='%(app_label)s_%(class)s_posedanto_organizo',
                                           null=True, default=None, on_delete=models.CASCADE)

    # сообщество владелец рабочего пространства
    posedanto_komunumo = models.ForeignKey(Komunumo, verbose_name=_('Posedanta komunumo'), blank=True,
                                           related_name='%(app_label)s_%(class)s_posedanto_komunumo',
                                           default=None, null=True, on_delete=models.CASCADE)

    # тип владельца рабочего пространства
    tipo = models.ForeignKey(LaborspacoPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # статус владельца рабочего пространства
    statuso = models.ForeignKey(LaborspacoPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                related_name='%(app_label)s_%(class)s_statuso',
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de laborspaco')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_posedantoj', _('Povas vidi posedantoj de laborspacoj')),
            ('povas_krei_laborspacoj_posedantoj', _('Povas krei posedantoj de laborspacoj')),
            ('povas_forigi_laborspacoj_posedantoj', _('Povas forigi posedantoj de laborspacoj')),
            ('povas_shangxi_laborspacoj_posedantoj', _('Povas ŝanĝi posedantoj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле laborspaco этой модели
        return '{}'.format(self.laborspaco)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(LaborspacoPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                                    using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if user_obj.is_superuser:
                all_perms = Permission.objects.filter(content_type__app_label='laborspacoj')
            else:
                # выбрать права, какие относятся к моделе
                model_perms = set([
                    'povas_vidi_laborspacoj_posedantoj',
                    'povas_krei_laborspacoj_posedantoj',
                    'povas_forigi_laborspacoj_posedantoj',
                    'povas_shangxi_laborspacoj_posedantoj'
                ])
                # выбираем права, какие есть у пользователя по данной моделе
                all_perms = set(user_obj.user_permissions.filter(
                    content_type__app_label='laborspacoj', codename__in=model_perms).values_list('codename', flat=True))

                # ЗДЕСЬ начинается блок добавления прав согласно условиям пользователя !!!
                # владелец пространства имеет все права
                if self.posedanto_uzanto == user_obj:
                    all_perms = all_perms.union({
                        'povas_vidi_laborspacoj_posedantoj',
                        'povas_krei_laborspacoj_posedantoj',
                        'povas_forigi_laborspacoj_posedantoj',
                        'povas_shangxi_laborspacoj_posedantoj',
                    })

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей рабочих пространств между собой, использует абстрактный класс UniversoBazaMaks
class LaborspacoLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de laborspacoj')),
            ('povas_krei_laborspacoj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de laborspacoj')),
            ('povas_forigi_laborspacoj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de laborspacoj')),
            ('povas_sxangxi_laborspacoj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(LaborspacoLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_ligiloj_tipoj',
                'laborspacoj.povas_krei_laborspacoj_ligiloj_tipoj',
                'laborspacoj.povas_forigi_laborspacoj_ligiloj_tipoj',
                'laborspacoj.povas_sxangxi_laborspacoj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь рабочих пространств между собой, использует абстрактный класс UniversoBazaMaks
class LaborspacoLigilo(UniversoBazaMaks):

    # рабочее пространство владелец связи
    posedanto = models.ForeignKey(Laborspaco, verbose_name=_('Laborspaco - posedanto'),
                                  related_name='%(app_label)s_%(class)s_posedanto',
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемое рабочее пространство
    ligilo = models.ForeignKey(Laborspaco, verbose_name=_('Laborspaco - ligilo'), blank=False,
                               null=False, default=None,
                               related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи рабочих пространств
    tipo = models.ForeignKey(LaborspacoLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_laborspacoj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de laborspacoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de laborspacoj')
        # права
        permissions = (
            ('povas_vidi_laborspacoj_ligiloj', _('Povas vidi ligiloj de laborspacoj')),
            ('povas_krei_laborspacoj_ligiloj', _('Povas krei ligiloj de laborspacoj')),
            ('povas_forigi_laborspacoj_ligiloj', _('Povas forigi ligiloj de laborspacoj')),
            ('povas_sxangxi_laborspacoj_ligiloj', _('Povas ŝanĝi ligiloj de laborspacoj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(LaborspacoLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_laborspacoj_ligiloj',
                'laborspacoj.povas_krei_laborspacoj_ligiloj',
                'laborspacoj.povas_forigi_laborspacoj_ligiloj',
                'laborspacoj.povas_sxangxi_laborspacoj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_laborspacoj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Категории кластеров, использует абстрактный класс UniversoBazaMaks
class KlasteroKategorio(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='laborspacoj_klasteroj_kategorioj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_kategorioj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Kategorio de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Kategorioj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_kategorioj', _('Povas vidi kategorioj de klasteroj')),
            ('povas_krei_klasteroj_kategorioj', _('Povas krei kategorioj de klasteroj')),
            ('povas_forigi_klasteroj_kategorioj', _('Povas forigi kategorioj de klasteroj')),
            ('povas_shangxi_klasteroj_kategorioj', _('Povas ŝanĝi kategorioj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroKategorio, self).save(force_insert=force_insert, force_update=force_update,
                                                 using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_kategorioj',
                'laborspacoj.povas_krei_klasteroj_kategorioj',
                'laborspacoj.povas_forigi_klasteroj_kategorioj',
                'laborspacoj.povas_shangxi_klasteroj_kategorioj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_kategorioj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_kategorioj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_kategorioj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы кластеров, использует абстрактный класс UniversoBazaMaks
class KlasteroTipo(UniversoBazaMaks):

    # ID записи для вывода
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # параллельный мир в котором есть эта сущность, может быть в нескольких
    realeco = models.ManyToManyField(Realeco, verbose_name=_('Realeco'),
                                     db_table='laborspacoj_klasteroj_tipoj_realeco_ligiloj')

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_tipoj', _('Povas vidi tipoj de klasteroj')),
            ('povas_krei_klasteroj_tipoj', _('Povas krei tipoj de klasteroj')),
            ('povas_forigi_klasteroj_tipoj', _('Povas forigi tipoj de klasteroj')),
            ('povas_shangxi_klasteroj_tipoj', _('Povas ŝanĝi tipoj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id += 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroTipo, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_tipoj',
                'laborspacoj.povas_krei_klasteroj_tipoj',
                'laborspacoj.povas_forigi_klasteroj_tipoj',
                'laborspacoj.povas_shangxi_klasteroj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус кластеров, использует абстрактный класс UniversoBazaMaks
class KlasteroStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                 encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_statusoj', _('Povas vidi statusoj de klasteroj')),
            ('povas_krei_klasteroj_statusoj', _('Povas krei statusoj de klasteroj')),
            ('povas_forigi_klasteroj_statusoj', _('Povas forigi statusoj de klasteroj')),
            ('povas_shangxi_klasteroj_statusoj', _('Povas ŝanĝi statusoj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                               using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_statusoj',
                'laborspacoj.povas_krei_klasteroj_statusoj',
                'laborspacoj.povas_forigi_klasteroj_statusoj',
                'laborspacoj.povas_shangxi_klasteroj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_statusoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2021) даём права на просмотр всем зарегистрированным
                cond = Q()

        return cond


# Модель связи Кластеров с рабочими пространствами Многие-ко-Многим с дополнительными полями
class KlasteroLaborspacoLigilo(models.Model):
    # UUID записи
    uuid = models.UUIDField(_('UUID'), primary_key=True, default=uuid4, editable=False)

    laborspaco = models.ForeignKey(Laborspaco, verbose_name=_('Kanvaso'), blank=False,
                                   related_name='%(app_label)s_%(class)s_laborspaco',
                                   default=None,
                                   on_delete=models.CASCADE)

    klastero = models.ForeignKey('Klastero', verbose_name=_('Laborspacoj klastero'), blank=False, default=None,
                                 related_name='%(app_label)s_%(class)s_klastero',
                                 on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klastero_laborspaco_ligilo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Klastero laborspaco ligilo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Klasteroj laborspacoj ligiloj')
        unique_together = ("laborspaco", "klastero")


# Кластеры, использует абстрактный класс UniversoBazaRealeco
class Klastero(UniversoBazaRealeco):

    # рабочее пространство в которое входит кластер
    laborspacoj = models.ManyToManyField(Laborspaco,
                                        verbose_name=_('Laborspacoj'),
                                        through='KlasteroLaborspacoLigilo',
                                        related_name='%(app_label)s_%(class)s_laborspacoj',
                                        blank=True)

    # категория кластеров
    kategorio = models.ManyToManyField(KlasteroKategorio, verbose_name=_('Kategorio'),
                                       db_table='laborspacoj_klasteroj_kategorioj_ligiloj')

    # тип кластеров
    tipo = models.ForeignKey(KlasteroTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # статус кластеров
    statuso = models.ForeignKey(KlasteroStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                related_name='%(app_label)s_%(class)s_statuso',
                                on_delete=models.CASCADE)

    # позиция в списке
    pozicio = models.IntegerField(_('Pozicio'), blank=True, null=True, default=None)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Klastero')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj', _('Povas vidi klasteroj')),
            ('povas_krei_klasteroj', _('Povas krei klasteroj')),
            ('povas_forigi_klasteroj', _('Povas forigi klasteroj')),
            ('povas_shangxi_klasteroj', _('Povas ŝanĝi klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.uuid, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(Klastero, self).save(force_insert=force_insert, force_update=force_update,
                                    using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения
            if user_obj.is_superuser:
                all_perms = Permission.objects.filter(content_type__app_label='laborspacoj')
            else:
                # выбрать права, какие относятся к моделе
                model_perms = set([
                    'povas_vidi_klasteroj',
                    'povas_krei_klasteroj',
                    'povas_forigi_klasteroj',
                    'povas_shangxi_klasteroj'
                ])
                # выбираем права, какие есть у пользователя по данной моделе
                all_perms = set(user_obj.user_permissions.filter(
                    content_type__app_label='laborspacoj', codename__in=model_perms).values_list('codename', flat=True))

                # ЗДЕСЬ начинается блок добавления прав согласно условиям пользователя !!!
                # владелец кластера имеет все права
                count_posedantoj = KlasteroPosedanto.objects.filter(klastero=self, posedanto_uzanto=user_obj).count()
                if count_posedantoj:
                    all_perms = all_perms.union({
                        'povas_vidi_klasteroj',
                        'povas_krei_klasteroj',
                        'povas_forigi_klasteroj',
                        'povas_shangxi_klasteroj'
                    })
                # владелец рабочего пространства имеет полные права 
                # если подключено несколько пространств, то должен быть владельцем всех пространств.
                # если не является владельцем какого либо пространства, то идёт ошибка
                for laborspaco in self.laborspacoj.all():
                    count_posedantoj =  LaborspacoPosedanto.objects.filter(laborspaco=laborspaco, posedanto_uzanto=user_obj).count()
                    if not count_posedantoj:
                        break
                if count_posedantoj:
                    all_perms = all_perms.union({
                        'povas_vidi_klasteroj',
                        'povas_krei_klasteroj',
                        'povas_forigi_klasteroj',
                        'povas_shangxi_klasteroj'
                    })
            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # на 20.02.2021 открыт просмотр кластеров для всех
                cond = Q()

        return cond


# Типы владельцев кластеров, использует абстрактный класс UniversoBazaMaks
class KlasteroPosedantoTipo(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_posedantoj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de posedantoj de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de posedantoj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_posedantoj_tipoj', _('Povas vidi tipoj de posedantoj de klasteroj')),
            ('povas_krei_klasteroj_posedantoj_tipoj', _('Povas krei tipoj de posedantoj de klasteroj')),
            ('povas_forigi_klasteroj_posedantoj_tipoj', _('Povas forigi tipoj de posedantoj de klasteroj')),
            ('povas_shangxi_klasteroj_posedantoj_tipoj', _('Povas ŝanĝi tipoj de posedantoj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroPosedantoTipo, self).save(force_insert=force_insert, force_update=force_update,
                                                     using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_posedantoj_tipoj',
                'laborspacoj.povas_krei_klasteroj_posedantoj_tipoj',
                'laborspacoj.povas_forigi_klasteroj_posedantoj_tipoj',
                'laborspacoj.povas_shangxi_klasteroj_posedantoj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Статус владельца кластера, использует абстрактный класс UniversoBazaMaks
class KlasteroPosedantoStatuso(UniversoBazaMaks):

    # уникальный личный ID
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_posedantoj_statusoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Statuso de posedantoj de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Statusoj de posedantoj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_posedantoj_statusoj',
             _('Povas vidi statusoj de posedantoj de klasteroj')),
            ('povas_krei_klasteroj_posedantoj_statusoj',
             _('Povas krei statusoj de posedantoj de klasteroj')),
            ('povas_forigi_klasteroj_posedantoj_statusoj',
             _('Povas forigi statusoj de posedantoj de klasteroj')),
            ('povas_shangxi_klasteroj_posedantoj_statusoj',
             _('Povas ŝanĝi statusoj de posedantoj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroPosedantoStatuso, self).save(force_insert=force_insert, force_update=force_update,
                                                        using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_posedantoj_statusoj',
                'laborspacoj.povas_krei_klasteroj_posedantoj_statusoj',
                'laborspacoj.povas_forigi_klasteroj_posedantoj_statusoj',
                'laborspacoj.povas_shangxi_klasteroj_posedantoj_statusoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_statusoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_statusoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj_statusoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Владельцы кластеров, использует абстрактный класс UniversoBazaRealeco
class KlasteroPosedanto(UniversoBazaRealeco):

    # кластер
    klastero = models.ForeignKey(Klastero, verbose_name=_('Tasko'), blank=False, null=False,
                                  related_name='%(app_label)s_%(class)s_klastero',
                                  default=None, on_delete=models.CASCADE)

    # пользователь владелец кластера
    posedanto_uzanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanta uzanto'), blank=True, null=True,
                                         related_name='%(app_label)s_%(class)s_posedanto_uzanto',
                                         default=None, on_delete=models.CASCADE)

    # организация владелец кластера
    posedanto_organizo = models.ForeignKey(Organizo, verbose_name=_('Posedanta organizo'), blank=True,
                                           related_name='%(app_label)s_%(class)s_posedanto_organizo',
                                           null=True, default=None, on_delete=models.CASCADE)

    # тип владельца кластера
    tipo = models.ForeignKey(KlasteroPosedantoTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)
    
    # статус владельца кластера
    statuso = models.ForeignKey(KlasteroPosedantoStatuso, verbose_name=_('Statuso'), blank=False, default=None,
                                related_name='%(app_label)s_%(class)s_statuso',
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_posedantoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Posedanto de klastero')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Posedantoj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_posedantoj', _('Povas vidi posedantoj de klasteroj')),
            ('povas_krei_klasteroj_posedantoj', _('Povas krei posedantoj de klasteroj')),
            ('povas_forigi_klasteroj_posedantoj', _('Povas forigi posedantoj de klasteroj')),
            ('povas_shangxi_klasteroj_posedantoj', _('Povas ŝanĝi posedantoj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле klastero этой модели
        return '{}'.format(self.klastero)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(KlasteroPosedanto, self).save(force_insert=force_insert, force_update=force_update,
                                             using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            if user_obj.is_superuser:
                all_perms = Permission.objects.filter(content_type__app_label='laborspacoj')
            else:
                # выбрать права, какие относятся к моделе
                model_perms = set([
                    'povas_vidi_klasteroj_posedantoj',
                    'povas_krei_klasteroj_posedantoj',
                    'povas_forigi_klasteroj_posedantoj',
                    'povas_shangxi_klasteroj_posedantoj'
                ])
                # выбираем права, какие есть у пользователя по данной моделе
                all_perms = set(user_obj.user_permissions.filter(
                    content_type__app_label='laborspacoj', codename__in=model_perms).values_list('codename', flat=True))

                # ЗДЕСЬ начинается блок добавления прав согласно условиям пользователя !!!
                # владелец пространства имеет все права
                if self.posedanto_uzanto == user_obj:
                    all_perms = all_perms.union({
                        'povas_vidi_klasteroj_posedantoj',
                        'povas_krei_klasteroj_posedantoj',
                        'povas_forigi_klasteroj_posedantoj',
                        'povas_shangxi_klasteroj_posedantoj',
                    })

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_posedantoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_posedantoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Типы связей кластеров между собой, использует абстрактный класс UniversoBazaMaks
class KlasteroLigiloTipo(UniversoBazaMaks):

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # автор (пользователь, который создал сущность или для которого создана сущность)
    autoro = models.ForeignKey(Uzanto, verbose_name=_('Aŭtoro'), blank=False, default=None,
                               related_name='%(app_label)s_%(class)s_autoro',
                               on_delete=models.CASCADE)

    # название, многоязычное в JSON формате
    nomo = models.JSONField(verbose_name=_('Titolo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)

    # описание, многоязычное в JSON формате
    priskribo = models.JSONField(verbose_name=_('Priskribo'), blank=True, null=False, default=default_lingvo,
                                   encoder=CallableEncoder)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_ligiloj_tipoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Tipo de ligiloj de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Tipoj de ligiloj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_ligiloj_tipoj',
             _('Povas vidi tipoj de ligiloj de klasteroj')),
            ('povas_krei_klasteroj_ligiloj_tipoj',
             _('Povas krei tipoj de ligiloj de klasteroj')),
            ('povas_forigi_klasteroj_ligiloj_tipoj',
             _('Povas forigi tipoj de ligiloj de klasteroj')),
            ('povas_sxangxi_klasteroj_ligiloj_tipoj',
             _('Povas ŝanĝi tipoj de ligiloj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}) {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(KlasteroLigiloTipo, self).save(force_insert=force_insert, force_update=force_update,
                                              using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_ligiloj_tipoj',
                'laborspacoj.povas_krei_klasteroj_ligiloj_tipoj',
                'laborspacoj.povas_forigi_klasteroj_ligiloj_tipoj',
                'laborspacoj.povas_sxangxi_klasteroj_ligiloj_tipoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_ligiloj_tipoj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_ligiloj_tipoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_ligiloj_tipoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Связь кластеров между собой, использует абстрактный класс UniversoBazaMaks
class KlasteroLigilo(UniversoBazaMaks):

    # кластер владелец связи
    posedanto = models.ForeignKey(Klastero, verbose_name=_('Tasko - posedanto'),
                                  related_name='%(app_label)s_%(class)s_posedanto',
                                  blank=False, null=False, default=None, on_delete=models.CASCADE)

    # связываемый кластер
    ligilo = models.ForeignKey(Klastero, verbose_name=_('Tasko - ligilo'), blank=False,
                               null=False, default=None,
                               related_name='%(app_label)s_%(class)s_ligilo',
                               on_delete=models.CASCADE)

    # тип связи кластеров
    tipo = models.ForeignKey(KlasteroLigiloTipo, verbose_name=_('Tipo'), blank=False, default=None,
                             related_name='%(app_label)s_%(class)s_tipo',
                             on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'laborspacoj_klasteroj_ligiloj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Ligilo de klasteroj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Ligiloj de klasteroj')
        # права
        permissions = (
            ('povas_vidi_klasteroj_ligiloj', _('Povas vidi ligiloj de klasteroj')),
            ('povas_krei_klasteroj_ligiloj', _('Povas krei ligiloj de klasteroj')),
            ('povas_forigi_klasteroj_ligiloj', _('Povas forigi ligiloj de klasteroj')),
            ('povas_sxangxi_klasteroj_ligiloj', _('Povas ŝanĝi ligiloj de klasteroj')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле uuid этой модели
        return '{}'.format(self.uuid)

    # реализация автоинкремента шаблона при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.sxablono_sistema:
            if self.sxablono_sistema_id is None or not self.sxablono_sistema_id:
                model = getattr(sys.modules[self.__module__], self.__class__.__name__)
                next_id = model.objects.all().aggregate(Max('sxablono_sistema_id'))['sxablono_sistema_id__max']

                if next_id is None:
                    next_id = 1
                else:
                    next_id = next_id + 1
                super(model, self).__setattr__('sxablono_sistema_id', next_id)

        super(KlasteroLigilo, self).save(force_insert=force_insert, force_update=force_update,
                                              using=using, update_fields=update_fields)

    # Права доступа
    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистрированных для приложения
            all_perms = set(perms.user_registrita_perms(apps=('laborspacoj',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'laborspacoj.povas_vidi_klasteroj_ligiloj',
                'laborspacoj.povas_krei_klasteroj_ligiloj',
                'laborspacoj.povas_forigi_klasteroj_ligiloj',
                'laborspacoj.povas_sxangxi_klasteroj_ligiloj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='laborspacoj', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('laborspacoj.povas_vidi_klasteroj_ligiloj')
                    or user_obj.has_perm('laborspacoj.povas_vidi_klasteroj_ligiloj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                # cond = Q(uuid__isnull=True)
                # На текущий момент (25.05.2020) даём права на просмотр всем зарегистрированным
                cond = Q()

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('laborspacoj.povas_vidi_klasteroj_ligiloj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
