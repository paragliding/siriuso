"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from graphene import relay, ObjectType, String
from graphene_django import DjangoObjectType
from graphene.types import Field, Int, ID, Boolean, DateTime
from graphql_relay.node.node import from_global_id
from siriuso.api.filters import SiriusoFilterConnectionField
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions
from siriuso.api.types import SiriusoLingvo
from django.db.models import Q, F, Case, When, DateTimeField, OuterRef, Subquery
from django.core.cache import cache
from uzantoj.models import *
from fotoj.api.schema import FotojFotoDosieroNode
from fotoj.models import FotojFotoDosiero, FotojUzantojAvataro, FotojUzantojKovrilo
import re


class UzantoProprietoj(ObjectType):
    __parent = None

    def __init__(self, parent, *args, **kwargs):
        self.__parent = parent
        super(UzantoProprietoj, self).__init__(*args, **kwargs)

    # Признак товарищества к текущему пользователю
    mia_gekamarado = Boolean()

    # Признак запроса в товарищи
    mia_gekamarado_peto = Boolean()

    # Признак кандидата в товарищи
    kandidato_gekamarado = Boolean()

    # Всего друзей
    tuta_gekamaradoj = Int()

    # Рейтинг по количеству Записей на Стене
    rating = Int()

    # Дата последней активности на Стене
    aktiva_dato = DateTime()

    def resolve_mia_gekamarado(self, info):
        if info.context.user.is_authenticated:
            # Используем краткосрочное кэширование для ускорения
            cache_key = 'api_gekamaradoj_uzanto_%s' % info.context.user.id
            gekamaradoj_listo = cache.get(cache_key)

            if not gekamaradoj_listo:
                # Получаем список товарищей для запросившего пользователя
                q1 = (Uzanto.objects.only('id')
                      .filter(Q(uzantoj_uzantojgekamaradoj_posedanto__gekamarado=info.context.user.id,
                                uzantoj_uzantojgekamaradoj_posedanto__akceptis1=True,
                                uzantoj_uzantojgekamaradoj_posedanto__akceptis2=True,
                                uzantoj_uzantojgekamaradoj_posedanto__arkivo=False,
                                uzantoj_uzantojgekamaradoj_posedanto__forigo=False),
                              ~Q(id=info.context.user.id))
                      .values_list('id', flat=True))

                q2 = (Uzanto.objects.only('id')
                      .filter(Q(uzantoj_uzantojgekamaradoj_gekamarado__posedanto_id=info.context.user.id,
                                uzantoj_uzantojgekamaradoj_gekamarado__akceptis1=True,
                                uzantoj_uzantojgekamaradoj_gekamarado__akceptis2=True,
                                uzantoj_uzantojgekamaradoj_gekamarado__forigo=False,
                                uzantoj_uzantojgekamaradoj_gekamarado__arkivo=False),
                              ~Q(id=info.context.user.id))
                      .values_list('id', flat=True))

                gekamaradoj_listo = set(q1) | set(q2)
                cache.set(cache_key, gekamaradoj_listo, timeout=5)
            return getattr(self.__parent, 'id') in gekamaradoj_listo
        return False

    def resolve_kandidato_gekamarado(self, info):
        if info.context.user.is_authenticated:
            # Используем краткосрочное кэширование для ускорения
            cache_key = 'api_kandidatoj_gekamaradoj_uzanto_%s' % info.context.user.id
            gekamaradoj_listo = cache.get(cache_key)

            if not gekamaradoj_listo:
                # Получаем список товарищей для запросившего пользователя
                q1 = (Uzanto.objects.only('id')
                      .filter(Q(uzantoj_uzantojgekamaradoj_posedanto__gekamarado=info.context.user.id,
                                uzantoj_uzantojgekamaradoj_posedanto__akceptis1=True,
                                uzantoj_uzantojgekamaradoj_posedanto__akceptis2__isnull=True,
                                uzantoj_uzantojgekamaradoj_posedanto__arkivo=False,
                                uzantoj_uzantojgekamaradoj_posedanto__forigo=False),
                              ~Q(id=info.context.user.id))
                      .values_list('id', flat=True))

                gekamaradoj_listo = set(q1)
                cache.set(cache_key, gekamaradoj_listo, timeout=5)
            return getattr(self.__parent, 'id') in gekamaradoj_listo
        return False

    def resolve_mia_gekamarado_peto(self, info):
        if info.context.user.is_authenticated:
            # Используем краткосрочное кэширование для ускорения
            cache_key = 'api_gekamaradoj_peto_uzanto_%s' % info.context.user.id
            gekamaradoj_listo = cache.get(cache_key)

            if not gekamaradoj_listo:
                # Получаем список товарищей для запросившего пользователя
                q1 = (Uzanto.objects.only('id')
                      .filter(Q(uzantoj_uzantojgekamaradoj_gekamarado__posedanto_id=info.context.user.id,
                                uzantoj_uzantojgekamaradoj_gekamarado__akceptis1=True,
                                uzantoj_uzantojgekamaradoj_gekamarado__akceptis2__isnull=True,
                                uzantoj_uzantojgekamaradoj_gekamarado__forigo=False,
                                uzantoj_uzantojgekamaradoj_gekamarado__arkivo=False),
                              ~Q(id=info.context.user.id))
                      .values_list('id', flat=True))

                gekamaradoj_listo = set(q1)
                cache.set(cache_key, gekamaradoj_listo, timeout=5)
            return getattr(self.__parent, 'id') in gekamaradoj_listo
        return False

    def resolve_tuta_gekamaradoj(self, info):
        # Используем краткосрочное кэширование для ускорения
        cache_key = 'api_tuta_gekamaradoj'
        count = cache.get(cache_key) or {}

        if getattr(self.__parent, 'id') not in count:
            q1 = (Uzanto.objects.only('id')
                  .filter(Q(uzantoj_uzantojgekamaradoj_posedanto__gekamarado_id=getattr(self.__parent, 'id'),
                            uzantoj_uzantojgekamaradoj_posedanto__akceptis1=True,
                            uzantoj_uzantojgekamaradoj_posedanto__akceptis2=True,
                            uzantoj_uzantojgekamaradoj_posedanto__arkivo=False,
                            uzantoj_uzantojgekamaradoj_posedanto__forigo=False),
                          ~Q(id=getattr(self.__parent, 'id')))
                  )

            q2 = (Uzanto.objects.only('id')
                  .filter(Q(uzantoj_uzantojgekamaradoj_gekamarado__posedanto_id=getattr(self.__parent, 'id'),
                            uzantoj_uzantojgekamaradoj_gekamarado__akceptis1=True,
                            uzantoj_uzantojgekamaradoj_gekamarado__akceptis2=True,
                            uzantoj_uzantojgekamaradoj_gekamarado__forigo=False,
                            uzantoj_uzantojgekamaradoj_gekamarado__arkivo=False),
                          ~Q(id=getattr(self.__parent, 'id')))
                  )
            count[getattr(self.__parent, 'id')] = q1.union(q2).count()
            cache.set(cache_key, count, timeout=3600*24*10)

        return count[getattr(self.__parent, 'id')]

    def resolve_rating(self, info):
        return getattr(self.__parent, 'rating', None)


class UzantoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):
    search_fields = (
        'id',
        'unua_nomo__enhavo__icontains',
        #'dua_nomo__enhavo__icontains',
        'familinomo__enhavo__icontains')

    json_filter_fields = {
        'unua_nomo__enhavo': ['contains', 'icontains'],
        'dua_nomo__enhavo': ['contains', 'icontains'],
        'familinomo__enhavo': ['contains', 'icontains'],
    }

    avataro = Field(FotojFotoDosieroNode)

    kovrilo = Field(FotojFotoDosieroNode)

    unua_nomo = Field(SiriusoLingvo)

    dua_nomo = Field(SiriusoLingvo)

    familinomo = Field(SiriusoLingvo)

    statuso = Field(SiriusoLingvo)

    statistiko = Field(UzantoProprietoj)

    kontakta_informo = String()

    class Meta:
        model = Uzanto
        fields = ['id', 'uuid', 'is_active', 'konfirmita', 'malbona_retposhto', 'unua_nomo', 'familinomo',
                       'dua_nomo', 'sekso', 'is_admin', ]
        filter_fields = {
            'id': ['exact'],
            'uuid': ['exact'],
            'is_active': ['exact'],
            'konfirmita': ['exact'],
            'malbona_retposhto': ['exact'],
            'sekso': ['exact'],
        }
        interfaces = (relay.Node,)

    def resolve_avataro(self, info):
        try:
            avataro = FotojUzantojAvataro.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, chefa_varianto=True)
        except (FotojUzantojAvataro.DoesNotExist, FotojUzantojAvataro.MultipleObjectsReturned):
            avataro = FotojFotoDosiero(posedanto_id=getattr(self, 'id'), forigo=False)
            avataro.bildo_baza = ('/static/main/images/virina-avataro.png' if getattr(self, 'sekso') == 'virina'
                       else '/static/main/images/vira-avataro.png')
            avataro.bildo_f = ('/static/main/images/virina-avataro-min.png' if getattr(self, 'sekso') == 'virina'
                       else '/static/main/images/vira-avataro-min.png')
            return avataro

        return avataro.avataro.dosiero

    def resolve_kovrilo(self, info):
        try:
            kovrilo = FotojUzantojKovrilo.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, chefa_varianto=True)
        except (FotojUzantojKovrilo.DoesNotExist, FotojUzantojKovrilo.MultipleObjectsReturned):
            kovrilo = FotojFotoDosiero(posedanto_id=getattr(self, 'id'), forigo=False)
            kovrilo.bildo = '/static/main/images/defaulte-kovrilo.png'
            return kovrilo

        return kovrilo.kovrilo.dosiero

    def resolve_statuso(self, info):
        try:
            statuso = UzantojStatuso.objects.get(posedanto_id=getattr(self, 'id'), forigo=False, arkivo=False)
            statuso = statuso.teksto
        except (UzantojStatuso.DoesNotExist, UzantojStatuso.MultipleObjectsReturned):
            statuso = None
        return statuso

    def resolve_statistiko(self, info):
        return UzantoProprietoj(parent=self)


class UzantoQuery(ObjectType):
    uzanto = Field(UzantoNode, id=ID(required=True))
    uzantoj = SiriusoFilterConnectionField(UzantoNode)
    gekamaradoj = SiriusoFilterConnectionField(UzantoNode, additional_fields={'uzantoId': Int()})
    kandidatoj_gekamaradoj = SiriusoFilterConnectionField(UzantoNode)

    def resolve_uzanto(self, info, id):
        # Обход ограничения GlobalID для Uzanto
        obj_id = id

        if not obj_id.isdigit():
            obj_id = from_global_id(id)[1]

        try:
            return Uzanto.objects.get(pk=obj_id)
        except Uzanto.DoesNotExist:
            pass

        return None

    def resolve_gekamaradoj(self, info, **kwargs):
        if info.context.user.is_authenticated:
            uzanto_id = kwargs.get('uzantoId') or info.context.user.id
        else:
            uzanto_id = kwargs.get('uzantoId')

        if uzanto_id:
            subquery1 = UzantojGekamaradoj.objects.filter(
                posedanto_id=OuterRef('id'), gekamarado_id=uzanto_id, arkivo=False, forigo=False
            ).order_by('-krea_dato')

            subquery2 = UzantojGekamaradoj.objects.filter(
                gekamarado_id=OuterRef('id'), posedanto_id=uzanto_id, arkivo=False, forigo=False
            ).order_by('-krea_dato')

            q = (Uzanto.objects
                 .filter(Q(uzantoj_uzantojgekamaradoj_posedanto__gekamarado_id=uzanto_id,
                           uzantoj_uzantojgekamaradoj_posedanto__akceptis1=True,
                           uzantoj_uzantojgekamaradoj_posedanto__akceptis2=True,
                           uzantoj_uzantojgekamaradoj_posedanto__arkivo=False,
                           uzantoj_uzantojgekamaradoj_posedanto__forigo=False)
                         , ~Q(id=uzanto_id), is_active=True, konfirmita=True)

                 .annotate(gekamarada_dato=Subquery(subquery1.values('krea_dato')[:1]))
                 ).union(Uzanto.objects
                 .filter(Q(uzantoj_uzantojgekamaradoj_gekamarado__posedanto_id=uzanto_id,
                           uzantoj_uzantojgekamaradoj_gekamarado__akceptis1=True,
                           uzantoj_uzantojgekamaradoj_gekamarado__akceptis2=True,
                           uzantoj_uzantojgekamaradoj_gekamarado__forigo=False,
                           uzantoj_uzantojgekamaradoj_gekamarado__arkivo=False)
                         , ~Q(id=uzanto_id), is_active=True, konfirmita=True)

                 .annotate(gekamarada_dato=Subquery(subquery2.values('krea_dato')[:1]))
                 )
            return q
        return Uzanto.objects.none()

    def resolve_kandidatoj_gekamaradoj(self, info, **kwargs):
        if info.context.user.is_authenticated:
            uzanto_id = kwargs.get('uzantoId') or info.context.user.id
        else:
            uzanto_id = kwargs.get('uzantoId')

        if uzanto_id:
            q = (Uzanto.objects
                 .filter(Q(uzantoj_uzantojgekamaradoj_posedanto__gekamarado_id=uzanto_id,
                           uzantoj_uzantojgekamaradoj_posedanto__akceptis1=True,
                           uzantoj_uzantojgekamaradoj_posedanto__akceptis2__isnull=True,
                           uzantoj_uzantojgekamaradoj_posedanto__arkivo=False,
                           uzantoj_uzantojgekamaradoj_posedanto__forigo=False),
                         ~Q(id=uzanto_id), is_active=True, konfirmita=True)
                 .annotate(gekamarada_dato=F('uzantoj_uzantojgekamaradoj_posedanto__krea_dato'))
                 )

            if 'orderBy' in kwargs:
                q = q.order_by(*kwargs['orderBy'])
            return q
        return Uzanto.objects.none()
