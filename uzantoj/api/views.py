"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from rest_framework import viewsets
from rest_framework import permissions
from django.db.models import F
from main.models import Uzanto
from .serializers import UzantoSerializer


class UzantoView(viewsets.ReadOnlyModelViewSet):
    queryset = Uzanto.objects.annotate(avataro_bildo=F('fotoj_fotojuzantojavataro_posedanto__bildo')).all()
    serializer_class = UzantoSerializer
    # lookup_field = 'id'


class MiView(viewsets.ReadOnlyModelViewSet):
    """
    Returns the data of the current user.
    Requires authentication.
    """
    queryset = Uzanto.objects.annotate(avataro_bildo=F('fotoj_fotojuzantojavataro_posedanto__bildo'))
    serializer_class = UzantoSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.get(**{self.lookup_field: self.request.user.id})
        return queryset

    def get_object(self):
        return self.get_queryset()
