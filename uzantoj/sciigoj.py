"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import models
from django.utils import timezone
from main.models import Uzanto
from .models import UzantojSciigoj
from itertools import islice
import uuid

# читать описание работы по ссылке https://pm.tehnokom.su/project/p13-2/wiki/sistema-uvedomlenii-polzovatelei
def sendu_sciigon(teksto, pluralo=None, teksto_parametroj=None, to=None, objektoj=None):
    count = 0

    if teksto:
        params = {'id__in': to} if isinstance(to, (list, tuple, set)) else dict()
        atributoj = dict()

        if pluralo and isinstance(pluralo, str):
            atributoj.update({'pluralo': pluralo})

        if teksto_parametroj and isinstance(teksto_parametroj, dict):
            nova_parametroj = dict()

            for par, val in teksto_parametroj.items():
                if isinstance(val, dict):
                    nova_parametroj[par] = {
                        'model': '%s.%s' % (val['obj']._meta.model._meta.app_label, val['obj']._meta.model.__name__),
                        'pk': val['obj'].pk if not isinstance(val['obj'].pk, uuid.UUID) else str(val['obj'].pk),
                        'field': val['field']
                    }
                else:
                    nova_parametroj[par] = val

            atributoj.update({'teksto_parametroj': nova_parametroj})

        if objektoj and isinstance(objektoj, (list, tuple, set)):
            koditaj_objektoj = list()

            for obj in objektoj:
                if issubclass(obj.__class__, models.Model):
                    koditaj_objektoj.append({
                        'model': '%s.%s' % (obj._meta.model._meta.app_label, obj._meta.model.__name__),
                        'pk': obj.pk if not isinstance(obj.pk, uuid.UUID) else str(obj.pk)
                    })

            if len(koditaj_objektoj):
                atributoj.update({'objektoj': koditaj_objektoj})

        novaj_sciigoj = (
            UzantojSciigoj(
                posedanto=uzanto,
                forigo=False,
                arkivo=False,
                publikigo=True,
                publikiga_dato=timezone.now(),
                teksto=teksto,
                atributoj=atributoj
            ) for uzanto in Uzanto.objects.filter(is_active=True, konfirmita=True, **params)
        )
        batch_size = 100
        count = 0

        while True:
            batch = list(islice(novaj_sciigoj, batch_size))
            if not batch:
                break

            count += len(UzantojSciigoj.objects.bulk_create(batch, batch_size=batch_size))

    return count
