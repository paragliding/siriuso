# Generated by Django 3.2.15 on 2022-10-25 07:02

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0001_squashed_0021_auto_20221025_0702'),
        ('uzantoj', '0001_squashed_0013_auto_20221018_1314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uzantojaliro',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojaliro_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AlterField(
            model_name='uzantojgekamaradoj',
            name='posedanto',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojgekamaradoj_posedanto', to=settings.AUTH_USER_MODEL, verbose_name='Posedanto'),
        ),
        migrations.AlterField(
            model_name='uzantojgekamaradoj',
            name='tipo',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojgekamaradoj_tipo', to='uzantoj.uzantojgekamaradojtipo', verbose_name='Tipo'),
        ),
        migrations.AlterField(
            model_name='uzantojgekamaradoj',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojgekamaradoj_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AlterField(
            model_name='uzantojgekamaradojtipo',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojgekamaradojtipo_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AlterField(
            model_name='uzantojsciigoj',
            name='posedanto',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojsciigoj_posedanto', to=settings.AUTH_USER_MODEL, verbose_name='Posedanto'),
        ),
        migrations.AlterField(
            model_name='uzantojsciigoj',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojsciigoj_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono'),
        ),
        migrations.AlterField(
            model_name='uzantojstatuso',
            name='posedanto',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojstatuso_posedanto', to=settings.AUTH_USER_MODEL, verbose_name='Posedanto'),
        ),
        migrations.AlterField(
            model_name='uzantojstatuso',
            name='wablomo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='uzantoj_uzantojstatuso_wablomo', to='main.siriusowablono', verbose_name='Ĝenerala ŝablono'),
        ),
    ]
