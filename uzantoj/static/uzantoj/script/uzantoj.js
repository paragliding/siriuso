$(document).ready(function(){
    uzantoj_init();
});

function uzantoj_init() {
    $('.uzantoj_joining, .uzanto_kandidato').on('click', uzantoj_action);
}

function uzantoj_action() {
    if(typeof gekamarado_url === "undefined") {
        console.log('AJAX Context not found!');
        return;
    }

    var target = $(this);
    var data = uzantoj_data(target);

    if(!data) {
        console.log('No data for AJAX');
        return;
    }

    if(target.hasClass('small_teksto')) {
        target.parent().find('a').unbind('click', uzantoj_action);
        toggleBlockForm(target.parents('li'), 24);
    } else {
        target.unbind('click', uzantoj_action);
        toggleBlockForm(target, 24);
    }

    $.ajax({
        url: gekamarado_url,
        type: 'POST',
        async: true,
        data: data,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false
    })
        .done(function (result) {
            if(result.status === 'ok') {
                setTimeout(function(){
                    if(target.hasClass('small_teksto')) {
                        var tuta = parseInt($('#kandidatoj_tuta').text()) - 1;
                        $('#kandidatoj_tuta').text(tuta);
                        target.parents('li').remove();

                        if(!tuta) {
                            $('#kandidatoj_tuta').parents('a').find('div').toggleClass('hidden-element');
                            $('.kandidatoj_listo').remove();
                        }
                        return;
                    }

                    toggleBlockForm(target);
                    target.on('click', uzantoj_action);
                    switch (result.ago) {
                        case 'konfirmo':
                            target.toggleClass('hidden-element');
                            target.parent().find('.uzanto_kamarado').toggleClass('hidden-element');
                            break;

                        case 'nuligo':
                            target.toggleClass('hidden-element');
                            target.parent().find('.uzantoj_joining').toggleClass('hidden-element');
                            break;

                        case 'proponita':
                            target.toggleClass('hidden-element');
                            target.parent().find('.uzanto_kandidato').toggleClass('hidden-element');
                            break;

                        default:
                            break;
                    }
                }, 500);
            } else {
                console.log("Bad server response: " + result.err);
                toggleBlockForm(target);
            }
        })
        .fail(function (jqXHR, textStatus) {
            toggleBlockForm(target);
            console.log("Request failed: " + textStatus);
        });
}

function uzantoj_data(obj) {
    var uuid;
    if(obj !== undefined && $(obj).parent().attr('data-uzanto_uuid') !== undefined) {
        uuid = $(obj).parent().attr('data-uzanto_uuid');
    } else {
        uuid = $('.uzanto_datumoj').attr('data-uzanto_uuid');
    }

    if(uuid !== undefined) {
        out = new FormData();
        out.append('respondanto', uuid);
        out.append('csrfmiddlewaretoken', getCookie('csrftoken'));

        if($(obj).attr('data-ago') !== undefined) {
            out.append('ago', $(obj).attr('data-ago'));
        }

        return out;
    }

    return false;
}