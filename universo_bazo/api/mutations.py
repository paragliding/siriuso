"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene
from graphene.types.generic import GenericScalar

from siriuso.utils import set_enhavo
from .schema import *
from ..models import Realeco


# Модель реальности Universo
class RedaktuRealeco(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_realeco = graphene.Field(RealecoNode,
        description=_('Созданная/изменённая реальность'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код реальности'))
        nomo = graphene.String(description=_('Название реальности'))
        nomo_json = GenericScalar(description=_('Название реальности в JSON формате для администраторов'))
        priskribo = graphene.String(description=_('Описание реальности'))
        priskribo_json = GenericScalar(description=_('Описание реальности в JSON формате для администраторов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_realeco = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_bazo.povas_krei_universo_realeco'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if (not kwargs.get('kodo', False)) and not message:
                            message = '{} "{}"'.format(_('При создании записи обязательно указание поля'), 'kodo')

                        if not message:
                            universo_realeco = Realeco.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=uzanto,
                                kodo=kwargs.get('kodo', False),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo_json', False)):
                                if uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco'):
                                    universo_realeco.nomo = kwargs.get('nomo_json')
                            elif (kwargs.get('nomo', False)):
                                set_enhavo(universo_realeco.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo_json', False)):
                                if uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco'):
                                    universo_realeco.priskribo = kwargs.get('priskribo_json')
                            elif (kwargs.get('priskribo', False)):
                                set_enhavo(universo_realeco.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_realeco.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('nomo_json', False) or kwargs.get('priskribo_json', False)
                            or kwargs.get('kodo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_realeco = Realeco.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_bazo.povas_shanghi_universo_realeco'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                universo_realeco.lasta_autoro = universo_realeco.autoro
                                universo_realeco.lasta_dato = timezone.now()
                                universo_realeco.forigo = kwargs.get('forigo', universo_realeco.forigo)
                                universo_realeco.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_realeco.arkivo = kwargs.get('arkivo', universo_realeco.arkivo)
                                universo_realeco.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_realeco.publikigo = kwargs.get('publikigo', universo_realeco.publikigo)
                                universo_realeco.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_realeco.kodo = kwargs.get('kodo', universo_realeco.kodo)
                                universo_realeco.autoro = uzanto

                                if (kwargs.get('nomo_json', False)):
                                    if uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco'):
                                        universo_realeco.nomo = kwargs.get('nomo_json', universo_realeco.nomo)
                                elif (kwargs.get('nomo', False)):
                                    set_enhavo(universo_realeco.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo_json', False)):
                                    if uzanto.has_perm('universo_bazo.povas_forigi_universo_realeco'):
                                        universo_realeco.priskribo = kwargs.get('priskribo_json', universo_realeco.priskribo)
                                elif (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_realeco.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_realeco.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Realeco.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuRealeco(status=status, message=message, universo_realeco=universo_realeco)


# Модель типов приложений Universo
class RedaktuUniversoAplikoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_apliko_tipo = graphene.Field(UniversoAplikoTipoNode,
        description=_('Созданный/изменённый тип приложения в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        nomo_json = GenericScalar(description=_('Название в JSON формате для администраторов'))
        priskribo = graphene.String(description=_('Описание'))
        priskribo_json = GenericScalar(description=_('Описание в JSON формате для администраторов'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_apliko_tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_bazo.povas_krei_universo_apliko_tipo'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            universo_apliko_tipo = UniversoAplikoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo_json', False)):
                                if uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_tipo'):
                                    universo_apliko_tipo.nomo = kwargs.get('nomo_json')
                            elif (kwargs.get('nomo', False)):
                                set_enhavo(universo_apliko_tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo_json', False)):
                                if uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_tipo'):
                                    universo_apliko_tipo.priskribo = kwargs.get('priskribo_json')
                            elif (kwargs.get('priskribo', False)):
                                set_enhavo(universo_apliko_tipo.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)

                            universo_apliko_tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('nomo_json', False) or kwargs.get('priskribo_json', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_apliko_tipo = UniversoAplikoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_tipo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_bazo.povas_shanghi_universo_apliko_tipo'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                universo_apliko_tipo.forigo = kwargs.get('forigo', universo_apliko_tipo.forigo)
                                universo_apliko_tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_apliko_tipo.arkivo = kwargs.get('arkivo', universo_apliko_tipo.arkivo)
                                universo_apliko_tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_apliko_tipo.publikigo = kwargs.get('publikigo', universo_apliko_tipo.publikigo)
                                universo_apliko_tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if (kwargs.get('nomo_json', False)):
                                    if uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_tipo'):
                                        universo_apliko_tipo.nomo = kwargs.get('nomo_json', universo_apliko_tipo.nomo)
                                elif (kwargs.get('nomo', False)):
                                    set_enhavo(universo_apliko_tipo.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo_json', False)):
                                    if uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_tipo'):
                                        universo_apliko_tipo.priskribo = kwargs.get('priskribo_json', universo_apliko_tipo.priskribo)
                                elif (kwargs.get('priskribo', False)):
                                    set_enhavo(universo_apliko_tipo.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)

                                universo_apliko_tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoAplikoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoAplikoTipo(status=status, message=message, universo_apliko_tipo=universo_apliko_tipo)


# Модель приложений Universo
class RedaktuUniversoAplikoVersio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    universo_apliko_versio = graphene.Field(UniversoAplikoVersioNode,
        description=_('Создание/изменёние приложения в Universo'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_id = graphene.Int(description=_('ID типа приложжения'))
        aktuala = graphene.Boolean(description=_('Признак актуальна ли версия (да или нет)'))
        numero_versio = graphene.Int(description=_('Номер версии'))
        numero_subversio = graphene.Int(description=_('Номер подверсии'))
        numero_korektado = graphene.Int(description=_('Номер исправления'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        universo_apliko_versio = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('universo_bazo.povas_krei_universo_apliko_versio'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = UniversoAplikoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except UniversoAplikoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип приложжения в Универсо'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            universo_apliko_versio = UniversoAplikoVersio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro=uzanto,
                                tipo=tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            universo_apliko_versio.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('tipo_id', False) or kwargs.get('aktuala', False)
                            or kwargs.get('numero_versio', False) or kwargs.get('numero_subversio', False)
                            or kwargs.get('numero_korektado', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            universo_apliko_versio = UniversoAplikoVersio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('universo_bazo.povas_forigi_universo_apliko_versio')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('universo_bazo.povas_shanghi_universo_apliko_versio'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                universo_apliko_versio.forigo = kwargs.get('forigo', universo_apliko_versio.forigo)
                                universo_apliko_versio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                universo_apliko_versio.arkivo = kwargs.get('arkivo', universo_apliko_versio.arkivo)
                                universo_apliko_versio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                universo_apliko_versio.publikigo = kwargs.get('publikigo', universo_apliko_versio.publikigo)
                                universo_apliko_versio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                universo_apliko_versio.kodo = kwargs.get('kodo', universo_apliko_versio.kodo)

                                universo_apliko_versio.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except UniversoAplikoVersio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuUniversoAplikoVersio(status=status, message=message, universo_apliko_versio=universo_apliko_versio)


class UniversoBazoMutations(graphene.ObjectType):
    redaktu_realeco = RedaktuRealeco.Field(
        description=_('''Создаёт или редактирует реальности''')
    )
    redaktu_universo_apliko_tipo = RedaktuUniversoAplikoTipo.Field(
        description=_('''Создаёт или редактирует тип приложения Universo''')
    )
    redaktu_universo_apliko_versio = RedaktuUniversoAplikoVersio.Field(
        description=_('''Создаёт или редактирует приложение Universo''')
    )
