"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from universo_bazo.models import Realeco, UniversoAplikoTipo, UniversoAplikoVersio
from siriuso.forms.widgets import LingvoInputWidget
from siriuso.utils.admin_mixins import TekstoMixin


# Форма для реальностей Universo
class RealecoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = Realeco
        fields = [field.name for field in Realeco._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Реальности Universo
@admin.register(Realeco)
class RealecoAdmin(admin.ModelAdmin, TekstoMixin):
    form = RealecoFormo
    list_display = ('id', 'kodo', 'nomo_teksto','autoro_nn','autoro_email')
    list_display_links = ('id','kodo','nomo_teksto')
    autocomplete_fields = ['autoro']
    readonly_fields = ('lasta_autoro','lasta_dato', )
    save_on_top = True
    save_as = True
    exclude = ('uuid',)

    class Meta:
        model = Realeco


# Форма для типов приложений Universo
class UniversoAplikoTipoFormo(forms.ModelForm):
    
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=False)
    priskribo = forms.CharField(widget=LingvoInputWidget(), label=_('Priskribo'), required=False)

    class Meta:
        model = UniversoAplikoTipo
        fields = [field.name for field in UniversoAplikoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out

    def clean_priskribo(self):
        out = self.cleaned_data['priskribo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы приложений Universo
@admin.register(UniversoAplikoTipo)
class UniversoAplikoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoAplikoTipoFormo
    list_display = ('nomo_teksto','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = UniversoAplikoTipo


# Форма для версий приложений Universo
class UniversoAplikoVersioFormo(forms.ModelForm):
    
    class Meta:
        model = UniversoAplikoVersio
        fields = [field.name for field in UniversoAplikoVersio._meta.fields if field.name not in ('krea_dato', 'uuid')]


# Версии приложений Universo
@admin.register(UniversoAplikoVersio)
class UniversoAplikoVersioAdmin(admin.ModelAdmin, TekstoMixin):
    form = UniversoAplikoVersioFormo
    list_display = ('tipo','versio','aktuala','autoro_nn','autoro_email')
    exclude = ('uuid',)
    autocomplete_fields = ['autoro']

    class Meta:
        model = UniversoAplikoVersio

