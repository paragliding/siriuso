"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _
import json

from lokalizo.models import *
from siriuso.forms.widgets import LingvoInputWidget
from siriuso.utils.admin_mixins import TekstoMixin


# Форма для многоязычных названий типов элементов локализации
class LokalizoElementoTipoFormo(forms.ModelForm):
    nomo = forms.CharField(widget=LingvoInputWidget(), label=_('Nomo'), required=True)

    class Meta:
        model = LokalizoElementoTipo
        fields = [field.name for field in LokalizoElementoTipo._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_nomo(self):
        out = self.cleaned_data['nomo']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Типы элементов локализации
@admin.register(LokalizoElementoTipo)
class LokalizoElementoTipoAdmin(admin.ModelAdmin, TekstoMixin):
    form = LokalizoElementoTipoFormo
    list_display = ('nomo_teksto', 'kodo', 'uuid')
    exclude = ('nomo_teksto',)

    class Meta:
        model = LokalizoElementoTipo


# Форма для многоязычного контента элементов локализации
class LokalizoElementoFormo(forms.ModelForm):
    teksto = forms.CharField(widget=LingvoInputWidget(), label=_('Teksto'), required=True)

    class Meta:
        model = LokalizoElemento
        fields = [field.name for field in LokalizoElemento._meta.fields if field.name not in ('krea_dato', 'uuid')]

    def clean_teksto(self):
        out = self.cleaned_data['teksto']
        try:
            out = json.loads(out)
            chefa_idx = out['lingvo'][out['chefa_varianto']] if out['chefa_varianto'] in out['lingvo'] else -1
            if not len(out['enhavo']) or (chefa_idx != -1 and not out['enhavo'][chefa_idx]):
                raise forms.ValidationError(_('This field is required.'))
        except json.JSONDecodeError:
            raise forms.ValidationError(_('Недопустимые данные в JSON поле'))
        return out


# Элементы локализации
@admin.register(LokalizoElemento)
class LokalizoElementoAdmin(admin.ModelAdmin, TekstoMixin):
    form = LokalizoElementoFormo
    list_display = ('id', 'kodo', 'teksto_teksto')
    exclude = ('id',)

    class Meta:
        model = LokalizoElemento
