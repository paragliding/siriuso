# Generated by Django 3.2.5 on 2021-08-16 02:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('universo_bazo', '0001_squashed_0014_auto_20211218_1901'),
        ('universo_uzantoj_websocket', '0002_auto_20210225_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='universouzantojwebsocket',
            name='realeco',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='universo_bazo.realeco', verbose_name='Realeco de Universo'),
        ),
    ]
