"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель категорий прав
class RedaktuRajtoKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    rajto_kategorioj = graphene.Field(RajtoKategorioNode,
        description=_('Созданная/изменённая категория прав'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        rajto_kategorioj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('rajtoj.povas_krei_rajtoj_kategorioj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            rajto_kategorioj = RajtoKategorio.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(rajto_kategorioj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(rajto_kategorioj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    rajto_kategorioj.realeco.set(realeco)
                                else:
                                    rajto_kategorioj.realeco.clear()

                            rajto_kategorioj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            rajto_kategorioj = RajtoKategorio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('rajtoj.povas_forigi_rajtoj_kategorioj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('rajtoj.povas_shanghi_rajtoj_kategorioj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:

                                rajto_kategorioj.forigo = kwargs.get('forigo', rajto_kategorioj.forigo)
                                rajto_kategorioj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                rajto_kategorioj.arkivo = kwargs.get('arkivo', rajto_kategorioj.arkivo)
                                rajto_kategorioj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                rajto_kategorioj.publikigo = kwargs.get('publikigo', rajto_kategorioj.publikigo)
                                rajto_kategorioj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                rajto_kategorioj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(rajto_kategorioj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(rajto_kategorioj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        rajto_kategorioj.realeco.set(realeco)
                                    else:
                                        rajto_kategorioj.realeco.clear()

                                rajto_kategorioj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except RajtoKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuRajtoKategorio(status=status, message=message, rajto_kategorioj=rajto_kategorioj)


# Модель типы прав
class RedaktuRajtoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    rajto_tipoj = graphene.Field(RajtoTipoNode,
        description=_('Созданный/изменённый тип прав'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        rajto_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('rajtoj.povas_krei_rajtoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            rajto_tipoj = RajtoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(rajto_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(rajto_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    rajto_tipoj.realeco.set(realeco)
                                else:
                                    rajto_tipoj.realeco.clear()

                            rajto_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            rajto_tipoj = RajtoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('rajtoj.povas_forigi_rajtoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('rajtoj.povas_shanghi_rajtoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )

                            if not message:

                                rajto_tipoj.forigo = kwargs.get('forigo', rajto_tipoj.forigo)
                                rajto_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                rajto_tipoj.arkivo = kwargs.get('arkivo', rajto_tipoj.arkivo)
                                rajto_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                rajto_tipoj.publikigo = kwargs.get('publikigo', rajto_tipoj.publikigo)
                                rajto_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                rajto_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(rajto_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(rajto_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        rajto_tipoj.realeco.set(realeco)
                                    else:
                                        rajto_tipoj.realeco.clear()

                                rajto_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except RajtoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuRajtoTipo(status=status, message=message, rajto_tipoj=rajto_tipoj)


# Модель прав
class RedaktuRajto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    rajto = graphene.Field(RajtoNode,
        description=_('Созданные/изменённые права'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        kategorio = graphene.List(graphene.Int,description=_('Список категорий прав'))
        tipo_id = graphene.Int(description=_('Тип прав'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        rajto = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user
        kategorio = RajtoKategorio.objects.none()

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('rajtoj.povas_krei_rajtoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        if not message:
                            if 'kategorio' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'kategorio')
                            elif not len(kwargs.get('kategorio')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'kategorio')
                            else:
                                kategorio_id = set(kwargs.get('kategorio'))

                                if len(kategorio_id):
                                    kategorio = RajtoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                    dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = RajtoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except RajtoTipo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверный тип прав'),'tipo_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            rajto = Rajto.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(rajto.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(rajto.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    rajto.realeco.set(realeco)
                                else:
                                    rajto.realeco.clear()

                            if 'kategorio' in kwargs:
                                if kategorio:
                                    rajto.kategorio.set(kategorio)
                                else:
                                    rajto.kategorio.clear()

                            rajto.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False) or kwargs.get('kategorio', False)
                            or kwargs.get('tipo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            rajto = Rajto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('rajtoj.povas_forigi_rajtoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('rajtoj.povas_shanghi_rajtoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            if not message:
                                if 'kategorio' in kwargs:
                                    kategorio_id = set(kwargs.get('kategorio'))

                                    if len(kategorio_id):
                                        kategorio = RajtoKategorio.objects.filter(id__in=kategorio_id, forigo=False, publikigo=True)
                                        dif = set(kategorio_id) - set(kategorio.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = RajtoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except RajtoTipo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверный тип прав'),'tipo_id')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')
                            if not message:

                                rajto.forigo = kwargs.get('forigo', rajto.forigo)
                                rajto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                rajto.arkivo = kwargs.get('arkivo', rajto.arkivo)
                                rajto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                rajto.publikigo = kwargs.get('publikigo', rajto.publikigo)
                                rajto.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                rajto.tipo = tipo if kwargs.get('tipo_id', False) else rajto.tipo
                                rajto.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(rajto.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(rajto.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        rajto.realeco.set(realeco)
                                    else:
                                        rajto.realeco.clear()
                                if 'kategorio' in kwargs:
                                    if kategorio:
                                        rajto.kategorio.set(kategorio)
                                    else:
                                        rajto.kategorio.clear()

                                rajto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except Rajto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuRajto(status=status, message=message, rajto=rajto)


# Модель наделение правами
class RedaktuRajtoRajtigo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    rajto_rajtigo = graphene.Field(RajtoRajtigoNode,
        description=_('Созданные/изменённые наделение правами'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        rajto_id = graphene.Int(description=_('Право'))
        posedanto_uzanto_id = graphene.Int(description=_('Пользователь получивший право'))
        posedanto_organizo_uuid = graphene.String(description=_('Организация получившая право'))
        komunumo_id = graphene.Int(description=_('Сообщество владелец проекта'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        rajto_rajtigo = None
        rajto = None
        posedanto_uzanto = None
        posedanto_organizo = None
        komunumo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('rajto.povas_krei_rajto_rajtigoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'posedanto_uzanto_id' in kwargs:
                                try:
                                    posedanto_uzanto = Uzanto.objects.get(
                                        id=kwargs.get('posedanto_uzanto_id'), 
                                        publikigo=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь')
                        if not message:
                            if 'rajto_id' in kwargs:
                                try:
                                    rajto = Rajto.objects.get(id=kwargs.get('rajto_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Rajto.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверные права'),'rajto_id')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'rajto_id')
                        if not message:
                            if 'posedanto_organizo_uuid' in kwargs:
                                try:
                                    posedanto_organizo = Organizo.objects.get(uuid=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except Organizo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверная организация'),'posedanto_organizo_uuid')
                        if not message:
                            if 'komunumo_id' in kwargs:
                                try:
                                    komunumo = Komunumo.objects.get(
                                        id=kwargs.get('komunumo_id'),
                                        forigo=False,
                                        arkivo=False
                                    )
                                except Komunumo.DoesNotExist:
                                    message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'komunumo_id')


                        if not message:
                            rajto_rajtigo = RajtoRajtigo.objects.create(
                                forigo=False,
                                arkivo=False,
                                rajto=rajto,
                                posedanto_uzanto=posedanto_uzanto,
                                posedanto_organizo=posedanto_organizo,
                                komunumo = komunumo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            rajto_rajtigo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('posedanto_organizo_uuid', False) or kwargs.get('rajto_id', False)
                            or kwargs.get('komunumo_id', False)
                            or kwargs.get('posedanto_uzanto_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            rajto_rajtigo = RajtoRajtigo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('rajto.povas_forigi_rajto_rajtigoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('rajto.povas_shanghi_rajto_rajtigoj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'posedanto_uzanto_id' in kwargs:
                                    try:
                                        posedanto_uzanto = Uzanto.objects.get(
                                            id=kwargs.get('posedanto_uzanto_id'), 
                                            publikigo=True)
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь')
                            if not message:
                                if 'rajto_id' in kwargs:
                                    try:
                                        rajto = Rajto.objects.get(id=kwargs.get('rajto_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Rajto.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверные права'),'rajto_id')
                            if not message:
                                if 'posedanto_organizo_uuid' in kwargs:
                                    try:
                                        posedanto_organizo = Organizo.objects.get(uuid=kwargs.get('posedanto_organizo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except Organizo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверная организация'),'posedanto_organizo_uuid')
                            if not message:
                                if 'komunumo_id' in kwargs:
                                    try:
                                        komunumo = Komunumo.objects.get(
                                            id=kwargs.get('komunumo_id'),
                                            forigo=False,
                                            arkivo=False
                                        )
                                    except Komunumo.DoesNotExist:
                                        message = '{} "{}"'.format(_('Неверное сообщество-владелец проекта'),'komunumo_id')

                            if not message:

                                rajto_rajtigo.forigo = kwargs.get('forigo', rajto_rajtigo.forigo)
                                rajto_rajtigo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                rajto_rajtigo.arkivo = kwargs.get('arkivo', rajto_rajtigo.arkivo)
                                rajto_rajtigo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                rajto_rajtigo.publikigo = kwargs.get('publikigo', rajto_rajtigo.publikigo)
                                rajto_rajtigo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                rajto_rajtigo.posedanto_uzanto = posedanto_uzanto if kwargs.get('posedanto_uzanto_id', False) else rajto_rajtigo.posedanto_uzanto
                                rajto_rajtigo.rajto = rajto if kwargs.get('rajto_id', False) else rajto_rajtigo.rajto
                                rajto_rajtigo.posedanto_organizo = posedanto_organizo if kwargs.get('posedanto_organizo_uuid', False) else rajto_rajtigo.posedanto_organizo
                                rajto_rajtigo.komunumo = komunumo if kwargs.get('komunumo_id', False) else rajto_rajtigo.komunumo

                                rajto_rajtigo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except RajtoRajtigo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuRajtoRajtigo(status=status, message=message, rajto_rajtigo=rajto_rajtigo)


class RajtojMutations(graphene.ObjectType):
    redaktu_rajto_kategorio = RedaktuRajtoKategorio.Field(
        description=_('''Создаёт или редактирует категории прав''')
    )
    redaktu_rajto_tipo = RedaktuRajtoTipo.Field(
        description=_('''Создаёт или редактирует типы прав''')
    )
    redaktu_rajto = RedaktuRajto.Field(
        description=_('''Создаёт или редактирует права''')
    )
    redaktu_rajto_rajtigo = RedaktuRajtoRajtigo.Field(
        description=_('''Создаёт или редактирует наделение правами''')
    )
