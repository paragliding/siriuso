"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import random
import string
import sys

from django.contrib.auth.models import Permission
from django.db import models
from django.db.models import Max, Q
from django.utils.translation import gettext_lazy as _

from informiloj.models import InformilojSciigoTipo
from komunumoj.models import Komunumo
from main.models import SiriusoBazaAbstrakta3, Uzanto, SiriusoBazaAbstrakta2, SiriusoBazaAbstraktaKomunumoj
from siriuso.models.postgres import CallableEncoder
from siriuso.utils import default_lingvo, get_enhavo
from siriuso.utils import perms
from muroj.models import MuroEnskribo, MurojUzantoEnskribo


# Модель настроек пользователей, относящаяся ко всем чатам
class MesagxiloUzantoOpcio(SiriusoBazaAbstrakta3):
    # участник чата, поле связи 1к1
    uzanto = models.OneToOneField(Uzanto, verbose_name=_('Uzanto mesagxilo'), blank=False, default=None, unique=True,
                                      on_delete=models.CASCADE)

    # Кто может писать мне личные сообщения -      код   - код списка
    #     Все пользователи                          0
    #     Только друзья                             1
    #     Друзья и друзья друзей                    2
    #     Никто                                     3
    #     Все, кроме...                         
    #         Кому разрешен доступ
    #             всем пользователям                4
    #             только друзьям                    5
    #             друзьям и друзья друзей           6
    #             некоторым друзьям (выбор списком) 7           11
    #                 выбор списком
    #                     списки, пользователи
    #         Кому запрещен доступ (выбор списком)  8           12
    #             выбор списком
    #                 списки, пользователи
    #     Некоторые друзья                          
    #         перечисление друзей (выбор списком)   9           13
    privata = models.IntegerField(_('Privata'), unique=False, default=0)


    # разрешения добавления в групповые чаты
    #     Все пользователи                          0
    #     Только друзья                             1
    #     Друзья и друзья друзей                    2
    #     Никто                                     3
    #     Все, кроме...                             
    #         Кому разрешен доступ
    #             всем пользователям                4
    #             только друзьям                    5
    #             друзьям и друзья друзей           6
    #             некоторым друзьям (выбор списком) 7           21
    #                 выбор списком
    #                     списки, пользователи
    #         Кому запрещен доступ (выбор списком)  8           22
    #             выбор списком
    #                 списки, пользователи
    #     Некоторые друзья                       
    #         перечисление друзей (выбор списком)   9           23
    grupo = models.IntegerField(_('Grupo'), unique=False, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_uzanto_opcio'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mesagxilo uzanto opcio')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Mesagxiloj uzanto opcio')
        # права
        permissions = (
            ('povas_vidi_uzanto_opcio', _('Povas vidi uzanto opcio')),
            ('povas_krei_uzanto_opcio', _('Povas krei uzanto opcio')),
            ('povas_forigi_uzanto_opcio', _('Povas forigu uzanto opcio')),
            ('povas_shanghi_uzanto_opcio', _('Povas ŝanĝi uzanto opcio')),
        )

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_uzanto_opcio', 'mesagxilo.povas_krei_uzanto_opcio',
                'mesagxilo.povas_forigi_uzanto_opcio', 'mesagxilo.povas_shanghi_uzanto_opcio'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_uzanto_opcio')
                    or user_obj.has_perm('mesagxilo.povas_vidi_uzanto_opcio')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_uzanto_opcio'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Модель списков доступа пользователям
class MesagxiloUzantoAliro(SiriusoBazaAbstrakta2):

    # к каким настройкам пользователя относится
    opcio = models.ForeignKey('MesagxiloUzantoOpcio', verbose_name=_('Uzanto opcio'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # код списка пользователей
    #  Кто может писать мне личные сообщения:
    # 11 - Все, кроме.. некоторые друзья,
    # 12 -         Кому запрещен доступ (выбор списком)
    # и далее, согласно верхнему описанию
    grupo = models.IntegerField(_('Grupo'), unique=False, default=0)

    # список пользователей
    uzantoj = models.ManyToManyField(Uzanto, verbose_name=_('Uzanto opcio mesagxo'), blank=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_uzanto_aliro'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mesagxilo uzanto aliro')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Mesagxiloj uzanto aliro')
        # права
        permissions = (
            ('povas_vidi_uzanto_aliro', _('Povas vidi uzanto aliro')),
            ('povas_krei_uzanto_aliro', _('Povas krei uzanto aliro')),
            ('povas_forigi_uzanto_aliro', _('Povas forigu uzanto aliro')),
            ('povas_shanghi_uzanto_aliro', _('Povas ŝanĝi uzanto aliro')),
        )

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_uzanto_aliro', 'mesagxilo.povas_krei_uzanto_aliro',
                'mesagxilo.povas_forigi_uzanto_aliro', 'mesagxilo.povas_shanghi_uzanto_aliro'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_uzanto_aliro')
                    or user_obj.has_perm('mesagxilo.povas_vidi_uzanto_aliro')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_uzanto_aliro'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond



# Генерация случайного названия и переименование загружаемых файлов
def mesagxilo_bildo(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'mesagxilo/bildo/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# Модель чат, использует абстрактный класс SiriusoBazaAbstrakta3
class MesagxiloBabilejo(SiriusoBazaAbstraktaKomunumoj):
    # autoro - создатель чата (удобно знать предыдущего создателя при передаче прав)

    # ID записи
    id = models.IntegerField(_('ID'), unique=True, default=0)

    # название чата, от туда будет браться название с нужным языковым тегом
    nomo = models.JSONField(verbose_name=_('Nomo'), blank=True, null=False, default=default_lingvo,
                              encoder=CallableEncoder)
                              
    # личный (True) или групповой (False) чат
    personal = models.BooleanField(_('Personal'), default=True)

    # информационное изображение
    bildo_info = models.ImageField(_('Infa bildo'), upload_to=mesagxilo_bildo, blank=True)

    # закреплённое сообщение
    mesagxo = models.ForeignKey('MesagxiloMesagxo', verbose_name=_('Kovris mesagxo'), blank=True, default=None,
                                on_delete=models.SET_NULL, null=True)

    # Настройки
    # Если групповой, то делится на публичный (открыт для входа и поиска) и приватный (закрыт для посторонних)
    opcio_privata = models.BooleanField(_('Opcio privata'), default=True)

    # Могут приглашать участников в беседу (ложь - только администраторы чата)
    opcio_partoprenantoj = models.BooleanField(_('Opcio partoprenantoj'), default=True)

    # Могут редактировать информацию беседы (ложь - только администраторы чата)
    opcio_info = models.BooleanField(_('Opcio informoj'), default=True)

    # Могут менять закреплённое сообщение (ложь - только администраторы чата)
    opcio_kovris = models.BooleanField(_('Opcio kovris'), default=True)

    # Могут добавлять администраторов (истина - создатель и администраторы чата, ложь - только создатель чата)
    opcio_aldoni = models.BooleanField(_('Opcio aldoni'), default=False)

    # Запрещено полное удаление сообщения (админы могут видеть - истина)
    opcio_del = models.BooleanField(_('Opcio del'), default=False)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_babilejo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Babilejo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Babilejoj')
        # права
        permissions = (
            ('povas_vidi_babilejo_modelo', _('Povas vidi babilejo modelo')),
            ('povas_krei_babilejo_modelo', _('Povas krei babilejo modelo')),
            ('povas_forigi_babilejo_modelo', _('Povas forigu babilejo modelo')),
            ('povas_shanghi_babilejo_modelo', _('Povas ŝanĝi babilejo modelo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле nomo этой модели
        return '{}: {}'.format(self.id, get_enhavo(self.nomo, empty_values=True)[0])

    # реализация автоинкремента при сохранении
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):

        if self.id is None or not self.id:
            model = getattr(sys.modules[self.__module__], self.__class__.__name__)
            next_id = model.objects.all().aggregate(Max('id'))['id__max']

            if next_id is None:
                next_id = 1
            else:
                next_id = next_id + 1
            super(model, self).__setattr__('id', next_id)

        super(MesagxiloBabilejo, self).save(force_insert=force_insert, force_update=force_update,
                                            using=using, update_fields=update_fields)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_babilejo_modelo', 'mesagxilo.povas_krei_babilejo_modelo',
                'mesagxilo.povas_forigi_babilejo_modelo', 'mesagxilo.povas_shanghi_babilejo_modelo',
                'mesagxilo.povas_krei_partoprenanto', 'mesagxilo.povas_krei_mesagxo'
            ))

            # Добавляем прав создателю чата
            if user_obj==self.autoro:
                all_perms.update(['povas_vidi_babilejo_modelo','povas_krei_babilejo_modelo',
                'povas_forigi_babilejo_modelo', 'povas_shanghi_babilejo_modelo',
                'povas_krei_partoprenanto', 'povas_krei_mesagxo'])
            # Добавляем участников чата на просмотр
            try:
                partoprenanto = MesagxiloPartoprenanto.objects.get(
                        babilejo=self, partoprenanto=user_obj)
                all_perms.add('povas_vidi_babilejo_modelo')
            except MesagxiloPartoprenanto.DoesNotExist:
                pass
            # Если чат собщества, то добавляем на просмотр членов сообщества
            # KomunumoMembro - члены сообщества

            # Добавляем права админам
            try:
                partoprenanto = MesagxiloPartoprenanto.objects.get(
                        babilejo=self, partoprenanto=user_obj, is_admin=True)
                all_perms.update(['povas_krei_babilejo_modelo',
                    'povas_forigi_babilejo_modelo', 'povas_shanghi_babilejo_modelo',
                    'povas_krei_partoprenanto', 'povas_krei_mesagxo'])
            except MesagxiloPartoprenanto.DoesNotExist:
                pass

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_babilejo_modelo')
                    or user_obj.has_perm('mesagxilo.povas_vidi_babilejo_modelo')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)
                #Просмотр создателям чатов
                cond |= Q(autoro=user_obj)
                # Просмотр участникам чатов
                babilejoj = MesagxiloPartoprenanto.objects.filter(
                    partoprenanto=user_obj
                ).values_list('babilejo')
                cond |= Q(uuid__in=babilejoj)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_babilejo_modelo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Участники чатов, использует абстрактный класс SiriusoBazaAbstrakta3
class MesagxiloPartoprenanto(SiriusoBazaAbstrakta3):
    # участник чата
    partoprenanto = models.ForeignKey(Uzanto, verbose_name=_('Partoprenanto'), blank=False, default=None,
                                      related_name='%(app_label)s_%(class)s_partoprenanto', on_delete=models.CASCADE)

    # если владельцем чата является сообщество
    posedanto = models.ForeignKey(Komunumo, verbose_name=_('Posedanto'), blank=True, null=True, default=None,
                                  on_delete=models.SET_NULL)

    # чат
    babilejo = models.ForeignKey(MesagxiloBabilejo, verbose_name=_('Babilejo'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # права админа чата
    is_admin = models.BooleanField(_('Admin babilejo'), default=False)

    # тип способа уведомления
    sciigoj = models.ManyToManyField(InformilojSciigoTipo,
                                     through='MesagxiloSciigoj',
                                     verbose_name=_('Sciigo (mesagxilo)'), blank=True)

    # оповещать о новых сообщениях в целом
    sciigi = models.BooleanField(_('Sciigi'), default=True)

    # последнее прочитанное сообщение (что бы указывать откуда начинаются не прочитанные/новые)
    lasta_mesagxo = models.ForeignKey('MesagxiloMesagxo', verbose_name=_('Lasta mesagxo kiun vi legis'), blank=True,
                                      default=None,
                                      on_delete=models.SET_NULL, null=True)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_partoprenanto'
        # читабельное название модели, в единственном числе
        verbose_name = _('Partoprenanto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Partoprenantoj')
        unique_together = ("partoprenanto", "babilejo")
        # права
        permissions = (
            ('povas_vidi_partoprenanto', _('Povas vidi partoprenanto')),
            ('povas_krei_partoprenanto', _('Povas krei partoprenanto')),
            ('povas_forigi_partoprenanto', _('Povas forigu partoprenanto')),
            ('povas_shanghi_partoprenanto', _('Povas ŝanĝi partoprenanto')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # return str(self.uuid)
        return '{}) {} - {} {} {}'.format(self.babilejo.id,
                                          get_enhavo(self.babilejo.nomo, empty_values=True)[0],
                                          get_enhavo(self.partoprenanto.unua_nomo, empty_values=True)[0],
                                          get_enhavo(self.partoprenanto.dua_nomo, empty_values=True)[0],
                                          get_enhavo(self.partoprenanto.familinomo, empty_values=True)[0])

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_partoprenanto', 'mesagxilo.povas_krei_partoprenanto',
                'mesagxilo.povas_forigi_partoprenanto', 'mesagxilo.povas_shanghi_partoprenanto'
            ))

            # Добавляем прав владельцу
            if user_obj==self.partoprenanto:
                all_perms.update(['povas_vidi_partoprenanto', 'povas_krei_partoprenanto',
                'povas_forigi_partoprenanto', 'povas_shanghi_partoprenanto'])

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_partoprenanto')
                    or user_obj.has_perm('mesagxilo.povas_vidi_partoprenanto')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)
                #Просмотр владельцу
                cond |= Q(partoprenanto=user_obj)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_partoprenanto'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


class MesagxiloSciigoj(models.Model):
    partoprenanto = models.ForeignKey('MesagxiloPartoprenanto', verbose_name=_('MesagxiloPartoprenanto'), blank=False,
                                      default=None,
                                      on_delete=models.CASCADE)

    sciigo = models.ForeignKey(InformilojSciigoTipo, verbose_name=_('InformilojSciigoTipo'), blank=False, default=None,
                               on_delete=models.CASCADE)

    # Берём за образец телеграм

    # Есть уведомление от личной переписки в целом - не введено
    # Есть от конкретного чата - канала, конкретной личной переписки + sciigi
    # Есть приостановление на время + sciigi_dato
    # Есть уведомление только когда к тебе обращаются конкретно - в групповых чатах имеется ввиду - не найдено

    # У телеграма на почту нет, потому что он сам приложение, в данном случае как пример ВКонтакте и Фейсбук,
    #  где выбор: внутренние, на почту, на телефон
    # Нужно учитывать что в конечном итоге будут 3 варианта уведомлений

    # оповещать о новых сообщениях
    #  в броузере - звуковое оповещение
    #  для почты - оповещение не чаще раза в сутки (при false)
    sciigi = models.BooleanField(_('Sciigi'), default=True)

    # для внутреннего: дата и время включения оповещений (если дата/время после текущей, то не оповещать) - можно остановить оповещение на часы и т.д.
    #  для почты: дата последней отправки оповещения
    sciigi_dato = models.DateTimeField(_('Dato de sciigi'), auto_now_add=False, auto_now=False, blank=True,
                                       null=True, default=None)

    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_sciigoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mesagxilo sciigo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Mesagxoj sciigoj')
        unique_together = ("partoprenanto", "sciigo")


# Сообщения чатов, использует абстрактный класс SiriusoBazaAbstrakta3
class MesagxiloMesagxo(SiriusoBazaAbstrakta3):
    # чат
    babilejo = models.ForeignKey(MesagxiloBabilejo, verbose_name=_('Babilejo'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # кто написал (владелец) сообщения
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  related_name='%(app_label)s_%(class)s_posedanto', on_delete=models.CASCADE)

    # если написано от имени сообщества
    komunumo = models.ForeignKey(Komunumo, verbose_name=_('Komunumo'), blank=True, null=True, default=None,
                                 on_delete=models.SET_NULL)

    # текст сообщения, от туда будет браться текст с нужным языковым тегом
    teksto = models.JSONField(verbose_name=_('Teksto'), blank=True, null=False, default=default_lingvo,
                                encoder=CallableEncoder)

    # Записи стен
    enskribo = models.ForeignKey(MuroEnskribo, verbose_name=_('Muroj enskribo'), blank=True, null=True, default=None,
                                 on_delete=models.SET_NULL)
    
    # Записи стен пользователей
    enskribo_uzanto = models.ForeignKey(MurojUzantoEnskribo, verbose_name=_('Muroj enskribo'), blank=True, null=True, default=None,
                                 on_delete=models.SET_NULL)

    # комментируемое сообщение
    mesagxo = models.ForeignKey('self', verbose_name=_('Komento mesagxo'), blank=True, default=None,
                                on_delete=models.SET_NULL, null=True)

    # вложения (инстина - есть вложения)
    investoj = models.BooleanField(_('Investoj'), default=False)

    # прочитано сообщение другим пользователем
    # изменяется бакэндом при изминении Uzantoj.UzantojSciigoj поля vidita --- доделать!!!???
    vidita =  models.BooleanField(_('Vidita'), blank=True, default=False)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_mesagxo'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mesagxo')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Mesagxoj')
        # права
        permissions = (
            ('povas_vidi_mesagxo', _('Povas vidi mesagxo')),
            ('povas_krei_mesagxo', _('Povas krei mesagxo')),
            ('povas_forigi_mesagxo', _('Povas forigu mesagxo')),
            ('povas_shanghi_mesagxo', _('Povas ŝanĝi mesagxo')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        # для представления объекта будет выведено поле teksto этой модели
        return '{}'.format(get_enhavo(self.teksto, empty_values=True)[0])

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_mesagxo', 'mesagxilo.povas_krei_mesagxo',
                'mesagxilo.povas_forigi_mesagxo', 'mesagxilo.povas_shanghi_mesagxo',
                'mesagxilo.povas_krei_investoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_mesagxo')
                    or user_obj.has_perm('mesagxilo.povas_vidi_mesagxo')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_mesagxo'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond

# Сообщения чатов для пользователей, использует абстрактный класс SiriusoBazaAbstrakta2 (нет смысла в полях публикаций и архивации)
class MesagxiloMesagxoUzanto(SiriusoBazaAbstrakta2):

    # у кого в общении показывать сообщение
    posedanto = models.ForeignKey(Uzanto, verbose_name=_('Posedanto'), blank=False, default=None,
                                  related_name='%(app_label)s_%(class)s_posedanto', on_delete=models.CASCADE)

    # сообщение
    mesagxo = models.ForeignKey(MesagxiloMesagxo, verbose_name=_('Mesagxo'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_mesagxo_uzanto'
        # читабельное название модели, в единственном числе
        verbose_name = _('Mesagxo uzanto')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Mesagxoj uzantoj')
        # права
        permissions = (
            ('povas_vidi_mesagxo_uzanto', _('Povas vidi mesagxo uzanto')),
            ('povas_krei_mesagxo_uzanto', _('Povas krei mesagxo uzanto')),
            ('povas_forigi_mesagxo_uzanto', _('Povas forigu mesagxo uzanto')),
            ('povas_shanghi_mesagxo_uzanto', _('Povas ŝanĝi mesagxo uzanto')),
        )

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_mesagxo_uzanto', 'mesagxilo.povas_krei_mesagxo_uzanto',
                'mesagxilo.povas_forigi_mesagxo_uzanto', 'mesagxilo.povas_shanghi_mesagxo_uzanto'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_mesagxo_uzanto')
                    or user_obj.has_perm('mesagxilo.povas_vidi_mesagxo_uzanto')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_mesagxo_uzanto'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond


# Генерация случайного названия и переименование загружаемых файлов
def mesagxilo_dosiero(instance, filename):
    letters = string.ascii_letters + string.digits
    rnd_string = ''.join(random.choice(letters) for i in range(10))
    # указание папки для хранения файлов и собирание конечного названия с сохранением изначального расширения
    return 'mesagxilo/dosiero/{0}.{1}'.format(rnd_string, filename.split('.')[-1])


# вложения к сообщениям чатов, использует абстрактный класс SiriusoBazaAbstrakta3
class MesagxiloInvestoj(SiriusoBazaAbstrakta3):
    # чат
    babilejo = models.ForeignKey(MesagxiloBabilejo, verbose_name=_('Babilejo'), blank=False, default=None,
                                 on_delete=models.CASCADE)

    # сообщение
    mesagxo = models.ForeignKey(MesagxiloMesagxo, verbose_name=_('Mesagxo'), blank=False, default=None,
                                on_delete=models.CASCADE)

    # присоединенный файл
    dosiero = models.FileField(_('Dosiero'), upload_to=mesagxilo_dosiero, blank=True)

    # тип присоединённого файла (0 - изображение, 1 - видео, 2 - документ, 3 - аудио)
    type_dosiero = models.IntegerField(_('Type dosiero'), unique=False, default=0)

    # дополнительные настройки для модели
    class Meta:
        # название таблицы в базе данных для этой модели
        db_table = 'mesagxilo_investoj'
        # читабельное название модели, в единственном числе
        verbose_name = _('Investoj')
        # читабельное название модели, во множественном числе
        verbose_name_plural = _('Investoj')
        # права
        permissions = (
            ('povas_vidi_investoj', _('Povas vidi investon')),
            ('povas_krei_investoj', _('Povas krei investon')),
            ('povas_forigi_investoj', _('Povas forigu investon')),
            ('povas_shanghi_investoj', _('Povas ŝanĝi investon')),
        )

    # выбор объекта для отображения в интерфейсах, в том числе администратора
    def __str__(self):
        return str(self.uuid)

    def _get_user_permissions(self, user_obj):
        # Объявляем пустой список прав
        user_perms = Permission.objects.none()

        if user_obj.is_authenticated:
            # Права зарегистриованных для приложения мессенджера
            all_perms = set(perms.user_registrita_perms(apps=('mesagxilo',)))

            # Преобразуем список прав к имени права без приложения, отфильтровывая только нужные для текущей модели
            # и одно право на создание для дочерней модели
            all_perms = set(perm.split('.')[-1] for perm in all_perms if perm in (
                'mesagxilo.povas_vidi_investoj', 'mesagxilo.povas_krei_investoj',
                'mesagxilo.povas_forigi_investoj', 'mesagxilo.povas_shanghi_investoj'
            ))

            # Делаем выборку прав из модели прав Django
            user_perms = Permission.objects.filter(content_type__app_label='mesagxilo', codename__in=all_perms)

        return user_perms

    def _get_group_permissions(self, user_obj):
        return Permission.objects.none()

    @staticmethod
    def _get_perm_cond(user_obj):
        if user_obj.is_authenticated:
            # Для авторизированного пользователя
            if (perms.has_registrita_perm('mesagxilo.povas_vidi_investoj')
                    or user_obj.has_perm('mesagxilo.povas_vidi_investoj')):
                # Если есть право просмотра у зарегистрированного или есть право Django у пользователя
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        else:
            # Для неавторизированных пользователей
            if perms.has_neregistrita_perm('mesagxilo.povas_vidi_investoj'):
                # Если есть права на просмотр
                cond = Q()
            else:
                # Если права нет, то задаем заведомо невыполнимое условие, например, первичный ключ равен NULL
                cond = Q(uuid__isnull=True)

        return cond
