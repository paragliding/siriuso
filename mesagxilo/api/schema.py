"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene  # сам Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from graphene_django import DjangoObjectType  # Класс описания модели Django для Graphene
from graphene_permissions.permissions import AllowAny  # Класс доступа к типу Graphene

from siriuso.utils import lingvo_kodo_normaligo, get_lang_kodo
from siriuso.api.filters import SiriusoFilterConnectionField  # Коннектор для получения данных
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions  # Миксины для описания типов  Graphene
from siriuso.api.types import SiriusoLingvo  # Объект Graphene для представления мультиязычного поля
from versioj.api.schema import VersioMesagxiloMesagxoNode
from versioj.models import VersioMesagxiloMesagxo
from ..models import *  # модели приложения

import re


# Модель списков доступа пользователям
class MesagxiloUzantoAliroNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = MesagxiloUzantoAliro
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'opcio__uuid': ['exact'],
            'uzantoj__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Модель настроек пользователей, относящаяся ко всем чатам
class MesagxiloUzantoOpcioNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    aliro = SiriusoFilterConnectionField(MesagxiloUzantoAliroNode,
                                            description=_('Выводит сообщения чатов'))

    class Meta:
        model = MesagxiloUzantoOpcio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'uzanto__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_aliro(self, info, **kwargs):
        return MesagxiloUzantoAliro.objects.filter(opcio=self)


class MesagxiloBildoMixin:
    """
    Миксин разрешает файлы изображений как абсолютный URL
    """

    @staticmethod
    def __resolve_bildo(request, bildo, default=None):
        if re.search(r'^/static/', str(bildo)):
            image = bildo.name
        else:
            image = getattr(bildo, 'url') if bildo else default
        return request.build_absolute_uri(image) if image else None

    def resolve_bildo_info(self, info):
        return MesagxiloBildoMixin.__resolve_bildo(info.context, getattr(self, 'bildo_info'))


# Участники чатов
class MesagxiloPartoprenantoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = MesagxiloPartoprenanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'partoprenanto__id': ['exact'],
            'babilejo__uuid': ['exact'],
            'babilejo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

# Сообщения чатов
class MesagxiloMesagxoNode(SiriusoAuthNode, SiriusoPermissions, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('Текст сообщения'))

    versioj = SiriusoFilterConnectionField(VersioMesagxiloMesagxoNode, description=_('Версии страницы'))

    class Meta:
        model = MesagxiloMesagxo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'mesagxo__uuid': ['exact'],
            'babilejo__uuid': ['exact'],
            'babilejo__id': ['exact'],
            'posedanto__id': ['exact'],
            'komunumo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioMesagxiloMesagxo

        perm_name = 'versioj.povas_vidi_mesagxilon_mesagxon_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Сообщения чатов по пользователю
class MesagxiloMesagxoUzantoNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'mesagxo__teksto__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('Текст сообщения'))

    class Meta:
        model = MesagxiloMesagxoUzanto
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'mesagxo__uuid': ['exact'],
            'posedanto__id': ['exact'],
            'mesagxo__arkivo': ['exact'],
            'mesagxo__publikigo': ['exact'],
            'mesagxo__mesagxo__uuid': ['exact'],
            'mesagxo__babilejo__uuid': ['exact'],
            'mesagxo__babilejo__id': ['exact'],
            'mesagxo__posedanto__id': ['exact'],
            'mesagxo__komunumo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_teksto(self, info, **kwargs):
        try:
            tek=MesagxiloMesagxo.objects.get(uuid=self.mesagxo.uuid)
        except MesagxiloMesagxo.DoesNotExist:
            return None
        return tek.teksto


# Модель чат
class MesagxiloBabilejoNode(SiriusoAuthNode, MesagxiloBildoMixin, SiriusoObjectId, DjangoObjectType):
    permission_classes = (AllowAny,)
    json_filter_fields = {
        'id': ['icontains', ],
        'nomo__enhavo': ['contains', 'icontains']
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование чата'))

    partoprenantoj = SiriusoFilterConnectionField(MesagxiloPartoprenantoNode,
                                            description=_('Выводит список участников чатов'))

    mesagxoj = SiriusoFilterConnectionField(MesagxiloMesagxoNode,
                                            description=_('Выводит сообщения чатов'))

    mesagxoj_uzanto = SiriusoFilterConnectionField(MesagxiloMesagxoUzantoNode,
                                            description=_('Выводит сообщения чатов по пользователю'))

    class Meta:
        model = MesagxiloBabilejo
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'mesagxo__uuid': ['exact'],
            'autoro__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_partoprenantoj(self, info, **kwargs):
        return MesagxiloPartoprenanto.objects.filter(babilejo=self)

    def resolve_mesagxoj(self, info, **kwargs):
        return MesagxiloMesagxo.objects.filter(babilejo=self)

    def resolve_mesagxoj_uzanto(self, info, **kwargs):
        return MesagxiloMesagxoUzanto.objects.filter(mesagxo__babilejo=self, posedanto=info.context.user)



class MesagxiloDosieroMixin:
    """
    Миксин разрешает присоединенные файлы как абсолютный URL
    """

    @staticmethod
    def __resolve_dosiero(request, dosiero, default=None):
        if re.search(r'^/static/', str(dosiero)):
            image = dosiero.name
        else:
            image = getattr(dosiero, 'url') if dosiero else default
        return request.build_absolute_uri(image) if image else None

    def resolve_dosiero(self, info):
        return MesagxiloDosieroMixin.__resolve_dosiero(info.context, getattr(self, 'dosiero'))


# вложения к сообщениям чатов
class MesagxiloInvestojNode(SiriusoAuthNode, MesagxiloDosieroMixin, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = MesagxiloInvestoj
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'mesagxo__uuid': ['exact'],
            'babilejo__uuid': ['exact'],
            'babilejo__id': ['exact']
        }
        interfaces = (graphene.relay.Node,)


class MesagxiloSciigojNode(SiriusoAuthNode, DjangoObjectType):
    permission_classes = (AllowAny,)

    class Meta:
        model = MesagxiloSciigoj
        filter_fields = {
        }
        interfaces = (graphene.relay.Node,)


class MesagxiloQuery(graphene.ObjectType):
    mesagxiloj_uzanto_opcio = SiriusoFilterConnectionField(MesagxiloUzantoOpcioNode,
                                                      description=_('Выводит все доступные опции пользователей'))
    mesagxiloj_uzanto_aliro = SiriusoFilterConnectionField(MesagxiloUzantoAliroNode,
                                                      description=_('Выводит все доступные настройки опций пользователей'))
    mesagxiloj_babilejo = SiriusoFilterConnectionField(MesagxiloBabilejoNode,
                                                       description=_('Выводит все доступные чаты'))
    mesagxiloj_partoprenanto = SiriusoFilterConnectionField(MesagxiloPartoprenantoNode,
                                                            description=_('Выводит все доступные участники чатов'))
    mesagxiloj_mesagxo = SiriusoFilterConnectionField(MesagxiloMesagxoNode,
                                                      description=_('Выводит все доступные сообщения чатов'))
    mesagxiloj_mesagxo_uzanto = SiriusoFilterConnectionField(MesagxiloMesagxoUzantoNode,
                                                      description=_('Выводит все доступные сообщения чатов по пользователю'))
    mesagxiloj_investoj = SiriusoFilterConnectionField(MesagxiloInvestojNode,
                                                       description=_(
                                                           'Выводит все доступные вложения к сообщениям чатов'))
    mesagxiloj_sciigoj = SiriusoFilterConnectionField(MesagxiloSciigojNode,
                                                      description=_('Выводит все доступные '))
