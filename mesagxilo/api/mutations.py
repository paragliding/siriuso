"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from mesagxilo.tasks import sciigi_mesagxilo
from siriuso.api.types import ErrorNode
from siriuso.utils import set_enhavo
from siriuso.utils.thumbnailer import get_image_properties, get_web_image
from .schema import *
from ..models import *
from .subscription import MesagxiloEventoj

# Максимальный размер загружаемого файла
MAX_FILE_SIZE = 1024 * 1024 * 10  # 10 МБ

class MesSciigoj(graphene.InputObjectType):
    sciigo = graphene.String(description=_('Код типа подписки'))
    sciigi = graphene.Boolean(description=_('Оповещать о новых сообщениях из данного подписки'))
    sciigi_dato = graphene.DateTime(description=_('Дата/время'))

# Модель настроек пользователей, относящаяся ко всем чатам
class RedaktuMesagxiloUzantoOpcio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    opcio = graphene.Field(MesagxiloUzantoOpcioNode, description=_('Созданный/изменённый чат'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        privata = graphene.Int(description=_('Кто может писать мне личные сообщения'))
        grupo = graphene.Int(description=_('Разрешения добавления в групповые чаты'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        opcio = None
        mesagxo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mesagxilo.povas_krei_uzanto_opcio'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            opcio = MesagxiloUzantoOpcio.objects.create(
                                forigo=False,
                                arkivo=False,
                                uzanto = uzanto,
                                privata=kwargs.get('privata', 0),
                                grupo=kwargs.get('grupo', 0),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            opcio.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('privata', False) or kwargs.get('grupo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            opcio = MesagxiloUzantoOpcio.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mesagxilo.povas_forigi_uzanto_opcio')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mesagxilo.povas_shanghi_uzanto_opcio'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                opcio.privata = kwargs.get('privata', opcio.privata)
                                opcio.grupo = kwargs.get('grupo', opcio.grupo)
                                opcio.forigo = kwargs.get('forigo', opcio.forigo)
                                opcio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                opcio.arkivo = kwargs.get('arkivo', opcio.arkivo)
                                opcio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                opcio.publikigo = kwargs.get('publikigo', opcio.publikigo)
                                opcio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                opcio.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MesagxiloUzantoOpcio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloUzantoOpcio(status=status, message=message, opcio=opcio)

# Модель настроек пользователей, относящаяся ко всем чатам
class RedaktuMesagxiloUzantoAliro(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    aliro = graphene.Field(MesagxiloUzantoAliroNode, description=_('Созданный/изменённый чат'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        opcio_uuid = graphene.String(description=_('UUID к каким настройкам пользователя относится'))
        grupo = graphene.Int(description=_('Разрешения добавления в групповые чаты'))
        uzantoj_id = graphene.List(graphene.Int, required=True,
                description=_('список id пользователей'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        aliro = None
        opcio = None
        uzantoj = []
        mesagxo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('mesagxilo.povas_krei_uzanto_aliro'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('opcio_uuid', False)):
                                try:
                                    opcio = MesagxiloUzantoOpcio.objects.get(uuid=kwargs.get('opcio_uuid'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except MesagxiloUzantoOpcio.DoesNotExist:
                                    message = _('Неверный пользователь конференции')
                            else:
                                message = _('Не указан UUID настройки пользователя')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if kwargs.get('uzantoj_id', False):
                                for uzantojn in kwargs.get('uzantoj_id'):
                                    try:
                                        uzantoj.append(Uzanto.objects.get(id=uzantojn, is_active=True,
                                                konfirmita=True))
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный id пользователя:'+uzantojn)

                        if not message:
                            aliro = MesagxiloUzantoAliro.objects.create(
                                forigo=False,
                                opcio = opcio,
                                grupo=kwargs.get('grupo', 0),
                            )

                            aliro.uzantoj.set(uzantoj)

                            aliro.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) 
                            or kwargs.get('uzantoj_id', False) or kwargs.get('grupo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('uzantoj_id', False):
                            for uzantojn in kwargs.get('uzantoj_id'):
                                try:
                                    uzantoj.append(Uzanto.objects.get(id=uzantojn, is_active=True,
                                            konfirmita=True))
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный id пользователя:'+uzantojn)

                    if not message:
                        # Ищем запись для изменения
                        try:
                            aliro = MesagxiloUzantoAliro.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mesagxilo.povas_forigi_uzanto_aliro')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mesagxilo.povas_shanghi_uzanto_aliro'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                aliro.grupo = kwargs.get('grupo', aliro.grupo)
                                aliro.forigo = kwargs.get('forigo', aliro.forigo)
                                aliro.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None

                                if uzantoj:
                                    aliro.uzantoj.set(uzantoj)
                                aliro.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MesagxiloUzantoAliro.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloUzantoAliro(status=status, message=message, aliro=aliro)

# Сохраняем сообщение
def Mesagxo(mesagxo, teksto, info, **kwargs):

    def eventoj():
        # Отправить сообщение о новой записи в чате всем остальным участникам чата
        sciigi_mesagxilo.delay(mesagxo.posedanto.uuid, mesagxo.babilejo.uuid)
        MesagxiloEventoj.publikigi(mesagxo.babilejo, mesagxo)

    update_fields = ['teksto']
    set_enhavo(mesagxo.teksto, teksto, info.context.LANGUAGE_CODE)
    mesagxo.save(update_fields=update_fields)

    # добавляем всем подписчикам чата сообщения им
    try:
        uzantoj = MesagxiloPartoprenanto.objects.filter(babilejo=mesagxo.babilejo, forigo=False, 
                                                                    arkivo=False, publikigo=True)
        for uz in uzantoj:
            mesagxouzanto = MesagxiloMesagxoUzanto.objects.create(
                                        forigo=False,
                                        posedanto=uz.partoprenanto,
                                        mesagxo=mesagxo
                                    )
            mesagxouzanto.save()
    except MesagxiloPartoprenanto.DoesNotExist:
        pass

    transaction.on_commit(eventoj)

    return 'сохранили сообщение'

# Модель чат
class RedaktuMesagxiloBabilejo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    babilejo = graphene.Field(MesagxiloBabilejoNode, description=_('Созданный/изменённый чат'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название чата'))
        mesagxo_uuid = graphene.String(description=_('UUID закреплённого сообщения'))
        personal = graphene.Boolean(description=_('Признак личной переписки'))
        opcio_partoprenantoj = graphene.Boolean(description=_('Могут приглашать участников в беседу'))
        opcio_privata = graphene.Boolean(description=_('Если групповой, то делится на публичный (открыт для входа и поиска) и приватный (закрыт для посторонних)'))
        opcio_info = graphene.Boolean(description=_('Могут редактировать информацию беседы'))
        opcio_kovris = graphene.Boolean(description=_('Могут менять закреплённое сообщение'))
        opcio_aldoni = graphene.Boolean(description=_('Могут добавлять администраторов'))
        opcio_del = graphene.Boolean(description=_('Запрет на полное удаление сообщения'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        bildo_info = graphene.String(description=_('информационное изображение'))
        uzantoj_id = graphene.List(graphene.Int, description=_('список участников чата, кроме создателя'))
        sciigoj = graphene.List(MesSciigoj, description=_('Список подписок'))
        sciigi = graphene.Boolean(description=_('Оповещать о новых сообщениях из данного чата'))
        teksto = graphene.String(description=_('Текст первого сообщения'))
        # при создании персонального чата teksto - обязательное поле

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        babilejo = None
        tipo = None
        bildo_info = None
        mesagxo = None
        komunumo = None
        uuid = kwargs.get('uuid', False)
        errors = list()
        uzantoj = set()
        novo_sciigoj = []
        uzanto = info.context.user
        uzantoj.add(uzanto)

        if uzanto.is_authenticated:
            with transaction.atomic():
                # защита от создания второго персонального чата между теми-же самыми участниками
                if (not uuid)and(kwargs.get('personal', True)):
                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        uz2 = None
                        if (kwargs.get('uzantoj_id', False)):
                            for uz in kwargs.get('uzantoj_id'):
                                try:
                                    uz=Uzanto.objects.get(id=uz, is_active=True,
                                            konfirmita=True)
                                    if uz!=uzanto:# достаём второго участника беседы
                                        uz2 = uz
                                    uzantoj.add(uz)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь чата')

                    if (len(uzantoj)>2 and kwargs.get('personal',True)):
                        message=_('Для личного чата членов не более двух')
                    else:
                        babil = MesagxiloBabilejo.objects.filter(mesagxilopartoprenanto__partoprenanto=uzanto,
                                    personal=True, forigo=False,
                                    arkivo=False, publikigo=True
                                    ).filter(mesagxilopartoprenanto__partoprenanto=uz2).values('uuid')
                        for b in babil:
                            uuid = b.get('uuid')
                            # запись найдена нужно добавить только текст в чат
                

                # Создаём новую запись
                if not uuid:
                    if (uzanto.has_perm('mesagxilo.povas_krei_babilejo_modelo')) \
                            or (kwargs.get('personal', True)):
                            #если создаём персональный чат, то можно

                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not message:
                            if not (kwargs.get('nomo', False)):
                                if not kwargs.get('personal', True):
                                    message = '{} "{}"'.format(_('Не заполнено обязательное поле для группового чата'), 'nomo')
                            elif kwargs.get('personal', True):
                                    message = '{} "{}"'.format(_('Должно быть пустым поле для персонального чата'), 'nomo')

                        if not message:
                            if not (kwargs.get('teksto', False)):
                                if kwargs.get('personal', True):
                                    message = '{} "{}"'.format(_('Не заполнено обязательное поле для персонального чата'), 'teksto')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('mesagxo_uuid', False)):
                                try:
                                    mesagxo = MesagxiloMesagxo.objects.get(uuid=kwargs.get('mesagxo_uuid'),
                                                                           forigo=False,
                                                                           arkivo=False, publikigo=True)
                                except MesagxiloMesagxo.DoesNotExist:
                                    message = _('Неверное закреплённое сообщение')

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if (kwargs.get('uzantoj_id', False)):
                                for uz in kwargs.get('uzantoj_id'):
                                    try:
                                        uzantoj.add(Uzanto.objects.get(id=uz, is_active=True,
                                                konfirmita=True))
                                    except Uzanto.DoesNotExist:
                                        message = _('Неверный пользователь чата')

                        # проверяем кол-во членов если личный чат
                        if not message:
                            if (len(uzantoj)>2 and kwargs.get('personal',True)):
                                message=_('Для личного чата членов не более двух')

                        if not message:
                            if kwargs.get('sciigoj', False):
                                for sciigo in kwargs.get('sciigoj'):
                                    try:
                                        novo_sciigoj.append(MesSciigoj(
                                            sciigo=InformilojSciigoTipo.objects.get(kodo=sciigo.sciigo, forigo=False)))
                                    except InformilojSciigoTipo.DoesNotExist:
                                        message = _('Неверный возможный вариант подписки' + sciigo)

                        # Проверяем присланное изображения
                        if not (message):
                            if 'bildo_info' in info.context.FILES:
                                file = get_web_image(info.context.FILES['bildo_info'])
                                file_prop = get_image_properties(file)

                                if file_prop:
                                    if file.size > MAX_FILE_SIZE:
                                        errors.append(
                                            ErrorNode(field='bildo_info',
                                                      message=_('Размер файла изображения больше 10 МБайт'))
                                        )
                                    elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                        errors.append(
                                            ErrorNode(field='bildo_info',
                                                      message=_(
                                                          'Неверный формат изображения, разрешены: JPEG, GIF, PNG'))
                                        )
                                    else:
                                        bildo_info = info.context.FILES['bildo_info']

                                else:
                                    errors.append(ErrorNode(field='bildo_info',
                                                            message=_('Файл не является изображением')))

                        if not message:
                            babilejo = MesagxiloBabilejo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                personal=kwargs.get('personal', True),
                                bildo_info=bildo_info,
                                opcio_partoprenantoj=kwargs.get('opcio_partoprenantoj', True),
                                opcio_privata=kwargs.get('opcio_privata', True),
                                opcio_info=kwargs.get('opcio_info', True),
                                opcio_kovris=kwargs.get('opcio_kovris', True),
                                opcio_aldoni=kwargs.get('opcio_aldoni', False),
                                opcio_del=kwargs.get('opcio_del', False),
                                mesagxo=mesagxo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(babilejo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            babilejo.save()


                            for uz in uzantoj:
                                partoprenanto = MesagxiloPartoprenanto.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    partoprenanto=uz,
                                    babilejo=babilejo,
                                    is_admin=False,
                                    sciigi=kwargs.get('sciigi', False),
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )
                                if uz==uzanto:
                                    partoprenanto.is_admin=True

                                partoprenanto.save()

                                for sciigon in novo_sciigoj:
                                    sci = MesagxiloSciigoj.objects.create(
                                        partoprenanto=partoprenanto,
                                        sciigo=InformilojSciigoTipo.objects.get(kodo=sciigon.sciigo, forigo=False),
                                        sciigi=sciigon.sciigi if sciigon.sciigi else False,
                                        sciigi_dato=sciigon.sciigi_dato if sciigon.sciigi_dato else None
                                    )
                                    partoprenanto.sciigoj.all()

                            # добавляем сообщение в созданный чат
                            if (kwargs.get('teksto', False)):
                                mesagxo = MesagxiloMesagxo.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    posedanto=uzanto,
                                    babilejo=babilejo,
                                    komunumo=komunumo,
                                    # investoj=kwargs.get('investoj', False),
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )

                                Mesagxo(mesagxo, kwargs.get('teksto'), info)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or 'bildo_info' in info.context.FILES
                            or kwargs.get('opcio_privata', False) 
                            or kwargs.get('opcio_partoprenantoj', False) or kwargs.get('opcio_info', False)
                            or kwargs.get('opcio_kovris', False) or kwargs.get('opcio_aldoni', False)
                            or kwargs.get('mesagxo_uuid', False) or kwargs.get('opcio_del', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'mesagxo_uuid' in kwargs and not message:
                        try:
                            mesagxo = MesagxiloMesagxo.objects.get(uuid=kwargs.get('mesagxo_uuid'), forigo=False,
                                                                   arkivo=False, publikigo=True)
                        except MesagxiloMesagxo.DoesNotExist:
                            message = _('Неверное закреплённое сообщение')

                    # Проверяем присланное изображения
                    if not (message):
                        if 'bildo_info' in info.context.FILES:
                            file = get_web_image(info.context.FILES['bildo_info'])
                            file_prop = get_image_properties(file)

                            if file_prop:
                                if file.size > MAX_FILE_SIZE:
                                    errors.append(
                                        ErrorNode(field='bildo_info',
                                                  message=_('Размер файла изображения больше 10 МБайт'))
                                    )
                                elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                    errors.append(
                                        ErrorNode(field='bildo_info',
                                                  message=_('Неверный формат изображения, разрешены: JPEG, GIF, PNG'))
                                    )
                                else:
                                    bildo_info = info.context.FILES['bildo_info']

                            else:
                                errors.append(ErrorNode(field='avataro',
                                                        message=_('Файл не является изображением')))

                    if not message:
                        # Ищем запись для изменения
                        try:
                            babilejo = MesagxiloBabilejo.objects.get(uuid=uuid, forigo=False)
                            if (not uzanto.has_perm('mesagxilo.povas_forigi_babilejo_modelo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mesagxilo.povas_shanghi_babilejo_modelo'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                babilejo.opcio_info = kwargs.get('opcio_info', babilejo.opcio_info)
                                babilejo.opcio_kovris = kwargs.get('opcio_kovris', babilejo.opcio_kovris)
                                babilejo.opcio_aldoni = kwargs.get('opcio_aldoni', babilejo.opcio_aldoni)
                                babilejo.opcio_del = kwargs.get('opcio_del', babilejo.opcio_del)
                                babilejo.mesagxo = mesagxo or babilejo.mesagxo
                                babilejo.bildo_info = bildo_info or babilejo.bildo_info
                                babilejo.opcio_partoprenantoj = kwargs.get('opcio_partoprenantoj',
                                                                           babilejo.opcio_partoprenantoj)
                                babilejo.opcio_privata = kwargs.get('opcio_privata',
                                                                           babilejo.opcio_privata)
                                babilejo.forigo = kwargs.get('forigo', babilejo.forigo)
                                babilejo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                babilejo.arkivo = kwargs.get('arkivo', babilejo.arkivo)
                                babilejo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                babilejo.publikigo = kwargs.get('publikigo', babilejo.publikigo)
                                babilejo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(babilejo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                babilejo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MesagxiloBabilejo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloBabilejo(status=status, message=message, babilejo=babilejo)

# Участники чатов
class RedaktuMesagxiloPartoprenanto(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    partoprenanto = graphene.Field(MesagxiloPartoprenantoNode,
                                   description=_('Созданная/изменённая запись степени награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        partoprenanto_id = graphene.String(description=_('Код участника чата'))
        babilejo_id = graphene.String(description=_('Код чата'))
        lasta_mesagxo_uuid = graphene.String(description=_('UUID последнего прочитанного сообщения'))
        is_admin = graphene.Boolean(description=_('Наличие прав админа чата'))
        sciigoj = graphene.List(MesSciigoj, description=_('Список подписок'))
        sciigi = graphene.Boolean(description=_('Оповещать о новых сообщениях из данного чата'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        partoprenanto = None
        lasta_mesagxo = None
        babilejo = None
        partoprenanto_user = None
        novo_sciigoj = []
        uzantos = info.context.user

        if uzantos.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzantos.has_perm('mesagxilo.povas_krei_partoprenanto'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('partoprenanto_id', False)):
                                try:
                                    partoprenanto_user = Uzanto.objects.get(id=kwargs.get('partoprenanto_id'),
                                                                            is_active=True,
                                                                            konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный участник чата')
                            else:
                                message = _('Не указан участник чата')

                        if not message:
                            if (kwargs.get('babilejo_id', False)):
                                try:
                                    babilejo = MesagxiloBabilejo.objects.get(id=kwargs.get('babilejo_id'), forigo=False,
                                                                             arkivo=False, publikigo=True)
                                except MesagxiloBabilejo.DoesNotExist:
                                    message = _('Неверный код чата')
                            else:
                                message = _('Не указан код чата')

                        if not message:
                            if (kwargs.get('lasta_mesagxo_uuid', False)):
                                try:
                                    lasta_mesagxo = MesagxiloMesagxo.objects.get(uuid=kwargs.get('lasta_mesagxo_uuid'),
                                                                                 forigo=False,
                                                                                 arkivo=False, publikigo=True)
                                except MesagxiloMesagxo.DoesNotExist:
                                    message = _('Неверное закреплённое сообщение')

                        # проверяем уникальность пары пользователя и чата
                        if not message:
                            try:
                                MesagxiloPartoprenanto.objects.get(partoprenanto=partoprenanto_user.id,
                                                                   babilejo=babilejo.uuid)
                                message = _('Данный пользователь уже подписан на данный чат')
                            except MesagxiloPartoprenanto.DoesNotExist:
                                pass

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if kwargs.get('sciigoj', False):
                                for sciigo in kwargs.get('sciigoj'):
                                    try:
                                        novo_sciigoj.append(MesSciigoj(
                                            sciigo=InformilojSciigoTipo.objects.get(kodo=sciigo.sciigo, forigo=False)))
                                    except InformilojSciigoTipo.DoesNotExist:
                                        message = _('Неверный возможный вариант подписки' + sciigo)

                        if not message:
                            partoprenanto = MesagxiloPartoprenanto.objects.create(
                                forigo=False,
                                arkivo=False,
                                partoprenanto=partoprenanto_user,
                                babilejo=babilejo,
                                lasta_mesagxo=lasta_mesagxo,
                                is_admin=kwargs.get('is_admin', False),
                                sciigi=kwargs.get('sciigi', False),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            partoprenanto.save()

                            for sciigon in novo_sciigoj:
                                sci = MesagxiloSciigoj.objects.create(
                                    partoprenanto=partoprenanto,
                                    sciigo=InformilojSciigoTipo.objects.get(kodo=sciigon.sciigo, forigo=False),
                                    sciigi=sciigon.sciigi if sciigon.sciigi else False,
                                    sciigi_dato=sciigon.sciigi_dato if sciigon.sciigi_dato else None
                                )
                                partoprenanto.sciigoj.all()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('partoprenanto_id', False) or kwargs.get('babilejo_id', False)
                            or kwargs.get('is_admin', False) or kwargs.get('sciigi', False)
                            or kwargs.get('sciigoj', False)
                            or kwargs.get('lasta_mesagxo_uuid', False) or kwargs.get('arkivo', False)
                            or kwargs.get('forigo', False) or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    # Проверяем наличие записи с таким кодом
                    if not message:
                        if (kwargs.get('partoprenanto_id', False)):
                            try:
                                partoprenanto_user = Uzanto.objects.get(id=kwargs.get('partoprenanto_id'),
                                                                        is_active=True,
                                                                        konfirmita=True)
                            except Uzanto.DoesNotExist:
                                message = _('Неверный участник чата')

                    if not message:
                        if (kwargs.get('babilejo_id', False)):
                            try:
                                babilejo = MesagxiloBabilejo.objects.get(id=kwargs.get('babilejo_id'), forigo=False,
                                                                         arkivo=False, publikigo=True)
                            except MesagxiloBabilejo.DoesNotExist:
                                message = _('Неверный код чата')

                    if not message:
                        if (kwargs.get('lasta_mesagxo_uuid', False)):
                            try:
                                lasta_mesagxo = MesagxiloMesagxo.objects.get(uuid=kwargs.get('lasta_mesagxo_uuid'),
                                                                             forigo=False,
                                                                             arkivo=False, publikigo=True)
                            except MesagxiloMesagxo.DoesNotExist:
                                message = _('Неверное закреплённое сообщение')

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('sciigoj', False):
                            for sciigo in kwargs.get('sciigoj'):
                                try:
                                    novo_sciigoj.append(MesSciigoj(
                                        sciigo=InformilojSciigoTipo.objects.get(kodo=sciigo.sciigo, forigo=False)))
                                except InformilojSciigoTipo.DoesNotExist:
                                    message = _('Неверный возможный вариант подписки' + sciigo)

                    if not message:
                        # Ищем запись для изменения
                        try:
                            partoprenanto = MesagxiloPartoprenanto.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzantos.has_perm('mesagxilo.povas_forigi_partoprenanto')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzantos.has_perm('mesagxilo.povas_shanghi_partoprenanto'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                partoprenanto.is_admin = kwargs.get('is_admin', partoprenanto.is_admin)
                                partoprenanto.sciigi = kwargs.get('sciigi', partoprenanto.sciigi)
                                partoprenanto.partoprenanto = partoprenanto_user or partoprenanto.partoprenanto
                                partoprenanto.babilejo = babilejo or partoprenanto.babilejo
                                partoprenanto.lasta_mesagxo = lasta_mesagxo or partoprenanto.lasta_mesagxo
                                partoprenanto.forigo = kwargs.get('forigo', partoprenanto.forigo)
                                partoprenanto.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                partoprenanto.arkivo = kwargs.get('arkivo', partoprenanto.arkivo)
                                partoprenanto.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                partoprenanto.publikigo = kwargs.get('publikigo', partoprenanto.publikigo)
                                partoprenanto.publikiga_dato = timezone.now() if kwargs.get('publikigo',
                                                                                            False) else None

                                partoprenanto.sciigoj.clear()
                                if kwargs.get('sciigoj', False):
                                    for sciigon in kwargs.get('sciigoj'):
                                        # редактирование!!!
                                        sci = MesagxiloSciigoj.objects.create(
                                            partoprenanto=partoprenanto,
                                            sciigo=InformilojSciigoTipo.objects.get(kodo=sciigon.sciigo, forigo=False),
                                            sciigi=sciigon.sciigi if sciigon.sciigi else False,
                                            sciigi_dato=sciigon.sciigi_dato if sciigon.sciigi_dato else None
                                        )
                                    partoprenanto.sciigoj.all()

                                partoprenanto.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except MesagxiloPartoprenanto.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloPartoprenanto(status=status, message=message, partoprenanto=partoprenanto)

# Сообщения чатов
class RedaktuMesagxiloMesagxo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    mesagxo = graphene.Field(MesagxiloMesagxoNode, description=_('Созданное/изменённое сообщение'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        teksto = graphene.String(description=_('Текст сообщения'))
        enskribo_uuid = graphene.String(description=_('UUID записи стены сообщества'))
        enskribo_uzanto_uuid = graphene.String(description=_('UUID записи стены пользователя'))
        mesagxo_uuid = graphene.String(description=_('UUID комментируемого сообщения'))
        babilejo_id = graphene.Int(description=_('Код чата'))
        investoj = graphene.Boolean(description=_('Наличие вложения'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        forigo_all = graphene.Boolean(description=_('Признак удаления записи у всех участников'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        komunumo_id = graphene.Int(description=_('Код сообщества, если сообщение от сообщества'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        mesagxo = None
        mesagxoj = None
        komunumo = None
        babilejo = None
        enskribo = None
        enskribo_uzanto = None
        uzanto = Uzanto.objects.get(id=info.context.user.id)
        partoprenanto = None #является ли участником чата

        def eventoj():
            MesagxiloEventoj.redakti(mesagxo.babilejo, mesagxo)

        if uzanto.is_authenticated:
            with transaction.atomic():
                if not message:
                    if kwargs.get('babilejo_id', False):
                        try:
                            babilejo = MesagxiloBabilejo.objects.get(id=kwargs.get('babilejo_id'), forigo=False,
                                                                         arkivo=False, publikigo=True)
                            try:
                                partoprenanto = MesagxiloPartoprenanto.objects.get(babilejo_id=babilejo,
                                    partoprenanto=uzanto, forigo=False, arkivo=False, publikigo=True)
                            except MesagxiloPartoprenanto.DoesNotExist:
                                partoprenanto = None
                        except MesagxiloBabilejo.DoesNotExist:
                            message = _('Неверный код чата')

                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if (uzanto.has_perm('mesagxilo.povas_krei_mesagxo'))or\
                            (partoprenanto is not None):
                            #если сообщение от участника чата
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not kwargs.get('babilejo_id', False):
                            message = _('Поле babilejo_id обязательно для заполнения')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if kwargs.get('mesagxo_uuid', False):
                                try:
                                    mesagxoj = MesagxiloMesagxo.objects.get(uuid=kwargs.get('mesagxo_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except MesagxiloMesagxo.DoesNotExist:
                                    message = _('Неверное комментируемое сообщение')

                        if not message:
                            if kwargs.get('komunumo_id', False):
                                try:
                                    komunumo = Komunumo.objects.get(id=kwargs.get('komunumo_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                                except Komunumo.DoesNotExist:
                                    message = _('Неверный код сообщества')

                        if not message:
                            if kwargs.get('enskribo_uuid', False):
                                try:
                                    enskribo = MuroEnskribo.objects.get(uuid=kwargs.get('enskribo_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except MuroEnskribo.DoesNotExist:
                                    message = _('Неверная запись на стене сообщества')

                        if not message:
                            if kwargs.get('enskribo_uzanto_uuid', False):
                                try:
                                    enskribo_uzanto = MurojUzantoEnskribo.objects.get(uuid=kwargs.get('enskribo_uzanto_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except MurojUzantoEnskribo.DoesNotExist:
                                    message = _('Неверная запись на стене пользователя')

                        if not message:
                            mesagxo = MesagxiloMesagxo.objects.create(
                                forigo=False,
                                arkivo=False,
                                mesagxo=mesagxoj,
                                posedanto=uzanto,
                                enskribo=enskribo,
                                enskribo_uzanto=enskribo_uzanto,
                                babilejo=babilejo,
                                komunumo=komunumo,
                                investoj=kwargs.get('investoj', False),
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            Mesagxo(mesagxo, kwargs.get('teksto'), info)

                            status = True
                            message = _('Запись создана')

                    else:
                        if not message:
                            message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('mesagxo_uuid', False) or kwargs.get('babilejo_id', False)
                            or kwargs.get('investoj', False) or kwargs.get('teksto', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('enskribo', False) or kwargs.get('enskribo_uzanto', False)
                            or kwargs.get('publikigo', False) or kwargs.get('komunumo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    # Проверяем наличие записи с таким кодом
                    if not message:
                        if kwargs.get('mesagxo_uuid', False):
                            try:
                                mesagxoj = MesagxiloMesagxo.objects.get(id=kwargs.get('mesagxo_uuid'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                            except MesagxiloMesagxo.DoesNotExist:
                                message = _('Неверный участник чата')

                    if not message:
                        if kwargs.get('komunumo_id', False):
                            try:
                                komunumo = Komunumo.objects.get(id=kwargs.get('komunumo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                            except Komunumo.DoesNotExist:
                                message = _('Неверный код сообщества')

                    if not message:
                        if kwargs.get('enskribo_uuid', False):
                            try:
                                enskribo = MuroEnskribo.objects.get(uuid=kwargs.get('enskribo_uuid'),
                                                                        forigo=False,
                                                                        arkivo=False, publikigo=True)
                            except MuroEnskribo.DoesNotExist:
                                message = _('Неверная запись на стене сообщества')

                    if not message:
                        if kwargs.get('enskribo_uzanto_uuid', False):
                            try:
                                enskribo_uzanto = MurojUzantoEnskribo.objects.get(uuid=kwargs.get('enskribo_uzanto_uuid'),
                                                                        forigo=False,
                                                                        arkivo=False, publikigo=True)
                            except MurojUzantoEnskribo.DoesNotExist:
                                message = _('Неверная запись на стене пользователя')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            mesagxo = MesagxiloMesagxo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('mesagxilo.povas_forigi_mesagxo')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('mesagxilo.povas_shanghi_mesagxo'):
                                message = _('Недостаточно прав для изменения')
                            elif mesagxo.posedanto!=uzanto:#сам не является создателем сообщения
                                message = _('Не является создателем сообщения')
                            else:
                                update_fields = [
                                    'mesagxo', 'babilejo', 'komunumo', 'enskribo_uzanto', 'enskribo',
                                    'investoj', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato',
                                ]

                                mesagxo.investoj = kwargs.get('investoj', mesagxo.investoj)
                                mesagxo.mesagxo = mesagxoj or mesagxo.mesagxo
                                mesagxo.babilejo = babilejo or mesagxo.babilejo
                                mesagxo.komunumo = komunumo or mesagxo.komunumo
                                mesagxo.enskribo = enskribo or mesagxo.enskribo
                                mesagxo.enskribo_uzanto = enskribo_uzanto or mesagxo.enskribo_uzanto

                                # проверить если сам создатель - только тогда можно MesagxoUzanto ставить на удаление
                                if kwargs.get('forigo'):#пометка на удаление
                                    if mesagxo.posedanto==uzanto:#сам является создателем сообщения
                                        if kwargs.get('forigo_all',False):#удалить сообщения у всех
                                            # сюда добавить проверку у группы - можно ли удалять
                                            MesagxiloMesagxoUzanto.objects.filter(mesagxo=mesagxo,
                                                                forigo=False).update(forigo=True,
                                                                foriga_dato = timezone.now())
                                            if (mesagxo.babilejo.personal):
                                                #проверяем, есть ли еще записи в чате
                                                if MesagxiloMesagxo.objects.filter(babilejo=mesagxo.babilejo,
                                                        forigo=True).count()==0:
                                                    #удаляем сам чат
                                                    MesagxiloBabilejo.objects.filter(uuid=mesagxo.babilejo.uuid).update(
                                                        forigo=True, foriga_dato = timezone.now())
                                                mesagxo.forigo = kwargs.get('forigo', mesagxo.forigo)
                                                mesagxo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                                update_fields.extend(['forigo', 'foriga_dato'])
                                            elif (not mesagxo.babilejo.opcio_del):
                                                mesagxo.forigo = kwargs.get('forigo', mesagxo.forigo)
                                                mesagxo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                                update_fields.extend(['forigo', 'foriga_dato'])

                                        else: #удаляем только для себя
                                            MesagxiloMesagxoUzanto.objects.filter(mesagxo=mesagxo,
                                                                posedanto=uzanto,
                                                                forigo=False).update(forigo=True,
                                                                foriga_dato = timezone.now())


                                mesagxo.arkivo = kwargs.get('arkivo', mesagxo.arkivo)
                                mesagxo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                mesagxo.publikigo = kwargs.get('publikigo', mesagxo.publikigo)
                                mesagxo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('teksto', False):
                                    set_enhavo(mesagxo.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                    update_fields.append('teksto')

                                mesagxo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')

                                # Оповещаем о редактировании
                                transaction.on_commit(eventoj)

                        except MesagxiloMesagxo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloMesagxo(status=status, message=message, mesagxo=mesagxo)


# вложения к сообщениям чатов
class RedaktuMesagxiloInvestoj(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    investoj = graphene.Field(MesagxiloInvestojNode, description=_('Созданная/изменённая запись степени награды'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        mesagxo_uuid = graphene.String(description=_('UUID комментируемого сообщения'))
        babilejo_id = graphene.String(description=_('Код чата'))
        dosiero = graphene.String(description=_('Присоединенный файл'))
        type_dosiero = graphene.String(
            description=_('Тип присоединённого файла (0 - изображение, 1 - видео, 2 - документ, 3 - аудио)'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        investoj = None
        errors = list()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            # Доступ только для администратора
            if uzanto.is_admin or uzanto.is_superuser:
                with transaction.atomic():
                    # Создаём новую запись
                    if not kwargs.get('uuid', False):
                        if uzanto.has_perm('mesagxilo.povas_krei_investoj'):
                            # Проверяем наличие всех полей
                            if kwargs.get('forigo', False) and not message:
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'),
                                                           'forigo')

                            if kwargs.get('arkivo', False) and not message:
                                message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'),
                                                           'arkivo')

                            # Проверяем наличие записи с таким кодом
                            if not message:
                                if (kwargs.get('mesagxo_uuid', False)):
                                    try:
                                        mesagxoj = MesagxiloMesagxo.objects.get(uuid=kwargs.get('mesagxo_uuid'),
                                                                                forigo=False,
                                                                                arkivo=False, publikigo=True)
                                    except MesagxiloMesagxo.DoesNotExist:
                                        message = _('Неверное комментируемое сообщение')

                            if not message:
                                if (kwargs.get('babilejo_id', False)):
                                    try:
                                        babilejo = MesagxiloBabilejo.objects.get(id=kwargs.get('babilejo_id'),
                                                                                 forigo=False,
                                                                                 arkivo=False, publikigo=True)
                                    except MesagxiloBabilejo.DoesNotExist:
                                        message = _('Неверный код чата')

                            # Проверяем присланный файл
                            if not (message):
                                if 'dosiero' in info.context.FILES:
                                    if kwargs.get('type_dosiero', -1) == 0:
                                        file = get_web_image(info.context.FILES['dosiero'])
                                        file_prop = get_image_properties(file)
                                        if file_prop:
                                            if file.size > MAX_FILE_SIZE:
                                                errors.append(
                                                    ErrorNode(field='dosiero',
                                                              message=_('Размер файла изображения больше 10 МБайт'))
                                                )
                                            elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                                errors.append(
                                                    ErrorNode(field='dosiero',
                                                              message=_(
                                                                  'Неверный формат файла, разрешены: JPEG, GIF, PNG'))
                                                )
                                            else:
                                                dosiero = info.context.FILES['dosiero']
                                        else:
                                            errors.append(ErrorNode(field='dosiero',
                                                                    message=_('Файл не является разрешенным')))
                                    else:
                                        errors.append(ErrorNode(field='dosiero',
                                                                message=_('Файл не является разрешенным')))

                            if not message:
                                investoj = MesagxiloInvestoj.objects.create(
                                    forigo=False,
                                    arkivo=False,
                                    mesagxo=mesagxoj,
                                    babilejo=babilejo,
                                    dosiero=dosiero,
                                    type_dosiero=kwargs.get('type_dosiero', False),
                                    publikigo=kwargs.get('publikigo', False),
                                    publikiga_dato=timezone.now()
                                )

                                investoj.save()

                                status = True
                                message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')
                    else:
                        # Изменяем запись
                        if not (kwargs.get('mesagxo_uuid', False) or kwargs.get('babilejo_id', False)
                                or kwargs.get('type_dosiero', False) or 'dosiero' in info.context.FILES
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                            message = _('Не задано ни одно поле для изменения')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('mesagxo_uuid', False)):
                                try:
                                    mesagxoj = MesagxiloMesagxo.objects.get(id=kwargs.get('mesagxo_uuid'), forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except MesagxiloMesagxo.DoesNotExist:
                                    message = _('Неверный участник чата')

                        if not message:
                            if (kwargs.get('babilejo_id', False)):
                                try:
                                    babilejo = MesagxiloBabilejo.objects.get(id=kwargs.get('babilejo_id'), forigo=False,
                                                                             arkivo=False, publikigo=True)
                                except MesagxiloBabilejo.DoesNotExist:
                                    message = _('Неверный код чата')

                        # Проверяем присланный файл
                        if not (message):
                            if 'dosiero' in info.context.FILES:
                                if kwargs.get('type_dosiero', -1) == 0:
                                    file = get_web_image(info.context.FILES['dosiero'])
                                    file_prop = get_image_properties(file)
                                    if file_prop:
                                        if file.size > MAX_FILE_SIZE:
                                            errors.append(
                                                ErrorNode(field='dosiero',
                                                          message=_('Размер файла изображения больше 10 МБайт'))
                                            )
                                        elif file_prop.get('format') not in ['JPEG', 'PNG', 'GIF']:
                                            errors.append(
                                                ErrorNode(field='dosiero',
                                                          message=_('Неверный формат файла, разрешены: JPEG, GIF, PNG'))
                                            )
                                        else:
                                            dosiero = info.context.FILES['dosiero']
                                    else:
                                        errors.append(ErrorNode(field='dosiero',
                                                                message=_('Файл не является разрешенным')))
                                else:
                                    errors.append(ErrorNode(field='dosiero',
                                                            message=_('Файл не является разрешенным')))

                        if not message:
                            # Ищем запись для изменения
                            try:
                                investoj = MesagxiloInvestoj.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                                if (not uzanto.has_perm('mesagxilo.povas_forigi_investoj')
                                        and kwargs.get('forigo', False)):
                                    message = _('Недостаточно прав для удаления')
                                elif not uzanto.has_perm('mesagxilo.povas_shanghi_investoj'):
                                    message = _('Недостаточно прав для изменения')
                                else:
                                    investoj.type_dosiero = kwargs.get('type_dosiero', investoj.type_dosiero)
                                    investoj.mesagxo = mesagxoj or investoj.mesagxo
                                    investoj.babilejo = babilejo or investoj.babilejo
                                    investoj.dosiero = dosiero or investoj.dosiero
                                    investoj.forigo = kwargs.get('forigo', investoj.forigo)
                                    investoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                    investoj.arkivo = kwargs.get('arkivo', investoj.arkivo)
                                    investoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                    investoj.publikigo = kwargs.get('publikigo', investoj.publikigo)
                                    investoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                    if kwargs.get('teksto', False):
                                        set_enhavo(investoj.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)

                                        investoj.save()
                                    status = True
                                    message = _('Запись успешно изменена')
                            except MesagxiloInvestoj.DoesNotExist:
                                message = _('Запись не найдена')
            else:
                message = _('Недостаточно прав')
        else:
            message = _('Требуется авторизация')

        return RedaktuMesagxiloInvestoj(status=status, message=message, investoj=investoj)


class MesagxiloMutations(graphene.ObjectType):
    redaktu_mesagxilo_uzanto_opcio = RedaktuMesagxiloUzantoOpcio.Field(
        description=_('''Создаёт или редактирует настройки пользователей''')
    )
    redaktu_mesagxilo_uzanto_aliro = RedaktuMesagxiloUzantoAliro.Field(
        description=_('''Создаёт или редактирует доступы пользователей''')
    )
    redaktu_mesagxilo_babilejo = RedaktuMesagxiloBabilejo.Field(
        description=_('''Создаёт или редактирует модель чата''')
    )
    redaktu_mesagxilo_partoprenanto = RedaktuMesagxiloPartoprenanto.Field(
        description=_('''Создаёт или редактирует участников чатов''')
    )
    redaktu_mesagxilo_mesagxo = RedaktuMesagxiloMesagxo.Field(
        description=_('''Создаёт или редактирует сообщения чатов''')
    )
    redaktu_mesagxilo_investoj = RedaktuMesagxiloInvestoj.Field(
        description=_('''Создаёт или редактирует вложения к сообщениям чатов''')
    )
