"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from .models import *
from siriuso.views import SiriusoTemplateView


# Страницы
class Paxo(SiriusoTemplateView):

    template_name = 'enhavo/paxo.html'
    mobile_template_name = 'enhavo/portebla/t_paxo.html'

    http_method_names = ['get']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        queryset_paxo = EnhavoEnhavo.objects.values('uuid', 'nomo__enhavo', 'teksto__enhavo', 'id')

        paxo = get_object_or_404(queryset_paxo, id=kwargs['paxo_id'], forigo=False)

        if paxo['teksto__enhavo'] is not None:
            enhavo = {
                'nomo': paxo['nomo__enhavo'],
                'teksto': paxo['teksto__enhavo'],
            }
        else:
            enhavo = None

        context.update({'paxo': paxo,
               'enhavo': enhavo})

        return context
