"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.db import transaction  # создание и изменения будем делать в блокирующейтранзакции
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo
from .schema import *
from ..models import *


# Модель типов звёздных систем
class RedaktuKosmoStelsistemoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kosmo_stelsistemoj_tipoj = graphene.Field(KosmoStelsistemoTipoNode, 
        description=_('Созданный/изменённый тип звёздных систем'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kosmo_stelsistemoj_tipoj = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kosmo.povas_krei_kosmo_stelsistemoj_tipoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        if not message:
                            kosmo_stelsistemoj_tipoj = KosmoStelsistemoTipo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kosmo_stelsistemoj_tipoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kosmo_stelsistemoj_tipoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                else:
                                    kosmo_stelsistemoj_tipoj.realeco.clear()

                            kosmo_stelsistemoj_tipoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kosmo_stelsistemoj_tipoj = KosmoStelsistemoTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kosmo.povas_forigi_kosmo_stelsistemoj_tipoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kosmo.povas_shanghi_kosmo_stelsistemoj_tipoj'):
                                message = _('Недостаточно прав для изменения')

                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            if not message:

                                kosmo_stelsistemoj_tipoj.forigo = kwargs.get('forigo', kosmo_stelsistemoj_tipoj.forigo)
                                kosmo_stelsistemoj_tipoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kosmo_stelsistemoj_tipoj.arkivo = kwargs.get('arkivo', kosmo_stelsistemoj_tipoj.arkivo)
                                kosmo_stelsistemoj_tipoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kosmo_stelsistemoj_tipoj.publikigo = kwargs.get('publikigo', kosmo_stelsistemoj_tipoj.publikigo)
                                kosmo_stelsistemoj_tipoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kosmo_stelsistemoj_tipoj.autoro = uzanto

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kosmo_stelsistemoj_tipoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kosmo_stelsistemoj_tipoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kosmo_stelsistemoj_tipoj.realeco.set(realeco)
                                    else:
                                        kosmo_stelsistemoj_tipoj.realeco.clear()

                                kosmo_stelsistemoj_tipoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KosmoStelsistemoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKosmoStelsistemoTipo(status=status, message=message, kosmo_stelsistemoj_tipoj=kosmo_stelsistemoj_tipoj)


# Модель звёздных систем
class RedaktuKosmoStelsistemo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kosmo_stelsistemoj = graphene.Field(KosmoStelsistemoNode, 
        description=_('Созданный/изменённый модель звёздных систем'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        nomo = graphene.String(description=_('Название'))
        priskribo = graphene.String(description=_('Описание'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        tipo_id = graphene.Int(description=_('Тип звёздной системы Универсо'))
        realeco = graphene.List(graphene.Int,description=_('Список параллельных миров'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kosmo_stelsistemoj = None
        tipo = None
        realeco = Realeco.objects.none()
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kosmo.povas_krei_kosmo_stelsistemoj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        if not message:
                            if not kwargs.get('nomo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'nomo')

                        if not message:
                            if not kwargs.get('priskribo'):
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'priskribo')

                        if not message:
                            if 'realeco' not in kwargs:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'realeco')
                            elif not len(kwargs.get('realeco')):
                                message = '{} "{}"'.format(_('Необходимо указать хотя бы одну группу'),'realeco')
                            else:
                                realeco_id = set(kwargs.get('realeco'))

                                if len(realeco_id):
                                    realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                    dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                    if len(dif):
                                        message='{}: {}'.format(
                                                    _('Указаны несуществующие реальности Универсо'),
                                                    str(dif)
                                        )

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if 'tipo_id' in kwargs:
                                try:
                                    tipo = KosmoStelsistemoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KosmoStelsistemoTipo.DoesNotExist:
                                    message = _('Неверный тип звёздной системы Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')

                        if not message:
                            kosmo_stelsistemoj = KosmoStelsistemo.objects.create(
                                forigo=False,
                                arkivo=False,
                                autoro = uzanto,
                                tipo = tipo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            if (kwargs.get('nomo', False)):
                                set_enhavo(kosmo_stelsistemoj.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            if (kwargs.get('priskribo', False)):
                                set_enhavo(kosmo_stelsistemoj.priskribo, 
                                           kwargs.get('priskribo'), 
                                           info.context.LANGUAGE_CODE)
                            if 'realeco' in kwargs:
                                if realeco:
                                    kosmo_stelsistemoj.realeco.set(realeco)
                                else:
                                    kosmo_stelsistemoj.realeco.clear()

                            kosmo_stelsistemoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('nomo', False) or kwargs.get('priskribo', False)
                            or kwargs.get('tipo_id',False) or kwargs.get('realeco', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kosmo_stelsistemoj = KosmoStelsistemo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kosmo.povas_forigi_kosmo_stelsistemoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kosmo.povas_shanghi_kosmo_stelsistemoj'):
                                message = _('Недостаточно прав для изменения')
                            if not message:
                                if 'realeco' in kwargs:
                                    realeco_id = set(kwargs.get('realeco'))

                                    if len(realeco_id):
                                        realeco = Realeco.objects.filter(id__in=realeco_id, forigo=False, publikigo=True)
                                        dif = set(realeco_id) - set(realeco.values_list('id', flat=True))

                                        if len(dif):
                                            message='{}: {}'.format(
                                                        _('Указаны несуществующие реальности Универсо'),
                                                        str(dif)
                                            )
                            # проверяем наличие записей с таким кодом в списке
                            if not message:
                                if 'tipo_id' in kwargs:
                                    try:
                                        tipo = KosmoStelsistemoTipo.objects.get(id=kwargs.get('tipo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KosmoStelsistemoTipo.DoesNotExist:
                                        message = _('Неверный тип звёздной системы Универсо')
                                else:
                                    message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'tipo_id')
                            if not message:

                                kosmo_stelsistemoj.forigo = kwargs.get('forigo', kosmo_stelsistemoj.forigo)
                                kosmo_stelsistemoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kosmo_stelsistemoj.arkivo = kwargs.get('arkivo', kosmo_stelsistemoj.arkivo)
                                kosmo_stelsistemoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kosmo_stelsistemoj.publikigo = kwargs.get('publikigo', kosmo_stelsistemoj.publikigo)
                                kosmo_stelsistemoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kosmo_stelsistemoj.autoro = uzanto
                                kosmo_stelsistemoj.tipo = tipo if kwargs.get('tipo_id', False) else kosmo_stelsistemoj.tipo

                                if (kwargs.get('nomo', False)):
                                    set_enhavo(kosmo_stelsistemoj.nomo, kwargs.get('nomo'), 
                                               info.context.LANGUAGE_CODE)
                                if (kwargs.get('priskribo', False)):
                                    set_enhavo(kosmo_stelsistemoj.priskribo, kwargs.get('priskribo'), 
                                               info.context.LANGUAGE_CODE)
                                if 'realeco' in kwargs:
                                    if realeco:
                                        kosmo_stelsistemoj.realeco.set(realeco)
                                    else:
                                        kosmo_stelsistemoj.realeco.clear()

                                kosmo_stelsistemoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KosmoStelsistemo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKosmoStelsistemo(status=status, message=message, kosmo_stelsistemoj=kosmo_stelsistemoj)


# Модель кубов (ячейки, чанки) звёздных систем
class RedaktuKosmoStelsistemoKubo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kosmo_stelsistemoj_kuboj = graphene.Field(KosmoStelsistemoKuboNode, 
        description=_('Созданный/изменённый куб (ячейки, чанки) звёздных систем'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))
        stelsistemo_id = graphene.Int(description=_('Код типа ресурсов Универсо'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kosmo_stelsistemoj_kuboj = None
        stelsistemo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('kosmo.povas_krei_kosmo_stelsistemoj_kuboj'):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')
                        
                        # проверяем наличие записей с таким кодом
                        if not message:
                            if 'stelsistemo_id' in kwargs:
                                try:
                                    stelsistemo = KosmoStelsistemo.objects.get(id=kwargs.get('stelsistemo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                                except KosmoStelsistemo.DoesNotExist:
                                    message = _('Неверная звёздная система Универсо')
                            else:
                                message = '{} "{}"'.format(_('Поле обязательно для заполнения'),'stelsistemo_id')

                        if not message:
                            kosmo_stelsistemoj_kuboj = KosmoStelsistemoKubo.objects.create(
                                forigo=False,
                                arkivo=False,
                                stelsistemo = stelsistemo,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            kosmo_stelsistemoj_kuboj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('stelsistemo_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            kosmo_stelsistemoj_kuboj = KosmoStelsistemoKubo.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('kosmo.povas_forigi_kosmo_stelsistemoj_kuboj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('kosmo.povas_shanghi_kosmo_stelsistemoj_kuboj'):
                                message = _('Недостаточно прав для изменения')

                            # проверяем наличие записей с таким кодом
                            if not message:
                                if 'stelsistemo_id' in kwargs:
                                    try:
                                        stelsistemo = KosmoStelsistemo.objects.get(id=kwargs.get('stelsistemo_id'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                                    except KosmoStelsistemo.DoesNotExist:
                                        message = _('Неверная звёздная система Универсо')
                            if not message:

                                kosmo_stelsistemoj_kuboj.forigo = kwargs.get('forigo', kosmo_stelsistemoj_kuboj.forigo)
                                kosmo_stelsistemoj_kuboj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kosmo_stelsistemoj_kuboj.arkivo = kwargs.get('arkivo', kosmo_stelsistemoj_kuboj.arkivo)
                                kosmo_stelsistemoj_kuboj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kosmo_stelsistemoj_kuboj.publikigo = kwargs.get('publikigo', kosmo_stelsistemoj_kuboj.publikigo)
                                kosmo_stelsistemoj_kuboj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kosmo_stelsistemoj_kuboj.stelsistemo = stelsistemo if kwargs.get('stelsistemo_id', False) else kosmo_stelsistemoj_kuboj.stelsistemo

                                kosmo_stelsistemoj_kuboj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except KosmoStelsistemoKubo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuKosmoStelsistemoKubo(status=status, message=message, kosmo_stelsistemoj_kuboj=kosmo_stelsistemoj_kuboj)


class KosmoMutations(graphene.ObjectType):
    redaktu_kosmo_stelsistemo_tipo = RedaktuKosmoStelsistemoTipo.Field(
        description=_('''Создаёт или редактирует модель типа звёздных систем''')
    )
    redaktu_kosmo_stelsistemoj = RedaktuKosmoStelsistemo.Field(
        description=_('''Создаёт или редактирует модель звёздных систем''')
    )
    redaktu_kosmo_stelsistemoj_kuboj = RedaktuKosmoStelsistemoKubo.Field(
        description=_('''Создаёт или редактирует модель кубов (ячейки, чанки) звёздных систем''')
    )
