"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

import graphene # сам Graphene
from graphene_django import DjangoObjectType # Класс описания модели Django для Graphene
from siriuso.api.mixins import SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions # Миксины для описания типов  Graphene
from graphene_permissions.permissions import AllowAny # Класс доступа к типу Graphene
from django.utils.translation import gettext_lazy as _  # Функция перевода внутренних сообщений Django
from siriuso.api.filters import SiriusoFilterConnectionField # Коннектор для получения данных
from siriuso.api.types import SiriusoLingvo # Объект Graphene для представления мультиязычного поля
from siriuso.utils import lingvo_kodo_normaligo, get_lang_kodo
from ..models import * # модели приложения

from versioj.models import VersioEsploradojTemoKomento, VersioEsploradojTemo, VersioEsploradojKategorio
from versioj.api.schema import VersioEsploradojTemoKomentoNode, VersioEsploradojTemoNode, VersioEsploradojKategorioNode


# Комментарии тем исследований групп
class EsploradojTemoKomentoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'teksto__enhavo': ['contains', 'icontains'],
    }

    teksto = graphene.Field(SiriusoLingvo, description=_('текст комментария'))
    versioj = SiriusoFilterConnectionField(VersioEsploradojTemoKomentoNode, description=_('Версии страницы'))

    class Meta:
        model = EsploradojTemoKomento
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'komento__id': ['exact'],
            'komento__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioEsploradojTemoKomento

        perm_name = 'versioj.povas_vidi_esploradoj_temon_komenton_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()

# Типы тем исследований (справочник)
class EsploradojTemoTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа исследования'))

    class Meta:
        model = EsploradojTemoTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


# Темы исследований групп
class EsploradojTemoNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название темы'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание темы'))

    komentoj = SiriusoFilterConnectionField(EsploradojTemoKomentoNode,
                                            description=_('Выводит список комментариев темы'))

    tuta_komentoj = graphene.Int(description=_('Общее количество комментариев в теме'))
    versioj = SiriusoFilterConnectionField(VersioEsploradojTemoNode, description=_('Версии тем'))

    class Meta:
        model = EsploradojTemo
        filter_fields = {
            'uuid': ['exact'],
            'fermita': ['exact'],
            'fiksa': ['exact'],
            'fiksa_listo': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact'],
            'kategorio__id': ['exact'],
            'kategorio__posedanto__id': ['exact'],
            'kategorio__uuid': ['exact'],
            'kategorio__posedanto__uuid': ['exact'],
        }
        interfaces = (graphene.relay.Node,)

    def resolve_komentoj(self, info, **kwargs):
        return EsploradojTemoKomento.objects.filter(posedanto=self)

    def resolve_tuta_komentoj(self, info, **kwargs):
        return EsploradojTemoKomento.objects.filter(posedanto=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioEsploradojTemo

        perm_name = 'versioj.povas_vidi_esploradoj_temon_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


# Типы категорий тем (справочник)
class EsploradojKategorioTipoNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'kodo': ['icontains',]
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Наименование типа категории'))

    class Meta:
        model = EsploradojKategorioTipo
        filter_fields = {
            'uuid': ['exact'],
            'kodo': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (graphene.relay.Node,)


# Категории тем групп
class EsploradojKategorioNode(SiriusoAuthNode, SiriusoObjectId, SiriusoPermissions, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
        'nomo__enhavo': ['contains', 'icontains'],
        'priskribo__enhavo': ['contains', 'icontains'],
    }

    nomo = graphene.Field(SiriusoLingvo, description=_('Название категории'))

    priskribo = graphene.Field(SiriusoLingvo, description=_('Описание категории'))

    temoj = SiriusoFilterConnectionField(EsploradojTemoNode, description=_('Выводит список тем категории'))

    tuta_temoj = graphene.Int(description=_('Общее количество тем в категории'))

    versioj = SiriusoFilterConnectionField(VersioEsploradojKategorioNode, description=_('Версии категорий'))

    class Meta:
        model = EsploradojKategorio
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
            'posedanto__id': ['exact'],
            'posedanto__uuid': ['exact']
        }
        interfaces = (graphene.relay.Node,)

    def resolve_temoj(self, info, **kwargs):
        return EsploradojTemo.objects.filter(kategorio=self)

    def resolve_tuta_temoj(self, info, **kwargs):
        return EsploradojTemo.objects.filter(kategorio=self, forigo=False, arkivo=False, publikigo=True).count()

    def resolve_versioj(self, info, **kwargs):
        user = info.context.user
        model = VersioEsploradojKategorio

        perm_name = 'versioj.povas_vidi_esploradoj_kategorion_version'
        lingvo = lingvo_kodo_normaligo(get_lang_kodo(info.context))

        if user.has_perm(perm_name, self):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        if ((user.is_authenticated and perms.has_registrita_perm(perm_name))
                or (not user.is_authenticated and perms.has_neregistrita_perm(perm_name))):
            return model.objects.filter(posedanto=self, lingvo__kodo=lingvo)

        return model.objects.none()


# Таблица подписок пользователей на Темы исследований
class EsploradojSciigojNode(SiriusoAuthNode, DjangoObjectType):

    permission_classes = (AllowAny,)
    json_filter_fields = {
    }

    class Meta:
        model = EsploradojSciigoj
        filter_fields = {
            'uuid': ['exact'],
            'forigo': ['exact'],
            'arkivo': ['exact'],
            'publikigo': ['exact'],
        }
        interfaces = (graphene.relay.Node,)


class EsploradojQuery(graphene.ObjectType):
    esploradoj_kategorioj_tipoj = SiriusoFilterConnectionField(
        EsploradojKategorioTipoNode,
        description=_('Выводит все доступные типы категорий исследований')
    )
    esploradoj_kategorioj = SiriusoFilterConnectionField(
        EsploradojKategorioNode,
        description=_('Выводит все доступные категории исследований')
    )
    esploradoj_temoj_tipoj = SiriusoFilterConnectionField(
        EsploradojTemoTipoNode,
        description=_('Выводит все доступные типы тем исследований')
    )
    esploradoj_temoj = SiriusoFilterConnectionField(
        EsploradojTemoNode,
        description=_('Выводит все доступные темы конферений')
    )
    esploradoj_temoj_komentoj = SiriusoFilterConnectionField(
        EsploradojTemoKomentoNode,
        description=_('Выводит все доступные комментарии к темам исследований')
    )
    esploradoj_sciigoj = SiriusoFilterConnectionField(
       EsploradojSciigojNode,
       description=_('Выводит всех подписанных пользователей к темам исследований')
    )
