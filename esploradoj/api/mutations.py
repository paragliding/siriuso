"""
Grave! Important! Важно!

Proletoj el ĉiuj landoj, unuiĝu!
Workers of the world, unite!
Пролетарии всех стран, соединяйтесь!

https://tkom.pro
"""

from django.utils import timezone
from django.db import transaction  # создание и изменения будем делать в блокирующей транзакции
from django.utils.translation import gettext_lazy as _
import graphene

from siriuso.utils import set_enhavo, get_lang_kodo, perms
from esploradoj.tasks import sciigi_esploradoj #, send_email_comment_publication
from .schema import EsploradojKategorioTipoNode, EsploradojKategorioNode, EsploradojTemoTipoNode, \
    EsploradojTemoNode, EsploradojTemoKomentoNode, EsploradojSciigojNode
from ..models import *


class RedaktuEsploradojKategorioTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio_tipo = graphene.Field(EsploradojKategorioTipoNode,
                                    description=_('Созданная/изменённая запись типа категории исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('esploradoj.povas_krei_kategorian_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            EsploradojKategorioTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except EsploradojKategorioTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = EsploradojKategorioTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')

                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = EsploradojKategorioTipo.objects.get(uuid=kwargs.get('uuid'), forigo=False,
                                                                      )

                            if (not uzanto.has_perm('esploradoj.povas_forigi_kategorian_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('esploradoj.povas_shanghi_kategorian_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojKategorioTipo.DoesNotExist:
                            message = _('Запись не найдена')

        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojKategorioTipo(status=status, message=message, kategorio_tipo=tipo)


class RedaktuEsploradojKategorio(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    kategorio = graphene.Field(EsploradojKategorioNode, description=_('Созданная/изменённая категории исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа исследования'))
        posedanto_id = graphene.Int(description=_('Код владельца исследования'))
        aliro_kodo = graphene.String(description=_('Код разрешений доступа'))
        nomo = graphene.String(description=_('Название категории исследования'))
        priskribo = graphene.String(description=_('Описание категории исследования'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        posedanto = None
        aliro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    # Проверяем наличие всех полей
                    if kwargs.get('forigo', False) and not message:
                        message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                    if kwargs.get('arkivo', False) and not message:
                        message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                    if not message:
                        if not (kwargs.get('tipo_kodo', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                        else:
                            try:
                                tipo = EsploradojKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                            except EsploradojKategorioTipo.DoesNotExist:
                                message = _('Неверный тип категории исследования')

                    if not message:
                        if not (kwargs.get('posedanto_id', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                        else:
                            try:
                                posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                 arkivo=False, publikigo=True)
                            except Komunumo.DoesNotExist:
                                message = _('Неверный владелец исследования')

                    if not message:
                        if not (kwargs.get('aliro_kodo', False)):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'aliro_kodo')
                        else:
                            try:
                                aliro = KomunumojAliro.objects.get(kodo=kwargs.get('aliro_kodo'), forigo=False,
                                                                   arkivo=False)
                            except KomunumojAliro.DoesNotExist:
                                message = _('Неверная настройка прав доступа к исследованию')

                    if not (kwargs.get('nomo', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                    if not (kwargs.get('priskribo', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                    if not message:
                        if (uzanto.has_perm('esploradoj.povas_krei_kategorion', posedanto)
                                or uzanto.has_perm('esploradoj.povas_krei_kategorion')):
                            kategorio = EsploradojKategorio.objects.create(
                                autoro=uzanto,
                                tipo=tipo,
                                posedanto=posedanto,
                                aliro=aliro,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['nomo', 'priskribo']
                            set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                            kategorio.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                        else:
                            message = _('Недостаточно прав')

                else:
                    # Изменяем запись
                    if not (kwargs.get('posedanto_id', False) or kwargs.get('tipo_kodo', False)
                            or kwargs.get('aliro_kodo', False) or kwargs.get('nomo', False)
                            or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = EsploradojKategorioTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                        arkivo=False, publikigo=True)
                        except EsploradojKategorioTipo.DoesNotExist:
                            message = _('Неверный тип категории исследования')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = Komunumo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                             arkivo=False, publikigo=True)
                        except Komunumo.DoesNotExist:
                            message = _('Неверный владелец типа категории исследования')

                    if 'aliro_kodo' in kwargs and not message:
                        try:
                            aliro = KomunumojAliro.objects.get(kodo=kwargs.get('aliro_kodo'), forigo=False,
                                                               arkivo=False, publikigo=True)
                        except KomunumojAliro.DoesNotExist:
                            message = _('Неверный доступ к категории исследования')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            kategorio = EsploradojKategorio.objects.get(
                                uuid=kwargs.get('uuid'), forigo=False
                            )
                            if (not (uzanto.has_perm('esploradoj.povas_shanghi_kategorion', kategorio.posedanto)
                                     or uzanto.has_perm('esploradoj.povas_shanghi_kategorion'))
                                    or (posedanto
                                        and not (uzanto.has_perm('esploradoj.povas_krei_kategorion', kategorio.posedanto))
                                                 or not uzanto.has_perm('esploradoj.povas_krei_kategorion'))
                                    or (kwargs.get('esploradoj.povas_forigi_kategorion', False)
                                        and not (uzanto.has_perm('esploradoj.povas_forigi_kategorion')
                                                 or uzanto.has_perm('esploradoj.povas_forigi_kategorion',
                                                                    kategorio.posedanto))
                                        )):
                                message = _('Недостаточно прав')
                            else:
                                kategorio.tipo = tipo or kategorio.tipo
                                kategorio.posedanto = posedanto or kategorio.posedanto
                                kategorio.aliro = aliro or kategorio.aliro
                                kategorio.forigo = kwargs.get('forigo', kategorio.forigo)
                                kategorio.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                kategorio.arkivo = kwargs.get('arkivo', kategorio.arkivo)
                                kategorio.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                kategorio.publikigo = kwargs.get('publikigo', kategorio.publikigo)
                                kategorio.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                kategorio.lasta_autoro = uzanto
                                kategorio.lasta_dato = timezone.now()

                                update_fields = [
                                    'tipo', 'aliro',
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                if kwargs.get('nomo', False):
                                    set_enhavo(kategorio.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')
                                if kwargs.get('priskribo', False):
                                    set_enhavo(kategorio.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                kategorio.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojKategorio.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojKategorio(status=status, message=message, kategorio=kategorio)


class RedaktuEsploradojTemoTipo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo_tipo = graphene.Field(EsploradojTemoTipoNode,
                               description=_('Созданная/изменённая запись типа темы исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        kodo = graphene.String(description=_('Код типа'))
        nomo = graphene.String(description=_('Название типа категории'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        tipo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('esploradoj.povas_krei_teman_tipon'):
                        # Проверяем наличие всех полей
                        if not kwargs.get('kodo', False):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kodo')

                        # Проверяем наличие записи с таким кодом
                        try:
                            EsploradojTemoTipo.objects.get(kodo=kwargs.get('kodo'), forigo=False)
                            message = _('Запись с таим кодом уже существует')
                        except EsploradojTemoTipo.DoesNotExist:
                            pass

                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not message:
                            tipo = EsploradojTemoTipo.objects.create(
                                kodo=kwargs.get('kodo'),
                                speciala=False,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                            )

                            set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            tipo.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('kodo', False) or kwargs.get('nomo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                                or kwargs.get('publikigo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            tipo = EsploradojTemoTipo.objects.get(
                                    uuid=kwargs.get('uuid'), forigo=False
                                   )
                            if (not uzanto.has_perm('esploradoj.povas_forigi_teman_tipon')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('esploradoj.povas_shanghi_teman_tipon'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                tipo.kodo = kwargs.get('kodo', tipo.kodo)
                                tipo.forigo = kwargs.get('forigo', tipo.forigo)
                                tipo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                tipo.arkivo = kwargs.get('arkivo', tipo.arkivo)
                                tipo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                tipo.publikigo = kwargs.get('publikigo', tipo.publikigo)
                                tipo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('nomo', False):
                                    set_enhavo(tipo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)

                                tipo.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojTemoTipo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojTemoTipo(status=status, message=message, temo_tipo=tipo)


class RedaktuEsploradojTemo(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo = graphene.Field(EsploradojTemoNode, description=_('Созданная/изменённая тема исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        tipo_kodo = graphene.String(description=_('Код типа темы исследования'))
        fermita = graphene.Boolean(description=_('Признак закрытой темы'))
        komentado_aliro_kodo = graphene.String(description=_('Код разрешений писать в закрытую тему'))
        kategorio_id = graphene.Int(description=_('Код категории исследования'))
        nomo = graphene.String(description=_('Название темы исследования'))
        priskribo = graphene.String(description=_('Описание темы исследования'))
        teksto = graphene.String(description=_('Текст стартового комментария'))
        fiksa = graphene.Boolean(description=_('Признак закрепления темы'))
        fiksa_listo = graphene.Int(description=_('Позиция среди закреплённых тем'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        kategorio = None
        tipo = None
        posedanto = None
        komentado_aliro = None
        temo = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if (uzanto.has_perm('esploradoj.povas_krei_temon')
                            or perms.has_registrita_perm('esploradoj.povas_krei_temon')):
                        # Проверяем наличие всех обязательных полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('nomo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'nomo')

                        if not (kwargs.get('priskribo', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'priskribo')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not message:
                            if not (kwargs.get('tipo_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'tipo_kodo')
                            else:
                                try:
                                    tipo = EsploradojTemoTipo.objects.get(
                                        kodo=kwargs.get('tipo_kodo'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except EsploradojTemoTipo.DoesNotExist:
                                    message = _('Неверный тип темы исследования')

                        if not message:
                            if not (kwargs.get('kategorio_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'kategorio_id')
                            else:
                                try:
                                    kategorio = EsploradojKategorio.objects.get(
                                        id=kwargs.get('kategorio_id'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except EsploradojKategorio.DoesNotExist:
                                    message = _('Неверная категория исследования ')

                        if not message:
                            if not (kwargs.get('komentado_aliro_kodo', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'komentado_aliro_kodo')
                            else:
                                try:
                                    komentado_aliro = KomunumojAliro.objects.get(
                                        kodo=kwargs.get('komentado_aliro_kodo'), forigo=False, arkivo=False, publikigo=True
                                    )
                                except KomunumojAliro.DoesNotExist:
                                    message = _('Неверные права на доступ к закрытой теме')

                        if not message:
                            temo = EsploradojTemo.objects.create(
                                autoro=uzanto,
                                tipo=tipo,
                                posedanto=posedanto,
                                kategorio=kategorio,
                                komentado_aliro=komentado_aliro,
                                fermita=kwargs.get('fermita', False),
                                fiksa=kwargs.get('fiksa', False),
                                fiksa_listo=kwargs.get('fiksa_listo', "0"),
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            update_fields = ['nomo', 'priskribo']
                            set_enhavo(temo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                            set_enhavo(temo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)

                            temo.save(update_fields=update_fields)

                            komento = EsploradojTemoKomento.objects.create(
                                posedanto=temo,
                                autoro=uzanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=True,
                                publikiga_dato=timezone.now()
                            )

                            update_fields = []
                            update_fields.append('teksto')
                            set_enhavo(komento.teksto, kwargs.get('teksto'), lingvo=get_lang_kodo(info.context))
                            komento.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('tipo_kodo', False)
                                or kwargs.get('komentado_aliro_id', False) or kwargs.get('nomo', False)
                                or kwargs.get('priskribo', False) or kwargs.get('publikigo', False)
                                or kwargs.get('fermita', False) or kwargs.get('kategorio_id', False)
                                or kwargs.get('fiksa', False) or kwargs.get('fiksa_listo', False)
                                or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'tipo_kodo' in kwargs and not message:
                        try:
                            tipo = EsploradojTemoTipo.objects.get(kodo=kwargs.get('tipo_kodo'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                        except EsploradojTemoTipo.DoesNotExist:
                            message = _('Неверный тип категории исследования')

                    if 'kategorio_id' in kwargs and not message:
                        try:
                            kategorio = EsploradojKategorio.objects.get(id=kwargs.get('kategorio_id'),
                                                                        forigo=False,
                                                                        arkivo=False, publikigo=True)
                        except EsploradojKategorio.DoesNotExist:
                            message = _('Неверная категория исследования')

                    if 'komentado_aliro_kodo' in kwargs and not message:
                        try:
                            komentado_aliro = KomunumojAliro.objects.get(
                                kodo=kwargs.get('komentado_aliro_kodo'), forigo=False, arkivo=False, publikigo=True
                            )
                        except KomunumojAliro.DoesNotExist:
                            message = _('Неверный доступ к категории исследования')

                    # Ищем запись для изменения
                    if not message:
                        try:
                            temo = EsploradojTemo.objects.get(uuid=kwargs.get('uuid'), forigo=False
                                                                )
                            if (not uzanto.has_perm('esploradoj.povas_forigi_teman')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('esploradoj.povas_shanghi_teman'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                temo.tipo = tipo or temo.tipo
                                temo.kategorio = kategorio or temo.kategorio
                                temo.posedanto = posedanto or temo.posedanto
                                temo.komentado_aliro = komentado_aliro or temo.komentado_aliro
                                temo.fermita = kwargs.get('fermita', temo.forigo)
                                temo.fiksa = kwargs.get('fiksa', temo.fiksa)
                                temo.fiksa_listo = kwargs.get('fiksa_listo', temo.fiksa_listo)
                                temo.forigo = kwargs.get('forigo', temo.forigo)
                                temo.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                temo.arkivo = kwargs.get('arkivo', temo.arkivo)
                                temo.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                temo.publikigo = kwargs.get('publikigo', temo.publikigo)
                                temo.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                temo.lasta_autoro = uzanto
                                temo.lasta_dato = timezone.now()

                                update_fields = [
                                    'fermita', 'fiksa', 'fiksa_listo', 'tipo', 'kategorio', 'komentado_aliro',
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                ]

                                if kwargs.get('nomo', False):
                                    set_enhavo(temo.nomo, kwargs.get('nomo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('nomo')
                                if kwargs.get('priskribo', False):
                                    set_enhavo(temo.priskribo, kwargs.get('priskribo'), info.context.LANGUAGE_CODE)
                                    update_fields.append('priskribo')

                                temo.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojTemo.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojTemo(status=status, message=message, temo=temo)


class RedaktuEsploradojTemoKomento(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    temo_komento = graphene.Field(EsploradojTemoKomentoNode,
                                  description=_('Созданная/изменённая запись комментария к теме исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        teksto = graphene.String(description=_('Текст комментария исследования'))
        posedanto_id = graphene.Int(description=_('Код владельца комментария'))
        komento_uuid = graphene.String(description=_('Код комментируемого сообщения'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        posedanto = None
        komento = None
        komentoj = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if (uzanto.has_perm('esploradoj.povas_krei_teman_komenton')
                            or perms.has_registrita_perm('esploradoj.povas_krei_teman_komenton')):
                        # Проверяем наличие всех полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('teksto', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'teksto')

                        if not message:
                            if not (kwargs.get('posedanto_id', False)):
                                message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'posedanto_id')
                            else:
                                try:
                                    posedanto = EsploradojTemo.objects.get(id=kwargs.get('posedanto_id'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except EsploradojTemo.DoesNotExist:
                                    message = _('Неверная тема исследования')

                        if not message:
                            if (kwargs.get('komento_uuid', False)):
                                try:
                                    komentoj = EsploradojTemoKomento.objects.get(uuid=kwargs.get('komento_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                                except EsploradojTemoKomento.DoesNotExist:
                                    message = _('Неверное комментируемое сообщение')

                        if not message:
                            komento = EsploradojTemoKomento.objects.create(
                                posedanto=posedanto,
                                autoro=uzanto,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now(),
                                komento=komentoj,
                            )

                            update_fields = []
                            set_enhavo(komento.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                            update_fields.append('teksto')

                            komento.save(update_fields=update_fields)

                            status = True
                            message = _('Запись создана')

                            # Отправить сообщение о новой записи в чате всем остальным участникам чата 
                            sciigi_esploradoj.delay(komento.uuid)
                            # send_email_comment_publication.delay(komento.uuid)

                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('teksto', False) or kwargs.get('posedanto_id', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)
                            or kwargs.get('publikigo', False) or kwargs.get('komento_id', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if 'posedanto_id' in kwargs and not message:
                        try:
                            posedanto = EsploradojTemo.objects.get(id=kwargs.get('posedanto_id'), forigo=False,
                                                                    arkivo=False, publikigo=True)
                        except EsploradojTemo.DoesNotExist:
                            message = _('Неверный тип категории исследования')

                    if not message:
                        if (kwargs.get('komento_uuid', False)):
                            try:
                                komentoj = EsploradojTemoKomento.objects.get(uuid=kwargs.get('komento_uuid'),
                                                                            forigo=False,
                                                                            arkivo=False, publikigo=True)
                            except EsploradojTemoKomento.DoesNotExist:
                                message = _('Неверное комментируемое сообщение')

                    if not message:
                        # Ищем запись для изменения
                        try:
                            komento = EsploradojTemoKomento.objects.get(uuid=kwargs.get('uuid'), forigo=False
                                                                        )
                            if (not uzanto.has_perm('esploradoj.povas_forigi_teman_komenton')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('esploradoj.povas_shanghi_teman_komenton'):
                                message = _('Недостаточно прав для изменения')
                            else:

                                update_fields = [
                                    'posedanto', 'forigo', 'foriga_dato', 'arkivo', 'arkiva_dato',
                                    'publikigo', 'publikiga_dato', 'lasta_autoro', 'lasta_dato',
                                    'komento',
                                ]

                                komento.posedanto = posedanto or komento.posedanto
                                komento.komento = komentoj or komento.komento
                                komento.forigo = kwargs.get('forigo', komento.forigo)
                                komento.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                komento.arkivo = kwargs.get('arkivo', komento.arkivo)
                                komento.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                komento.publikigo = kwargs.get('publikigo', komento.publikigo)
                                komento.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None

                                if kwargs.get('teksto', False):
                                    set_enhavo(komento.teksto, kwargs.get('teksto'), info.context.LANGUAGE_CODE)
                                    update_fields.append('teksto')

                                komento.save(update_fields=update_fields)
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojTemoKomento.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojTemoKomento(status=status, message=message, temo_komento=komento)


# Таблица подписок пользователей на Темы исследований
class RedaktuEsploradojSciigoj(graphene.Mutation):
    status = graphene.Boolean()
    message = graphene.String()
    sciigoj = graphene.Field(EsploradojSciigojNode, description=_('Созданная/изменённая подписка пользователя на тему исследования'))

    class Arguments:
        uuid = graphene.UUID(description=_('UUID записи'))
        temo_id = graphene.Int(description=_('Код типа темы исследования'))
        sciigoj_kodo = graphene.List(graphene.String, required=True,
            description=_('Список кодов типов подписок для подписываемого пользователя на тему исследования'))
        autoro_id = graphene.Int(description=_('Код подписываемого пользователя на тему исследования'))
        forigo = graphene.Boolean(description=_('Признак удаления записи'))
        arkivo = graphene.Boolean(description=_('Признак архивной записи'))
        publikigo = graphene.Boolean(description=_('Признак опубликованной записи'))

    @staticmethod
    def mutate(root, info, **kwargs):
        status = False
        message = None
        inf_sciigoj = None
        novo_sciigoj = []
        temo = None
        autoro = None
        uzanto = info.context.user

        if uzanto.is_authenticated:
            with transaction.atomic():
                # Создаём новую запись
                if not kwargs.get('uuid', False):
                    if uzanto.has_perm('esploradoj.povas_krei_sciigoj'):
                        # Проверяем наличие всех обязательных полей
                        if kwargs.get('forigo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'forigo')

                        if kwargs.get('arkivo', False) and not message:
                            message = '{} "{}"'.format(_('При создании записи не допустимо указание поля'), 'arkivo')

                        if not (kwargs.get('temo_id', False) or message):
                            message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'temo_id')
                        else:
                            try:
                                temo = EsploradojTemo.objects.get(id=kwargs.get('temo_id'), forigo=False,
                                                                     arkivo=False, publikigo=True)
                            except EsploradojTemo.DoesNotExist:
                                message = _('Неверный тип темы исследования')

                        # Проверяем наличие записи с таким кодом
                        if not message:
                            if (kwargs.get('autoro_id', False)):
                                try:
                                    autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                                konfirmita=True)
                                except Uzanto.DoesNotExist:
                                    message = _('Неверный пользователь исследования')
                            else:
                                autoro = uzanto

                        # проверяем наличие записей с таким кодом в списке
                        if not message:
                            if kwargs.get('sciigoj_kodo', False):
                                for sciigo in kwargs.get('sciigoj_kodo'):
                                    try:
                                        novo_sciigoj.append(InformilojSciigoTipo.objects.get(kodo=sciigo, forigo=False))
                                        # novo_sciigoj.append(InformilojSciigoTipo.objects.filter(kodo__in=sciigo, forigo=False))
                                    except InformilojSciigoTipo.DoesNotExist:
                                        message = _('Неверный возможный вариант подписки'+sciigo)

                        if not message:
                            sciigoj = EsploradojSciigoj.objects.create(
                                autoro=autoro,
                                temo=temo,
                                forigo=False,
                                arkivo=False,
                                publikigo=kwargs.get('publikigo', False),
                                publikiga_dato=timezone.now()
                            )

                            sciigoj.sciigoj.set(novo_sciigoj)

                            sciigoj.save()

                            status = True
                            message = _('Запись создана')
                    else:
                        message = _('Недостаточно прав')
                else:
                    # Изменяем запись
                    if not (kwargs.get('autoro_id', False) or kwargs.get('sciigoj_kodo', False)
                            or kwargs.get('temo_id', False) or kwargs.get('publikigo', False)
                            or kwargs.get('forigo', False) or kwargs.get('arkivo', False)):
                        message = _('Не задано ни одно поле для изменения')

                    if not (kwargs.get('temo_id', False) or message):
                        message = '{} "{}"'.format(_('Не заполнено обязательное поле'), 'temo_id')
                    else:
                        try:
                            temo = EsploradojTemo.objects.get(id=kwargs.get('temo_id'), forigo=False,
                                                                arkivo=False, publikigo=True)
                        except EsploradojTemo.DoesNotExist:
                            message = _('Неверный тип темы исследования')

                    # Проверяем наличие записи с таким кодом
                    if not message:
                        if (kwargs.get('autoro_id', False)):
                            try:
                                autoro = Uzanto.objects.get(id=kwargs.get('autoro_id'), is_active=True,
                                            konfirmita=True)
                            except Uzanto.DoesNotExist:
                                message = _('Неверный пользователь исследования')
                        else:
                            autoro = uzanto

                    # проверяем наличие записей с таким кодом в списке
                    if not message:
                        if kwargs.get('sciigoj_kodo', False):
                            for sciigo in kwargs.get('sciigoj_kodo'):
                                try:
                                    novo_sciigoj.append(InformilojSciigoTipo.objects.filter(kodo__in=sciigo, forigo=False))
                                except InformilojSciigoTipo.DoesNotExist:
                                    message = _('Неверный возможный вариант подписки'+sciigo)

                    # Ищем запись для изменения
                    if not message:
                        try:
                            sciigoj = EsploradojSciigoj.objects.get(uuid=kwargs.get('uuid'), forigo=False)
                            if (not uzanto.has_perm('esploradoj.povas_forigi_sciigoj')
                                    and kwargs.get('forigo', False)):
                                message = _('Недостаточно прав для удаления')
                            elif not uzanto.has_perm('esploradoj.povas_shanghi_sciigoj'):
                                message = _('Недостаточно прав для изменения')
                            else:
                                sciigoj.temo = temo or sciigoj.temo
                                sciigoj.autoro = autoro or sciigoj.autoro
                                sciigoj.forigo = kwargs.get('forigo', sciigoj.forigo)
                                sciigoj.foriga_dato = timezone.now() if kwargs.get('forigo', False) else None
                                sciigoj.arkivo = kwargs.get('arkivo', sciigoj.arkivo)
                                sciigoj.arkiva_dato = timezone.now() if kwargs.get('arkivo', False) else None
                                sciigoj.publikigo = kwargs.get('publikigo', sciigoj.publikigo)
                                sciigoj.publikiga_dato = timezone.now() if kwargs.get('publikigo', False) else None
                                sciigoj.lasta_autoro = uzanto
                                sciigoj.lasta_dato = timezone.now()

                                sciigoj.sciigoj.set(novo_sciigoj)

                                sciigoj.save()
                                status = True
                                message = _('Запись успешно изменена')
                        except EsploradojSciigoj.DoesNotExist:
                            message = _('Запись не найдена')
        else:
            message = _('Требуется авторизация')

        return RedaktuEsploradojSciigoj(status=status, message=message, sciigoj=sciigoj)


class EsploradojMutations(graphene.ObjectType):
    redaktu_esploradoj_kategorio_tipo = RedaktuEsploradojKategorioTipo.Field(
        description=_('''Создаёт или редактирует типы категории исследования''')
    )
    redaktu_esploradoj_kategorio = RedaktuEsploradojKategorio.Field(
        description=_('''Создаёт или редактирует категории исследования''')
    )
    redaktu_esploradoj_temo_tipo = RedaktuEsploradojTemoTipo.Field(
        description=_('''Создаёт или редактирует типы темы исследования''')
    )
    redaktu_esploradoj_temo = RedaktuEsploradojTemo.Field(
        description=_('''Создаёт или редактирует типы темы исследования''')
    )
    redaktu_esploradoj_temo_komento = RedaktuEsploradojTemoKomento.Field(
        description=_('''Создаёт или редактирует комментарии исследования''')
    )
    redaktu_esploradoj_sciigoj = RedaktuEsploradojSciigoj.Field(
        description=_('''Создаёт или редактирует подписки пользователей на исследования''')
    )
